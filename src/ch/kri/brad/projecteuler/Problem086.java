package ch.kri.brad.projecteuler;

public class Problem086 {

	public static void main(String[] args) {
		System.out.println("Answer = " + numSolutions(1000000));
	}

	/**
	 * x, y and z are the sides of the cuboid. This is a bit slow - we could combine y and z into a single loop,
	 * and just calculate the number of possible combinations that lead to the given sum (y+z).
	 */
	public static int numSolutions(int limit) {
		int count = 0;
		int x = 0;
		while (count <= limit) {
			x++;
			for (int y = 1; y <= x; y++) {
				for (int z = 1; z <= y; z++) {
					// There are three possible paths, but the shortest one is when the sides of the
					// pythagorean triangle are closest to each other in length.
					// Hence: a = x, b = y+z
					int a = x;
					int b = y + z;

					// See if we have an integer solution
					double result = Math.sqrt(a * a + b * b);
					if (result == (int) result) count++;
				}
			}
		}
		return x;
	}
}
