package hackerRank.java.strings;

import java.util.Scanner;

public class JavaAnagrams {

    static boolean isAnagram(String a, String b) {
        int[] aChars = charCount(a.toLowerCase());
        int[] bChars = charCount(b.toLowerCase());
        
        boolean match = true;
        for (int i = 0; i < aChars.length && match; i++) {
        	match = (aChars[i] == bChars[i]);
        }
        return match;
    }	
    
    static int[] charCount(String s) {
    	int[] count = new int[26];
    	for (int i = 0; i < s.length(); i++) {
    		count[s.charAt(i)-'a']++;
    	}
    	return count;
    }
	
	/**
	 * Main method provided - do not change
	 */
	  public static void main(String[] args) {
	        Scanner scan = new Scanner(System.in);
	        String a = scan.next();
	        String b = scan.next();
	        scan.close();
	        boolean ret = isAnagram(a, b);
	        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
	    }

}
