package ch.kri.brad.projecteuler.Problem054;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PokerHandTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPair() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Eight));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Two));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.Pair);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}

	@Test
	public void testTwoPair() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Eight));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Eight));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.TwoPair);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testThreeOfAKind() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Eight));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Six));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.ThreeOfAKind);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testFullHouse() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Six));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.FullHouse);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testFourOfAKind() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Six));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.FourOfAKind);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testStraight() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Spade, PlayingCard.CardRank.Seven));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ten));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Diamond, PlayingCard.CardRank.Nine));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Eight));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.Straight);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testFlush() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Seven));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Ace));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Nine));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Heart, PlayingCard.CardRank.Eight));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.Flush);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}
	
	@Test
	public void testStraightFlush() {
		PokerHand pairHand = new PokerHand();
		try {
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Six));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Seven));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Ten));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Nine));
			pairHand.addCard(new PlayingCard(PlayingCard.CardSuit.Club, PlayingCard.CardRank.Eight));
			assertEquals(pairHand.getHandType(), PokerHand.HandType.StraightFlush);
		} catch (Exception e) {
			fail("Exception: " + e.toString());
		}
	}

}
