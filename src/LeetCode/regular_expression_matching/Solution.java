package LeetCode.regular_expression_matching;

// Does not pass all test cases, for example, "aaaaaa" with RE "a*b*c*a"
class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.isMatch("aa", "a")); // false
        System.out.println(s.isMatch("aa", "a*")); // true
        System.out.println(s.isMatch("aa", ".*")); // true
        System.out.println(s.isMatch("abbbbxccz", "ab*.c*z")); // true
        System.out.println(s.isMatch("aaaaa", "a*a")); // true <-- special case
        System.out.println(s.isMatch("aaa", "ab*a*c*a")); // true <-- not currently working
    }

    // We toss in handling for the following special cases, because we don't want to
    // spend hundreds of lines on full RE handling:
    // - Regular expressions of the form x*x are simplified to x*
    public boolean isMatch(String s, String re) {
        // Special case handling
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < re.length(); i++) {
            char c = re.charAt(i);
            sb.append(c);
            if (c == '*' && i < re.length()-1) {
                if (re.charAt(i-1) == re.charAt(i+1)) i++;
            }
        }
        re = sb.toString();

        int pos_s = 0;
        int pos_re = 0;
        boolean match = true;
        while (match && pos_s < s.length() && pos_re < re.length()) {
            if (pos_re < re.length()-1 && re.charAt(pos_re+1) == '*') {
                char c = re.charAt(pos_re);
                while(pos_s < s.length() && (s.charAt(pos_s) == c || c == '.')) pos_s++;
                pos_re += 2;
            } else if (re.charAt(pos_re) == '.') {
                pos_re++; pos_s++;
            } else  if (s.charAt(pos_s) == re.charAt(pos_re)) {
                pos_re++; pos_s++;
            } else { // failure
                match = false;
            }
        }
        if (pos_s < s.length() || pos_re < re.length()) match = false;
        return match;
    }
}
