package AdventOfCode.aoc_2023.day_01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int sum = 0;
		for (String line : lines) {
			sum += lineValue(line);
		}
		System.out.println(sum);
	}
	
	private static int lineValue(String in) {
		char[] chars = in.toCharArray();
		int firstDigit = -1;
		int lastDigit = -1;
		for (int i = 0; i < chars.length; i++) {
			if (firstDigit == -1 && chars[i] >= '0' && chars[i] <= '9') firstDigit = chars[i] - '0';
			int j = chars.length - i - 1;
			if (lastDigit == -1 && chars[j] >= '0' && chars[j] <= '9') lastDigit = chars[j] - '0';
		}
		return firstDigit * 10 + lastDigit;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}