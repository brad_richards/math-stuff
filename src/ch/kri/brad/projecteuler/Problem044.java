package ch.kri.brad.projecteuler;
import java.util.HashSet;
import java.util.Iterator;


public class Problem044 {
	static HashSet<Long> pentagonalNumbers = new HashSet<Long>();
	
	final static long LIMIT = 10000;

	static class PentPair {
		long p1;
		long p2;
		
		PentPair(long p1, long p2) {
			this.p1 = p1; this.p2 = p2;
		}
	}
	
	/**
	 * Very ugly brute force...
	 */
	public static void main(String[] args) {
		// Generate the pentagonal numbers
		for (long i = 1; i <= LIMIT; i++) {
			pentagonalNumbers.add(i * (3 * i - 1) / 2);
		}
		
		// Look for pairs whose sum and difference is pentagonal
		PentPair bestPair = null;
		long smallestDiff = Long.MAX_VALUE;
		
		for (Iterator<Long> i = pentagonalNumbers.iterator(); i.hasNext(); ) {
			long p1 = i.next();
			for (Iterator<Long> j = pentagonalNumbers.iterator(); j.hasNext(); ) {
				long p2 = j.next();
				if (p1 < p2) {
					long sum = p1 + p2;
					long diff = p2 - p1;
					if (diff < smallestDiff && pentagonalNumbers.contains(sum) && pentagonalNumbers.contains(diff)) {
						bestPair = new PentPair(p1, p2);
						smallestDiff = diff;
					}
				}
			}
		}
		
		System.out.println("Pair = [" + bestPair.p1 + ", " + bestPair.p2 + "], diff = " + smallestDiff);
	}

}
