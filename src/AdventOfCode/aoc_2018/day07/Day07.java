package AdventOfCode.aoc_2018.day07;

import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This is really a sort of best-first search. We first create a DAG (directed acyclic graph -
 * anyway, we surely hope it is acyclic!). Then we initialize our search with the start node. We
 * maintain a list of all nodes reached in the search, but rather than simply appending
 * (breadth-first) or prepending (depth-first) new nodes, we add the new nodes and sort
 * alphabetically. Nodes coming alphabetically first are "best", and processed next. However, a node
 * can only be processed if all of its predecessors have been processed.
 * 
 * For ease of construction, maintain the steps in two HashMaps. For "successorNodes" the key is the
 * predecessor node, and the value is an ArrayList of successor nodes. For "predecessorNodes" the
 * key is a successor node, and the value is an ArrayList of predecessors.
 * 
 * Note that there are multiple nodes with no predecessors, i.e., there is no single, unique
 * starting point. Hence, we begin our search by filling the node-list with all nodes that have no
 * predecessors.
 */
public class Day07 {
	private static HashMap<Character, ArrayList<Character>> successorNodes = new HashMap<>();
	private static HashMap<Character, ArrayList<Character>> predecessorNodes = new HashMap<>();

	private static class Edge {
		Character from;
		Character to;
	}

	public static void main(String[] args) {
		readInput();
		String nodesVisited = bestFirstSearch();
		System.out.println(nodesVisited);
	}

	private static void readInput() {
		InputStream fis = Day07.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String line = in.nextLine();
				Edge edge = parseInput(line);

				// Create nodes as necessary
				if (!successorNodes.containsKey(edge.from)) {
					predecessorNodes.put(edge.from, new ArrayList<Character>());
					successorNodes.put(edge.from, new ArrayList<Character>());
				}
				if (!successorNodes.containsKey(edge.to)) {
					predecessorNodes.put(edge.to, new ArrayList<Character>());
					successorNodes.put(edge.to, new ArrayList<Character>());
				}

				// Add edge into HashMaps, if not already present
				ArrayList<Character> successors = successorNodes.get(edge.from);
				if (!successors.contains(edge.to)) successors.add(edge.to);

				ArrayList<Character> predecessors = predecessorNodes.get(edge.to);
				if (!predecessors.contains(edge.from)) predecessors.add(edge.from);
			}
		}
		System.out.println(successorNodes.size() + " nodes read");

		System.out.println("\nNodes and their successors");
		for (Character c : successorNodes.keySet()) {
			System.out.print(c + ": ");
			for (Character cc : successorNodes.get(c))
				System.out.print(cc);
			System.out.println();
		}

		System.out.println("\nNodes and their predecessors");
		for (Character c : predecessorNodes.keySet()) {
			System.out.print(c + ": ");
			for (Character cc : predecessorNodes.get(c))
				System.out.print(cc);
			System.out.println();
		}

	}

	public static Edge parseInput(String line) {
		Edge edge = new Edge();
		edge.from = line.charAt(5);
		edge.to = line.charAt(36);
		return edge;
	}

	private static String bestFirstSearch() {
		ArrayList<Character> nextNodes = new ArrayList<>();
		
		// To start, find all nodes with no predecessors
		for (Character c : predecessorNodes.keySet()) {
			if (predecessorNodes.get(c).isEmpty()) nextNodes.add(c);
		}
		Collections.sort(nextNodes);
		
		StringBuffer buf = new StringBuffer();
		while (!nextNodes.isEmpty()) {
			// Find first node all of whose predecessors are fulfilled.
			int pos = -1; // node that we are about to find
			for (int i = 0; pos == -1 && i < nextNodes.size(); i++) {
				ArrayList<Character> predecessors = predecessorNodes.get(nextNodes.get(i));
				boolean allFound = true;
				for (Character c : predecessors) {
					allFound &= (buf.indexOf(c.toString()) != -1);
				}
				if (allFound) pos = i; // Found our node!
			}

			// Remove the node from the list and add it to the list of completed steps
			Character currentNode = nextNodes.remove(pos);
			buf.append(currentNode);

			// Don't add redundant nodes
			for (Character node : successorNodes.get(currentNode)) {
				if (!nextNodes.contains(node)) nextNodes.add(node);
			}
			Collections.sort(nextNodes);
			System.out.println(buf);
		}
		return buf.toString();
	}
}
