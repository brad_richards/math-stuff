package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Factoring;


public class Problem033 {
	private static ArrayList<Fraction> solutions = new ArrayList<Fraction>();
	
	private static class Fraction {
		int numerator;
		int denominator;
		
		Fraction(int numerator, int denominator) {
			this.numerator = numerator; this.denominator = denominator;
		}
		
		public void reduceFraction() {
			ArrayList<Integer> commonPrimeFactors = Factoring.getCommonPrimeFactors(numerator, denominator);
			for (Integer i : commonPrimeFactors) {
				boolean divisible = true;
				while (divisible) {
					divisible = (numerator % i == 0) & (denominator % i == 0);
					if (divisible) {
					numerator = numerator / i;
					denominator = denominator / i;
					}
				}
			}
		}
		
		public void multiply(Fraction f) {
			this.numerator *= f.numerator;
			this.denominator *= f.denominator;
		}
	}

	public static void main(String[] args) {
		findSolutions();
		Fraction answer = new Fraction(1,1);
		for (Fraction f : solutions) answer.multiply(f);
		answer.reduceFraction();
		System.out.println("The answer is " + answer.denominator);
	}	
	
	/**
	 * Small problem - brute force
	 * 
	 * Fractions have the form ab / cd
	 */
	public static void findSolutions() {
		int numerator, denominator;
		for (int a = 1; a <= 9; a++) {
			for (int b = 0; b <= 9; b++) {
				for (int c = 1; c <= 9; c++) {
					for (int d = 0; d <= 9; d++) {
						if (validForProblem(a,b,c,d)) {
						if (a==c) {
							numerator = b;
							denominator = d;
						} else if (a == d) {
							numerator = b;
							denominator = c;
						} else if (b == c) {
							numerator = a;
							denominator = d;
						} else {
							numerator = a;
							denominator = c;
						}
						double correctValue = ((double)a * 10 + b) / (c * 10 + d);
						double weirdValue = ((double) numerator) / denominator;
						if (Math.abs(correctValue - weirdValue) < 0.0001) {
							System.out.println(a + " " + b + "/" + c + " " + d);
							solutions.add(new Fraction(numerator, denominator));
						}
						}
					}
				}
			}
		}
	}

	/**
	 * Only valid for this problem if
	 * - two digits are the same
	 * - if b == d then b != 0
	 */
	private static boolean validForProblem(int a, int b, int c, int d) {
		boolean valid = (a==c)|(a==d)|(b==c)|(b==d);
		if (valid) {
			if (b==d) valid = (b != 0);
		}
		if (valid) {
			valid = ( (a*10+b) < (c*10+d) );
		}
		return valid;
	}	
}
