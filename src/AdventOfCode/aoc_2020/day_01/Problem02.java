package AdventOfCode.aoc_2020.day_01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	private static List<Integer> numbers;

	public static void main(String[] args) throws IOException {
		readInput();

		boolean found = false;
		for (int i = 0; i < numbers.size() - 2 && !found; i++) {
			for (int j = i + 1; j < numbers.size() - 1 && !found; j++) {
				for (int k = j + 1; k < numbers.size() && !found; k++) {
					int sum = numbers.get(i) + numbers.get(j) + numbers.get(k);
					if (sum == 2020) {
						System.out.println("Answer = " + (numbers.get(i) * numbers.get(j) * numbers.get(k)));
						found = true;
					}
				}
			}
		}
		if (!found) System.out.println("No solution found");
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			numbers = stream.map(Integer::parseInt).collect(Collectors.toList());
		}
	}
}
