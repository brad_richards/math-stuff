package ch.kri.brad.projecteuler;

public class Problem069 {

	/** Intuitively obvious from the definition of the totient function: "the number of integers k in the range 1 ≤ k ≤ n for which the greatest common divisor gcd(n, k) = 1".
	 * 
	 * Hence, we want the number below 1000000 with the largest number of unique prime devisors. 2*3*5*7*11*13*17 = 510510, which is the answer
	 */
	public static void main(String[] args) {
	}

}
