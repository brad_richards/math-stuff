package AdventOfCode.aoc_2018.day09;

import java.util.Arrays;

/**
 * Ok, part 1 is lazy, and too inefficient for part 2. For part 2, we need an actual circular, doubly-linked list to implement the process without shoving stuff around in arrays.
 * @author brad
 *
 */
public class Day09_part2 {
	public static int NUM_PLAYERS = 426;
	public static int LAST_MARBLE = 7205800;

	public static void main(String[] args) {
		long[] playerScores = new long[NUM_PLAYERS];
		Circle2 circle = new Circle2(); // Starts with marble 0 in place
		
		long start = System.currentTimeMillis();
		
		int currentPlayer = 0;
		for (int marble = 1; marble <= LAST_MARBLE; marble++) {
			playerScores[currentPlayer] += circle.addMarble(marble);
			currentPlayer++;
			if (currentPlayer >= NUM_PLAYERS) currentPlayer -= NUM_PLAYERS;
		}
		
		long end = System.currentTimeMillis();
		System.out.println("Time elapsed = " + (end - start) + "ms");
		System.out.println("Maximum score is " + Arrays.stream(playerScores).max().getAsLong());
	}
}
