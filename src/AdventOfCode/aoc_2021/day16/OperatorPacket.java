package AdventOfCode.aoc_2021.day16;

import java.util.ArrayList;

public class OperatorPacket extends Packet {
	public ArrayList<Packet> subpackets = new ArrayList<>();
	public boolean lengthTypeID; // true for subpackets, false for length
	public long value; // Calculated after all subpackets have been parsed
	
	protected OperatorPacket(int version, int type) {
		super(version, type);
	}
	
	public long evaluate() {
		switch (type) {
		case 0 ->  { value = 0; for (Packet p : subpackets) value += p.evaluate(); }
		case 1 ->  { value = 1; for (Packet p : subpackets) value *= p.evaluate(); }
		case 2 ->  {
			value = Long.MAX_VALUE;
			for (Packet p : subpackets) {
				long v = p.evaluate();
				if (v < value) value = v;
			}
		}
		case 3 ->  {
			value = Long.MIN_VALUE;
			for (Packet p : subpackets) {
				long v = p.evaluate();
				if (v > value) value = v;
			}
		}
		case 5 ->  { value = subpackets.get(0).evaluate() > subpackets.get(1).evaluate() ? 1 : 0; }
		case 6 ->  { value = subpackets.get(0).evaluate() < subpackets.get(1).evaluate() ? 1 : 0; }
		case 7 ->  { value = subpackets.get(0).evaluate() == subpackets.get(1).evaluate() ? 1 : 0; }
		}
		return value;
	}
	
	public void parseSubpackets() {
		if (lengthTypeID) { // 11-bits give number of subpackets
			long numPackets = Binary.getBits(in, 11, nextBit);
			nextBit += 11;
			for (int i = 0; i < numPackets; i++) {
				subpackets.add(Packet.parsePacket());
			}
		} else { // 15 bits give total length
			long length = Binary.getBits(in, 15, nextBit);
			nextBit += 15;
			long endPosition = nextBit + length;
			while (nextBit < endPosition) {
				subpackets.add(Packet.parsePacket());
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(": subpackets");
		for (Packet p : subpackets) sb.append("\n  " + p.toString());
		return sb.toString();
	}
}
