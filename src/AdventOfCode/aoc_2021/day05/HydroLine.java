package AdventOfCode.aoc_2021.day05;

public class HydroLine {
	int end1x;
	int end2x;
	int end1y;
	int end2y;
	
	public static HydroLine parseHydroLine(String in) {
		String[] parts = in.split(" -> ");
		String[] end1 = parts[0].split(",");
		String[] end2 = parts[1].split(",");
		HydroLine hl = new HydroLine();
		hl.end1x = Integer.parseInt(end1[0]);
		hl.end1y = Integer.parseInt(end1[1]);
		hl.end2x = Integer.parseInt(end2[0]);
		hl.end2y = Integer.parseInt(end2[1]);
		return hl;
	}
}
