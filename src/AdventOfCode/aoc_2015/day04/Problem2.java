package AdventOfCode.aoc_2015.day04;

import java.security.MessageDigest;

public class Problem2 {
	public static void main(String[] args) throws Exception {
		String password = "yzbqklnj";

		MessageDigest md = MessageDigest.getInstance("MD5");
		for (long i = 1; i < Long.MAX_VALUE; i++) {
			if (i % 100000 == 0) System.out.println(i);
			String input = password + i;
			byte[] digest = md.digest(input.getBytes());

			String hash = "";
			for (byte b : digest)
				hash += String.format("%02x", b);

			if (hash.subSequence(0, 6).equals("000000")) {
				System.out.println( "Answer = " + i);
				break;
			}
		}
	}
}