package kattis.b_datatypes.floats;

import java.util.Scanner;

public class metronome {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int beats = in.nextInt();
		double turns = beats / 4.0;
		System.out.format("%.2f", turns);
	}

}
