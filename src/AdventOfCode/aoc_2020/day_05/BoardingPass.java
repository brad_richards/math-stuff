package AdventOfCode.aoc_2020.day_05;

public class BoardingPass {
	private final int row;
	private final int col;

	public BoardingPass(String code) {
		String rowCode = code.substring(0,7);
		this.row = parseRow(rowCode);
		String colCode = code.substring(7);
		this.col = parseCol(colCode);
	}
	
	private int parseRow(String rowCode) {
		int row = 0;
		int range = 128;
		for (char c : rowCode.toCharArray()) {
			range /= 2;
			if (c == 'B') row += range;
		}
		return row;
	}
	
	private int parseCol(String colCode) {
		int col = 0;
		int range = 8;
		for (char c : colCode.toCharArray()) {
			range /= 2;
			if (c == 'R') col += range;
		}
		return col;
	}
	
	public int getSeatID() {
		return row*8 + col;
	}
	
	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}
}
