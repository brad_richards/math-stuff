package AdventOfCode.aoc_2018.day12;

import java.util.HashMap;

/**
 * Initial thoughts:
 * 
 * - Parse the initial configuration. Ensure that there are at least 2*generations empty pots on
 * each end, because plants may spread
 * 
 * - Parse the rule input into a HashSet, eliminating all "no plant" rules.
 * 
 * - Repeat for the given number of generations: Generate a new matrix from the old, using the given
 * rules.
 * 
 * - Calculate the result
 * 
 * For data types: arrays might be more efficient, but in the end Strings will be simpler to work
 * with.
 * 
 * This works for part 1. For part 2, we note that the actual rules result in plants shifting
 * steadily to the right. Hence, we adapt the solution to maintain the plants "left-justified" with
 * an integer offset. When we detect that the array has not changed (though the offset has
 * increased), we can directly calculate the final result.
 * 
 * Amusingly, the difference in the value each generation is precisely 42. After 200 generations,
 * the value is 9568. The final answer is then:
 * 
 * 9568 + 42 * (5000000000 - 200) = 210000001168
 * 
 * Only...it's not?
 */
public class Day12 {
	private static final int GENERATIONS = 20;

	private static class PlantPots {
		String currentPosition;
		int currentOffset;
	}

	public static void main(String[] args) {
		PlantPots pots = new PlantPots();
		InputReader in = new InputReader();
		in.readInputs();

		pots.currentPosition = in.getInitialPosition();
		pots.currentOffset = 4; // starting offset, with four empty pots on the left
		HashMap<String, Character> rules = in.getRules();

		int generation = -1;
		boolean finished = false;
		int thisValue = 0;
		while (!finished) {
			generation++;
			thisValue = calculateValue(pots);
			System.out.println(thisValue);
			if (generation == 20) System.out.println("Part 1 answer is " + thisValue);
			finished = applyRules(rules, pots);
		}

		// Calculate part 2 answer
		System.out.println(pots.currentPosition);
		long part2 = (50000000000l - generation) * (calculateValue(pots) - thisValue) + thisValue;
		System.out.println("Part 2 answer is " + part2);
	}

	/**
	 * The "currentPosition" is always adjusted so that we have exactly four empty pots on each
	 * end.
	 * 
	 * @return True if the new configuration is identical to the old configuration (except for
	 *         offset)
	 */
	private static boolean applyRules(HashMap<String, Character> rules, PlantPots pots) {
		StringBuffer buf = new StringBuffer();

		// Result will certainly have two empty pots at the start
		buf.append("..");

		// Calculate the rest of the new position
		for (int i = 2; i < pots.currentPosition.length() - 2; i++) {
			String key = pots.currentPosition.substring(i - 2, i + 3);
			if (rules.containsKey(key)) buf.append('#');
			else buf.append('.');
		}

		// Justify the buffer so that there are exactly four empty pots on each end
		while (buf.substring(0, 5).equals(".....")) {
			buf.deleteCharAt(0);
			pots.currentOffset--;
		}
		while (!buf.substring(0, 4).equals("....")) {
			buf.insert(0, ".");
			pots.currentOffset++;
		}
		while (buf.substring(buf.length() - 5, buf.length()).equals("....."))
			buf.deleteCharAt(buf.length() - 1);
		while (!buf.substring(buf.length() - 4, buf.length()).equals("...."))
			buf.append(".");

		String oldPosition = pots.currentPosition;
		pots.currentPosition = buf.toString();
		return oldPosition.equals(pots.currentPosition);
	}

	private static int calculateValue(PlantPots pots) {
		int value = 0;

		// Sum the pot locations, and count the plants so that we can apply the offset
		int numPlants = 0;
		for (int i = 0; i < pots.currentPosition.length(); i++) {
			if (pots.currentPosition.charAt(i) == '#') {
				value += i;
				numPlants++;
			}
		}
		value -= numPlants * pots.currentOffset;

		return value;
	}
}
