package AdventOfCode.aoc_2022.day14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;
	static ArrayList<ArrayList<Point>> paths = new ArrayList<>();
	static char[][] map;

	public static void main(String[] args) throws Exception {
		readInput();
		createPaths(); // Translated input lines
		createMap();

		int units = dropSand();
		System.out.format("Part 2 answer = %d%n", units);

//		printMap();

	}

	private static int dropSand() {
		int units = 0;
		while (dropUnit())
//			printMap(); System.out.println("\n");
			units++;
		return units;
	}

	private static boolean dropUnit() {
		Point cursor = new Point(500, 0);
		if (map[0][500] == 'o') return false;
		boolean stuck = false;
		while (!stuck) {
			if (map[cursor.y + 1][cursor.x] == '.') {
				cursor.y++;
			} else if (map[cursor.y + 1][cursor.x - 1] == '.') {
				cursor.y++;
				cursor.x--;
			} else if (map[cursor.y + 1][cursor.x + 1] == '.') {
				cursor.y++;
				cursor.x++;
			} else {
				map[cursor.y][cursor.x] = 'o';
				stuck = true;
			}
		}
		return true;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

	private static void createPaths() {
		for (String line : lines) {
			ArrayList<Point> path = new ArrayList<>();
			paths.add(path);
			String[] parts = line.split(" -> ");
			for (String part : parts) {
				String[] values = part.split(",");
				path.add(new Point(Integer.parseInt(values[0]), Integer.parseInt(values[1])));
			}
		}
	}

	private static void createMap() {
		// Find maximum X and Y values and initialize map
		Point maxValues = getMaxValues();
		// System.out.format("Max values are %d, %d%n", maxValues.x, maxValues.y);
		
		// Cheat for part 2 - we make the map 1000 units wide...
		maxValues.x = 1000;
		map = new char[maxValues.y + 3][maxValues.x + 1];
		for (int y = 0; y < map.length; y++) {
			for (int x = 0; x < map[0].length; x++) {
				map[y][x] = '.';
			}
		}
		
		// Add floor
		for (int x = 0; x <= maxValues.x; x++) {
			map[map.length-1][x] = '#';
		}

		for (ArrayList<Point> path : paths) {
			Point cursor = path.get(0);
			map[cursor.y][cursor.x] = '#';
			for (int i = 1; i < path.size(); i++) {
				Point target = path.get(i);
				while (cursor.x != target.x || cursor.y != target.y) {
					if (cursor.x != target.x) { // adjust x
						cursor.x += (cursor.x < target.x) ? 1 : -1;
					} else { // adjust y
						cursor.y += (cursor.y < target.y) ? 1 : -1;
					}
					map[cursor.y][cursor.x] = '#';
				}
			}
		}
	}

	private static Point getMaxValues() {
		int maxX = 0;
		int maxY = 0;
		for (ArrayList<Point> path : paths) {
			for (Point point : path) {
				if (point.x > maxX) maxX = point.x;
				if (point.y > maxY) maxY = point.y;
			}
		}
		return new Point(maxX, maxY);
	}

	// Note: may not entirely fit on the console
	private static void printMap() {
		// print map
		for (int y = 0; y < map.length; y++) {
			System.out.format("%4d   ", y);
			for (int x = 0; x < map[0].length; x++) {
				System.out.print(map[y][x]);
			}
			System.out.println();
		}
	}
}