package AdventOfCode.aoc_2021.day14;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Scanner;

public class Problem01 {
	private static ArrayList<PairRule> rules = new ArrayList<>();
	private static String startingPolymer;
	private static TreeMap<Character, Long> elements = new TreeMap<>();
	
	private static class PairRule {
		char e1;
		char e2;
		char insert;
		
		public PairRule(char e1, char e2, char insert) {
			this.e1 = e1; this.e2 = e2; this.insert = insert;
		}
	}

	public static void main(String[] args) {
		readInput();
		
		String polymer = startingPolymer;
		for (int i = 0; i < 10; i++) {
			polymer = applyRules(polymer);
			System.out.println(polymer);
		}
		
		for (char c : polymer.toCharArray()) {
			if (!elements.containsKey(c)) elements.put(c, 0l);
			elements.put(c, elements.get(c)+1);
		}
		
		long highest = 0;
		long lowest = Long.MAX_VALUE;
		for (long value : elements.values()) {
			if (value > highest) highest = value;
			if (value < lowest) lowest = value;
		}
		
		System.out.println(highest - lowest);
	}
	
	private static String applyRules(String in) {
		StringBuffer sb = new StringBuffer();
		sb.append(in.charAt(0));
		
		for (int i = 0; i < in.length()-1; i++) {
			PairRule rule = findRule(in.charAt(i), in.charAt(i+1));
			sb.append(rule.insert);
			sb.append(rule.e2);
		}
		return sb.toString();
	}
	
	private static PairRule findRule(char e1, char e2) {
		for (PairRule rule : rules) {
			if (rule.e1 == e1 && rule.e2 == e2) return rule;
		}
		return null;
	}
	
	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("sample.txt"));
		startingPolymer = in.nextLine();
		in.nextLine(); // skip blank line
		
		// Read pair-rules
		while (in.hasNextLine()) {
			String line = in.nextLine();
			PairRule rule = new PairRule(line.charAt(0), line.charAt(1), line.charAt(6));
			rules.add(rule);
		}
	}
}
