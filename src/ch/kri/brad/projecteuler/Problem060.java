package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.Primes;

/**
 * The primes 3, 7, 109 and 673 are quite remarkable. By taking any two primes and concatenating
 * them in any order the result will always be prime. For example, taking 7 and 109, both 7109 and
 * 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four
 * primes with this property.
 * 
 * Find the lowest sum for a set of five primes for which any two primes concatenate to produce
 * another prime.
 * 
 * General approach: For all primes below some limit: generate all pairs of primes meeting the
 * property. Extend the pairs to triplets. Extend the triplets to 4-tuples, and finally to 5-tuples.
 * The tuples are instances of the inner class PrimeSet, which contains an equality relation that
 * tests whether all elements are identical; the tuples are collected in a Set.
 * 
 * @author brad
 * 
 */
public class Problem060 {
	private static boolean[] primeSieve;
	private static ArrayList<Long> primeList;
	private static final int LIMIT = 10000;
	private static HashSet<PrimeSet>[] tupleLists;
	private static HashSet<Long> candidatePrimes;
	
	public static void main(String[] args) {
		// Prepare the primes that we will need. Note that, if we build tuples with 3-digits, we need all primes
		// up to 6-digits to test the results.
		Primes.generatePrimesErastothenes(LIMIT * LIMIT);
		primeSieve = Primes.getRawSieve();
		primeList = Primes.getPrimes();
		
		// Prepare the sets of tuples that we will need: sizes 2, 3, 4 and 5 in an array
		tupleLists = new HashSet[4];
		for (int i = 0; i < 4; i++) tupleLists[i] = new HashSet<>();
		
		// Calculate all pairs of primes meeting the properties
		HashSet<PrimeSet> pairs = tupleLists[0];
		for (int i = 0; i < primeList.size()-1; i++) {
			long a = primeList.get(i);
			if (a > LIMIT) break;	// Ugly: Quit when too large
			for (int j = i+1; j < primeList.size(); j++) {
				long b = primeList.get(j);
				if (b > LIMIT) break;	// Ugly: Quit when too large
				if (hasProperty(a,b)) {
					pairs.add(new PrimeSet(a,b));
				}
			}
		}
		
		// The only candidate primes are those found in the pairs
		candidatePrimes = new HashSet<>();
		for (PrimeSet primeSet : pairs) {
			for (Long element : primeSet.elements) {
				candidatePrimes.add(element);
			}
		}
		
		// Enlarge the tuples step-by-step
		for (int tupleSize = 3; tupleSize <= 5; tupleSize++) {
			HashSet<PrimeSet> oldTuples = tupleLists[tupleSize-3];
			HashSet<PrimeSet> newTuples = tupleLists[tupleSize-2];
			
			for (PrimeSet old : oldTuples) {
				for (Long newPrime : candidatePrimes) {
					// Only proceed if newPrime is larger than any prime currently in the set
					long currentLargest = old.getLargest();
					if (newPrime > currentLargest) {
						if (hasProperty(old, newPrime)) {
							PrimeSet newTuple = new PrimeSet(old, newPrime); 
							newTuples.add(newTuple);
							System.out.println("New tuple: " + newTuple.toString());
						}
					}
				}
			}
		}
	}

	/**
	 * A PrimeSet is a set of primes that meet the property described above. A PrimeSet may contain
	 * any number of primes, but however many it contains, concatenating any two of its elements in
	 * any order produces a prime number.
	 * 
	 * @author brad
	 * 
	 */
	public static class PrimeSet {
		TreeSet<Long> elements = new TreeSet<Long>();

		/**
		 * Constructor to create a PrimeSet with two elements
		 * @param a
		 * @param b
		 */
		public PrimeSet(Long a, Long b)  {
			elements.add(a);
			elements.add(b);
		}
		
		/**
		 * Constructor to create an enlarged PrimeSet
		 */
		public PrimeSet(PrimeSet s, Long a) {
			for (Long b : s.elements) {
				elements.add(b);
			}
			elements.add(a);
		}
		
		/**
		 * Return the largest prime in this set
		 */
		public Long getLargest() {
			return elements.last();
		}
		
		@Override
		public boolean equals(Object o) {
			boolean areEqual = false;
			if (o instanceof PrimeSet) {
				if (((PrimeSet) o).elements.size() == elements.size()) {
					areEqual = true;
					for (Long i : elements) {
						if (!((PrimeSet) o).elements.contains(i)) {
							areEqual = false;
							break;
						}
					}
				}
			}
			return areEqual;
		}
		
		@Override
		public String toString() {
			StringBuffer b = new StringBuffer();
			long sum = 0;
			for (Long e : elements) {
				b.append(e.toString() + ", ");
				sum += e;
			}
			b.append("  Sum = " + Long.toString(sum));
			return b.toString();
		}
	}
	
	private static boolean hasProperty(Long a, Long b) {
		boolean hasProperty = false;
		int[] digitsA = Digits.toDigits(a);
		int[] digitsB = Digits.toDigits(b);
		int[] digitsC = concatArray(digitsA, digitsB);
		int c = (int) Digits.fromDigits(digitsC);
		if (primeSieve[c-2]) {
			int[] digitsD = concatArray(digitsB, digitsA);
			int d = (int) Digits.fromDigits(digitsD);
			if (primeSieve[d-2]) hasProperty = true;
		}
		return hasProperty;		
	}
	
	private static boolean hasProperty(PrimeSet s, Long a) {
		boolean hasProperty = true;
		for (Long b : s.elements) {
			if (!hasProperty(a,b)) {
				hasProperty = false;
				break;
			}
		}
		return hasProperty;
	}
	
	private static int[] concatArray(int[] a, int[] b) {
		int[] c = new int[a.length + b.length];
		for (int i = 0; i < a.length; i++) c[i] = a[i];
		for (int i = 0; i < b.length; i++) c[i+a.length] = b[i];
		return c;
	}
}
