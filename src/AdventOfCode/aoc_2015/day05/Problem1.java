package AdventOfCode.aoc_2015.day05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();

		int count = 0;
		for (String line : lines) {
			if (isNice(line)) count++;
		}
		System.out.println("Part 1 answer is " + count);
	}
	
	private static boolean isNice(String in) {
		char[] chars = in.toCharArray();
		boolean isNice = false;
		
		int countVowels = 0;
		for (int i = 0; countVowels < 3 && i < chars.length; i++) {
			if (chars[i] == 'a' || chars[i] == 'e' || chars[i] == 'i' || chars[i] == 'o' || chars[i] == 'u') countVowels++;
		}
		
		if (countVowels >= 3) {
			boolean goodPairFound = false;
			boolean badPairFound = false;
			for (int i = 0; !badPairFound && i < chars.length-1; i++) {
				goodPairFound |= chars[i] == chars[i+1];
				badPairFound |= (chars[i] == 'a' && chars[i+1] == 'b');
				badPairFound |= (chars[i] == 'c' && chars[i+1] == 'd');
				badPairFound |= (chars[i] == 'p' && chars[i+1] == 'q');
				badPairFound |= (chars[i] == 'x' && chars[i+1] == 'y');
			}
			
			isNice = goodPairFound && !badPairFound;
		}
		return isNice;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}