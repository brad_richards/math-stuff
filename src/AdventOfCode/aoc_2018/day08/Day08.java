package AdventOfCode.aoc_2018.day08;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Day08 {
	public static void main(String[] args) {
		InputReader reader = new InputReader();
		Node root = reader.readTree();
		printTree(root, 0);
		System.out.println("Part 1: sum of metadata = " + sumMetadata(root));
		System.out.println("Part 2: value of root = " + root.getValue());
	}
	
	private static int sumMetadata(Node node) {
		ArrayList<Node> children = node.getChildren();
		ArrayList<Integer> metadata = node.getMetadata();
		int sum = node.getMetadataSum();
		for (Node child : children) sum += sumMetadata(child);
		return sum;
	}
	
	private static void printTree(Node node, int indent) {
		ArrayList<Node> children = node.getChildren();
		for (int i = 0; i < indent; i++) System.out.print("  ");
		System.out.println(children.size() + "-" + node.getMetadata().size() + " (" + node.getMetadataSum() + ")");
		for (Node child : children) printTree(child, indent+1);
	}
}
