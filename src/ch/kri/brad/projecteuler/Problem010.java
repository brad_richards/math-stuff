package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Primes;



public class Problem010 {

	public static void main(String[] args) {
		// Generate far too many primes
		Primes.generatePrimesErastothenes(2000000);
		
		ArrayList<Long> primes = Primes.getPrimes();
		
		long sum = 0;
		for (int i = 0; i < primes.size(); i++) {
			sum += primes.get(i);
		}
		
		System.out.println("Answer = " + sum);
	}

}
