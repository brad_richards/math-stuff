package ch.kri.brad.projecteuler;
import java.util.Arrays;

import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.Primes;


public class Problem041 {
	/**
	 * General approach: generate candidate 9-digit pandigital numbers in
	 * descending order. Test each of these, until we find a prime.
	 */
	public static void main(String[] args) {
		Primes.generatePrimesErastothenes((int) Math.sqrt(987654321), false);
		for (int numOfDigits = 9; numOfDigits > 1; numOfDigits--) {		
			int[] validDigits = new int[numOfDigits];
			for (int i = 0; i < numOfDigits; i++) validDigits[i] = numOfDigits - i;
			generateAndTest(validDigits, new int[numOfDigits], 0);
		}
	}

	static void generateAndTest(int[] validDigits, int[] digits, int nextDigit) {
		if (nextDigit == validDigits.length) { // base case; all digits placed
			long number = Digits.fromDigits(digits);
			if (Primes.isPrime(number)) {
				System.out.println("Answer = " + number);
				System.exit(0); // brutal and ugly...
			}
		} else {
			for (int useDigit : validDigits) {
				if (useDigit > 0) {
					// Get the next possible digit to use, and remove it from a copy of
					// the valid-digits
					int[] restValidDigits = Arrays
							.copyOf(validDigits, validDigits.length);
					restValidDigits[validDigits.length - useDigit] = 0;

					// Put this digit into a copy of our number, and recurse
					int[] newDigits = Arrays.copyOf(digits, digits.length);
					newDigits[nextDigit] = useDigit;
					generateAndTest(restValidDigits, newDigits, nextDigit + 1);
				}
			}
		}
	}
}
