package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Primes;

/**
 * The number of solutions is driven by the number of different factors. First, we fine the sequence of unique primes that comes
 * closest to the desired total, without exceeding it: 2 * 3 * 5 * 7 * 11 etc... We take the highest of these primes and find the
 * highest factor of 2 that does not exceed it: this is the maximum number of repetitions of any prime factor.
 * 
 * The though was that we could then generate and try a limited set of possiblities - but even this is far, far too slow...
 */
public class Problem110 {
	private static final long[] primes = {2, 3, 5, 7, 11, 13, 17, 19};
	private static final long requiredNumSolutions = 1000;
	private static int maxPrimeIndex = 1;
	private static int maxRepetitions = 1;
	
	public static void main(String[] args) {
		// Find the highest prime we will need to use
		long product = 1;
		long numSolutions = 0;
		for (int i = 0; numSolutions < requiredNumSolutions && i < primes.length; i++) {
			product *= primes[i];
			numSolutions = numSolutions(product);
			System.out.println(primes[i] + " " + numSolutions);
			maxPrimeIndex = i-1;
		}
		
		// Find the max number of repetitions for any prime
		maxRepetitions = (int) (Math.log10(primes[maxPrimeIndex+1]) / Math.log10(2.0f));
		
		System.out.println(primes[maxPrimeIndex] + " " + maxRepetitions);
	}

	/**
	 * Return the number of solutions for a given value of n We can assert that x is in the range
	 * (n+1)...(2n), and we can then solve for Y: y = x * n / (x - n)
	 */
	private static long numSolutions(long n) {
		int numSolutions = 0;
		for (long x = n + 1; x <= 2 * n; x++) {
			long xn = x * n;
			long x_minus_n = x - n;
			if (xn % x_minus_n == 0) numSolutions++;
		}
		return numSolutions;
	}
}
