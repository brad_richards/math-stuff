package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import ch.kri.brad.utility.Primes;

// currently far too slow. Must be possible to limit selection of a and b based
// on the need that b³ is evenly divisible by a²
// 
// Generative: a² is a factor of b³
// - Unclear why, but a is always itself a cube
// - Generate a from 0 or more prime factors (each prime factor 3 times)
// - Generate possible b by multiplying a² by one or more additional factors (whose product is x)
// - The value (x-a) must be a prime < 1000000


public class Problem131 {
	private static TreeSet<Long> primes;

	public static void main(String[] args) {
		// First, generate all primes up to 1 million
		Primes.generatePrimesErastothenes(1000000, true, false);
		primes = Primes.getPrimeTree();
		System.out.println("Primes generated");
		
		long maxB = (long) Math.pow(Long.MAX_VALUE, 0.333);
		// Work from the larger cube, backwards
		for (long b = 1; b < 	maxB; b++) {
			ArrayList<Long> primeFactorsB = Primes.primeFactors(b);
			ArrayList<Long> aCandidates = getCandidatesA(primeFactorsB, b);
			
//			System.out.println("B = " + b);
//			System.out.println("Prime factors of B: " + primeFactorsB);
//			System.out.println("A-candidates: " + aCandidates);
//			System.out.println();
			
			for (long a : aCandidates) {
//			for (long a = b - 1; a > 0; a--) {
				long diff = b*b*b - a*a*a;
				if (diff % (a*a) == 0) {
					long possPrime = diff / (a*a);
					if (primes.contains(possPrime)) {
						System.out.println("a = " + a + ", b = " + b + ", prime = " + possPrime);
					}
				}
			}
		}
		
	}
	
	private static ArrayList<Long> getCandidatesA(ArrayList<Long> primeFactorsB, long b) {
		ArrayList<Factor> factorsOfA = createFactorsList(primeFactorsB);
		ArrayList<Long> candidatesA = new ArrayList<>();
		getCandidatesA(factorsOfA, 0, candidatesA, 1);
		filterCandidatesA(candidatesA, b);
		return candidatesA;
	}

	private static class Factor {
		long factor;
		long qty;
		
		public Factor(long factor, long qty) { this.factor = factor; this.qty = qty; }
	}
	
	private static ArrayList<Factor> createFactorsList(ArrayList<Long> primeFactorsB) {
		ArrayList<Factor> factorsOfA = new ArrayList<>();
		for (long factor : primeFactorsB) {
			int cursor = 0;
			while (cursor < factorsOfA.size() && factor > factorsOfA.get(cursor).factor) {
				cursor++;
			}
			if (cursor < factorsOfA.size() && factor == factorsOfA.get(cursor).factor) { // found existing factor
				factorsOfA.get(cursor).qty += 1;
			} else { // new factor
				factorsOfA.add(new Factor(factor, 1));
			}
		}
		return factorsOfA;
	}
	
	// Recurse through factors, working each factor in turn to create all possible
	// combinations. The smallest will be 1, the largest will be "b". We have the factors
	// of b, but we really work with b³ so we have triple the number of factors. We want
	// to construct a², so that cuts the number of factors in half.
	private static void getCandidatesA(ArrayList<Factor> factors, int factorNum, ArrayList<Long> candidatesA, long product) {
		if (factorNum >= factors.size()) { // Base case
			candidatesA.add(product);
		} else {
			Factor factor = factors.get(factorNum);
			long maxNum = factor.qty * 3 / 2;
			long thisFactorValue = 1;
			for (int i = 0; i <= maxNum; i++) {
				getCandidatesA(factors, (factorNum+1), candidatesA, (product*thisFactorValue));
				thisFactorValue *= factor.factor;
			}
		}		
	}
	
	// Value of A must be > 1 and a³ < b³
	private static void filterCandidatesA(ArrayList<Long> candidatesA, long b) {
		Iterator<Long> i = candidatesA.iterator();
		while (i.hasNext()) {
			Long a = i.next();
			if (a*a*a >= b*b*b) i.remove();
		}
	}
}
