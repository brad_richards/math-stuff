package AdventOfCode.aoc_2022.day16;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

/**
 * A path is a possible sequence of moves, consisting of movement and
 * valve-openings. However, we are not going to bother tracking where we have
 * been - revisiting old locations will automatically create lower-value paths.
 * 
 * We track the status of all values in a special HashMap, so that we know if
 * they are open (true) or closed (false)
 * 
 * We also have the following attributes:
 * 
 * - The number of moves made so far (maximum 20 allowed) - Our current location
 * (valve name) - The current per-turn value increase - The total value, which
 * determines the quality of our path
 */
public class Solution1 implements Comparable<Solution1> {
	public static HashMap<String, Valve> valves;
	public static final int timeLimit = 30;

	public static int highestPrognosis = -1;

	final HashMap<String, Boolean> valveStatus;
	int numMoves = 0;
	String location = "AA";
	int increasePerTurn = 0;
	int total = 0;

	// Predictive values used for pruning
	int prognosis = 0;
	int maxValue = 0;

	public Solution1() {
		// Initialize the valveStatus
		valveStatus = new HashMap<>();
		for (String name : valves.keySet()) {
			valveStatus.put(name, false);
		}
	}

	// Cloning constructor
	public Solution1(Solution1 oldPath) {
		this.valveStatus = (HashMap<String, Boolean>) oldPath.valveStatus.clone();
		this.numMoves = oldPath.numMoves;
		this.location = oldPath.location;
		this.increasePerTurn = oldPath.increasePerTurn;
		this.total = oldPath.total;

	}
	
	// Predictive values used guiding the search
	private void updatePredictions() {
		this.prognosis = prognosis();
		this.maxValue = maxPossible();
		if (maxValue < prognosis) {
			System.out.println("This cannot happen!");
		}
	}

	@Override
	public String toString() {
		String s = numMoves + " moves, at " + location + ": per turn " + increasePerTurn + " total "
				+ total;
		s += " (max " + maxValue + ", prog " + prognosis + " highest " + highestPrognosis + ")\n";
		for (String name : valves.keySet()) {
			Valve v = valves.get(name);
			Boolean b = valveStatus.get(name);
			if (v.getFlowRate() > 0) {
				s += "  " + name + " is " + (b ? "open" : "closed") + "\n";
			}
		}
		return s;
	}

	public void getNextPaths(PriorityQueue<Solution1> allPaths) {
		Valve valve = valves.get(location);
		for (Valve tunnel : valve.getTunnels()) {
			Solution1 p = new Solution1(this);
			p.location = tunnel.getName();
			p.numMoves += 1;
			p.total += p.increasePerTurn;
			p.updatePredictions();
			if (p.maxValue >= highestPrognosis) allPaths.add(p); // Only add it,
																// if it could
																// be useful
//			if (p.numMoves > 24) System.out.println(p);

			// Possibly a second path, if the valve is not open and has a value
			// > 0
			if (!valveStatus.get(p.location) && valves.get(p.location).getFlowRate() > 0 && p.numMoves < timeLimit) {
				Solution1 p2 = new Solution1(p);
				p2.numMoves += 1;
				p2.total += p2.increasePerTurn;
				p2.increasePerTurn += valves.get(p2.location).getFlowRate();
				p2.valveStatus.put(p2.location, true);
				p2.updatePredictions();
				if (p2.maxValue >= highestPrognosis) allPaths.add(p2);
			}
		}
	}

	private int prognosis() {
		int prognosis = total + (timeLimit - numMoves) * increasePerTurn;
		if (prognosis > highestPrognosis) highestPrognosis = prognosis;
		return prognosis;
	}

	// Look at the remaining open valves, and calculate (generously) the
	// maximum value that this solution could ever reach
	private int maxPossible() {
		ArrayList<Integer> values = new ArrayList<>();
		for (String name : valves.keySet()) {
			if (!valveStatus.get(name)) {
				Valve v = valves.get(name);
				if (v.getFlowRate() > 0) {
					values.add(v.getFlowRate());
				}
			}
		}
		Collections.sort(values);

		int flow = increasePerTurn;
		int maxValue = total;
		for (int t = 1; t <= (timeLimit - numMoves); t++) {
			maxValue += flow;
			if (t % 2 == 0) {
				int pos = (values.size() - (t/2));
				if (pos >= 0) flow += values.get(pos);
			}
		}
		return maxValue;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass() == this.getClass()) {
			Solution1 s = (Solution1) o;
			return this.prognosis == s.prognosis;
		}
		return false;
	}
	
	// Deliberately reversed, because these elements are stored in a PriorityQueue where smaller is better
	@Override
	public int compareTo(Solution1 s) {
		return Integer.compare(s.prognosis, prognosis);
	}
}
