package AdventOfCode.aoc_2019.IntCodeComputer;

public class Instruction {
	private int opCode;
	private int size;
	private boolean[] positionMode = new boolean[3];
	
	public Instruction(int instruction) {
		this.opCode = instruction % 100;
		instruction /= 100;
		for (int i = 0; i < 3; i++) {
			positionMode[i] = (instruction % 10) == 0;
			instruction /= 10;
		}
		this.size = OpCodes.opCodes.getSize(this.opCode);
	}
	
	public int getOpCode() { return opCode; }
	
	public int getSize() { return size; }
	
	public boolean isPositionMode(int parmNumber) { // parmNumber is one-based
		return positionMode[parmNumber-1]; // array is zero-based
	}
}
