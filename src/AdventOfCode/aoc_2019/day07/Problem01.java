package AdventOfCode.aoc_2019.day07;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import AdventOfCode.aoc_2019.IntCodeComputer.IntCodeComputer;

public class Problem01 {
	private static int[] originalMemory;
	private static IntCodeComputer computer;
	private static int bestResult = 0;

	public static void main(String[] args) {
		// read memory
		originalMemory = readInput();
		computer = new IntCodeComputer(originalMemory);

		ArrayList<Integer> phaseSettings = new ArrayList<>();
		phaseSettings.add(0); phaseSettings.add(1); phaseSettings.add(2); phaseSettings.add(3); phaseSettings.add(4);  
		runAmplifier(phaseSettings, 0);
		
		// Print answer
		System.out.println(bestResult);
	}
	
	/**
	 * Depth-first search in Java - yuck
	 */
	private static void runAmplifier(ArrayList<Integer> availablePhaseSettings, int input) {
		if (availablePhaseSettings.size() == 0) { // base case
			if (input > bestResult) bestResult = input;
		} else {
			for (int i = 0; i < availablePhaseSettings.size(); i++) {
				ArrayList<Integer> copiedPhaseSettings = (ArrayList<Integer>) availablePhaseSettings.clone();
				Integer phaseSetting = copiedPhaseSettings.remove(i);
				computer.reset(originalMemory);
				computer.addInput(phaseSetting);
				computer.addInput(input);
				computer.executeProgram();
				int output = computer.getOutputs().get(0);
				runAmplifier(copiedPhaseSettings, output);
			}
		}
	}
	
	private static int[] readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		String line;
		try (Scanner in = new Scanner(fis) ) {
			line = in.nextLine();
		}
		String[] parts = line.split(",");
		int[] intParts = new int[parts.length];
		for (int i = 0; i < intParts.length; i++) intParts[i] = Integer.parseInt(parts[i]);
		return intParts;
	}
}
