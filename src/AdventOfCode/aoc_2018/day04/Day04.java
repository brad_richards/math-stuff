package AdventOfCode.aoc_2018.day04;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeSet;

public class Day04 {
	// HashMap Key is the Guard-ID, Value is an ArrayList of the guard's naps
	private static HashMap<Integer, ArrayList<Interval>> naps = new HashMap<>();

	private static class Interval {
		int value1;
		int value2;

		public Interval(int v1, int v2) {
			this.value1 = v1;
			this.value2 = v2;
		}
	}

	public static void main(String[] args) {
		readNaps();
		partOne();
		partTwo();
	}

	/**
	 * Parse the naps from the input file. The inputs are randomly ordered, but a simple
	 * alphabetical sort works. To do this, we just read the strings into a TreeSet. We then process
	 * the data. No error handling; the data is assumed to be correct. Note that we do not actually
	 * need any date or time information beyond the minutes
	 */
	private static void readNaps() {
		TreeSet<String> inputValues = new TreeSet<>();
		InputStream fis = Day04.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				inputValues.add(in.nextLine());
			}
		}

		// Now process the strings in sorted order
		LocalTime time = null;
		int guardID = -1;
		int fallAsleepMinute = -1;
		for (String value : inputValues) {
			String dateTime = value.substring(1, 17);
			if (value.charAt(19) == 'G') { // new guard ID, variable number of digits
				String guardValue = value.substring(26);
				guardValue = guardValue.substring(0, guardValue.indexOf(' '));
				guardID = Integer.parseInt(guardValue);
			} else if (value.charAt(19) == 'f') { // falls asleep
				fallAsleepMinute = Integer.parseInt(dateTime.substring(14, 16));
			} else { // wakes up
				int wakesUpMinute = Integer.parseInt(dateTime.substring(14, 16));
				ArrayList<Interval> guardNaps = naps.get(guardID);
				if (guardNaps == null) {
					guardNaps = new ArrayList<Interval>();
					naps.put(guardID, guardNaps);
				}
				guardNaps.add(new Interval(fallAsleepMinute, wakesUpMinute));
			}
		}
		System.out.println(naps.size() + " guards found");
	}

	/**
	 * Solve part one
	 */
	private static void partOne() {
		int sleepyGuard = guardWithMostSleep();
		System.out.println("Sleepiest guard is " + sleepyGuard);

		int sleepiestMinute = mapNaps(naps.get(sleepyGuard)).value1;
		System.out.println("Sleepiest minute is " + sleepiestMinute);

		System.out.println("Part 1 answer = " + sleepyGuard * sleepiestMinute);
	}
	
	/**
	 * Find the guard who spends the most time asleep
	 */
	private static int guardWithMostSleep() {
		int sleepyGuardID = -1;
		int mostMinutes = -1;
		for (int guardID : naps.keySet()) {
			ArrayList<Interval> guardNaps = naps.get(guardID);
			int sum = 0;
			for (Interval nap : guardNaps) {
				sum += nap.value2 - nap.value1;
			}
			if (sum > mostMinutes) {
				mostMinutes = sum;
				sleepyGuardID = guardID;
			}
		}
		return sleepyGuardID;
	}

	/**
	 * Find the minute the guard is most often asleep. From the problem description, we can just use
	 * an array from 0 to 59. We return <minute, times-asleep> as an Interval (I know, I know, not nice...)
	 */
	private static Interval mapNaps(ArrayList<Interval> guardNaps) {
		int[] minutes = new int[60];
		for (Interval nap : guardNaps) {
			for (int minute = nap.value1; minute < nap.value2; minute++)
				minutes[minute]++;
		}

		int maxMinute = 0;
		for (int minute = 1; minute < 60; minute++)
			if (minutes[minute] > minutes[maxMinute]) maxMinute = minute;

		return new Interval(maxMinute, minutes[maxMinute]);
	}
	
	/**
	 * Find the guard that is most often asleep on some particular minute (part 2 of the task)
	 */
	private static void partTwo() {
		int sleepyGuard = -1;
		int numTimesAsleepOnMinute = -1;
		int favoriteMinute = -1;
		for (int guardID : naps.keySet()) {
			ArrayList<Interval> guardNaps = naps.get(guardID);
			Interval interval = mapNaps(guardNaps);
			int nTAOM = interval.value2;
			if (nTAOM > numTimesAsleepOnMinute) {
				sleepyGuard = guardID;
				favoriteMinute = interval.value1;
				numTimesAsleepOnMinute = nTAOM;
			}
		}
		System.out.println("Guard " + sleepyGuard + " is asleep " + numTimesAsleepOnMinute + " times on his favorite minute, which is minute " + favoriteMinute);
		System.out.println("Answer to part two = " + favoriteMinute * sleepyGuard);
	}
}
