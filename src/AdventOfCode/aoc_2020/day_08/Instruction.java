package AdventOfCode.aoc_2020.day_08;

public class Instruction {
	public enum InstructionType { ACC, JMP, NOP; }
	
	private InstructionType type;
	private int arg;
	private boolean executed;
	
	public Instruction(String line) {
		String[] parts = line.split(" ");
		this.type = InstructionType.valueOf(parts[0].toUpperCase());
		this.arg = Integer.parseInt(parts[1]);
	}
	
	public Instruction(InstructionType type, int arg ) {
		this.type = type;
		this.arg = arg;
		this.executed = false;
	}

	public InstructionType getType() {
		return type;
	}

	public int getArg() {
		return arg;
	}

	public boolean isExecuted() {
		return executed;
	}

	public void setType(InstructionType type) {
		this.type = type;
	}

	public void setArg(int arg) {
		this.arg = arg;
	}

	public void setExecuted(boolean executed) {
		this.executed = executed;
	}
}
