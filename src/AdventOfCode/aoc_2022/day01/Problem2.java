package AdventOfCode.aoc_2022.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Nothing like overkill...
 */
public class Problem2 {
	private static class Elf implements Comparable<Elf> {
		private long total = 0;
		
		public void addFood(long food) {
			total += food;
		}
		
		@Override
		public boolean equals(Object o) {
			if (o != null && o.getClass() == this.getClass()) {
				Elf e = (Elf) o;
				return e.total == this.total;
			}
			return false;
		}
		
		@Override
		public int compareTo(Elf e) {
			return (int) Math.signum(this.total - e.total);
		}
		
		@Override
		public String toString() {
			return Long.toString(total);
		}
	}
	
	static ArrayList<Elf> elves = new ArrayList<>();
	
	static List<String> lines;
	
	
	public static void main(String[] args) throws Exception {
		readInput();
		Elf elf = new Elf();
		elves.add(elf);
		for (String line : lines) {
			if (line.isEmpty()) {
				elf = new Elf();
				elves.add(elf);
			} else {
				elf.addFood(Long.parseLong(line));
			}
		}
		
		long largest = 0;
		for (Elf e : elves) {
			if (e.total > largest) largest = e.total;
		}
		
		Collections.sort(elves);
		
		long total = 0;
		for (int i = elves.size()-3; i < elves.size(); i++) total += elves.get(i).total;
		System.out.println(total);
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
