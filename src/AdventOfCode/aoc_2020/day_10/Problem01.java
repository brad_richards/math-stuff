package AdventOfCode.aoc_2020.day_10;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Long> jolts = ir.getValues();
		jolts.add((long) 0);
		Collections.sort(jolts);
		jolts.add(jolts.get(jolts.size()-1)+3);
		
		int[] diffs = new int[3];
		for (int i = 0; i < jolts.size()-1; i++) {
			int diff = (int) (jolts.get(i+1) - jolts.get(i)); 
			diffs[diff-1]++;
		}
		
		System.out.println("Answer = " + diffs[0]*diffs[2]);
	}

}
