package kattis.b_datatypes.floats;

import java.util.Scanner;

/**
 * A complex problem description for a simple idea
 */
public class betting {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int percent = in.nextInt();
		
		double option1 = 100.0 / percent;
		double option2 = 100.0 / (100 - percent);
		
		System.out.println(option1);
		System.out.println(option2);
	}

}
