package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * For our purposes, we just use a 6 in place of the 9 digit. We then reject any
 * solution that does not allow us to generate all of the pairs: 01, 04, 06, 16,
 * 18, 25, 36, 46
 */
public class Problem090 {
	static int[][] combinations = new int[210][]; // All sorted combinations of digits 0-9
	static int cIndex = 0; // index into combinations for recursive generation

	static ArrayList<Dice> dice = new ArrayList<>();
	
	private static class Dice {
		public int[] die1;
		public int[] die2;
		
		public Dice(int[] die1, int[] die2) {
			this.die1 = die1; this.die2 = die2;
		}
	}

	public static void main(String[] args) {
		generateCombos(new int[6], 0);

		for (int i = 0; combinations[i][0] == 0; i++) { // Must have digit 0
			for (int j = i+1; j < combinations.length; j++) {
				Dice d = new Dice(combinations[i], combinations[j]);
				if (generatesPairs(d)) dice.add(d);
			}
		}
		
		System.out.println("Number of dice found " + dice.size());		
	}

	/**
	 * Generate all possible combinations of the digits 0-8, in sorted order
	 */
	private static void generateCombos(int[] combo, int digit) {
		if (digit >= combo.length) { // Base case - have finished combo
			addCombo(combo);
		} else if (digit == 0) { // Inductive case, first digit
			for (int value = 0; value < digit+5; value++) {
				combo[digit] = value;
				generateCombos(combo, digit+1);
			}
		} else { // Inductive case - loop through allowed values for this digit
			for (int value = combo[digit-1]+1; value < digit+5; value++) {
				combo[digit] = value == 9 ? 6 : value; // Replace 9's with 6's on last digit
				generateCombos(combo, digit+1);
			}
		}
	}
	
	private static void addCombo(int[] combo) {
		combinations[cIndex++] = Arrays.copyOf(combo, combo.length);
	}
	
	public static boolean generatesPairs(Dice candidate) {
		int[][] digitPairs = { { 0, 1 }, { 0, 4 }, { 0, 6 }, { 1, 6 }, { 1, 8 }, { 2, 5 }, { 3, 6 }, { 4, 6 } };

		boolean containsAll = true;
		for (int[] digitPair : digitPairs) {
			containsAll &= (contains(candidate.die1, digitPair[0]) && contains(candidate.die2, digitPair[1])
					|| contains(candidate.die1, digitPair[1]) && contains(candidate.die2, digitPair[0]));
			if (!containsAll) break; // just for optimization
		}
		return containsAll;
	}

	public static boolean contains(int[] die, int digit) {
		for (int val : die)
			if (val == digit) return true;
		return false;
	}
}
