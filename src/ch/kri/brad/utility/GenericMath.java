package ch.kri.brad.utility;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The purpose of this class is to provide basic mathematical operations for
 * a wide variety of data types. Basic operations (add, subtract, etc.) are
 * provided for pairs of values. Aggregation functions (average, max, min, etc.)
 * are provided for arrays of values.
 */
public class GenericMath {

	private static <T> Class<?> checkTypes(T... args) throws InvalidTypeException {
		assert (args != null && args.length >= 1);
		Class<?> requiredClass = args[0].getClass();
		for (int i = 1; i < args.length; i++) {
			Class<?> actualClass = args[i].getClass();
			if (! requiredClass.equals(actualClass)) {
				throw new InvalidTypeException("Types of the values must be the same");
			}
		}
		return requiredClass;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T add(T x, T y) throws InvalidTypeException {
		Class<?> argClass = checkTypes(x, y);
		T result = null;
		
		if (argClass.equals(Integer.class)) {
			result = (T) new Integer( (Integer) x + (Integer) y);
		} else if (argClass.equals(Float.class)){
			result = (T) new Float( (Float) x + (Float) y);
		} else if (argClass.equals(Long.class)){
			result = (T) new Long( (Long) x + (Long) y);			
		} else if (argClass.equals(Double.class)){
			result = (T) new Double( (Double) x + (Double) y);
		} else if (argClass.equals(BigDecimal.class)) {
			result = (T) ((BigDecimal) x).add( (BigDecimal) y);
		} else if (argClass.equals(BigInteger.class)) {
			result = (T) ((BigInteger) x).add( (BigInteger) y);
		} else {
			throw new InvalidTypeException("Unsupported type for 'add' operation");
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T subtract(T x, T y) throws InvalidTypeException {
		Class<?> argClass = checkTypes(x, y);
		T result = null;
		
		if (argClass.equals(Integer.class)) {
			result = (T) new Integer( (Integer) x - (Integer) y);
		} else if (argClass.equals(Float.class)){
			result = (T) new Float( (Float) x - (Float) y);
		} else if (argClass.equals(Long.class)){
			result = (T) new Long( (Long) x - (Long) y);			
		} else if (argClass.equals(Double.class)){
			result = (T) new Double( (Double) x - (Double) y);
		} else if (argClass.equals(BigDecimal.class)) {
			result = (T) ((BigDecimal) x).subtract( (BigDecimal) y);
		} else if (argClass.equals(BigInteger.class)) {
			result = (T) ((BigInteger) x).subtract( (BigInteger) y);
		} else {
			throw new InvalidTypeException("Unsupported type for 'subtract' operation");
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T multiply(T x, T y) throws InvalidTypeException {
		Class<?> argClass = checkTypes(x, y);
		T result = null;
		
		if (argClass.equals(Integer.class)) {
			result = (T) new Integer( (Integer) x * (Integer) y);
		} else if (argClass.equals(Float.class)){
			result = (T) new Float( (Float) x * (Float) y);
		} else if (argClass.equals(Long.class)){
			result = (T) new Long( (Long) x * (Long) y);			
		} else if (argClass.equals(Double.class)){
			result = (T) new Double( (Double) x * (Double) y);
		} else if (argClass.equals(BigDecimal.class)) {
			result = (T) ((BigDecimal) x).multiply( (BigDecimal) y);
		} else if (argClass.equals(BigInteger.class)) {
			result = (T) ((BigInteger) x).multiply( (BigInteger) y);
		} else {
			throw new InvalidTypeException("Unsupported type for 'multiply' operation");
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T divide(T x, T y) throws InvalidTypeException {
		Class<?> argClass = checkTypes(x, y);
		T result = null;
		
		if (argClass.equals(Integer.class)) {
			result = (T) new Integer( (Integer) x / (Integer) y);
		} else if (argClass.equals(Float.class)){
			result = (T) new Float( (Float) x / (Float) y);
		} else if (argClass.equals(Long.class)){
			result = (T) new Long( (Long) x / (Long) y);			
		} else if (argClass.equals(Double.class)){
			result = (T) new Double( (Double) x / (Double) y);
		} else if (argClass.equals(BigDecimal.class)) {
			result = (T) ((BigDecimal) x).divide( (BigDecimal) y);
		} else if (argClass.equals(BigInteger.class)) {
			result = (T) ((BigInteger) x).divide( (BigInteger) y);
		} else {
			throw new InvalidTypeException("Unsupported type for 'divide' operation");
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T sum(T... args) throws InvalidTypeException {
		Class<?> argClass = checkTypes(args);

		T result;
		if (args.length == 1) {
			result = args[0];
		} else {
			result = add(args[0], args[1]);
			for (int i = 2; i < args.length; i++) {
				result = add(result, args[i]);
			}
		}
		return result;
	}

	
	@SuppressWarnings("unchecked")
	public static <T> T average(T... args) throws InvalidTypeException {
		Class<?> argClass = checkTypes(args);
		T sum = sum(args);
		T average = null;
		int numArgs = args.length;
		if (Number.class.isAssignableFrom(argClass)) {
			T divisor = (T) createNumberFromValue(argClass, numArgs);
			average = divide(sum, divisor);
		} else {
			throw new InvalidTypeException("Unsupported type for 'average' operation");
		}
		return average;
	}

	private static Number createNumberFromValue(Class<?> targetClass, Number value) {
		Number returnValue = null;
		if (targetClass.equals(Integer.class)) {
			returnValue = new Integer((Integer) value);
		} else if (targetClass.equals(Float.class)) {
			returnValue = new Float((Float) value);
		} else if (targetClass.equals(Long.class)) {
			returnValue = new Long((Long) value);
		} else if (targetClass.equals(Double.class)) {
			returnValue = new Double((Double) value);
		} else if (targetClass.equals(BigDecimal.class)) {
			returnValue = new BigDecimal(value.toString());
		} else if (targetClass.equals(BigInteger.class)) {
			returnValue = new BigInteger(value.toString());
		}
		return returnValue;
	}
	
	public static class InvalidTypeException extends Exception {
		public InvalidTypeException(String msg) {
			super(msg);
		}
	}

	/**
	 * Adaptation of Euclid's GCD algorithm; num1 and num2 must both be non-zero
	 */
	public static long gcd(long num1, long num2) {
		long tmp;
		if (num2 > num1) {
			tmp = num2;
			num2 = num1;
			num1 = tmp;
		}
		while (num2 != 0) {
			tmp = num1 % num2;
			num1 = num2;
			num2 = tmp;
		}
		return num1;
	}
}
