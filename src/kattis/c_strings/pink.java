package kattis.c_strings;

import java.util.Scanner;

/**
 * Use the contains-method to check the contents of a string
 */
public class pink {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int count = in.nextInt();

        int numPinkColors = 0;
        for (int i = 0; i < count; i++) {
            String color = in.next();
            color = color.toLowerCase();
            if ( color.contains("pink") || color.contains("rose")) numPinkColors++;
        }
        in.close();
        
        if (numPinkColors > 0)
            System.out.println(numPinkColors);
        else
            System.out.println("I must watch Star Wars with my daughter");
        
    }

}