package ch.kri.brad.projecteuler.Problem082;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Arrays;

/**
 * In this version, we take the approach of extending the array of best paths from left to right. For any given cell not in the first column: The value of the best path to that cell
 * is the minimum of three values: the matrix value, plus the best path to one of the three neighboring cells. Note that values in each column may have to be re-calculated several times,
 * if the optimal path involves several vertical step.
 * 
 * We track the best values achievable, but not actually the paths that create them.
 */
public class Problem082 {
	private static final String FILE_NAME = "p082_matrix.txt";
	static final int SIZE = 80;
	static int[][] matrix = new int[SIZE][SIZE];
	private static int[][] bestValues = new int[SIZE][SIZE];

//	private static final int[][] testMatrix =
//		{ { 131, 673, 234, 103, 18 },
//		  { 201, 96, 342, 965, 150 },
//		  { 630, 803, 746, 422, 111 },
//		  { 537, 699, 497, 121, 956 },
//		  { 805, 732, 524, 37, 331 } };

	public static void main(String[] args) {
		 readFile();

//		matrix = testMatrix;
	
		// Initialize bestValues
		for (int row = 0; row < SIZE; row++) {
			Arrays.fill(bestValues[row], Integer.MAX_VALUE/10);
		}
		for (int row = 0; row < SIZE; row++) {
			bestValues[row][0] = matrix[row][0];
		}
		
		// Propagate to each subsequent column
		for (int col = 1; col < SIZE; col++) {
			bestValuesForCol(col);
		}
		
		// Find the smallest value in the final column
		int smallestValue = Integer.MAX_VALUE;
		for (int row = 0; row < SIZE; row++) {
			if (bestValues[row][SIZE-1] < smallestValue) smallestValue = bestValues[row][SIZE-1];
		}
		
		System.out.println("Answer: " + smallestValue);
	}

	private static void bestValuesForCol(int col) {
		int numChanges = -1;
		while (numChanges != 0) {
			numChanges = 0;
			for (int row = 0; row < SIZE; row++) {
				int possVal = bestValues[row][col-1] + matrix[row][col];
				if (betterValue(row, col, possVal)) numChanges++;
				
				if (row > 0) {
					possVal = bestValues[row-1][col] + matrix[row][col];
					if (betterValue(row, col, possVal)) numChanges++;
				}
				
				if (row < SIZE-2) {
					possVal = bestValues[row+1][col] + matrix[row][col];
					if (betterValue(row, col, possVal)) numChanges++;
				}
			}
//			printBestValues();
		}
	}
	
	private static void printBestValues() {
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				System.out.printf("%8d", bestValues[row][col]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	private static boolean betterValue(int row, int col, int value) {
		boolean result = bestValues[row][col] > value;
		if (result) bestValues[row][col] = value;
		return result;
	}
	
	/**
	 * Method to read the input file and create the internal representation
	 */
	private static void readFile() {
		URL url = Problem082.class.getResource(FILE_NAME);
		File f = new File(url.getPath());
		if (f.exists()) {
			try (BufferedReader in = new BufferedReader(new FileReader(f))) {
				String line = in.readLine();
				int row = -1;
				while (line != null) {
					row++;
					String[] nodeValues = line.split(",");
					if (nodeValues.length != SIZE)
						System.out.println("Error in input file: wrong row length");
					for (int col = 0; col < nodeValues.length; col++) {
						matrix[row][col] = Integer.parseInt(nodeValues[col]);
					}
					line = in.readLine();
				}
				if (row != SIZE - 1)
					System.out.println("Error in input file: wrong number of rows");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
