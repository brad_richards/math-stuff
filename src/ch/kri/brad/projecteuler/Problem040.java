package ch.kri.brad.projecteuler;
import ch.kri.brad.utility.Digits;


public class Problem040 {
	static int[] digits; // With a bit of extra space
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		createNumber(1000000);
		int answer = digits[0] * digits[9] * digits[99] * digits[999] * digits[9999] * digits[99999] * digits[999999];
		System.out.println(answer);
	}
	
	private static void createNumber(int numDigits) {
		digits = new int[numDigits+10]; // a bit of extra space
		int pos = 0;
		int currentNumber = 1;
		while (pos < numDigits) {
			int[] digitsOfNumber = Digits.toDigits(currentNumber);
			for (int digit : digitsOfNumber) {
				digits[pos++] = digit;
			}
			currentNumber++;
		}
	}
}
