package AdventOfCode.aoc_2020.day_08;

import java.io.IOException;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		Processor proc = new Processor(ir.getProgram());
		System.out.println("Result is " + proc.executeUntilLoop());
	}

}
