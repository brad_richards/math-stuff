package AdventOfCode.aoc_2022.day11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static List<Monkey> monkeys = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		readInput();

		for (int round = 0; round < 20; round++) {
			for (Monkey monkey : monkeys) monkey.throwItems(monkeys);
			
			for (Monkey monkey : monkeys) System.out.println(monkey);
			System.out.println();
		}
		
		ArrayList<Integer> business = new ArrayList<>();
		for (Monkey monkey : monkeys) business.add(monkey.getCount());
		Collections.sort(business);
		System.out.println("Part 1 answer = " + (business.get(business.size()-1)*business.get(business.size()-2)));
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		
		for (int i = 0; i < lines.size(); i+=7) {
			if (lines.get(i).startsWith("Monkey")) {
				monkeys.add(parseMonkey(i+1));
			}
		}
	}
	
	private static Monkey parseMonkey(int pos) {
		String[] tmp = lines.get(pos).split(":");
		tmp = tmp[1].split(",");
		long[] items = new long[tmp.length];
		for (int i = 0; i < tmp.length; i++) items[i] = Long.parseLong(tmp[i].trim());
		
		tmp = lines.get(pos+1).split("=");
		String operation = tmp[1].trim();
		tmp = operation.split(" ");
		char operator = tmp[1].charAt(0);
		String operand = tmp[2];
		
		tmp = lines.get(pos+2).split("by");
		long divBy = Integer.parseInt(tmp[1].trim());
		
		tmp = lines.get(pos+3).split("monkey");
		int trueTarget = Integer.parseInt(tmp[1].trim());
		
		tmp = lines.get(pos+4).split("monkey");
		int falseTarget = Integer.parseInt(tmp[1].trim());
		
		return new Monkey(monkeys.size(), operator, operand, divBy, trueTarget, falseTarget, items);
	}
}