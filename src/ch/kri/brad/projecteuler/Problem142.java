package ch.kri.brad.projecteuler;

import java.util.TreeSet;

/**
 * I needed some hints from the Internet for this one...
 * 
 * The trick in this problem is: we do not want to iterate on x, y and z, as there are two many
 * possible combinations. Instead, we want to iterate on the perfect-squares, and derive x, y and z
 * from these. Hence, we first transform the equations in such a way that we can iterate over three
 * perfect squares.
 * 
 * Let our six equations be:
 * 
 * A = X + Y
 * B = Y + Z
 * C = X + Z
 * D = X - Y
 * E = Y - Z
 * F = X - Z
 * 
 * Suppose that we iterate over A, B, and C; and want to find everything from these three values.
 * 
 * A - B = (X + Y) - (Y + Z) = (X + Z) = F // gives us F
 * A - C = (X + Y) - (X + Z) = (Y - Z) = E // gives us E
 * C - B = (X + Z) - (Y + Z) = (X - Y) = D // gives us D
 * 
 * To derive the original three variables
 * 
 * X = (C + F) / 2
 * Y = (B + E) / 2
 * Z = (A + D) / 2
 */
public class Problem142 {
	private static TreeSet<Long> squares = new TreeSet<>();

	public static void main(String[] args) {
		squares.add(1l);
		squares.add(4l);

		// We can assume that A, B and C are all different. Iterate in three nested loops.
		// Note that A > C > B if all results are to be positive
		for (long a = 3; a < 1000; a++) {
			long A = a * a;
			squares.add(A);
			
			// Iterate over C before B (C must be bigger than B)
			for (long c = 2; c < a; c++) {
				long C = c * c;
				long E = A - C;
				
				if (E > 0 && squares.contains(E)) {
					for (long b = 1; b < c; b++) {
						long B = b * b;
						long F = A - B;
						long D = C - B;
						if (F > 0 && D > 0 && squares.contains(F) && squares.contains(D)) {
							if ((C + F) % 2 == 0 && (B + E) % 2 == 0 && (A + D) % 2 == 0) {
								long X = (C + F) / 2;
								long Y = (B + E) / 2;
								long Z = (B - E) / 2;
								
								if (X > 0 && Y > 0 && Z > 0) {
									System.out.println("Solution: " + X + ", " + Y + ", " + Z + " = " + (X + Y + Z));
								}
							}							
						}
					}
				}
			}
		}
	}
}
