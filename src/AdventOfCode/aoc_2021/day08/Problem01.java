package AdventOfCode.aoc_2021.day08;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Problem01 {
	private static ArrayList<Data> data = new ArrayList<>();

	public static void main(String[] args) {
		readInput();

		// Part 1
		int count = 0;
		for (Data d : data) {
			for (String s : d.outputs)
				if (s.length() == 2 || s.length() == 3 || s.length() == 4 || s.length() == 7) count++;
		}
		System.out.println(count);

		// Part 2
		
		long sum = 0;
		for (Data d : data) {
			String[] digits = findDigits(d);
			int value = getValue(d, digits);
			sum += value;
		}

		System.out.println(sum);
	}
	
	private static int getValue(Data d, String[] digits) {
		int value = 0;
		for (String s : d.outputs) {
			value *= 10;
			for (int i = 0; i < digits.length; i++) {
				if (digits[i].equals(s)) value += i;
			}
		}
		return value;
	}

	private static String[] findDigits(Data d) {
		String[] digits = new String[10];

		// Find digits with unique numbers of segments
		digits[1] = findByLength(d.patterns, 2);
		digits[4] = findByLength(d.patterns, 4);
		digits[7] = findByLength(d.patterns, 3);
		digits[8] = findByLength(d.patterns, 7);

		// Digit 9 contains all of digit 4, with 6 segments
		digits[9] = findByAll(d.patterns, 6, digits[4]);

		// Digit 3 contains all of digit 7, with 5 segments
		digits[3] = findByAll(d.patterns, 5, digits[7]);

		// Digit 0 contains all of digit 7, with 6 segments
		digits[0] = findByAll(d.patterns, 6, digits[7]);

		// Digit 6 is the only remaining digit with 6 segments
		digits[6] = findByLength(d.patterns, 6);

		// Digit 5 is a subset of digit 9
		digits[5] = findBySubset(d.patterns, digits[9]);

		// Only remaining digit is 2
		digits[2] = lastRemaining(d.patterns);
		
		return digits;
	}

	private static String lastRemaining(String[] patterns) {
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i] != null) {
				String s = patterns[i];
				patterns[i] = null;
				return s;
			}
		}
		return null;
	}

	private static String findBySubset(String[] patterns, String superset) {
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i] != null) {
				boolean isSubset = true;
				for (char c : patterns[i].toCharArray()) {
					isSubset &= (superset.indexOf(c) >= 0);
				}
				if (isSubset) {
					String s = patterns[i];
					patterns[i] = null;
					return s;
				}
			}
		}
		return null;
	}

	private static String findByLength(String[] patterns, int length) {
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i] != null && patterns[i].length() == length) {
				String s = patterns[i];
				patterns[i] = null;
				return s;
			}
		}
		return null;
	}

	/**
	 * Has the specified length, and contains all of the letters contained in
	 * the variable args
	 */
	private static String findByAll(String[] patterns, int length, String... contains) {
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i] != null && patterns[i].length() == length) {
				boolean containsAll = true;
				for (String s : contains) {
					for (char c : s.toCharArray()) {
						containsAll &= (patterns[i].indexOf(c) >= 0);
					}
				}
				if (containsAll) {
					String s = patterns[i];
					patterns[i] = null;
					return s;
				}
			}
		}
		return null;
	}

	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			Data d = new Data();
			String[] parts = line.trim().split("\\|");
			d.patterns = parts[0].trim().split(" ");
			for (int i = 0; i < d.patterns.length; i++)
				d.patterns[i] = sortChars(d.patterns[i]);
			d.outputs = parts[1].trim().split(" ");
			for (int i = 0; i < d.outputs.length; i++)
				d.outputs[i] = sortChars(d.outputs[i]);
			data.add(d);
		}
	}

	private static String sortChars(String in) {
		char[] chars = in.toCharArray();
		Arrays.sort(chars);
		return String.valueOf(chars);
	}
}
