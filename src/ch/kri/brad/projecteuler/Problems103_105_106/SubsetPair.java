package ch.kri.brad.projecteuler.Problems103_105_106;

import java.util.Iterator;
import java.util.Set;

public class SubsetPair {
	Set<Integer> subset1;
	Set<Integer> subset2;

	public SubsetPair(Set<Integer> subset1, Set<Integer> subset2) {
		this.subset1 = subset1;
		this.subset2 = subset2;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("<{");
		for (Iterator<Integer> i = subset1.iterator(); i.hasNext();) {
			buf.append(i.next());
			if (i.hasNext()) buf.append(" ");
		}
		buf.append("}, {");
		for (Iterator<Integer> i = subset2.iterator(); i.hasNext();) {
			buf.append(i.next());
			if (i.hasNext()) buf.append(" ");
		}
		buf.append("}>");
		return buf.toString();
	}
}