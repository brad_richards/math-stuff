package AdventOfCode.aoc_2019.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem01 {
	public static void main(String[] args) {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			Integer fuel = stream
					.map(Integer::parseInt)
					.collect(Collectors.summingInt(i -> i / 3 - 2));

			System.out.println(fuel);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
