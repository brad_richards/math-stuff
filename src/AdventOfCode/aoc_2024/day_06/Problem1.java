package AdventOfCode.aoc_2024.day_06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Map represented in an int[][] array:
// 0 - open space
// -1 - obstacle
// 1 - Guard visits this space
//
// Current guard position is in the x,y variables.
// Current guard orientation is the char "orientation"
public class Problem1 {
	static List<String> lines;
	static int[][] map;
	static int x, y; // Guard position
	static Orientation orientation;

	public static void main(String[] args) throws Exception {
		readInput();
		inputToMap(lines);
		guardWanders();
		System.out.println(countVisitedPositions());
	}

	private static int countVisitedPositions() {
		int count = 0;
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] > 0) {
					count++;
				}
			}
		}
		return count;
	}

	private static void guardWanders() {
		// deltaX and deltaY set by guard orientation
		int deltaX = 0, deltaY = 0;
		switch (orientation) {
			case UP -> { deltaX = 0; deltaY = -1; }
			case DOWN -> { deltaX = 0; deltaY = 1; }
			case RIGHT -> { deltaX = 1; deltaY = 0; }
			case LEFT -> { deltaX = -1; deltaY = 0; }
		}
		while ( x+deltaX >= 0 && y+deltaY >= 0 && x+deltaX < map[0].length && y+deltaY < map.length) {
			if (map[y+deltaY][x+deltaX] >= 0) {
				x = x + deltaX;
				y = y + deltaY;
				map[y][x] = 1;
			} else { // rotate without moving
				switch (orientation) {
					case UP -> {orientation = Orientation.RIGHT; deltaX = 1; deltaY = 0; }
					case RIGHT -> {orientation = Orientation.DOWN; deltaX = 0; deltaY = 1; }
					case DOWN -> {orientation = Orientation.LEFT; deltaX = -1; deltaY = 0; }
					case LEFT -> {orientation = Orientation.UP; deltaX = 0; deltaY = -1; }
				}
			}
		}
	}

	private static void inputToMap(List<String> lines) throws Exception {
		map = new int[lines.size()][lines.get(0).length()];
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				if (line.charAt(j) == '#') {
					map[i][j] = -1;
				} else if (line.charAt(j) != '.') {
					x = j; y = i;
					map[y][x] = 1;
					switch (line.charAt(j)) {
						case '^' -> orientation = Orientation.UP;
						case 'v' -> orientation = Orientation.DOWN;
						case '>' -> orientation = Orientation.RIGHT;
						case '<' -> orientation = Orientation.LEFT;
						default -> throw new Exception("Unexpected character " + line.charAt(j));
					}
				}
			}
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}