package AdventOfCode.aoc_2018.day13;

public class Track {
	private boolean up, down, left, right;
	
	public Track(boolean up, boolean down, boolean left, boolean right) {
		this.up = up; this.down = down; this.left = left; this.right = right;
	}
	public boolean goUp() {
		return up;
	}
	public boolean goDown() {
		return down;
	}
	public boolean goLeft() {
		return left;
	}
	public boolean goRight() {
		return right;
	}
	public boolean isIntersection() {
		return up && down && left && right;
	}
	
	@Override
	public String toString() {
		if (up && down && !left && !right) return "|";
		else if (left && right && !up && !down) return "-";
		else if (up && down && left && right) return "+";
		else if (down && left || up && right) return "\\";
		else if (down && right || up && left) return "/";
		else if (!up && !down && !left && !right) return " ";
		else return "?";
	}
}