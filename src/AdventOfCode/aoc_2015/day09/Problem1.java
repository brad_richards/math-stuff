package AdventOfCode.aoc_2015.day09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Small enough that we just do an exhaustive search. Efficiency is not an issue.
 * 
 * Inputs are put into a matrix, to give quick access to distances. The names are stored in a List,
 * and the position in the list corresponds to the position in the distance-matrix.
 */
public class Problem1 {
	static List<String> lines;
	static List<String> names = new ArrayList<>();
	static int[][] distance;

	public static void main(String[] args) throws Exception {
		readInput();

		int shortestDistance = Integer.MIN_VALUE;
		for (int i = 0; i < distance.length; i++) {
			ArrayList<Integer> visited = new ArrayList<>();
			visited.add(i);
			int shortestThisStart = findShortest(visited, 0);
			if (shortestThisStart > shortestDistance) shortestDistance = shortestThisStart;
		}
		System.out.println("Part 1 answer = " + shortestDistance);
	}
	
	private static int findShortest(List<Integer> visited, int distSoFar) {
		if (visited.size() == names.size()) { // Base case
			return distSoFar;
		} else {
			int shortest = Integer.MIN_VALUE;
			for (int pos = 0; pos < names.size(); pos++) {
				if (!visited.contains(pos)) {
					List<Integer> clone = new ArrayList<>(visited);
					clone.add(pos);
					int dist = findShortest(clone, distSoFar + distance[visited.get(visited.size()-1)][pos]);
					if (dist > shortest) shortest = dist;
				}
			}
			return shortest;
		}
	}
	

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}

		// First, create the names-list
		for (String line : lines) {
			String[] parts = line.split(" ");
			getIndex(parts[0]);
			getIndex(parts[2]);
		}

		// Now create the distances matrix
		distance = new int[names.size()][names.size()];
		for (String line : lines) {
			String[] parts = line.split(" ");
			int i = getIndex(parts[0]);
			int j = getIndex(parts[2]);
			distance[i][j] = Integer.parseInt(parts[4].trim());
			distance[j][i] = Integer.parseInt(parts[4].trim());
		}
	}

	private static int getIndex(String name) {
		if (!names.contains(name)) names.add(name);
		return names.indexOf(name);
	}
}