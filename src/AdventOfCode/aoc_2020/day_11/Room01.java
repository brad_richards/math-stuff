package AdventOfCode.aoc_2020.day_11;

import java.util.List;

/**
 * A room contains an array that represents the status of the seating.
 * For efficiency, this is a char-array, using the three characters
 * from the problem description.
 * 
 * This class offers a factory method that produces a new room configuration
 * after applying all rules to this room.
 */
public class Room01 {
	private char[][] room;
	
	public Room01(List<String> input) {
		room = new char[input.size()][input.get(0).length()];
		for (int i = 0; i < input.size(); i++) {
			room[i] = input.get(i).toCharArray();
		}
	}
	
	private Room01(char[][] room) {
		this.room = room;
	}
	
	/**
	 * Apply all rules to create a new room
	 */
	public Room01 applyRules() {
		char[][] newRoom = new char[room.length][room[0].length];

		for (int r = 0; r < room.length; r++) {
			for (int c = 0; c < room[0].length; c++) {
				if (room[r][c] == '.') {
					newRoom[r][c] = '.';
				} else {
					int occupiedNeighbors = occupiedNeighbors(r, c);
					if (room[r][c] == 'L')
						if (occupiedNeighbors == 0)
							newRoom[r][c] = '#';
						else
							newRoom[r][c] = 'L';
					else if (room[r][c] == '#') // must be the case
						if (occupiedNeighbors >= 4)
							newRoom[r][c] = 'L';
						else
							newRoom[r][c] = '#';
					else
						System.out.println("Error in seating found!");
				}
			}
		}
		
		return new Room01(newRoom);
	}
	
	private int occupiedNeighbors(int row, int col) {
		int count = 0;
		for (int r = Math.max(0, row-1); r <= Math.min(room.length-1, row+1); r++) {
			for (int c = Math.max(0, col-1); c <= Math.min(room[0].length-1, col+1); c++) {
				if ( (r != row || c != col) && room[r][c] == '#') count++;
			}			
		}
		return count;
	}
	
	public int occupiedSeats() {
		int count = 0;
		for (char[] row : room) {
			for (char c : row) {
				if (c == '#') count++;
			}
		}
		return count;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) return false;
		Room01 roomIn = (Room01) o;
		boolean same = true;
		for (int r = 0; same && r < room.length; r++) {
			for (int c = 0; same && c < room[0].length; c++) {
				same = room[r][c] == roomIn.room[r][c];
			}
		}
		return same;
	}
}
