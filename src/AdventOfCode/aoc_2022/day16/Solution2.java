package AdventOfCode.aoc_2022.day16;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

/**
 * A path is a possible sequence of moves, consisting of movement and
 * valve-openings. However, we are not going to bother tracking where we have
 * been - revisiting old locations will automatically create lower-value paths.
 * 
 * We track the status of all values in a special HashMap, so that we know if
 * they are open (true) or closed (false)
 * 
 * We also have the following attributes:
 * 
 * - The number of moves made so far (maximum 20 allowed) - Our current location
 * (valve name) - The current per-turn value increase - The total value, which
 * determines the quality of our path
 */
public class Solution2 implements Comparable<Solution2> {
	public static HashMap<String, Valve> valves;
	public static final int timeLimit = 26;

	public static int highestPrognosis = -1;

	final HashMap<String, Boolean> valveStatus;
	int numMoves = 0;
	String location1 = "AA";
	String location2 = "AA";
	int increasePerTurn = 0;
	int total = 0;

	// Predictive values used for pruning
	int prognosis = 0;
	int maxValue = 0;

	public Solution2() {
		// Initialize the valveStatus
		valveStatus = new HashMap<>();
		for (String name : valves.keySet()) {
			valveStatus.put(name, false);
		}
	}

	// Cloning constructor
	public Solution2(Solution2 oldPath) {
		this.valveStatus = (HashMap<String, Boolean>) oldPath.valveStatus.clone();
		this.numMoves = oldPath.numMoves;
		this.location1 = oldPath.location1;
		this.location2 = oldPath.location2;
		this.increasePerTurn = oldPath.increasePerTurn;
		this.total = oldPath.total;

		// Predictive values used for pruning
		this.prognosis = prognosis();
		this.maxValue = maxPossible();
		if (maxValue < prognosis) {
			System.out.println(this);

			System.out.println("This cannot happen!");
		}
	}

	@Override
	public String toString() {
		String s = numMoves + " moves, at " + location1 + "/" + location2 + ": per turn " + increasePerTurn + " total "
				+ total;
		s += " (max " + maxValue + ", prog " + prognosis + " highest " + highestPrognosis + ")\n";
		for (String name : valves.keySet()) {
			Valve v = valves.get(name);
			Boolean b = valveStatus.get(name);
			if (v.getFlowRate() > 0) {
				s += "  " + name + " is " + (b ? "open" : "closed") + "\n";
			}
		}
		return s;
	}

	public void getNextPaths(PriorityQueue<Solution2> allPaths) {
		Valve valve1 = valves.get(location1);
		Valve valve2 = valves.get(location2);
		for (Valve tunnel1 : valve1.getTunnels()) {
			for (Valve tunnel2 : valve2.getTunnels()) {
				Solution2 p = new Solution2(this);
				p.location1 = tunnel1.getName();
				p.location2 = tunnel2.getName();
				p.numMoves += 1;
				p.total += p.increasePerTurn;
				addNewPath(p, allPaths);
				
				if (!valveStatus.get(p.location1) && valves.get(p.location1).getFlowRate() > 0 && p.numMoves < timeLimit) {
					Solution2 p2 = new Solution2(p);
					p2.numMoves += 1;
					p2.total += p2.increasePerTurn;
					p2.increasePerTurn += valves.get(p2.location1).getFlowRate();
					p2.valveStatus.put(p2.location1, true);
					addNewPath(p2, allPaths);
				}
				
				if (!valveStatus.get(p.location2) && valves.get(p.location2).getFlowRate() > 0 && p.numMoves < timeLimit) {
					Solution2 p3 = new Solution2(p);
					p3.numMoves += 1;
					p3.total += p3.increasePerTurn;
					p3.increasePerTurn += valves.get(p3.location2).getFlowRate();
					p3.valveStatus.put(p3.location2, true);
					addNewPath(p3, allPaths);
				}
				
				if (!valveStatus.get(p.location1) && valves.get(p.location1).getFlowRate() > 0 && !valveStatus.get(p.location2) && valves.get(p.location2).getFlowRate() > 0 && p.numMoves < timeLimit) {
					Solution2 p4 = new Solution2(p);
					p4.numMoves += 1;
					p4.total += p4.increasePerTurn;
					p4.increasePerTurn += valves.get(p4.location1).getFlowRate() + valves.get(p4.location2).getFlowRate();
					p4.valveStatus.put(p4.location1, true);
					p4.valveStatus.put(p4.location2, true);
					addNewPath(p4, allPaths);
				}
			}
		}
	}
	
	private void addNewPath(Solution2 s, PriorityQueue<Solution2> allPaths) {
		if (s.maxValue >= highestPrognosis) {
				allPaths.add(s);
		}
	}

	private int prognosis() {
		int prognosis = total + (timeLimit - numMoves) * increasePerTurn;
		if (prognosis > highestPrognosis) highestPrognosis = prognosis;
		return prognosis;
	}

	// Look at the remaining open valves, and calculate (generously) the
	// maximum value that this solution could ever reach
	private int maxPossible() {
		ArrayList<Integer> values = new ArrayList<>();
		for (String name : valves.keySet()) {
			if (!valveStatus.get(name)) {
				Valve v = valves.get(name);
				if (v.getFlowRate() > 0) {
					values.add(v.getFlowRate());
				}
			}
		}
		Collections.sort(values);

		int flow = increasePerTurn;
		int maxValue = total;
		for (int t = 1; t <= (timeLimit - numMoves); t++) {
			maxValue += flow;
			// Twice, because we have the elephant
			if (t % 2 == 0 && values.size() > 0)
				flow += values.remove(values.size()-1);
			if (t % 2 == 0 && values.size() > 0)				
				flow += values.remove(values.size()-1);
		}
		return maxValue;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass() == this.getClass()) {
			Solution2 s = (Solution2) o;
			return this.prognosis == s.prognosis;
		}
		return false;
	}
	
	// Deliberately reversed, because these elements are stored in a PriorityQueue where smaller is better
	@Override
	public int compareTo(Solution2 s) {
		return Integer.compare(s.prognosis, prognosis);
	}
}
