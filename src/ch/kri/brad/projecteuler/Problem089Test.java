package ch.kri.brad.projecteuler;

import static org.junit.Assert.*;

import org.junit.Test;

public class Problem089Test {

	@Test
	public void test() {
		assertEquals(Problem089.intToRoman(1), "I");
		assertEquals(Problem089.intToRoman(2), "II");
		assertEquals(Problem089.intToRoman(3), "III");
		assertEquals(Problem089.intToRoman(4), "IV");
		assertEquals(Problem089.intToRoman(5), "V");
		assertEquals(Problem089.intToRoman(6), "VI");
		assertEquals(Problem089.intToRoman(7), "VII");
		assertEquals(Problem089.intToRoman(8), "VIII");
		assertEquals(Problem089.intToRoman(9), "IX");
		assertEquals(Problem089.intToRoman(10), "X");
		assertEquals(Problem089.intToRoman(16), "XVI");
		assertEquals(Problem089.intToRoman(19), "XIX");
		assertEquals(Problem089.intToRoman(1606), "MDCVI");
	}

	@Test
	public void testRomanToInt() {
		assertEquals(Problem089.romanToInt("I"), 1);
		assertEquals(Problem089.romanToInt("II"), 2);
		assertEquals(Problem089.romanToInt("III"), 3);
		assertEquals(Problem089.romanToInt("IV"), 4);
		assertEquals(Problem089.romanToInt("V"), 5);
		assertEquals(Problem089.romanToInt("VI"), 6);
		assertEquals(Problem089.romanToInt("VII"), 7);
		assertEquals(Problem089.romanToInt("VIII"), 8);
		assertEquals(Problem089.romanToInt("IX"), 9);
		assertEquals(Problem089.romanToInt("X"), 10);
		assertEquals(Problem089.romanToInt("XVI"), 16);
		assertEquals(Problem089.romanToInt("XIX"), 19);
		assertEquals(Problem089.romanToInt("MDCVI"), 1606);
	}

}
