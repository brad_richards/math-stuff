package AdventOfCode.aoc_2024.day_05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	private static record Pair(int before, int after) {};
	private static List<Pair> pageRules = new ArrayList<Pair>();
	private static List<List<Integer>> updates = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		readInput();

		// Get the page ordering rules
		int cursor = 0;
		String line = lines.get(cursor++);
		while (!line.isEmpty()) {
			String[] parts = line.split("\\|");
			pageRules.add(new Pair(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])));
			line = lines.get(cursor++);
		}

		// Get the update lists
		while (cursor < lines.size()) {
			line = lines.get(cursor++);
			List<Integer> pages = Arrays.stream(line.split(",")).map(Integer::parseInt).toList();
			updates.add(pages);
		}

		// Part 1: Just check the update lists, and add up the results
		int sum1 = 0;
		int sum2 = 0;
		for (List<Integer> update : updates) {
			int valCorrect = correctOrder(update);
			if (valCorrect != 0)
				sum1 += valCorrect;
			else
				sum2 += fixUpdate(update);
		}
		System.out.println("Answer to part 1: " + sum1);
		System.out.println("Answer to part 2: " + sum2);
	}

	// Correct the page order in an update. For each entry, we count the number
	// of rules where this entry comes before other entries in the update.
	// Assuming the data are correct, these values shoudl all be different.
	// Hence, a nice TreeMap.
	private static int fixUpdate(List<Integer> update) {
		TreeMap<Integer, Integer> counts = new TreeMap<>();
		for (int i = 0; i < update.size(); i++) {
			int beforeVal = update.get(i);
			int count = 0;
			for (int j = 0; j < update.size(); j++) {
				if (i != j) {
					int afterVal = update.get(j);
					Pair requiredPair = new Pair(beforeVal, afterVal);
					if (pageRules.contains(requiredPair)) count++;
				}
			}
			counts.put(count, beforeVal);
		}

		// Actually, the update is in the reverse order, but we don't care.
		// We just want the middle value.
		List<Integer> newUpdate = new ArrayList<>();
		for (Integer value : counts.values()) newUpdate.add(value);
		return newUpdate.get(newUpdate.size()/2);
	}

	// Check whether an update is in the correct order.
	// - If so, return the value of the middle page
	// - If not, return 0;
	private static int correctOrder(List<Integer> update) {
		boolean allOk = true;
		for (int i = 0; allOk && i < update.size()-1; i++) {
			for (int j = i+1; allOk && j < update.size(); j++) {
				Pair requiredPair = new Pair(update.get(i), update.get(j));
				if (!pageRules.contains(requiredPair)) allOk = false;
			}
		}

		if (allOk) {
			return update.get(update.size()/2);
		} else {
			return 0;
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}