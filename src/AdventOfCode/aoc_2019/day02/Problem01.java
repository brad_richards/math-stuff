package AdventOfCode.aoc_2019.day02;

import java.io.InputStream;
import java.util.Scanner;

import AdventOfCode.aoc_2019.IntCodeComputer.IntCodeComputer;

public class Problem01 {
	private static int[] memory = {1,9,10,3,2,3,11,0,99,30,40,50};

	public static void main(String[] args) {
		// read memory
		readInput();
		IntCodeComputer computer = new IntCodeComputer(memory);

		// execute program
		computer.executeProgram();
		
		// Print results
		for (int i = 0; i < memory.length; i++) System.out.print(computer.getMemoryLocation(i) + ",");
	}

	private static void readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		String line;
		try (Scanner in = new Scanner(fis) ) {
			line = in.nextLine();
		}
		String[] parts = line.split(",");
		memory = new int[parts.length];
		for (int i = 0; i < memory.length; i++) memory[i] = Integer.parseInt(parts[i]);
		
		memory[1] = 12; memory[2] = 2;
	}
}
