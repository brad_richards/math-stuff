package AdventOfCode.aoc_2023.day_03;

import java.io.IOException;

public class Problem2 {

	public static void main(String[] args) throws IOException {
		InputParser.readInput();

		// Find all * symbols with exactly two adjacent values
		long sum = 0;
		for (Symbol symbol : InputParser.symbols) {
			if (symbol.symbol == '*' && symbol.values.size() == 2) {
				sum += symbol.values.get(0).value * symbol.values.get(1).value;
			}
		}
		
		System.out.println(sum);
	}

}
