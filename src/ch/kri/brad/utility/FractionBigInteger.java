package ch.kri.brad.utility;

import java.math.BigInteger;

public class FractionBigInteger extends Number implements Comparable<FractionBigInteger> {
	private BigInteger numerator;
	private BigInteger denominator;
	
	public FractionBigInteger(BigInteger numerator, BigInteger denominator) {
		BigInteger gcd = gcd(numerator, denominator);
		this.numerator = numerator.divide(gcd);
		this.denominator = denominator.divide(gcd);
	}

	public FractionBigInteger(long numerator, long denominator) {
		long gcd = gcd(numerator, denominator);
		this.numerator = BigInteger.valueOf(numerator / gcd);
		this.denominator = BigInteger.valueOf(denominator/gcd);
	}
	
	public FractionBigInteger inverse() {
		return new FractionBigInteger(denominator, numerator);
	}
	
	public FractionBigInteger add(long intValue) {
		return new FractionBigInteger(numerator.add(denominator.multiply(BigInteger.valueOf(intValue))), denominator);
	}

	public BigInteger getNumerator() {
		return numerator;
	}

	public BigInteger getDenominator() {
		return denominator;
	}

	@Override
	public int intValue() {
		return (int) longValue();
	}

	@Override
	public long longValue() {
		return numerator.longValue() / denominator.longValue();
	}

	@Override
	public float floatValue() {
		return (float) doubleValue();
	}

	@Override
	public double doubleValue() {		
		return numerator.doubleValue() / denominator.doubleValue();
	}

	@Override
	public int compareTo(FractionBigInteger o) {
		Double thisDouble = this.doubleValue();
		Double otherDouble = o.doubleValue();
		return thisDouble.compareTo(otherDouble);
	}
	
	@Override
	public String toString() {
		return numerator.toString() + "/" + denominator.toString();
	}
	
	/**
	 * Adaptation of Euclid's GCD algorithm; num1 and num2 must both be non-zero
	 */
	private BigInteger gcd(BigInteger num1, BigInteger num2) {
		BigInteger tmp;
		if (num2.compareTo(num1) > 0) {
			tmp = num2;
			num2 = num1;
			num1 = tmp;
		}
		while (!num2.equals(BigInteger.ZERO)) {
			tmp = num1.mod(num2);
			num1 = num2;
			num2 = tmp;
		}
		return num1;
	}
	
	/**
	 * Adaptation of Euclid's GCD algorithm; num1 and num2 must both be non-zero
	 */
	private long gcd(long num1, long num2) {
		long tmp;
		if (num2 > num1) {
			tmp = num2;
			num2 = num1;
			num1 = tmp;
		}
		while (num2 != 0) {
			tmp = num1 % num2;
			num1 = num2;
			num2 = tmp;
		}
		return num1;
	}
}
