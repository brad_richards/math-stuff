package ch.kri.brad.misc;

import java.util.ArrayList;

/**
 * Discover the number of possible combinations on the Android 9-point-unlock method. We associate each point with an
 * integer from 0-8, numbering right-to-left and then top-to-bottom.
 * 
 * We only calculate the possibilities for one corner, one side and the middle. To get the final result, we multiply the
 * corner and side results by 4.
 * 
 * @author brad
 * 
 */
public class NinePointCombinations {

	// For each node, list the nodes that cannot be reached, and the node that is between them
	static int[][][] disallowedTransitions = { { { 2, 1 }, { 6, 3 }, { 8, 4 } }, { { 7, 4 } },
			{ { 0, 1 }, { 6, 4 }, { 8, 5 } }, { { 5, 4 } }, {}, { { 3, 4 } }, { { 0, 3 }, { 2, 4 }, { 8, 7 } },
			{ { 1, 4 } }, { { 0, 4 }, { 2, 5 }, { 6, 7 } } };

	public static void main(String[] args) {

		Integer[] tmpSolution = { 0 }; // corner
		int cornerSolutions = 4 * getSolutions(tmpSolution);

		tmpSolution[0] = 1; // side
		int sideSolutions = 4 * getSolutions(tmpSolution);

		tmpSolution[0] = 4; // middle
		int middleSolutions = getSolutions(tmpSolution);

		int totalSolutions = cornerSolutions + sideSolutions + middleSolutions;

		System.out.println("Total solutions = " + totalSolutions);
	}

	/**
	 * Given a starting dot, count the total number of solutions that can be created
	 * 
	 * @parm startingSolution an Integer array containing the starting position
	 * @return the number of solutions starting from this position
	 */
	private static int getSolutions(Integer[] startingSolution) {
		ArrayList<Integer[]>[] solutionsByLength = new ArrayList[9];

		// An initial ArrayList containing only the startingSolution
		solutionsByLength[0] = new ArrayList<Integer[]>();
		solutionsByLength[0].add(startingSolution);

		// Now extend the solution 8 times, saving each result in solutionsByLength
		for (int i = 0; i < 8; i++) {
			solutionsByLength[i + 1] = extendSolutions(solutionsByLength[i]);
		}

		// Add up the total number of solutions of lengths 4 to 9
		int totalSolutions = 0;
		for (int i = 3; i < 9; i++)
			totalSolutions += solutionsByLength[i].size();

		// That's our answer!
		return totalSolutions;
	}

	private static ArrayList<Integer[]> extendSolutions(ArrayList<Integer[]> shortSolutions) {
		ArrayList<Integer[]> longSolutions = new ArrayList<Integer[]>();
		int longSolutionLength = shortSolutions.get(0).length + 1;

		// Extend each short solution individually
		for (Integer[] shortSolution : shortSolutions) {
			boolean[] nextNodes = getNextNodes(shortSolution); // Each entry is true if the corresponding node is
																// possible
			for (int i = 0; i < 9; i++) {
				if (nextNodes[i]) {
					Integer[] longSolution = new Integer[longSolutionLength];
					for (int j = 0; j < shortSolution.length; j++) {
						longSolution[j] = shortSolution[j];
					}
					longSolution[longSolutionLength - 1] = i;
					longSolutions.add(longSolution);
				}
			}
		}
		return longSolutions;
	}

	/**
	 * For each node that can be moved to from the current position, the corresponding boolean value is true. For nodes
	 * that are unreachable, or that have already been used, the value is false
	 * 
	 * @param shortSolution
	 * @return
	 */
	private static boolean[] getNextNodes(Integer[] shortSolution) {
		boolean[] nodes = new boolean[9];

		// Initially all nodes are possible
		for (int i = 0; i < 9; i++)
			nodes[i] = true;

		// Eliminate nodes that have been used
		for (Integer i : shortSolution)
			nodes[i] = false;

		// Eliminate unreachable nodes. Note that nodes may have become reachable if the intervening node has been used
		// (is false, in our array)
		int lastNode = shortSolution[shortSolution.length - 1];
		for (int[] pairInfo : disallowedTransitions[lastNode]) {
			int unreachableNode = pairInfo[0];
			int intermediateNode = pairInfo[1];
			if (nodes[intermediateNode]) nodes[unreachableNode] = false;
		}

		return nodes;
	}
}
