package AdventOfCode.aoc_2021.day04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	private static List<String> lines;
	private static ArrayList<Integer> numberSequence;
	private static ArrayList<Board> boards;

	public static void main(String[] args) throws IOException {
		readInput();
		numberSequence = parseNumberSequence(lines.get(0));
		boards = parseBoards(lines);
		
		int score = 0;
		Board lastBoard = null;
		for (int numPos = 0; numPos < numberSequence.size() && boards.size() > 0; numPos++) {
			int number = numberSequence.get(numPos);
			for (int boardPos = 0; boardPos < boards.size(); boardPos++) {
				Board board = boards.get(boardPos);
				board.mark(number);
				if (board.hasWon()) {
					lastBoard = boards.remove(boardPos);
					boardPos--;
				}
			}
		}
		
		System.out.println(lastBoard.getScore());
	}
	
	private static ArrayList<Integer> parseNumberSequence(String in) {
		String[] parts = in.split(",");
		ArrayList<Integer> list = new ArrayList<>();
		for (String part : parts) list.add(Integer.parseInt(part));
		return list;
	}
	
	private static ArrayList<Board> parseBoards(List<String> lines) {
		ArrayList<Board> boards = new ArrayList<>();
		int pos = 2;
		while (lines.size() >= (pos + 4)) {
			boards.add(new Board(lines, pos));
			pos += 6;
		}
		return boards;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
