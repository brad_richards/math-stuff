package AdventOfCode.aoc_2020.day_07;

import java.util.List;
import java.util.HashMap;

public class BagCollection extends HashMap<String, Bag> {

	public BagCollection(List<String> lines) {
		super();

		// identify all bag types by looking at the start of the lines
		createBagTypes(lines);

		// now, fill in bag contents
		createBagContents(lines);
	}

	public boolean contains(Bag bag, Bag target) {
		if (bag.getContents().contains(target))
			return true;
		else {
			boolean result = false;
			for (Bag b : bag.getContents()) {
				if (this.contains(b, target)) {
					result = true;
					break;
				}
			}
			return result;
		}
	}

	private void createBagTypes(List<String> lines) {
		for (String line : lines) {
			String[] parts = line.split("bags contain");
			String color = parts[0].trim();
			this.put(color, new Bag(color));
		}
	}

	private void createBagContents(List<String> lines) {
		for (String line : lines) {
			String[] parts = line.split("bags contain");
			Bag bag = this.get(parts[0].trim());
			if (!parts[1].contains("no other bags")) {
				String[] bagTypes = parts[1].split(",");
				for (String bagType : bagTypes) {
					String[] bagDesc = bagType.trim().split(" ");
					int number = Integer.parseInt(bagDesc[0]);
					String color = bagDesc[1] + " " + bagDesc[2];
					Bag inBag = this.get(color);
					for (int i = 0; i < number; i++)
						bag.addContents(inBag);
				}
			}
		}
	}
}
