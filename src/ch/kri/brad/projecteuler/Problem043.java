package ch.kri.brad.projecteuler;
import java.util.Arrays;

import ch.kri.brad.utility.Digits;


public class Problem043 {

	private static long sum = 0;

	public static void main(String[] args) {
		int[] validDigits = new int[10];
		for (int i = 0; i <= 9; i++)
			validDigits[i] = 9 - i;
		generateAndTest(validDigits, new int[10], 0);
		System.out.println(sum);
	}

	/**
	 * Adapted from Problem041
	 * 
	 * @param validDigits
	 * @param digits
	 * @param nextDigit
	 */
	static void generateAndTest(int[] validDigits, int[] digits, int nextDigit) {
		if (nextDigit == validDigits.length) { // base case; all digits placed
			if (testNumber(digits)) {
				long number = Digits.fromDigits(digits);
				sum += number;
			}
		} else {
			for (int useDigit : validDigits) {
				if (useDigit > -1) {
					// Get the next possible digit to use, and remove it from a copy of
					// the valid-digits
					int[] restValidDigits = Arrays.copyOf(validDigits, validDigits.length);
					restValidDigits[validDigits.length - useDigit - 1] = -1;

					// Put this digit into a copy of our number, and recurse
					int[] newDigits = Arrays.copyOf(digits, digits.length);
					newDigits[nextDigit] = useDigit;
					generateAndTest(restValidDigits, newDigits, nextDigit + 1);
				}
			}
		}
	}

	static boolean testNumber(int[] digits) {
		boolean testOk = true;

		// Test 1
		int value = digits[1] * 100 + digits[2] * 10 + digits[3];
		if (value % 2 != 0) testOk = false;

		// Test 2
		value = digits[2] * 100 + digits[3] * 10 + digits[4];
		if (value % 3 != 0) testOk = false;

		// Test 3
		value = digits[3] * 100 + digits[4] * 10 + digits[5];
		if (value % 5 != 0) testOk = false;

		// Test 4
		value = digits[4] * 100 + digits[5] * 10 + digits[6];
		if (value % 7 != 0) testOk = false;

		// Test 5
		value = digits[5] * 100 + digits[6] * 10 + digits[7];
		if (value % 11 != 0) testOk = false;

		// Test 6
		value = digits[6] * 100 + digits[7] * 10 + digits[8];
		if (value % 13 != 0) testOk = false;

		// Test 7
		value = digits[7] * 100 + digits[8] * 10 + digits[9];
		if (value % 17 != 0) testOk = false;

		return testOk;
	}
}
