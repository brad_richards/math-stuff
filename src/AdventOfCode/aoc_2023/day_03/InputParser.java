package AdventOfCode.aoc_2023.day_03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputParser {
	// Structures used for the input parsing
	static List<Value> values = new ArrayList<>();
	static List<Symbol> symbols = new ArrayList<>();
	static List<Symbol> gears = new ArrayList<>();
	
	/**
	 * Parse all numeric values, noting their value and their leftmost position
	 * Parse all symbols into an array, noting their position
	 * Finally, mark numbers that are next to a symbol
	 */
	public static void readInput() throws IOException {
		List<String> lines;
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());

			// Parse out all numbers and symbols
			for (int y = 0; y < lines.size(); y++) {
				String line = lines.get(y);
				char[] chars = line.toCharArray();
				boolean inValue = false;
				Value value = null;
				for (int x = 0; x < chars.length; x++) {
					char c = chars[x];
					if (c >= '0' && c <= '9') {
						if (inValue) {
							value.value = value.value * 10 +  (c - '0');
						} else {
							value = new Value(x, y, (c - '0'));
							inValue = true;
						}
					} else {
						if (inValue) { // Finish off the value
							values.add(value);
							inValue = false;
							value = null;
						}
						if (c != '.') {
							symbols.add(new Symbol(x, y, c));
						}
					}
				}
				if (inValue) values.add(value);
			}
			
			// For each symbol, mark the numbers that it is next to
			for (Value value : values) {
				for (Symbol symbol : symbols) {
					if (isAdjacent(symbol, value)) {
						value.symbol = true;
						symbol.addValue(value);
					}
				}
			}
		}
	}
	
	private static boolean isAdjacent(Symbol symbol, Value value) {
		boolean adjacent = false;
		if (Math.abs(symbol.y - value.y) <= 1) {
			adjacent = (symbol.x >= (value.x - 1) && symbol.x <= (value.x + digits(value.value)));
		}
		return adjacent;
	}

	private static int digits(long value) {
		int digits = 1;
		while (value > 9) {
			value /= 10;
			digits++;
		}
		return digits;
	}
}
