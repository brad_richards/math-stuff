package AdventOfCode.aoc_2020.day_10;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Problem02 {
	private static List<Long> jolts;

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		jolts = ir.getValues();
		jolts.add((long) 0);
		Collections.sort(jolts);
		jolts.add(jolts.get(jolts.size() - 1) + 3);

		for (Long jolt : jolts)
			System.out.println(jolt);

		for (int i = 0; i < jolts.size() - 2; i++)
			if (jolts.get(i + 2) - jolts.get(i) > 3) System.out.println("Segment at position " + (i + 1));

		long combinations = 1;
		int segStart = 0;
		do {
			int segEnd = getNextSeg(segStart);
			combinations *= getCombinations(segStart, segEnd);
			segStart = segEnd;
		} while (segStart < (jolts.size() - 1));

		System.out.println("total combinations: " + combinations);
	}

	/**
	 * Locate the next position that cannot be deleted, because it separates
	 * elements with a difference greater than 3
	 */
	private static int getNextSeg(int segStart) {
		int pos = segStart + 2;
		while (pos <= jolts.size() - 1 && jolts.get(pos) - jolts.get(pos - 2) <= 3)
			pos++;
		return (pos - 1);
	}

	/**
	 * Within the defined segment (where we must retain the start and end
	 * elements), calculate the number of combinations. Segments are always
	 * short, so we just do this with brute force.
	 */
	private static long getCombinations(int segStart, int segEnd) {
		long permsFound = 0;
		if (segEnd - segStart == 1) {
			permsFound = 1;
		} else {
			int numElements = segEnd - segStart - 1;
			int numPermutations = (int) Math.pow(2, numElements);
			for (int perm = 0; perm < numPermutations; perm++) {
				if (permPasses(segStart, segEnd, perm)) permsFound++;
			}
		}
		return permsFound;
	}

	/**
	 * Given a segment, and a permuation value (0 bit means omit value), test to see
	 * if all continuous values are within 3.
	 */
	private static boolean permPasses(int segStart, int segEnd, int perm) {
		boolean passes = true;
		long currentValue = jolts.get(segStart);
		int cursor = segStart+1;
		do {
			if (cursor == segEnd || bitSet(perm, cursor - (segStart+1))) {
				passes = jolts.get(cursor) - currentValue <= 3;
				currentValue = jolts.get(cursor);
			}
			cursor++;
		} while (passes && cursor <= segEnd);
		return passes;
	}

	public static boolean bitSet(int value, int bit) {
		while (bit != 0) {
			value /= 2;
			bit--;
		}
		return value % 2 == 1;
	}
}

