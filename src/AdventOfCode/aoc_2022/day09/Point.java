package AdventOfCode.aoc_2022.day09;

public class Point implements Comparable<Point> {
	private final int X;
	private final int Y;

	public Point(int X, int Y) {
		this.X = X;
		this.Y = Y;
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass().equals(this.getClass())) {
			Point p = (Point) o;
			return p.X == X && p.Y == Y;
		}
		return false;
	}

	@Override
	public int compareTo(Point p) {
		if (p.X != X)
			return X - p.X;
		else
			return Y - p.Y;
	}

	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}
	
	@Override
	public String toString() {
		return String.format("(%d,%d)", X,Y);
	}
}
