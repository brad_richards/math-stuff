package AdventOfCode.aoc_2018.day03;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * General approach to the problem: read all claims, then create a 1000x1000 map for the fabric.
 * Each entry in this map has an integer value: 0 if unclaimed, a positive value indicating a
 * specific claim, or a negative value indicating the number of claims on that bit of fabric.
 * 
 * For simplicity, we rely on the fact that (a) claims begin with ID=1, and (b) claim IDs are
 * contiguous
 * 
 * To solve part 1, we only need to count the number of squares with negative values.
 * 
 * To solve part 2, we also maintain a boolean array, indexed by claim ID minus one, that tracks whether or
 * not a claim has an overlap
 */
public class Day03 {
	private static ArrayList<Claim> claims = new ArrayList<>();
	private static int[][] fabric = new int[1000][1000];
	private static boolean[] claimOverlaps;

	public static void main(String[] args) {
		readClaims();
		claimOverlaps = new boolean[claims.size()];

		mapClaims();
		int count = countNegatives();
		System.out.println("Part 1: " + count + " squares are claimed more than once");

		for (int i = 0; i < claimOverlaps.length; i++) {
			if (!claimOverlaps[i]) System.out.println("Part 2: No overlap for claim " + (i+1));
		}
	}

	/**
	 * Parse the claims from the input file
	 */
	private static void readClaims() {
		InputStream fis = Day03.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String value = in.nextLine();
				Claim claim = Claim.parseClaim(value);
				if (claim != null)
					claims.add(claim);
				else
					System.out.println("Parsing error on: '" + value + "'");
			}
		}
		System.out.println(claims.size() + " claims read");
	}

	/**
	 * Map each claim onto the fabric
	 */
	private static void mapClaims() {
		for (Claim claim : claims) {
			for (int x = claim.getX(); x < claim.getX() + claim.getWidth(); x++) {
				for (int y = claim.getY(); y < claim.getY() + claim.getHeight(); y++) {
					if (fabric[y][x] == 0) {
						fabric[y][x] = claim.getId();
					} else if (fabric[y][x] > 0) {
						claimOverlaps[fabric[y][x]-1] = true;
						claimOverlaps[claim.getId()-1] = true;
						fabric[y][x] = -2;
					} else { // negative
						claimOverlaps[claim.getId()-1] = true;
						fabric[y][x]--;
					}
				}
			}
		}
	}

	/**
	 * Count the number of negative squares
	 */
	private static int countNegatives() {
		int count = 0;
		for (int y = 0; y < fabric.length; y++) {
			for (int x = 0; x < fabric[0].length; x++) {
				if (fabric[y][x] < 0) count++;
			}
		}
		return count;
	}
}
