package AdventOfCode.aoc_2015.day02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		long total = 0;
		for (String line : lines) {
			total += gift(line);
		}
		System.out.println(total);
	}
	
	private static long gift(String line) {
		String[] parts = line.split("x");
		ArrayList<Long> dimensions = new ArrayList<>();
		for (String part : parts) dimensions.add(Long.parseLong(part));
		Collections.sort(dimensions);
		return 3 * dimensions.get(0) * dimensions.get(1) + 2 * dimensions.get(0) * dimensions.get(2) + 2 * dimensions.get(1) * dimensions.get(2);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}