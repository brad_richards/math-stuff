package AdventOfCode.aoc_2021.day07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	private static List<Integer> numbers;
	private static int middlePosition;
	private static int leftFuel;
	private static int middleFuel;
	private static int rightFuel;

	public static void main(String[] args) throws IOException {
		readInput();

		// Sort the numbers, then find the *average*
		Collections.sort(numbers);
		long sum = 0;
		for (int number : numbers) sum += number;		
		middlePosition = (int) sum / numbers.size();
		
		// Calculate the fuel usage for the median, and the positions left and right.
		leftFuel = getFuelUsage(middlePosition-1);
		middleFuel = getFuelUsage(middlePosition);
		rightFuel = getFuelUsage(middlePosition+1);
		
		// While the media is not the smallest, shift by one and calculate the new, relevant position
		while (middleFuel > leftFuel || middleFuel > rightFuel) {
			if (middleFuel > leftFuel) {
				middlePosition--;
				rightFuel = middleFuel;
				middleFuel = leftFuel;
				leftFuel = getFuelUsage(middlePosition-1);
				System.out.println("Shifted left");
			} else {
				middlePosition++;
				leftFuel = middleFuel;
				middleFuel = rightFuel;
				rightFuel = getFuelUsage(middlePosition+1);
				System.out.println("Shifted right");
			}
		}

		System.out.println(middleFuel);
	}
	
	private static int getFuelUsage(int pos) {
		int fuel = 0;
		for (int number : numbers) {
			int n = Math.abs(pos-number);
			fuel += n * (n+1) / 2;
		}
		return fuel;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		List<String> lines;
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		numbers = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(",");
			for (String part : parts) numbers.add(Integer.parseInt(part));
		}
	}

}
