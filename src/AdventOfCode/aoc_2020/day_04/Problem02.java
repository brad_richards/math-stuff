package AdventOfCode.aoc_2020.day_04;

import java.io.IOException;
import java.util.ArrayList;

public class Problem02 {

	public static void main(String[] args) throws IOException {
		InputReader2 ir = new InputReader2("input.txt");
		ArrayList<Passport2> passports = ir.getPassports();
		
		int count = 0;
		for (Passport2 p : passports) {
			if (p.isValid()) count++;
		}
		System.out.println(count + " of " + passports.size() + " passports are valid");
	}

}
