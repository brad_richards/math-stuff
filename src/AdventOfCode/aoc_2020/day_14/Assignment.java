package AdventOfCode.aoc_2020.day_14;

public class Assignment extends Instruction {
	private final int address;
	private final long value;

	public Assignment(String address, String value) {
		this.address = Integer.parseInt(address);
		this.value = Long.parseLong(value);
	}

	public int getAddress() {
		return address;
	}

	public long getValue() {
		return value;
	}
	
}
