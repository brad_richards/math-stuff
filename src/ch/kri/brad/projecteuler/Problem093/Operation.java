package ch.kri.brad.projecteuler.Problem093;

public enum Operation {
	PLUS, MINUS, TIMES, DIVIDE;

	private static Operation[][] operationPermuations = null;

	public float apply(float x, float y) {
		switch (this) {
		case PLUS:
			return x + y;
		case MINUS:
			return x - y;
		case TIMES:
			return x * y;
		case DIVIDE:
			return x / y;
		default:
			return 0; // dead code, but required
		}
	}

	public static Operation[][] getOperationPermutations() {
		if (operationPermuations == null) {
			Operation[][] operationPerms = new Operation[64][];
			int pos = 0;
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					for (int k = 0; k < 4; k++) {
							Operation[] operationPerm = new Operation[3];
							operationPerm[0] = Operation.values()[i];
							operationPerm[1] = Operation.values()[j];
							operationPerm[2] = Operation.values()[k];
							operationPerms[pos++] = operationPerm;
					}
				}
			}
			operationPermuations = operationPerms;
		}
		return operationPermuations;
	}
}