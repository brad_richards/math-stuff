package AdventOfCode.aoc_2020.day_15;

import java.util.List;

/**
 * For part 1, we can certainly use an array. We'll hope this also works for part 2 :-)
 */
public class NumberList1 {
	int[] numbers;
	int nextPos = 0; // Next position in array to fill
	
	public NumberList1(int size, List<Integer> initialNumbers) {
		numbers = new int[size];
		for (int number : initialNumbers) {
			numbers[nextPos++] = number;
		}
	}
	
	public void fillNumbers() {
		while (nextPos < numbers.length) {
			numbers[nextPos] = getNextNumber();
			nextPos++;
		}
	}
	
	/**
	 * Return the previous position where the most recent number was used.
	 */
	public int getNextNumber() {
		int cursor = nextPos - 2;
		int prevPos = nextPos-1;
		while (cursor >= 0 && prevPos == (nextPos-1)) {
			if (numbers[cursor] == numbers[nextPos-1]) {
				prevPos = cursor;
			} else {
				cursor--;
			}
		}
		return nextPos - prevPos - 1;
	}
	
	public int getByPos(int pos) {
		return numbers[pos];
	}
}
