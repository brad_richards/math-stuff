package AdventOfCode.aoc_2023.day_08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	// Directions - for the moment, at least, just the original string
	static String directions;
	
	// Maintain the list of nodes in a HashMap
	static final HashMap<String, Node> nodes = new HashMap<>();

	public static void main(String[] args) throws Exception {
		readInput();
		
		int count = 0;
		String current = "AAA";
		while (!current.equals("ZZZ")) {
			char direction = directions.charAt(count % directions.length());
			Node node = nodes.get(current);
			if (direction == 'R')
				current = node.right;
			else
				current = node.left;
			count++;				
		}
		System.out.println(count);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lines = stream.collect(Collectors.toList());
			
			directions = lines.get(0);
			for (int i = 2; i < lines.size(); i++) {
				Node node = new Node(lines.get(i));
				nodes.put(node.name, node);
			}
		}
	}
}