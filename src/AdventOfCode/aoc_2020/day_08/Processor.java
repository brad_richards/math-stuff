package AdventOfCode.aoc_2020.day_08;

import java.util.List;
import AdventOfCode.aoc_2020.day_08.Instruction.InstructionType;

public class Processor {
	private List<Instruction> program;
	private int pos = 0;
	private int accumulator = 0;

	public Processor(List<Instruction> program) {
		this.program = program;
	}

	/** for problem 1 */
	public int executeUntilLoop() {
		Instruction i = program.get(pos);
		while (!i.isExecuted()) {
			execute(i);
			i = program.get(pos);
		}
		return accumulator;
	}

	/**
	 * for problem 2 - returns true, if the program
	 * would continue with the next instruction after
	 * the given program.
	 */
	public boolean execute() {
		reset();
		Instruction i = program.get(pos);
		while (!i.isExecuted() && pos < program.size()) {
			execute(i);
			if (pos < program.size()) i = program.get(pos);
		}
		return (pos == program.size());
	}

	private void execute(Instruction i) {
		if (i.getType() == InstructionType.ACC) {
			accumulator += i.getArg();
			pos++;
		} else if (i.getType() == InstructionType.JMP) {
			pos += i.getArg();
		} else { // NOP
			pos++;
		}
		i.setExecuted(true);
	}
	
	private void reset() {
		for (Instruction i : program) i.setExecuted(false);
		pos = 0;
		accumulator = 0;
	}
	
	public int getAccumulator() {
		return accumulator;
	}
}
