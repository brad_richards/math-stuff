package LeetCode.median_of_two_sorted_arrays;

// This is fundamentally just the "merge" part of merge-sort
class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();

        int[] a = {1, 3};
        int[] b = {2};
        double answer  = s.findMedianSortedArrays(a, b);
    }

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] merged = new int[nums1.length + nums2.length];

        int m1 = 0;
        int n1 = 0;
        int n2 = 0;
        while (n1 < nums1.length && n2 < nums2.length) {
            merged[m1++] = (nums1[n1] <= nums2[n2] ? nums1[n1++] : nums2[n2++]);
        }
        // One of the arrays will still have elements left
        if (n1 < nums1.length) {
            for (int i = n1; i < nums1.length; i++) {
                merged[m1++] = nums1[i];
            }
        } else {
            for (int i = n2; i < nums2.length; i++) {
                merged[m1++] = nums2[i];
            }
        }

        if (merged.length % 2 == 1)
            return merged[merged.length/2];
        else
            return (merged[merged.length/2] + merged[merged.length/2 - 1]) / 2.0;
    }
}
