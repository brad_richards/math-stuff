package LeetCode.longest_palindromic_substring;

// For each position in the list
//   For each position in the list where that letter recurs
//     Test if those are the ends of a palindrome
public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.longestPalindrome("babad"));
        System.out.println(s.longestPalindrome("cbbd"));
        System.out.println(s.longestPalindrome("cbb"));
        System.out.println(s.longestPalindrome("radar"));
        System.out.println(s.longestPalindrome("asdf"));
    }

    public String longestPalindrome(String s) {
        char[] chars = s.toCharArray();
        int position = 0;
        int longest = 1;
        for (int i = 0; i < s.length()-longest; i++) {
            for (int j = i+1; j < s.length(); j++) {
                if (chars[i] == chars[j]) {
                    if ((j - i + 1) > longest && isPalindrome(chars, i, j)) {
                        position = i;
                        longest = j - i + 1;
                    }
                }
            }
        }
        return s.substring(position,position+longest);
    }

    private boolean isPalindrome(char[] chars, int start, int end) {
        while (start < end) {
            if (chars[start] != chars[end]) {
                return false;
            } else {
                start++;
                end--;
            }
        }
        return true;
    }
}
