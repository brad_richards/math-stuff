package AdventOfCode.aoc_2021.day19;

import java.util.Arrays;

public class Distance implements Comparable<Distance> {
	public int[] offsets = new int[3];
	public long totalDistance;
	
	public Distance(int d1, int d2, int d3) {
		offsets[0] = d1;
		offsets[1] = d2;
		offsets[2] = d3;
		
		totalDistance = offsets[0] * offsets[0] + offsets[1] * offsets[1] + offsets[2] * offsets[2];
		
		Arrays.sort(offsets);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Distance d = (Distance) o;
		return Arrays.equals(this.offsets, d.offsets);
	}
	
	@Override
	public int compareTo(Distance d) {
//		double thisSquare = Math.sqrt(offsets[0] * offsets[0] + offsets[1] * offsets[1] + offsets[2] * offsets[2]);
//		double otherSquare = Math.sqrt(d.offsets[0] * d.offsets[0] + d.offsets[1] * d.offsets[1] + d.offsets[2] * d.offsets[2]);
//		if (thisSquare < otherSquare) return -1;
//		else if (thisSquare > otherSquare) return 1;
//		else return 0;
		return (int) (this.totalDistance - d.totalDistance);
	}
	
	@Override
	public String toString() {
		return "(" + offsets[0] + "," + offsets[1] + "," + offsets[2] + ") = " + totalDistance;
	}
}
