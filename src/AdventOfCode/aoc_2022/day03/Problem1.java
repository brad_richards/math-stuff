package AdventOfCode.aoc_2022.day03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	
	
	public static void main(String[] args) throws Exception {
		readInput();
		long answer = 0;

		for (String line : lines) {
			char c = findCommon(line);
			long priority;
			if (Character.isLowerCase(c))
				priority = (c + 1 - 'a');
			else
				priority = (c + 27 - 'A');
			answer += priority;
		}
		
		System.out.println(answer);
	}
	
	private static char findCommon(String in) {
		String left = in.substring(0, in.length()/2);
		char[] leftChars = left.toCharArray();
		String right = in.substring(in.length()/2);
		char[] rightChars = right.toCharArray();
		
		// We assume that there is exactly one common characters
		for (char c : leftChars) {
			for (char d : rightChars) {
				if (c == d) return c;
			}
		}
		return ' ';
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
