package AdventOfCode.aoc_2023.day_01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int sum = 0;
		for (String line : lines) {
			sum += lineValue(line);
		}
		System.out.println(sum);
	}
	
	private static int lineValue(String in) {
		int firstDigit = -1;
		int lastDigit = -1;
		for (int i = 0; i < in.length(); i++) {
			// Check for first digit
			if (firstDigit == -1) {
				// As a digit
				if (in.charAt(i) >= '0' && in.charAt(i) <= '9') firstDigit = in.charAt(i) - '0';
				else firstDigit = digitSubstring(in, i);
			}

			// Check for last digit
			int j = in.length() - i - 1;
			if (lastDigit == -1) {
				// As a digit
				if (in.charAt(j) >= '0' && in.charAt(j) <= '9') lastDigit = in.charAt(j) - '0';
				else lastDigit = digitSubstring(in, j);
			}
		}
		return firstDigit * 10 + lastDigit;
	}
	
	// Return -1 if no matching value found
	private static int digitSubstring(String in, int pos) {
		String[] digits = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
		int value = -1;
		for (int i = 0; i < digits.length; i++) {
			String digit = digits[i];
			if (value == -1 && (pos + digit.length() <= in.length() && in.substring(pos, pos+digit.length()).equals(digit))) {
				value = i+1;
			}
		}
		return value;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}