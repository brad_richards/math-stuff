package kattis.e_loops;

import java.util.Scanner;

public class pet {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int bestScore = 0;
		int winner = 0;
		
		for (int i = 1; i <= 5; i++) {
			int score = 0;
			for (int j = 0; j < 4; j++) {
				score += in.nextInt();
			}
			if (score > bestScore) {
				bestScore = score;
				winner = i;
			}
		}
		System.out.println(winner + " " + bestScore);
		
	}

}
