package AdventOfCode.aoc_2015.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int floor = 0;
		int count = 0;
		for (char c : lines.get(0).toCharArray()) {
			count++;
			if (c == '(')
				floor++;
			else
				floor--;
			if (floor < 0) {
				System.out.println(count);
				break;
			}
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}