package AdventOfCode.aoc_2021.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem01 {
	private static List<Integer> numbers;

	public static void main(String[] args) throws IOException {
		readInput();

		int count = 0;
		for (int i = 0; i < numbers.size()-1; i++) {
			if (numbers.get(i+1) > numbers.get(i)) count++;
		}
		System.out.println(count);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			numbers = stream.map(Integer::parseInt).collect(Collectors.toList());
		}
	}

}
