package ch.kri.brad.projecteuler;

/**
 * I found the description of this problem confusing. The "prize fund" assumes
 * that the house must pay out an amount of money equal to the inverse of the
 * player's chance of winning. For n=4, this means 120/11 = 10.9, however, the
 * player payed 1 to play, so the house must provide a prize fund of 9.9. The
 * next higher integer is 10.
 * 
 * So: We need to calculate the win probability for N = 15, take the inverse and
 * subtract 1. I'm not seeing how to calculate this directly, but we can create a
 * triangle showing the possibilities:
 * 
 * 0 draws:                             1 
 *                                     / \
 * 1 draw (chances out of 2):         1   1
 *                                   / \ / \
 * 2 draws (chances out of 6):      1   3   2
 *                                 / \ / \ / \
 * 3 draws (chances out of 24):   1   6  11   6
 * 
 * etc... In practice, our triangle will be a 16x16 array, and we will use the
 * lower-left part of it. At the end, for 15 draws we need 8 blues, so we sum up
 * the left-most 8 entries (chances out of 16!)
 */
public class Problem121 {
	static long[][] triangle = new long[16][16];

	public static void main(String[] args) {
		triangle[0][0] = 1; // Initialize first row

		// Create the triangle
		for (int row = 1; row <= 15; row++) {
			for (int col = 0; col <= row; col++) {
				if (col == 0) { // only one number
					triangle[row][col] = 1;
				} else if (col == row) { // only one number
					triangle[row][col] = row * triangle[row - 1][col - 1];
				} else { // sum of two numbers
					triangle[row][col] = triangle[row - 1][col] + row * triangle[row - 1][col - 1];
				}
			}
		}

		// Calculate and print the answers for all rows
		long chances = 1;
		for (int row = 1; row <= 15; row++) {
			int blueDisksNeeded = row / 2 + 1;
			chances *= (row+1);
			
			// Sum the entries from the row of the triangle
			long sum = 0;
			for (int col = 0; col < (row + 1 - blueDisksNeeded); col++) sum += triangle[row][col];
			System.out.println(sum);

			long answer = ((long) Math.ceil((double) chances / (double) sum)) - 1;
			System.out.println("For " + row + " draws, the house needs " + answer);
		}
	}
}
