package AdventOfCode.aoc_2020.day_11;

import java.io.IOException;
import java.util.List;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> lines = ir.getValues();
		
		Room01 newRoom = new Room01(lines);
		Room01 room = null;
		while (!newRoom.equals(room)) {
			room = newRoom;
			newRoom = room.applyRules();
		}
		
		System.out.println("Seats occupied: " + room.occupiedSeats());
	}

}
