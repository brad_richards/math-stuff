package kattis.e_loops;

import java.util.Scanner;

public class oddecho {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int numWords = in.nextInt();
		for (int i = 1; i <= numWords; i++) {
			String word = in.next();
			if (i % 2 == 1) System.out.println(word);
		}
	}

}
