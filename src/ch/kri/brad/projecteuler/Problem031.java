package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Arrays;


public class Problem031 {
	private static int[] coins = {1, 2, 5, 10, 20, 50, 100, 200};
	private static ArrayList<int[]> solutions = new ArrayList<int[]>();
	
	/**
	 * Ah, finally, a problem that cries out to be solved recursively!
	 */
	public static void main(String[] args) {
		search(new int[coins.length], 0, 200);
		System.out.println(solutions.size());
	}

	private static void search(int[] solution, int position, int valueRemaining) {
		if (valueRemaining == 0) {
			solutions.add(Arrays.copyOf(solution, solution.length));
		} else if (position == coins.length-1) {
			// base case: fill remaining value with last coin, if possible
			if (valueRemaining % coins[position] == 0) {
				solution[position] = valueRemaining/coins[position];
				solutions.add(solution);
			} else {
				// failure! Do not add the solution.
			}
		} else {
			// inductive case
			for (int i = 0; i <= valueRemaining/coins[position]; i++) {
				solution[position] = i;
				search(solution, position+1, valueRemaining - i * coins[position]);
			}
		}
	}
}
