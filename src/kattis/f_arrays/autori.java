package kattis.f_arrays;

import java.util.Scanner;

/**
 * Use the split-method in the class String
 */
public class autori {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		String[] parts = input.split("-");
		for (String part : parts) System.out.print(part.charAt(0));
	}

}
