package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.ArrayList;


public class Problem025 {
	private static ArrayList<BigInteger> fibonacci = new ArrayList<BigInteger>();
	static {
		fibonacci.add(BigInteger.ONE);
		fibonacci.add(BigInteger.ONE);
	}

	/**
	 * We do not use our utility class Fibonacci, since we require BigIntegers, and since we have an
	 * odd criterion to fulfill (i.e., stopping based on the number of digits).
	 * 
	 * Note: to have 1000 digits, the value must be >= 10^999
	 */
	public static void main(String[] args) {
		BigInteger beforeLastNum = BigInteger.ONE;
		BigInteger lastNum = BigInteger.ONE;
		BigInteger limit = BigInteger.TEN.pow(999);
		BigInteger sum = beforeLastNum.add(lastNum);
		long count = 3;
		while (sum.compareTo(limit) < 0) {
			fibonacci.add(sum);
			beforeLastNum = lastNum;
			lastNum = sum;
			sum = beforeLastNum.add(lastNum);
			count++;
		}	
		System.out.println(count);
	}

}
