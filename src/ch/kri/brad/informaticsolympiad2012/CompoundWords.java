package ch.kri.brad.informaticsolympiad2012;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;


public class CompoundWords {
	private static ArrayList<String> prefixes;
	private static ArrayList<ArrayList<String>> prefixGroups;
	private static int numPrefixes;
	private static long prefixesUnique;
	
	private static ArrayList<String> suffixes;
	private static  ArrayList<ArrayList<String>> suffixGroups;
	private static int numSuffixes;
	private static long suffixesUnique;

	private static HashSet<String> words;
	
	private static long totalUniqueWords;
	
	public static void main(String[] args) {
		// readInputs();
		randomInputs();

		// Save the number of prefixes and suffixes for later reference
		numPrefixes = prefixes.size();
		numSuffixes = suffixes.size();
		
		// Sort the prefixes and suffixes by length, from shortest to longest
		prefixes = sortByLength(prefixes);
		suffixes = sortByLength(suffixes);
		
		System.out.println("Sorted");
		
		// Process the prefixes into prefix-groups, eliminating unique entries
		prefixesUnique = prefixGroups();
		
		// Process the suffixes into suffix-groups, eliminating unique entries
		suffixesUnique = suffixGroups();
		
		System.out.println("Grouped");

		// Calculate the number of unique words arising from unique prefixes and suffixes
		totalUniqueWords = prefixesUnique * numSuffixes + suffixesUnique * numPrefixes - prefixesUnique * suffixesUnique;
		
		// Add the number of words from each pair of prefix/suffix groups
		System.out.print("Number of prefix groups: " + prefixGroups.size() + " ");
		for (ArrayList<String> preGroup : prefixGroups) {
			System.out.print(".");
			for (ArrayList<String> sufGroup : suffixGroups) {
				totalUniqueWords += wordsFromGroups(preGroup, sufGroup);
			}
		}
		System.out.println();
		
		System.out.println("Answer: " + totalUniqueWords);
	}

	/**
	 * Sort an ArrayList of String by length, from shortest to longest
	 * 
	 * @param in the input ArrayList
	 * @return the sorted ArrayList
	 */
	private static ArrayList<String> sortByLength(ArrayList<String> in) {
		ArrayList<String> out = new ArrayList<String>();
		while (in.size() > 0) {
			int shortest = 0;
			int length = in.get(0).length();
			for (int i = 1; i < in.size(); i++) {
				String s = in.get(i);
				int sLength = s.length();
				if (sLength < length) {
					shortest = i;
					length = sLength;
				}
			}
			out.add(in.remove(shortest));
		}
		return out;
	}
	
	/**
	 * Search for groups of prefixes, where one of the strings is a prefix of all the others in the group. Example: "as" is a prefix of "asd", "adf" and "asdf"; these four strings would be in the same group.
	 * 
	 * Return a two-dimensional array, where each row contains one such group. Strings which are in no group are counted as unique strings and discarded.
	 * 
	 * @return The number of unique strings discarded
	 */
	private static int prefixGroups() {
		int uniqueStrings = 0;
		prefixGroups = new ArrayList<ArrayList<String>>();
		ArrayList<String> group = null;

		while(prefixes.size() > 0) {
			String shortestPrefix = prefixes.remove(0);
			for (Iterator<String> i = prefixes.iterator(); i.hasNext(); ) {
				String nextPrefix = i.next();
				if (nextPrefix.startsWith(shortestPrefix)) {
					if (group == null) { // We only create it if we really need it
						group = new ArrayList<String>();
						group.add(shortestPrefix);
					}
					group.add(nextPrefix);
					i.remove();
				}
			}
			if (group == null) {
				// This is a unique prefix
				uniqueStrings++;
			} else {
				// We have a new group
				prefixGroups.add(group);
				group = null;
			}
		}		
		return uniqueStrings;
	}
	
	/**
	 * Search for groups of suffixes, where one of the strings is a suffix of all the others in the group.
	 * 
	 * Return a two-dimensional array, where each row contains one such group. Strings which are in no group are counted as unique strings and discarded.
	 * 
	 * @return The number of unique strings discarded
	 */
	private static int suffixGroups() {
		int uniqueStrings = 0;
		suffixGroups = new ArrayList<ArrayList<String>>();
		ArrayList<String> group = null;

		while(suffixes.size() > 0) {
			String shortestSuffix = suffixes.remove(0);
			for (Iterator<String> i = suffixes.iterator(); i.hasNext(); ) {
				String nextSuffix = i.next();
				if (nextSuffix.endsWith(shortestSuffix)) {
					if (group == null) { // We only create it if we really need it
						group = new ArrayList<String>();
						group.add(shortestSuffix);
					}
					group.add(nextSuffix);
					i.remove();
				}
			}
			if (group == null) {
				// This is a unique suffix
				uniqueStrings++;
			} else {
				// We have a new group
				suffixGroups.add(group);
				group = null;
			}
		}		
		return uniqueStrings;
	}
	
	private static long wordsFromGroups(ArrayList<String> preGroup, ArrayList<String> sufGroup) {
		words = new HashSet<String>(preGroup.size() * sufGroup.size());
		
		for (String prefix : preGroup) {
			for (String suffix : sufGroup) {
				String word = prefix + suffix;
				words.add(word);
			}
		}
		return words.size();
	}

	private static void readInputs() {
		Scanner s = new Scanner(System.in);
		int numStrings = s.nextInt();
		prefixes = new ArrayList<String>();
		for (int i = 0; i < numStrings; i++) {
			prefixes.add(s.next());
		}
		numStrings = s.nextInt();
		suffixes = new ArrayList<String>();
		for (int i = 0; i < numStrings; i++) {
			suffixes.add(s.next());
		}
	}
	
	private static void randomInputs() {
		prefixes = new ArrayList<String>();
		for (int i = 0; i < 50000; i++) {
			prefixes.add(randomString());
		}
		suffixes = new ArrayList<String>();
		for (int i = 0; i < 50000; i++) {
			suffixes.add(randomString());
		}
		System.out.println("Inputs created");
	}
	
	private static String randomString() {
		int numChars = (int) (Math.random() * 97) + 3;
		char[] chars = new char[numChars];
		for (int i = 0; i < numChars; i++) {
			int r = (int) (Math.random() * 52);
			if (r < 26) {
				r += 'A';
			} else {
				r -= 26;
				r += 'a';
			}
			chars[i] = (char) r;
		}
		return new String(chars);
	}
	
}
