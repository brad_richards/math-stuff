package LeetCode.valid_arrangement_of_pairs;

// Thoughts: It is guaranteed that start and end are different. Therefore,
// there are two numbers
// that occur an odd number of times. If so, those are the starting/ending
// numbers. If so, the one that appears more often at the start than at the
// end is the starting number.
//
// We start by constructing a HashMap for each number, listing the number
// of times it occurs at the start/end of a pair, in order to identify
// the starting number.
//
// We build a second HashMap, based on the starting number of a pair,
// containing the pairs. Using this, we perform a simple
// depth-first search for the solution.

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        int[][] pairs = pairs = new int[][] {{1, 2}, {1, 3}, {2, 1}};

        Solution s = new Solution();
        int[][] answer = s.validArrangement(pairs);
        System.out.println(answer);
    }

    public int[][] validArrangement(int[][] in) {
        HashMap<Integer, int[]> counts = new HashMap();
        HashMap<Integer, List<int[]>> pairs = new HashMap<>();

        // Build maps
        for (int[] pairIn : in) {
            int[] count = counts.get(pairIn[0]);
            if (count == null) { count = new int[2]; counts.put(pairIn[0], count); }
            count[0]++;

            count = counts.get(pairIn[1]);
            if (count == null) { count = new int[2]; counts.put(pairIn[1], count); }
            count[1]++;

            List<int[]> pairList = pairs.get(pairIn[0]);
            if (pairList == null) { pairList = new ArrayList<>(); pairs.put(pairIn[0], pairList); }
            pairList.add(pairIn);
        }

        // Find starting number
        int firstNumber = -1;
        for (int key : counts.keySet()) {
            int[] count = counts.get(key);
            if (((count[0] + count[1]) % 2 == 1 && count[0] > count[1])) {
                firstNumber = key;
                break;
            }
        }

        // Search for solution
        List<int[]> solution = new ArrayList<>();
        depthFirstSearch(firstNumber, solution, pairs);
        int[][] answer = (int[][]) solution.toArray();
        return answer;
    }

    // returns true as soon as we have a solution
    private static boolean depthFirstSearch(int numberRequired, List<int[]> solution, HashMap<Integer, List<int[]>> pairs) {
        // Base case: no pairs left
        if (pairs.isEmpty()) return true;  // TODO: Not correct: sum of elements in the lists needs to be zero

        // Inductive case
        List<int[]> pairsList = pairs.get(numberRequired);
        List<int[]> nextPairs = new ArrayList<>(pairsList);
        for (int[] pair : nextPairs) {
            pairsList.remove(pair);
            solution.add(pair);
            if (depthFirstSearch(pair[1], solution, pairs)) return true;
            solution.removeLast();
            pairsList.add(pair);
        }
        return false;
    }
}