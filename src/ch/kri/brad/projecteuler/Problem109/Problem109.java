package ch.kri.brad.projecteuler.Problem109;

public class Problem109 {
	private static int[] scores = new int[62];
	
	public static void main(String[] args) {
		// initialize possible scores
		for (int i = 0; i < 20; i++) {
			scores[i*3] = i+1;
			scores[i*3+1] = 2 * (i+1);
			scores[i*3+2] = 3 * (i+1);
		}
		scores[60] = 25;
		scores[61] = 50;

		// solutions with only a double: 21 possibilities
		int totalCheckouts = 21;
		
		// solutions with 1 dart before the double
		totalCheckouts += 57 + 61 + 19 * 62;
		
		// solutions with 2 darts before the double - brute force counting
		int combos = countCombos(2, 49);
		totalCheckouts += combos;
		int lastMax = 49;
		
		for (int maxPoints = 59; maxPoints <= 97; maxPoints+=2) {
			combos += countCombos(lastMax+1, maxPoints);
			totalCheckouts += combos;
			lastMax = maxPoints;
		}
		
		System.out.println(totalCheckouts);
	}
	
	private static int countCombos(int min, int max) {
		int count = 0;
		for (int i = 0; i < scores.length-1; i++) {
			for (int j = i; j < scores.length; j++) {
				int points = scores[i] + scores[j];
				if (points >= min && points <= max) count++;
			}
		}
		System.out.println(count);
		return count;
	}

}
