package AdventOfCode.aoc_2024.day_02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
    static List<String> lines;

    public static void main(String[] args) throws Exception {
        readInput();

        int numSafe = 0;
        for (String line : lines) {
            if (isSafe(line)) numSafe++;
        }
        System.out.println(numSafe);
    }

    private static boolean isSafe(String line) {
        String[] parts = line.split(" ");
        int[] nums = new int[parts.length];
        for (int i = 0; i < parts.length; i++) {
            nums[i] = Integer.parseInt(parts[i]);
        }

        boolean increasing = isIncreasing(nums);
        return numsAreSafe(increasing, nums, false);
    }

    private static boolean numsAreSafe(boolean increasing, int[] nums, boolean dampened) {
        for (int i = 0; i < nums.length-1; i++) {
            int diff = (increasing) ? nums[i+1] - nums[i] : nums[i] - nums[i+1];
            if (diff < 1 || diff > 3) return (dampened) ? false : dampen(increasing, nums, i);
        }
        return true;
    }

    // Two options: remove number at [i] or at [i+1]
    private static boolean dampen(boolean increasing, int[] nums, int index) {
        int[] nums1 = new int[nums.length-1];
        int[] nums2 = new int[nums.length-1];
        for (int i = 0; i < nums.length; i++) {
            if (index > i) {
                nums1[i] = nums[i];
            } else if (index < i) {
                nums1[i-1] = nums[i];
            }
            if (index+1 > i) {
                nums2[i] = nums[i];
            } else if (index+1 < i) {
                nums2[i-1] = nums[i];
            }
        }
        boolean result1 = numsAreSafe(increasing, nums1, true);
        boolean result2 = numsAreSafe(increasing, nums2, true);
        return result1 || result2;
    }

    private static boolean isIncreasing(int[] nums) {
        int numIncreasing = 0;
        int numDecreasing = 0;
        for (int i = 0; i < nums.length-1; i++) {
            if (nums[i+1] - nums[i] > 0) numIncreasing++; else numDecreasing++;
        }
        return numIncreasing > numDecreasing;
    }

    private static void readInput() throws IOException {
        Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
        try (Stream<String> stream = Files.lines(path)) {
            lines = stream.collect(Collectors.toList());
        }
    }
}