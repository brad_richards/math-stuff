package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import ch.kri.brad.utility.Primes;



public class Problem049 {
	
	static final boolean[] primeSieve;
	
	static TreeSet<String> solutions = new TreeSet<String>();
	
	static {
		Primes.generatePrimesErastothenes(10000, true);
		primeSieve = Primes.getRawSieve();
	}

	/**
	 * General approach
	 * 1. Create list of all sets of 4 digits
	 * 2. For each set, create all possible permutations
	 *    and store the resulting Integers in a TreeSet
	 * 3. Remove the non-prime permutations
	 * 4. If at least three remaining then
	 *    - Convert the TreeSet to an Array
	 * 6. - Search for arithmetic sequence amongst 3 entries
	 * 7. - if sequence found
	 * 8.   - concatenate the three numbers and print
	 * 
	 * Initial solution assumed unique digits in the range 1-9. However, there
	 * are no other solutions that meet this criterion. Then we tried allowing
	 * unique digits in the range 0-9, but this brings two additional solutions.
	 * Finally, we tried non-unique digits in the range 1-9; this seems to be
	 * what is expected, yielding the additional solution 2969-6299-9629
	 */
	public static void main(String[] args) {
		ArrayList<int[]> digitSets = createDigitSets();
		
		for(int[] digitSet : digitSets) {
			TreeSet<Integer> permutations = createPermutations(digitSet);
			removeNonPrimes(permutations);
			if (permutations.size() >= 3) {
				int[] numbers = convertToArray(permutations);
				testForSequence(numbers);
			}
		}
		
		for (String solution : solutions) {
			System.out.println(solution);
		}
	}

	static ArrayList<int[]> createDigitSets() {
		ArrayList<int[]> digitSets = new ArrayList<int[]>();
//		for (int i = 0; i <= 6; i++) {              
//			for (int j = i+1; j <=7; j++) {           
//				for (int k = j+1; k <= 8; k++) {
//					for (int l = k+1; l <= 9; l++) {      
						for (int i = 0; i <= 9; i++) {
							for (int j = 0; j <=9; j++) {
								for (int k = 0; k <= 9; k++) {
									for (int l = 0; l <= 9; l++) {
						int[] digitSet = new int[4];
						digitSet[0] = i;
						digitSet[1] = j;
						digitSet[2] = k;
						digitSet[3] = l;
						digitSets.add(digitSet);
					}
				}
			}
		}		
		return digitSets;
	}
	
	static TreeSet<Integer> createPermutations(int[] digitSet) {
		TreeSet<Integer> permutations = new TreeSet<Integer>();
		int[] digits = new int[4];
		for (int i = 0; i <= 3; i++) {
			digits[i] = digitSet[0];			
			for (int j = 0; j <= 3; j++) {
				if (i != j) {
					digits[j] = digitSet[1];
					for (int k = 0; k <= 3; k++) {
						if (i != k & j != k) {
							digits[k] = digitSet[2];
							for (int l = 0; l <= 3; l++) {
								if (i != l & j != l & k != l) {
									digits[l] = digitSet[3];
									
									int value = digits[0]*1000+digits[1]*100+digits[2]*10+digits[3];
									permutations.add(value);
								}
							}
						}		
					}
				}
			}
		}
		return permutations;
	}
	
	static void removeNonPrimes(TreeSet<Integer> permutations) {
		Iterator<Integer> i = permutations.iterator();
		while (i.hasNext()) {
			int value = i.next();
			if (value < 2) i.remove();
			else if (!primeSieve[value-2]) i.remove();
		}
	}
	
	static int[] convertToArray(TreeSet<Integer> treeSet) {
		int[] array = new int[treeSet.size()];
		int index = 0;
		for (Integer t : treeSet) {
			array[index++] = t;
		}
		return array;
	}
	
	static void testForSequence(int[] numbers) {
		for (int pos1 = 0; pos1 < numbers.length-2; pos1++) {
			for (int pos2 = pos1+1; pos2 < numbers.length-1; pos2++) {
				int diff1 = numbers[pos2] - numbers[pos1];
				for (int pos3 = pos2+1; pos3 < numbers.length; pos3++) {
					int diff2 = numbers[pos3] - numbers[pos2];
					if (diff1 == diff2) {
						String result = Integer.toString(numbers[pos1]) + numbers[pos2] + numbers[pos3];
						solutions.add(result);
					}
				}
			}
		}
	}
}
