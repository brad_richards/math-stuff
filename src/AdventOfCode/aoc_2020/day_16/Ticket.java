package AdventOfCode.aoc_2020.day_16;

public class Ticket {
	private int[] fieldValues;
	
	public Ticket(int[] fieldValues) {
		this.fieldValues = fieldValues;
	}
	
	public int[] getFieldValues() {
		return fieldValues;
	}
}
