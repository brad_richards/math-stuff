package ch.kri.brad.projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.FractionBigInteger;

/**
 * A solution using continuing fractions, combining the methods from Problems 64 and 65.
 * 
 * http://www.mathblog.dk/project-euler-66-diophantine-equation/
 * 
 * http://www.ams.org/notices/200202/fea-lenstra.pdf
 */
public class Problem066 {
	private long D; // The number whose root we are finding
	private long intRoot; // The integer root of D

	public static void main(String[] args) {
		long count = 0;
		long D_value_for_max = 0;
		BigInteger maxValue = BigInteger.ZERO;
		for (long D_value = 2; D_value <= 1000; D_value++) {
			long intRoot = intSqrt(D_value);
			if (intRoot * intRoot != D_value) {
				Problem066 p = new Problem066(D_value, intRoot);
				
				// Calculate the square-root of D, using the method from problem 64.
				// Find the cycle length
				ArrayList<Digit_And_Fraction> steps = new ArrayList<>();
				int cycleLength = p.solve(steps);
				
				// The position of the first element in the repeating cycle
				int cycleStart = steps.size() - cycleLength;
						
				// Create a list of fraction denominators, which we will use to
				// calculate the value of the continuing fraction, using the
				// technique from cycle 65. Important: to get a correct answer,
				// we include odd-length cycles *twice*, and we omit the last
				// element from the last included cycle.
				//		
				// Example: for D = 14, the digits would be [3,1,2,1,6,1,2,1,6,1,2,1,...],
				// so a cycle of length 4 with one leading digit. Since the cycle is even
				// (length 4), we would include it only once: [3,1,2,1,6], but we omit the
				// last entry, hence yielding [3,1,2,1]. This yields the value 15/4, so our
				// answer is 15² = 14 * 4² + 1. For purposes of problem 66, we are
				// interested in the value "15"
				int totalValues = cycleStart + ((cycleLength % 2 == 1) ? 2 * cycleLength : cycleLength) - 1;
				long[] values = new long[totalValues];
				
				// Add leading elements
				int pos = 0;
				while (pos < cycleStart) {
					values[pos] = steps.get(pos).digit;
					pos++;
				}
				
				// Extra full cycle, if cycle length is odd
				if (cycleLength % 2 == 1) {
					for (int i = 0; i < cycleLength; i++) {
						values[pos++] = steps.get(cycleStart + i).digit;
					}
				}
				
				// Add cycle missing its last element
				for (int i = 0; i < (cycleLength-1); i++) {
					values[pos++] = steps.get(cycleStart + i).digit;
				}
				
				// Calculate value of continuing fraction, using method from Problem 65
				FractionBigInteger f = new FractionBigInteger(values[values.length-1], 1);
				for (int i = values.length - 2; i >= 0; i--) {
					f = f.inverse();
					f = f.add(values[i]);
				}
				
				System.out.println(f.getNumerator().toString() + "^2 - " + D_value + " * " + f.getDenominator() + "^2 = 1");
				
				BigInteger thisValue = f.getNumerator();
				if (thisValue.compareTo(maxValue) > 0) {
					maxValue = thisValue;
					D_value_for_max = D_value;
				}
			}
		}
		System.out.println("\nFor D = " + D_value_for_max + ", we have the maximum value: " + maxValue);
	}

	public Problem066(long n, long intRoot) {
		this.D = n;
		this.intRoot = intRoot;
	}
	
	/**
	 * Fill in the ArrayList with the steps in the solution, up to the beginning of the second repitition.
	 * 
	 * @param steps An empty ArrayList for us to fill in
	 * @return the number of steps included in the repeating portion of the solution
	 */
	public int solve(ArrayList<Digit_And_Fraction> steps) {
		Digit_And_Fraction df = new Digit_And_Fraction(intRoot, intRoot, 1);
		steps.add(df);
		int period = -1;
		while (period < 0) {
			Digit_And_Fraction next = df.nextStep();
			period = contains(steps, next);
			if (period < 0) steps.add(next);
			df = next;
		}
		return period;
	}

	/**
	 * Detects whether or not we have found a repeating state. If not, return -1. If so, return the
	 * length of the repeating cycle.
	 */
	private int contains(ArrayList<Digit_And_Fraction> steps, Digit_And_Fraction step) {
		int period = -1;
		for (int i = 0; i < steps.size() && period < 0; i++) {
			if (steps.get(i).equals(step)) period = steps.size() - i;
		}
		return period;
	}

	/**
	 * Class to represent the sum of an integer and the double-inverse of an S_Fraction
	 */

	/**
	 * We represent one step in the process as a digit, and the values a and b from the fraction:
	 * 
	 * (sqrt(n) - a) / b
	 * 
	 * For example, sqrt(23) begins with the value 4 + [ (sqrt23 - 4) / 1 ]
	 * 
	 * We represent this as digit = 4, a = 4, b = 1. The following step resolves the fraction to 1 /
	 * X,
	 * 
	 * where X is 1 + [ (sqrt23 - 3) / 7 ]
	 * 
	 * Which we represent as digit = 1, a = 3, b = 7
	 * 
	 * We can detect repetition at the point that we have two identical entries in sequence.
	 * 
	 */
	private class Digit_And_Fraction {
		private long digit;
		private long a, b;

		public Digit_And_Fraction(long digit, long a, long b) {
			this.digit = digit;
			this.a = a;
			this.b = b;
		}

		/**
		 * Method to transform this Digit_And_Fraction by one step, creating the next one in line
		 */
		public Digit_And_Fraction nextStep() {
			long new_b = (D - a * a) / b;
			long new_digit = (intRoot + a) / new_b;
			long new_a = new_digit * new_b - a;
			return new Digit_And_Fraction(new_digit, new_a, new_b);
		}

		@Override
		public boolean equals(Object obj) {
			Digit_And_Fraction df = (Digit_And_Fraction) obj;
			return (digit == df.digit && a == df.a && b == df.b);
		}

		@Override
		public String toString() {
			return digit + ", " + a + ", " + b;
		}
	}

	private static long intSqrt(long n) {
		long x = n / 2;
		do {
			x = x / 2;
		} while (x * x > n);

		while (x * x <= n)
			x++;

		return --x;
	}
}
