package ch.kri.brad.utility;

import static ch.kri.brad.utility.Digits.*;
import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class DigitsTest {
	private static final int[] digits9 = {9};
	private static final int[] digits15243 = {1,5,2,4,3};
	private static final int[] digits15243r = {3,4,2,5,1};
	private static final int[] digits10000 = {1,0,0,0,0};
	private static final int[] digits10000r = {0,0,0,0,1};
	
	@Test
	public void testNumDigits() {
		assertEquals(numDigits(0), 1);
		assertEquals(numDigits(1),1);
		assertEquals(numDigits(9),1);
		assertEquals(numDigits(999),3);
		assertEquals(numDigits(1000),4);
		assertEquals(numDigits(5000),4);
		assertEquals(numDigits(999999),6);
		assertEquals(numDigits(1000000),7);
	}

	@Test
	public void testNumDigitsBI() {
		assertEquals(numDigits(BigInteger.ZERO), 1);
		assertEquals(numDigits(BigInteger.ONE),1);
		assertEquals(numDigits(BigInteger.valueOf(9)),1);
		assertEquals(numDigits(BigInteger.valueOf(999)),3);
		assertEquals(numDigits(BigInteger.valueOf(1000)),4);
		assertEquals(numDigits(BigInteger.valueOf(5000)),4);
		assertEquals(numDigits(BigInteger.valueOf(999999)),6);
		assertEquals(numDigits(BigInteger.valueOf(1000000)),7);
	}

	@Test
	public void testToDigits() {
		assertTrue(Arrays.equals(toDigits(9), digits9));
		assertTrue(Arrays.equals(toDigits(15243), digits15243));
		assertTrue(Arrays.equals(toDigits(10000), digits10000));
	}
	
	@Test
	public void testSumOfDigits() {
		assertEquals(sumOfDigits(9), 9);
		assertEquals(sumOfDigits(999), 27);
		assertEquals(sumOfDigits(1000), 1);
		assertEquals(sumOfDigits(15243), 15);
	}

	@Test
	public void testSumOfDigitsBI() {
		assertEquals(sumOfDigits(BigInteger.valueOf(9)), 9);
		assertEquals(sumOfDigits(BigInteger.valueOf(999)), 27);
		assertEquals(sumOfDigits(BigInteger.valueOf(1000)), 1);
		assertEquals(sumOfDigits(BigInteger.valueOf(15243)), 15);
	}

	@Test
	public void testFromDigits() {
		assertEquals(fromDigits(digits9), 9);
		assertEquals(fromDigits(digits15243), 15243);
		assertEquals(fromDigits(digits10000), 10000);
	}

	@Test
	public void testToDigitsBigInt() {
		ArrayList<Integer> digits;
		ArrayList<Integer> correctDigits;
		
		digits = toDigits(new BigInteger("9"));
		correctDigits = new ArrayList<>();
		for (int i : digits9) correctDigits.add(i);
		assertEquals(digits, correctDigits);

		digits = toDigits(new BigInteger("15243"));
		correctDigits = new ArrayList<>();
		for (int i : digits15243) correctDigits.add(i);
		assertEquals(digits, correctDigits);
		
		digits = toDigits(new BigInteger("10000"));
		correctDigits = new ArrayList<>();
		for (int i : digits10000) correctDigits.add(i);
		assertEquals(digits, correctDigits);
	}

	@Test
	public void testToReversedDigitsBigInt() {
		ArrayList<Integer> digits;
		ArrayList<Integer> correctDigits;
		
		digits = toReversedDigits(new BigInteger("9"));
		correctDigits = new ArrayList<>();
		for (int i : digits9) correctDigits.add(i);
		assertEquals(digits, correctDigits);

		digits = toReversedDigits(new BigInteger("15243"));
		correctDigits = new ArrayList<>();
		for (int i : digits15243r) correctDigits.add(i);
		assertEquals(digits, correctDigits);
		
		digits = toReversedDigits(new BigInteger("10000"));
		correctDigits = new ArrayList<>();
		for (int i : digits10000r) correctDigits.add(i);
		assertEquals(digits, correctDigits);
	}

	@Test
	public void testFromDigitsBigInt() {
		ArrayList<Integer> digits;

		digits = new ArrayList<>();
		for (int i : digits9) digits.add(i);
		assertEquals(new BigInteger("9"), fromDigits(digits));

		digits = new ArrayList<>();
		for (int i : digits15243) digits.add(i);
		assertEquals(new BigInteger("15243"), fromDigits(digits));
		
		digits = new ArrayList<>();
		for (int i : digits10000) digits.add(i);
		assertEquals(new BigInteger("10000"), fromDigits(digits));
	}

}
