package AdventOfCode.aoc_2018.day07;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * There's nothing for it - simplest solution is a simulation, generating results as shown in the
 * problem description.
 * 
 * We create a class "elf" to represent the workers. We will hand elves tasks as needed, and "tick"
 * a clock to simulate time passing.
 */
public class Day07_part2 {
	private static HashMap<Character, ArrayList<Character>> successorNodes = new HashMap<>();
	private static HashMap<Character, ArrayList<Character>> predecessorNodes = new HashMap<>();
	private static ArrayList<Elf> elves = new ArrayList<>();

	private static final String INPUT_FILE = "input.txt";
	private static final int NUM_ELVES = 5;

	private static class Edge {
		Character from;
		Character to;
	}

	public static void main(String[] args) {
		readInput();

		// Create the elves
		for (int i = 0; i < NUM_ELVES; i++)
			elves.add(new Elf());

		simulate();
	}

	private static void readInput() {
		InputStream fis = Day07.class.getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String line = in.nextLine();
				Edge edge = parseInput(line);

				// Create nodes as necessary
				if (!successorNodes.containsKey(edge.from)) {
					predecessorNodes.put(edge.from, new ArrayList<Character>());
					successorNodes.put(edge.from, new ArrayList<Character>());
				}
				if (!successorNodes.containsKey(edge.to)) {
					predecessorNodes.put(edge.to, new ArrayList<Character>());
					successorNodes.put(edge.to, new ArrayList<Character>());
				}

				// Add edge into HashMaps, if not already present
				ArrayList<Character> successors = successorNodes.get(edge.from);
				if (!successors.contains(edge.to)) successors.add(edge.to);

				ArrayList<Character> predecessors = predecessorNodes.get(edge.to);
				if (!predecessors.contains(edge.from)) predecessors.add(edge.from);
			}
		}
		System.out.println(successorNodes.size() + " nodes read");

		// System.out.println("\nNodes and their successors");
		// for (Character c : successorNodes.keySet()) {
		// System.out.print(c + ": ");
		// for (Character cc : successorNodes.get(c))
		// System.out.print(cc);
		// System.out.println();
		// }
		//
		// System.out.println("\nNodes and their predecessors");
		// for (Character c : predecessorNodes.keySet()) {
		// System.out.print(c + ": ");
		// for (Character cc : predecessorNodes.get(c))
		// System.out.print(cc);
		// System.out.println();
		// }

	}

	public static Edge parseInput(String line) {
		Edge edge = new Edge();
		edge.from = line.charAt(5);
		edge.to = line.charAt(36);
		return edge;
	}

	private static void simulate() {
		ArrayList<Character> nextNodes = new ArrayList<>();
		StringBuffer buf = new StringBuffer();
		int time = 0;

		// To start, find all nodes with no predecessors
		for (Character c : predecessorNodes.keySet()) {
			if (predecessorNodes.get(c).isEmpty()) nextNodes.add(c);
		}
		Collections.sort(nextNodes);

		while (!finished(nextNodes)) {
			assignTasks(nextNodes, buf.toString());
			
			printStatus(time, buf.toString());
			
			for (Elf elf : elves) {
				Character taskFinished = elf.tick();
				if (taskFinished != null) {
					// Add to list of finished tasks
					buf.append(taskFinished);
					
					// Add newly available tasks to our list
					/// Don't add redundant nodes
					for (Character node : successorNodes.get(taskFinished)) {
						if (!nextNodes.contains(node)) nextNodes.add(node);
					}
				}
			}
			time++;
		}
		
		System.out.println("Total time required: " + time);
	}

	/**
	 * For each elf with no task, find the first node all of whose predecessors are fulfilled.
	 * Obviously, we don't assign anything if the task-list is empty
	 */
	private static void assignTasks(ArrayList<Character> nextTasks, String finishedTasks) {
		// First, sort the tasks in case new ones have been added
		Collections.sort(nextTasks);

		for (Elf elf : elves) {
			if (!elf.isBusy() && !nextTasks.isEmpty()) elf.setTask(nextTaskAvailable(nextTasks, finishedTasks));
		}
	}
	
	private static Character nextTaskAvailable(ArrayList<Character> nextTasks, String finishedTasks) {
		Character nextTask = null;
		if (!nextTasks.isEmpty()) {
			for (int i = 0; nextTask == null && i < nextTasks.size(); i++) {
				ArrayList<Character> predecessors = predecessorNodes.get(nextTasks.get(i));
				boolean allFound = true;
				for (Character c : predecessors) {
					allFound &= (finishedTasks.indexOf(c.toString()) != -1);
				}
				if (allFound) nextTask = nextTasks.remove(i); // Found the next task!
			}		
		}
		return nextTask;
	}

	/**
	 * We are finished if no elf is busy and no tasks remain
	 */
	private static boolean finished(ArrayList<Character> nextTasks) {
		boolean busyElf = false;
		for (Elf elf : elves) busyElf |= elf.isBusy();
		
		return !busyElf && nextTasks.isEmpty();
	}
	
	private static void printStatus(int time, String completedTasks) {
		System.out.printf("%5d", time);
		for (Elf elf : elves) {
			Character c = ' ';
			if (elf.isBusy()) c = elf.getTask();
			System.out.printf("  %c  ", c);
		}
		System.out.println(completedTasks);
	}
}
