package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Factoring;
import ch.kri.brad.utility.Factoring.AmicablePair;



public class Problem021 {

	public static void main(String[] args) {
		Factoring.generateAmicablePairs(9999);
		ArrayList<AmicablePair> amicablePairs = Factoring.getAmicablePairs();
	
		long sum = 0;
		for (Factoring.AmicablePair pair : amicablePairs) {
			sum += pair.getValue1() + pair.getValue2();
		}
		
		System.out.println("Sum = " + sum);
	}

}
