package ch.kri.brad.projecteuler;

public class Problem006 {

	public static void main(String[] args) {
		long sumOfSquares = 0;
		long sum = 0;
		for (long i = 1; i <= 100; i++) {
			sum += i;
			sumOfSquares += i*i;
		}
		long squareOfSum = sum*sum;
		long answer = squareOfSum - sumOfSquares;
		System.out.println("Answer = " + answer);
	}

}
