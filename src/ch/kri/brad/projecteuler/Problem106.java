package ch.kri.brad.projecteuler;

public class Problem106 {

	/**
	 * Disjoint sets of the same size, that could possible be equal. The numbers
	 * shown are the sorted positions of the elements (not their values)
	 * 
	 * INCOMPLETE: Only accounts for subsets of size 2!!!
	 * 
	 * For set of size 4: {1234}
	 * 
	 * - 1 4-combo à 1
	 *   {14} {23}
	 * 
	 * Set of size 5: {12345}
	 * 
	 * - 1 5-combos à 3 (2 + 1)
	 *   {15} {24}
	 *   {15} {23}
	 *   {15} {34}
	 * 
	 * - 2 4-combos à 1
	 *   {25} {34}
	 *   {14} {23}
	 * 
	 * 
	 * Set of size 6: {123456}
	 * 
	 * - 1 6-combos à 6 (3 + 2 + 1)
	 * - 2 5-combos à 3
	 * - 3 4-combos à 1
	 * 
	 * 
	 * Set of size 7:
	 * 
	 * - 1 7-combo à 10
	 * - 2 6-combo à 6
	 * - 3 5-combo à 3
	 * - 4 4-combo à 1
	 * 
	 * So for a set of size 12...
	 */
	public static void main(String[] args) {
		final int setSize = 12;
		int sum = 0;
		for (int i = 4; i <= setSize; i++) {
			int numCombos = setSize + 1 - i;
			int containing = triangleNumber(i-3);
			sum += numCombos * containing;
		}
		System.out.println(sum);
	}
	
	private static int triangleNumber(int n) {
		int sum = 0;
		for (int i = 1; i <= n; i++) sum += i;
		return sum;
	}
}
