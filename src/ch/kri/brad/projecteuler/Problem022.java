package ch.kri.brad.projecteuler;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFileChooser;


public class Problem022 {
	
	public static void main(String[] args) throws Exception {
		// Read in the names, place them in an array, and sort them
		ArrayList<String> names = readNames();
		Object[] aNames = names.toArray();
		Arrays.sort(aNames);
		
		// Calculate the scores
		long totalScore = 0;
		for (int i = 0; i < aNames.length; i++) {
			String name = (String) aNames[i];
			
			// Sum of letter values
			long letterSum = 0;
			char[] nameChars = name.toCharArray();
			for (char c : nameChars) {
				letterSum += c - 'A' + 1;
			}
			
			long score = (i+1) * letterSum;
			totalScore += score;
		}
		System.out.println("Answer = " + totalScore);
	}
	
	private static ArrayList<String> readNames() throws Exception {
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(null);		
		File f = chooser.getSelectedFile();
		ArrayList<String> names = new ArrayList<String>();
		StringBuffer name = null;
		
		FileReader in = new FileReader(f);
		boolean inName = false;
		int next = in.read();
		while(next > -1) {
			if (next == '"') {
				if (!inName) {
					// starting a new name
					inName = true;
					name = new StringBuffer();
				} else {
					// just finished a name - add it to the list
					inName = false;
					names.add(name.toString());
				}
			}

			// add letters to the current name
			if (inName & (next >= 'A' && next <= 'Z')) {
				name.append((char) next);
			}
			next = in.read();
		}
		in.close();
		return names;
	}

}
