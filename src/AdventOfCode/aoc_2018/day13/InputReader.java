package AdventOfCode.aoc_2018.day13;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import AdventOfCode.aoc_2018.day13.Cart.Facing;

public class InputReader {
	private static final String INPUT_FILE = "input.txt";
	private Track[][] playground;
	private ArrayList<Cart> carts = new ArrayList<>();

	// In order to dimension our array correctly, we need to know how many lines there are.
	// For this reason, we first read then entire input, and only then begin processing it.
	// Note: We assume that all lines are the same length.
	public void readInputs() {
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		ArrayList<String> inputLines = new ArrayList<>();
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				inputLines.add(in.nextLine());
			}
			
			playground = new Track[inputLines.size()][inputLines.get(0).length()];
			for (int row = 0; row < inputLines.size(); row++) {
				String line = inputLines.get(row);
				char[] cells = line.toCharArray();
				for (int col = 0; col < cells.length; col++) {
					playground[row][col] = createNewTrack(row, col, cells[col]);
				}
			}
		}
	}
	
	private Track createNewTrack(int row, int col, char c) {
		if (c == '|') return new Track(true, true, false, false);
		else if (c == '-') return new Track(false, false, true, true);
		else if (c == '+') return new Track(true, true, true, true);
		else if (c == ' ') return new Track(false, false, false, false);
		else if (c == '/') {
			if (row == 0 || !playground[row-1][col].goDown()) {
				return new Track(false, true, false, true); // top-left corner
			} else {
				return new Track(true, false, true, false); // bottom-right corner
			}
		} else if (c == '\\') {
			if (row == 0 || !playground[row-1][col].goDown()) {
				return new Track(false, true, true, false); // top-right corner
			} else {
				return new Track(true, false, false, true); // bottom-left corner
			}
			
		// Options that also create carts
		} else if ( c == '^') {
			carts.add(new Cart(col, row, Facing.UP));
			return new Track(true, true, false, false);
		} else if ( c == 'v') {
			carts.add(new Cart(col, row, Facing.DOWN));
			return new Track(true, true, false, false);
		} else if ( c == '<') {
			carts.add(new Cart(col, row, Facing.LEFT));
			return new Track(false, false, true, true);
		} else if ( c == '>') {
			carts.add(new Cart(col, row, Facing.RIGHT));
			return new Track(false, false, true, true);
		} else {
			System.out.println("Unexpected input character: " + c);
			return null;
		}
	}
	
	public Track[][] getPlayground() {
		return playground;
	}
	public ArrayList<Cart> getCarts() {
		return carts;
	}
}
