package ch.kri.brad.projecteuler;

import java.math.BigDecimal;

public class Problem053 {

	/**
	 * General approach: The numbers for any given "n" are a row of the triangle-numbers. These
	 * numbers are symmetrical, increase towards the center, and the first number is always "1".
	 * Hence, for each row "n", we begin with the second number, and calculate numbers moving
	 * towards the center. As soon as we find a number > 1000000, we can calculate how many numbers
	 * in this row meet the criterion.
	 */
	private static final long limit = 1000000;
	
	public static void main(String[] args) {
		long total = 0;
		for (long n = 2; n <= 100; n++) {
			for (long r = 2; r <= n/2; r++) {
				long numCombinations = combinations(n, r);
				if (numCombinations > limit) {
					long numberOnThisRow = n + 1 - 2 * r;
					total += numberOnThisRow;
					break;
				}
			}
		}
		System.out.println("Answer = " + total);
	}

	private static long combinations(long n, long r) {
		long combinations = 1;
		for (long i = 1; i <= r; i++) {
			combinations = combinations * (n+1-i) / i;
		}
		return combinations;
	}
}
