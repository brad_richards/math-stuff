package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Digits;

public class Problem206 {
	public static void main(String[] args) {
		// We are calculating X^2. Last digit of X and last 2 digits of answer are zero.
		// Therefore, reduce problem by dividing X by 10, and X^2 by 100.
		// Last digit of Y^2 is 9, so last digit of X is either 3 or 7
		// Y must be betwen 1.09 * 10^8 and 1.4 * 10^8
		long val = 109000000l;
		while (val < 140000000l) {
			long y = val + 3;
			long ySquared = y * y;
			int[] digits = Digits.toDigits(ySquared);
			boolean isAnswer = true;
			for (int i = 1; isAnswer == true && i <= 9; i++) {
				isAnswer = isAnswer && digits[(i-1)*2] == i;
			}
			if (isAnswer) System.out.println(y);
			
			y = val + 7;
			ySquared = y * y;
			digits = Digits.toDigits(ySquared);
			isAnswer = true;
			for (int i = 1; isAnswer == true && i <= 9; i++) {
				isAnswer = isAnswer && digits[(i-1)*2] == i;
			}
			if (isAnswer) System.out.println(y*10);
			
			val += 10;
		}
	}
}
