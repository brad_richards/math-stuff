package hackerRank.projectEuler;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem100HR {
	/**
	 * Analysis:
	 * 
	 * 1. a * (a+1) / b * (b+1) = P / Q
	 * 
	 * 2. Therefore (b+1)/(a+1) is just over sqrt(Q/P) and b/a is just under. If
	 * (Q/P) is a perfect square (e.g., 4), then no solution is possible.
	 * 
	 * The solutions appear cyclically. For the original Project Euler Problem
	 * (P/Q = 1/2), the ratios between solutions asymptotically approaches
	 * 5.8284271. Therefore, after finding each solutions, we can skip many
	 * numbers, to approach where the next solution must be.
	 * 
	 * In this more generalized problem, we dynamically generate this ratio, as
	 * we develop solutions.
	 */
	public static void main(String[] args) {
		long[][] testCases = readData();
		for (long[] testCase : testCases) {
			solve(testCase[0], testCase[1], testCase[2]);
		}
	}

	public static long[][] readData() {
		Scanner s = new Scanner(System.in);
		int num = s.nextInt();
		long[][] testCases = new long[num][3];
		for (int row = 0; row < num; row++) {
			for (int col = 0; col < 3; col++) {
				testCases[row][col] = s.nextLong();
			}
		}
		s.close();
		return testCases;
	}

	public static void solve(long p, long q, long d) {
		ArrayList<Long> solutions = new ArrayList<Long>();
		double solutionSpacing = 1.0;

		double sqrt2 = Math.sqrt(q / (double) p);

		// Special case: perfect squares have no solution
		long intSqrt = Math.round(sqrt2);
		if (intSqrt * intSqrt == q / p) {
			System.out.println("No solution");
			return;
		}

		for (long b = startSearch(p, q); b < Long.MAX_VALUE; b++) {
			long a = (long) (b / sqrt2);
			long numerator = a * (a + 1);
			long denominator = b * (b + 1);

			// Test for a solution
			if ((q * numerator) == (p * denominator)) {
				// System.out.print("Answer = " + (b + 1) + " of which blue " +
				// (a + 1));
				if ((b + 1) >= d) { // done
					System.out.println((a + 1) + " " + (b + 1));
					break;
				} else { // not yet big enough
					solutions.add(new Long(b));
					b = incSearch(solutions, b);
				}
			}
		}
	}

	/**
	 * Determine where to start looking for the first solution
	 */
	public static long startSearch(long p, long q) {
		long pMin = Math.min(p, q - p);
		return 2 * q / pMin - 1;
	}

	/**
	 * Given the current solutions, determine where to look for the next
	 * solution
	 */
	public static long incSearch(ArrayList<Long> solutions, long lastValue) {
		int size = solutions.size();
		if (size == 1) { // Only one solution; no ratio possible
			return lastValue;
		} else if (size == 2) {
			// Only two solutions; ratio possible, but no quality measurement.
			// Assume 1 digit accuracy; subtract 1 to avoid passing next
			// solution
			double ratio = solutions.get(1) / (double) solutions.get(0);
			long lRatio = ((long) ratio) - 1;
			if (lRatio <= 1) {
				return lastValue; // useless ratio; unlikely to happen
			} else {
				return lastValue * lRatio;
			}
		} else {
			// At least three solutions; we can compare the last three
			// solutions. They will be converging, so if we take the difference
			// between the two calculated ratios and decudt this from the last
			// ratio, this gives us a safe underestimate for the ratio to use
			// when skipping to the next search area.
			double ratio1 = solutions.get(size - 2) / (double) solutions.get(size - 3);
			double ratio2 = solutions.get(size - 1) / (double) solutions.get(size - 2);
			double useRatio = ratio2 - (ratio1 - ratio2);
			return (long) (lastValue * useRatio);
		}
	}

}
