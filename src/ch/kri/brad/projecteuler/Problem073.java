package ch.kri.brad.projecteuler;

import java.util.ArrayList;

import ch.kri.brad.utility.Primes;

public class Problem073 {

	public static void main(String[] args) {
		long fractionCount = 0;

		Primes.generatePrimeFactorsErastothenes(12000);
		for (int i = 3; i < Primes.getPrimeFactorSieve().size(); i++) {
			int n = i + 2; // Actual value being checked, from 5 through 12000 inclusive
			int oneThird = n / 3 + 1; // ceiling
			int oneHalf = (n % 2 == 0) ? n / 2 - 1 : n / 2; // floor
			
			ArrayList<Integer> primeFactors = Primes.getPrimeFactorSieve().get(i);
			if (primeFactors == null) { // prime number -- all values will work
				fractionCount += oneHalf + 1 - oneThird;
			} else { // not a prime number - look for values that have no common divisor with n
				for (int m = oneThird; m <= oneHalf; m++) {
					ArrayList<Integer> primeFactorsM = Primes.getPrimeFactorSieve().get(m-2);
					if (primeFactorsM == null || disjoint(primeFactors, primeFactorsM)) fractionCount++;
				}
			}
		}
		System.out.println(fractionCount);
	}

	private static boolean disjoint(ArrayList<Integer> x, ArrayList<Integer> y) {
		boolean disjoint = true;
		for (int i = 0; i < x.size() && disjoint; i++) {
			for (int j = 0; j < y.size() && disjoint; j++) {
				if (x.get(i).equals(y.get(j))) disjoint = false;
			}
		}
		return disjoint;
	}
	
}
