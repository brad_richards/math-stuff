package kattis.g_calculations;

import java.util.Scanner;

/**
 * First, read the two strings
 * 
 * In a loop:
 *   - Use the substring method to get the next three characters of the number-string
 *   - Add the correct character from the encoding string to the result
 */
public class pokechat {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String encodingString = in.nextLine();
		String numbers = in.nextLine();
		
		String result = "";
		int start = 0;
		while ( start < numbers.length() ) {
			String number = numbers.substring(start, start+3);
			int num = Integer.parseInt(number);
			result += encodingString.charAt(num-1);
			start += 3;
		}
		System.out.println(result);
	}

}
