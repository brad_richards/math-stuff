package ch.kri.brad.utility;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PythagoreanTriplesTest {
	public static final int[][] triples100 = { { 3, 4, 5 }, { 5, 12, 13 }, { 8, 15, 17 },
			{ 7, 24, 25 }, { 20, 21, 29 }, { 12, 35, 37 }, { 9, 40, 41 }, { 28, 45, 53 },
			{ 11, 60, 61 }, { 33, 56, 65 }, { 16, 63, 65 }, { 48, 55, 73 }, { 36, 77, 85 },
			{ 13, 84, 85 }, { 39, 80, 89 }, { 65, 72, 97 } };

	public static final int numTriples300 = 47; // Number of triples with C <= 300

	@Test
	public void test100() {
		int[][] result = PythagoreanTriples.pythagoreanTriples(100);
		assertTrue(result.length == triples100.length);
		for (int i = 0; i < result.length; i++) {
			int[] correctTriple = triples100[i];
			int[] resultTriple = result[i];
			for (int j = 0; j < 3; j++) {
				assertTrue(correctTriple[j] == resultTriple[j]);
			}
		}
	}

	@Test
	public void test300() {
		int[][] result = PythagoreanTriples.pythagoreanTriples(300);
		assertTrue(result.length == numTriples300);
	}
}
