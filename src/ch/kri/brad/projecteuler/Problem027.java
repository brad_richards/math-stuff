package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Primes;



public class Problem027 {
	private static boolean[] primeSieve; // Position 0 = integer 2
	
	public static void main(String[] args) {
		// Generate far too many primes
		Primes.generatePrimesErastothenes(1000000);
		primeSieve = Primes.getRawSieve();
		
		long bestNumPrimes = 0;
		long bestA = 0;
		long bestB = 0;
		
		for (long a = -999; a <= 999; a++) {
			for (long b = -999; b <= 999; b++) {
				boolean isPrime = true;
				long n = 0;
				while (isPrime) {
					long value = n * n + a * n + b;
					isPrime = value >= 2 && primeSieve[(int) value-2];
					if (isPrime) n++;
				}
				if (n > bestNumPrimes) {
					bestNumPrimes = n;
					bestA = a;
					bestB = b;
					System.out.println(n + ", " + a + ", " + b);
				}
			}
		}
		
		System.out.println("Answer = " + bestA * bestB);
	}

}
