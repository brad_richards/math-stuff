package AdventOfCode.aoc_2023.day_02;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Problem2 {

	public static void main(String[] args) throws IOException {
		List<List<Map<Color, Integer>>> games = Problem1.readInput();
		List<Map<Color, Integer>> gameMaxValues = Problem1.getMaxValues(games);
		
		// For each game, calculate the "power". Sum the powers.
		long sum = 0;
		for (Map<Color, Integer> game : gameMaxValues) {
			long power = 1;
			for (Integer value : game.values()) power *= value;
			sum += power;
		}
		
		System.out.println(sum);
	}

}
