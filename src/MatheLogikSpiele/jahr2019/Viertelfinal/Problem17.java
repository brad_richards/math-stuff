package MatheLogikSpiele.jahr2019.Viertelfinal;

import java.util.ArrayList;

public class Problem17 {
	public static class Square {
		int side, x, y;
		public Square(int side, int x, int y) {
			this.side = side;
			this.x = x;
			this.y = y;
		}
	}
	
	private static ArrayList<Square> best;
	private static ArrayList<Square> current = new ArrayList<>();

	public static void main(String[] args) {
		// Initialize program
		
		// Representation of the rectangle: true = covered, false = uncovered
		boolean[][] rect = new boolean[37][72];
		for (int row = 0; row < rect.length; row++) {
			for (int col = 0; col < rect[0].length; col++) {
				rect[row][col] = false;
			}
		}
		
		// Call recursive method, to hunt for solutions
		search(rect);
		
		// Display best solution found
		System.out.println("Best solution found consists of " + best.size());
		System.out.println(" squares of sizes: ");
		for (Square s : best) System.out.print(s.side + " ");
		System.out.println();
	}
	
	public static void search(boolean[][] rect) {
//		System.out.println("Entering search, current rectangle is:\n");
//		printRect(rect);
		
		int x = -1;
		int y = -1;
		// Find top-left open square in rect
		for (int row = 0; x == -1 && row < rect.length; row++) {
			for (int col = 0; y == -1 && col < rect[0].length; col++) {
				if (!rect[row][col]) {
					y = row;
					x = col;
				}
			}
		}

//		System.out.println("\nPlacing square at row " + y + " col " +  x + "\n");
		
		// If open square found && current.length < best.length-1
		if (x != -1 && (best == null || current.size() < (best.size() -1))) {
		
			// Find maximum size of square that can be placed at this location
			int rightX = x;
			for (int col = x; col < rect[0].length && !rect[y][col]; col++) {
				rightX = col;
			}
			int bottomY = y;
			for (int row = y; row < rect.length && !rect[row][x]; row++) {
				bottomY = row;
			}
			int maxSize = Math.min(rightX+1-x, bottomY+1-y);
		
			// Loop through possible square sizes; start at max, to find better solutions sooner
			for (int size = maxSize; size >= 1; size--) {
		   		// Copy rect and place square into copy
				boolean[][] rectCopy = cloneRect(rect);
				for (int row = y; row <= y + size - 1; row++) {
					for (int col = x; col <= x + size - 1; col++) {
						assert(!rectCopy[row][col]);
						rectCopy[row][col] = true;
					}
				}
				
		   		// Append square to current
//				System.out.println("Placing square of size " + size + " \n");
				
				current.add(new Square(size, x, y));
		
		   		// Recurse
				search(rectCopy);
		
		   		// Remove square from current
				current.remove(current.size()-1);
			}
			
		} else if (x == -1) {
			// Else if no open squares: we have found a better solution
			if (best != null) {
				System.out.println("Better solution found! Size " + current.size() + " instead of " + best.size());
				System.out.println(" squares of sizes: ");
				for (Square s : best) System.out.print(s.side + " ");
				System.out.println();
			}
			
			best = (ArrayList<Square>) current.clone();
		}
		// Else current.length >= best.length-1; cannot improve solution, so exit
	}
	
	public static boolean[][] cloneRect(boolean[][] rect) {
		boolean[][] newRect = new boolean[rect.length][rect[0].length];
		for (int col = 0; col < rect.length; col++) {
			for (int row = 0; row < rect[0].length; row++) {
				newRect[col][row] = rect[col][row];
			}
		}
		return newRect;
	}
	
	public static void printRect(boolean[][] rect) {
		for (int row = 0; row < rect.length; row++) {
			for (int col = 0; col < rect[0].length; col++) {
				System.out.print( rect[row][col] ? 'X' : '.' );
			}
			System.out.println();
		}
	}
}
