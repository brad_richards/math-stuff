package AdventOfCode.aoc_2020.day_04;

public class Passport1 {
	private String birthYear;
	private String issueYear;
	private String expiryYear;
	private String height;
	private String hairColor;
	private String eyeColor;
	private String passportID;
	private String countryID;

	public Passport1(String input) {
		String[] fields = input.split(" ");
		for (String field : fields) {
			String[] parts = field.split(":");
			switch (parts[0]) {
			case "byr":
				birthYear = parts[1];
				break;
			case "iyr":
				issueYear = parts[1];
				break;
			case "eyr":
				expiryYear = parts[1];
				break;
			case "hgt": // remove "cm" from end
				height = parts[1];
				break;
			case "hcl":
				hairColor = parts[1];
				break;
			case "ecl":
				eyeColor = parts[1];
				break;
			case "pid":
				passportID = parts[1];
				break;
			case "cid":
				countryID = parts[1];
				break;
			}
		}
	}

	public boolean isValid() {
		return birthYear != null && issueYear != null && expiryYear != null && height != null && hairColor != null
				&& eyeColor != null && passportID != null;
	}
}
