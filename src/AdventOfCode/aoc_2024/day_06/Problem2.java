package AdventOfCode.aoc_2024.day_06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Brute force: create the map of all positions. Then, for each position we move through,
// try placing an obstacle there and see if we walk in a loop of some sort.
public class Problem2 {
    static List<String> lines;
    static int[][] emptyMap;
    static int x, y; // Guard position
    static Orientation orientation;

    public static void main(String[] args) throws Exception {
        readInput();
        inputToMap(lines);
        int initX = x, initY = y; // Save guard's initial position
        Orientation initOrientation = orientation;
        int[][] guardMap = cloneMap(emptyMap);
        guardWanders(guardMap);
        // Set initial position back to 0, because not allowed
        guardMap[initY][initX] = 0;

        // For each position in guardMap that we touched (except the start position)
        // See if adding an obstacle there causes a loop.
        int count = 0;
        for (int i = 0; i < guardMap.length; i++) {
            for (int j = 0; j < guardMap[i].length; j++) {
                if (guardMap[i][j] > 0) {
                    int[][] tempMap = cloneMap(emptyMap);
                    tempMap[i][j] = -1;
                    x = initX;
                    y = initY;
                    orientation = initOrientation;

                    if (guardWanders(tempMap)) {
                        count++;
//                        System.out.println(y + ", " + x);
                    }
                }
            }
        }
        System.out.println(count);
    }

    private static int[][] cloneMap(int[][] map) {
        int[][] newMap = new int[map.length][map[0].length];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                newMap[i][j] = map[i][j];
            }
        }
        return newMap;
    }
    private static boolean guardWanders(int[][] map) {
        // deltaX and deltaY set by guard orientation
        int deltaX = 0, deltaY = 0;
        switch (orientation) {
            case UP -> { deltaX = 0; deltaY = -1; }
            case DOWN -> { deltaX = 0; deltaY = 1; }
            case RIGHT -> { deltaX = 1; deltaY = 0; }
            case LEFT -> { deltaX = -1; deltaY = 0; }
        }
        while ( x+deltaX >= 0 && y+deltaY >= 0 && x+deltaX < map[0].length && y+deltaY < map.length) {
            if (map[y + deltaY][x + deltaX] >= 0) {
                int newValue = orientation.ordinal()+1;
                if (map[y+deltaY][x+deltaX] == newValue) return true; // We have walked in a loop
                x = x + deltaX;
                y = y + deltaY;
                map[y][x] = newValue;
            } else { // rotate without moving
                switch (orientation) {
                    case UP -> {orientation = Orientation.RIGHT; deltaX = 1; deltaY = 0; }
                    case RIGHT -> {orientation = Orientation.DOWN; deltaX = 0; deltaY = 1; }
                    case DOWN -> {orientation = Orientation.LEFT; deltaX = -1; deltaY = 0; }
                    case LEFT -> {orientation = Orientation.UP; deltaX = 0; deltaY = -1; }
                }
            }
        }
        return false;
    }

    private static void inputToMap(List<String> lines) throws Exception {
        emptyMap = new int[lines.size()][lines.get(0).length()];
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            for (int j = 0; j < line.length(); j++) {
                if (line.charAt(j) == '#') {
                    emptyMap[i][j] = -1;
                } else if (line.charAt(j) != '.') {
                    x = j; y = i;
                    emptyMap[y][x] = 1;
                    switch (line.charAt(j)) {
                        case '^' -> orientation = Orientation.UP;
                        case 'v' -> orientation = Orientation.DOWN;
                        case '>' -> orientation = Orientation.RIGHT;
                        case '<' -> orientation = Orientation.LEFT;
                        default -> throw new Exception("Unexpected character " + line.charAt(j));
                    }
                }
            }
        }
    }

    private static void readInput() throws IOException {
        Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
        try (Stream<String> stream = Files.lines(path)) {
            lines = stream.collect(Collectors.toList());
        }
    }
}