package ch.kri.brad.projecteuler;

public class Problem015 {

	/**
	 * The challenge for the problem is the initial analysis:
	 * 
	 * First, all paths are symmetric, i.e. paths come in pairs, reflected across the diagonal of the square.
	 * 
	 * We can then construct the following table
	 * 
	 *    Size
	 *       1   1                    =   1 * 2 =   2 total paths
	 *       2   1 + 2                =   3 * 2 =   6 total paths
	 *       3   1 + 3 + 6            =  10 * 2 =  20 total paths
	 *       4   1 + 4 + 10 + 20      =  70 * 2 = 140 total paths
	 *       5   1 + 5 + 15 + 35 + 70 = 126 * 2 = 252 total paths
	 *       
	 * In this table, the new element in each row is the result from the previous row. Each of the other elements
	 * is the sum of the elements in the row above, in the same column and to the left.
	 * 
	 * Given this analysis, the implementation below is easy enough... 
	 */
	public static void main(String[] args) {
		long[][] nums = new long[20][20];
		long[] paths = new long[20];
		
		// Initialize the first row (size = 1)
		nums[0][0] = 1;
		paths[0] = 2;
		
		// Fill the array from top to bottom; note that row + 1 = size
		for (int row = 1; row < 20; row++) {
			for (int col = 0; col < row; col++) {
				long sum = 0;
				for (int i = 0; i <= col; i++) {
					sum += nums[row-1][i];
				}
				nums[row][col] = sum;
			}
			nums[row][row] = paths[row-1];
			
			// Add up the total paths
			long sum = 0;
			for (int i = 0; i <= row; i++) {
				System.out.print(nums[row][i] + " ");
				sum += nums[row][i];				
			}
			paths[row] = 2 * sum;
			System.out.println();
		}

		System.out.println("Answer = " + paths[19]);
	}
}
