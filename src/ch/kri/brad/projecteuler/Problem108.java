package ch.kri.brad.projecteuler;

/**
 * Any solution is of the form 1 / (n + i) + 1 / x = 1 / n
 * 
 * Where i ranges from 1 to n, and x is whatever value is necessary to make the equation true. Since
 * we are working with integers, this means that the values of i that work are those where
 * 
 * n * (n + i) is divisible by i.
 * 
 * In this solution, we simply count the number of solutions for which this is true. This takes
 * around 130 seconds.
 */
public class Problem108 {

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		long n = 3;
		int numSolutions = -1;
		while (numSolutions < 1000) {
			numSolutions = numSolutions(++n);
			long endTime = System.currentTimeMillis();
			System.out.println("For " + n + " there are " + numSolutions + " solutions "
					+ (endTime - startTime + "ms"));
		}
	}

	public static int numSolutions(long n) {
		int count = 0;
		for (long i = 1; i <= n; i++) {
			if ((n * (n + i)) % i == 0) {
				count++;
			}
		}
		return count;
	}

}
