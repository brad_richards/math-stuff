package ch.kri.brad.utility;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import ch.kri.brad.utility.GenericMath.InvalidTypeException;

public class GenericMathTest {

	@Test
	public void testAdd() {
		try {
			Integer a = new Integer(3);
			Integer b = new Integer(4);
			Integer c = GenericMath.add(a, b);
			assertTrue(c.equals(new Integer(7)));
			
			Double d = new Double(3);
			Double e = new Double(4);
			Double f = GenericMath.add(d, e);
			assertTrue(f.equals(new Double(7)));
			
			BigDecimal g = new BigDecimal(3);
			BigDecimal h = new BigDecimal(4);
			BigDecimal i = GenericMath.add(g, h);
			assertTrue(i.equals(new BigDecimal(7)));
			
		} catch (InvalidTypeException e) {
			fail("unexpected type exception");
		}
	}
	
	@Test
	public void testAverage() {
		try {
			Integer[] ints = new Integer[9];
			for (int i = 0; i < 9; i++) ints[i] = new Integer(i+1);
			Integer avg = GenericMath.average(ints);
			assertTrue(avg.equals(new Integer(5)));
			
			
		} catch (InvalidTypeException e) {
			fail("unexpected type exception");
		}
	}
}
