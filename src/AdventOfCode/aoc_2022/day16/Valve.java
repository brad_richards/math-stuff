package AdventOfCode.aoc_2022.day16;

import java.util.ArrayList;
import java.util.Objects;

public class Valve {
	private final String name;
	private final int flowRate;
	private final ArrayList<Valve> tunnels = new ArrayList<>();
	
	public Valve(String name, int flowRate) {
		this.name = name;
		this.flowRate = flowRate;
	}
	
	public void addTunnel(Valve tunnel) {
		tunnels.add(tunnel);
	}

	public String getName() {
		return name;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public ArrayList<Valve> getTunnels() {
		return tunnels;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Valve other = (Valve) obj;
		return Objects.equals(name, other.name);
	}
}
