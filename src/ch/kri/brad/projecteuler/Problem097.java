package ch.kri.brad.projecteuler;

public class Problem097 {

	/**
	 * Just calculate the last 10 digits, always taking module 10,000,000,000
	 */
	public static void main(String[] args) {
		long value = 28433;
		for (int i = 0; i < 7830457; i++) {
			value = (value * 2) % 10000000000l;
		}
		value = value + 1;
		value = value % 10000000000l;
		System.out.println(value);
	}

}
