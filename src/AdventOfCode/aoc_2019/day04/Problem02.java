package AdventOfCode.aoc_2019.day04;

public class Problem02 {
	private static final int START = 130254;
	private static final int END = 678275;
	private static final int numDigits = 6;

	public static void main(String[] args) {
		int count = 0;
		for (int i = START; i <= END; i++) {
			int[] digits = toDigits(i);
			if (meetsCriteria(digits)) count++;
		}
		System.out.println(count);
	}

	private static int[] toDigits(int in) {
		int[] digits = new int[numDigits];
		for (int pos = 0; pos < numDigits; pos++) {
			int digit = in % 10;
			in /= 10;
			digits[numDigits - pos - 1] = digit;
		}
		return digits;
	}
	
	private static boolean meetsCriteria(int[] digits) {
		boolean ascending = true;
		for (int i = 1; i < numDigits && ascending; i++) {
			ascending = (digits[i-1] <= digits[i]);
		}
		
		// Not worth trying to be clever - there are only 5 possible configurations
		boolean doubleFound =
				digits[0] == digits[1] && digits[1] != digits[2]
						||
				digits[0] != digits[1] && digits[1] == digits[2] && digits[2] != digits[3]
						||
				digits[1] != digits[2] && digits[2] == digits[3] && digits[3] != digits[4]
						||
				digits[2] != digits[3] && digits[3] == digits[4] && digits[4] != digits[5]
						||
				digits[3] != digits[4] && digits[4] == digits[5];
		
		return ascending && doubleFound;
	}
}
