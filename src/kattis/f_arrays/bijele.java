package kattis.f_arrays;

import java.util.Scanner;

/**
 * Define an int[] array containing the correct numbers
 * 
 * Read the input and compare it to the correct numbers
 */
public class bijele {

	public static void main(String[] args) {
		int[] correct = { 1, 1, 2, 2, 2, 8 };
		int[] actual = new int[6];
		
		Scanner in = new Scanner(System.in);
		for (int i = 0; i < actual.length; i++) {
			System.out.print( (correct[i] - in.nextInt()) + " ");
		}
	}

}
