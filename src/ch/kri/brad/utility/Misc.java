package ch.kri.brad.utility;

import java.math.BigDecimal;

public class Misc {
	public static long factorial(long n) {
		long factorial = 1;
		for (long i = 2; i <= n; i++) {
			factorial *= i;
		}
		return factorial;
	}
	
	public static BigDecimal bigFactorial(long n) {
		BigDecimal factorial = BigDecimal.ONE;
		for (long i = 2; i <= n; i++) {
			factorial = factorial.multiply(new BigDecimal(i));
		}
		return factorial;
	}
}
