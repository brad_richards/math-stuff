package AdventOfCode.aoc_2015.day06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static int[][] lights = new int[1000][1000];
	
	private static enum Action { OFF, TOGGLE, ON }
	
	private static class Rect {
		int X1, X2, Y1, Y2;
		Action action;
		
		public Rect(String in) {
			String[] parts = in.split(" ");
			if (parts[0].equals("toggle")) action = Action.TOGGLE;
			else if (parts[1].equals("on")) action = Action.ON;
			else action = Action.OFF;
			
			int offset = (action == Action.TOGGLE) ? 1 : 2;
			
			String[] coords = parts[offset].split(",");
			X1 = Integer.parseInt(coords[0]);
			Y1 = Integer.parseInt(coords[1]);
			
			coords = parts[offset+2].split(",");
			X2 = Integer.parseInt(coords[0]);
			Y2 = Integer.parseInt(coords[1]);
		}
	}

	public static void main(String[] args) throws Exception {
		readInput();
		for (String line : lines) {
			Rect r = new Rect(line);
			doAction(r);
		}
		
		int count = 0;
		for (int y = 0; y < lights.length; y++) {
			for (int x = 0; x < lights[0].length; x++) {
				count += lights[y][x];
			}
		}
		
		System.out.println("Part 1 answer = " + count);
	}
	
	private static void doAction(Rect r) {
		for (int y = r.Y1; y <= r.Y2; y++) {
			for (int x = r.X1; x <= r.X2; x++) {
		
				if (r.action == Action.ON) lights[y][x]++;
				else if  (r.action == Action.OFF) lights[y][x] = Math.max(0, lights[y][x]-1);
				else lights[y][x] += 2;
			}
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}