package ch.kri.brad.projecteuler.Problem098;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import AdventOfCode.aoc_2018.day10.Dot;

public class InputReader {
	private static final String INPUT_FILE = "input.txt";

	public ArrayList<String> readWords() {
		ArrayList<String> words = new ArrayList<>();
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String s = in.nextLine();
				parseWords(words, s);
			}
		}
		return words;
	}
	
	private void parseWords(ArrayList<String> words, String line) {
		String[] protoWords = line.split(",");
		for (int i = 0; i < protoWords.length; i++) {
			String word = protoWords[i].trim();
			word = word.substring(1, word.length()-1);
			words.add(word);
		}
	}
}
