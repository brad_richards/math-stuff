package ch.kri.brad.projecteuler.Problem093;

import java.util.Arrays;

/**
 * The options for the equations are much simpler than described. If we allow
 * all permutations of digits, and all permutations of operations and brackets,
 * we will have a lot of redundancy. We can eliminate a lot of this redundancy
 * by eliminating brackets entirely, and using only two orders of operation. If
 * we have the equation:
 * 
 * 1 a 2 b 3 c 4
 * 
 * where a, b, and c are operators, we need only three orders of execution to
 * generate all possible values: abc, bac, and acb. All other combinations
 * produce redundant results.
 * 
 * First attempt: Given the above restrictions, generate all permutations of
 * digits and all permutations of operators. Then, for each generated equation,
 * perform the three orders of evaluation discussed. This yields 4! * 4³ = 1536
 * permutations for each set of 4 digits. There are 10!/6!*4! = 2100
 * combinations of digits. Evaluating around 1512 * 210 * 2 = 600000 equations
 * is plenty fast enough - no need for further optimization.
 */
public class Problem093 {
	private static final int[] DIGITS = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public static void main(String[] args) {
		int[] bestDigitSet = null;
		int bestAnswerSoFar = -1;
		for (int[] digits : generateDigitCombinations()) {
			System.out.print("Digits ");
			for (int digit : digits)
				System.out.print(digit + " ");

			// Create boolean array, to track generated results
			boolean[] generatedValues = new boolean[maxPossibleValue(digits)];
			Arrays.fill(generatedValues, false);

			// Generate all results for this combination of digits
			for (int[] digitOrder : generateDigitPermuations(digits)) {
				for (Operation[] ops : Operation.getOperationPermutations()) {
					// creating equation objects is actually a waste of time -
					// procedural programming would be faster.
					Equation eq = new Equation(digitOrder, ops);
					int val = eq.evaluate1();
					if (val >= 0) generatedValues[val] = true;
					val = eq.evaluate2();
					if (val >= 0) generatedValues[val] = true;
					val = eq.evaluate3();
					if (val >= 0) generatedValues[val] = true;
				}
			}

			// Find the first missing result
			int firstMissing = -1;
			for (int i = 0; firstMissing < 0 && i < generatedValues.length; i++) {
				if (!generatedValues[i]) firstMissing = i;
			}
			System.out.println(" has first missing result at " + firstMissing);
			if (firstMissing > bestAnswerSoFar) {
				bestAnswerSoFar = firstMissing;
				bestDigitSet = digits;
			}
		}

		System.out.print("Best digit set is ");
		for (int digit : bestDigitSet)
			System.out.print(digit + " ");
		System.out.println();
	}

	private static int[][] generateDigitCombinations() {
		int[][] digitCombos = new int[210][];
		int pos = 0;
		for (int i = 0; i <= 6; i++) {
			for (int j = i + 1; j <= 7; j++) {
				for (int k = j + 1; k <= 8; k++) {
					for (int l = k + 1; l <= 9; l++) {
						int[] digits = new int[4];
						digits[0] = i;
						digits[1] = j;
						digits[2] = k;
						digits[3] = l;
						digitCombos[pos++] = digits;
					}
				}
			}
		}
		return digitCombos;
	}

	private static int[][] generateDigitPermuations(int[] digits) {
		int[][] digitPerms = new int[24][];
		int pos = 0;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i != j) for (int k = 0; k < 4; k++) {
					if (i != k && j != k) for (int l = 0; l < 4; l++) {
						if (i != l && j != l && k != l) {
							int[] digitPerm = new int[4];
							digitPerm[0] = digits[i];
							digitPerm[1] = digits[j];
							digitPerm[2] = digits[k];
							digitPerm[3] = digits[l];
							digitPerms[pos++] = digitPerm;
						}
					}
				}
			}
		}
		return digitPerms;
	}

	/**
	 * Determine the required array size. This is the product of all digits
	 * (unless they include 0 or 1), plus one (because we need all values from
	 * zero to the max)l
	 */
	private static int maxPossibleValue(int[] digits) {
		int value = 1;
		for (int digit : digits) {
			if (digit > 1)
				value *= digit;
			else if (digit == 1) value *= 2; // too much is better than not
												// enough
		}
		return ++value;
	}
}
