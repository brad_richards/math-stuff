package ch.kri.brad.utility;

import java.math.BigInteger;
import java.util.ArrayList;

public class Digits {
	public static int numDigits(long value) {
		int numDigits = 1;
		if (value != 0) numDigits = (int) (Math.log10(Math.abs(value))+1);
		return numDigits;
	}

	public static int numDigits(BigInteger value) {
		int numDigits = 1;
		while (value.compareTo(BigInteger.TEN) >= 0) {
			value = value.divide(BigInteger.TEN);
			numDigits++;
		}
		return numDigits;
	}
	
	public static int[] toDigits(long value) {
		long temp = Math.abs(value);
		int numDigits = (int) (Math.log10(temp)+1);
		int[] digits = new int[numDigits];
		for (int i = 1; i <= numDigits; i++) {
			int digit = (int) (temp % 10);
			temp = temp / 10;
			digits[numDigits - i] = digit;
		}
		return digits;
	}
	
	public static int sumOfDigits(long value) {
		int sum = 0;
		for (int d : toDigits(value)) {
			sum += d;
		}
		return sum;
	}
	
	public static int sumOfDigits(BigInteger value) {
		int sum = 0;
		for (int d : toDigits(value)) {
			sum += d;
		}
		return sum;
	}
	
	public static long fromDigits(int[] digits) {
		long temp = 0;
		for (int digit : digits) {
			temp = temp * 10 + digit;
		}
		return temp;
	}
	
	// BigInteger variants
	
	public static ArrayList<Integer> toDigits(BigInteger value) {
		BigInteger temp = value.abs();
		ArrayList<Integer> digits = new ArrayList<Integer>();
		while(temp.compareTo(BigInteger.ZERO) > 0) {
			int digit = temp.mod(BigInteger.TEN).intValue();
			digits.add(0, digit);
			temp = temp.divide(BigInteger.TEN);
		}
		if (digits.size() == 0) digits.add(0);
		return digits;
	}
	
	public static ArrayList<Integer> toReversedDigits(BigInteger value) {
		BigInteger temp = value.abs();
		ArrayList<Integer> digits = new ArrayList<Integer>();
		while(temp.compareTo(BigInteger.ZERO) > 0) {
			int digit = temp.mod(BigInteger.TEN).intValue();
			digits.add(digit);
			temp = temp.divide(BigInteger.TEN);
		}
		if (digits.size() == 0) digits.add(0);
		return digits;
	}
	
	public static BigInteger fromDigits(ArrayList<Integer> digits) {
		BigInteger value = BigInteger.valueOf(0);
		for (int digit : digits) {
			value = value.multiply(BigInteger.TEN);
			value = value.add(BigInteger.valueOf(digit));
		}
		return value;
	}
	
}
