package ch.kri.brad.projecteuler;

public class Problem009 {

	public static void main(String[] args) {
		int answer = 0;
		int targetValue = 1000;
		for (int a = 1; answer == 0 && a <= targetValue-2; a++) {
			int aSq = a*a;
			for (int b = 1; answer == 0 && b <= targetValue-2; b++) {
				int bSq = b*b;
				int sumSq = aSq+bSq;
				
				// Is this a square of an integer?
				int c = (int) Math.sqrt(sumSq);
				if ( (c*c) == sumSq) {
					if ((a + b + c) == targetValue) {
						answer = a * b * c;
					}
				}
			}
		}
		
		System.out.println("Answer = " + answer);
	}

}
