package ch.kri.brad.projecteuler;
public class Problem034 {

	public static void main(String[] args) {
		int answer = 0;
		for (int i = 3; i <= 2540160; i++) {
			if (i == getDigitsFactorial(i)) {
				answer += i;
				System.out.println(i);
			}
		}
		System.out.println("Answer = " + answer);
	}

	private static int getDigitsFactorial(int in) {
		int value = 0;
		while (in > 0) {
			int digit = in % 10;
			in = in / 10;

			int factorial = 1;
			for (int i = 2; i <= digit; i++)
				factorial *= i;
			value += factorial;
		}
		return value;
	}
}
