package AdventOfCode.aoc_2020.day_17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	private List<String> lines;
	private boolean[][] grid;
	
	public InputReader(String fileName) throws IOException {
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		parseInput();
	}

	/**
	 * Note: the input grids are always square.
	 */
	private void parseInput() {
		int size = lines.size();
		grid = new boolean[size][size];
		
		for (int y = 0; y < size; y++) {
			String line = lines.get(y);
			for (int x = 0; x < size; x++) {
				grid[y][x] = (line.charAt(x) == '#');
			}
		}
	}
	
	public boolean[][] getGrid() {
		return grid;
	}
}
