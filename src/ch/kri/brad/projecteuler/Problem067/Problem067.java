package ch.kri.brad.projecteuler.Problem067;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;

/**
 * We follow paths from top to bottom. The key insight: Two paths lead to each node in the next
 * layer; it is only necessary to keep the path with the highest value. Hence, the number of partial
 * paths at any point is equal to the number of nodes in the last layer processed.
 * 
 * Rather than actually building path structures (remembering the left-right decisions along the way),
 * we abbreviate the process by just adding a "pathValue" to each node, traversing the structure from
 * top-to-bottom, and calculating these values. Once we have traversed the structure, we search for
 * the maximum value on the bottom row - and that is our answer.
 * 
 * We represent a path as a boolean array: false means left, true means right.
 */
public class Problem067 {
	private static final String FILE_NAME = "p067_triangle.txt";
	private Node root = null; // The root of the triangle
	private int rowsInTriangle;

	public static void main(String[] args) {
		Problem067 p = new Problem067();
		p.go();
	}
	
	
	
	public void go() {
		readFile();
		// printTriangle();
		
		Node firstInRow = root;
		// process each row
		while (firstInRow != null) {
			Node nodeInRow = firstInRow;
			// process each node in this row
			while (nodeInRow != null) {
				int maxParentValue = 0;
				
				Node leftParent = nodeInRow.leftParent; 
				if (leftParent != null && leftParent.pathValue > maxParentValue) maxParentValue = leftParent.pathValue; 

				Node rightParent = nodeInRow.rightParent; 
				if (rightParent != null && rightParent.pathValue > maxParentValue) maxParentValue = rightParent.pathValue;
				
				nodeInRow.pathValue = nodeInRow.value + maxParentValue;
				
				nodeInRow = nodeInRow.rightSibling;
			}
			firstInRow = firstInRow.leftChild;
		}
		
		// Find the largest value
		Node n = getFirstNodeOnRow(rowsInTriangle);
		int maxValue = 0;
		while (n != null) {
			if (n.pathValue > maxValue) maxValue = n.pathValue;
			n = n.rightSibling;
		}
		System.out.println("Answer = " + maxValue);
	}

	/**
	 * Method to read the input file and create the internal representation of nodes
	 */
	private void readFile() {
		URL url = getClass().getResource(FILE_NAME);
		File f = new File(url.getPath());
		if (f.exists()) {
			try (BufferedReader in = new BufferedReader(new FileReader(f))) {
				String line = in.readLine();
				while (line != null) {
					String[] nodeValues = line.split(" ");
					rowsInTriangle = nodeValues.length; // 1-based
					if (rowsInTriangle == 1) { // Special: defining the root node
						root = new Node(Integer.parseInt(nodeValues[0]));
					} else { // Every subsequent row
						// Get the first node on the parent row
						Node currentParent = getFirstNodeOnRow(rowsInTriangle - 1);
						int currentValue = 0;
						Node currentChild = new Node(Integer.parseInt(nodeValues[currentValue]));
						while (currentParent != null) {
							currentParent.leftChild = currentChild;
							currentChild.rightParent = currentParent;
							Node nextChild = new Node(Integer.parseInt(nodeValues[++currentValue]));
							currentChild.rightSibling = nextChild;
							currentParent.rightChild = nextChild;
							nextChild.leftParent = currentParent;

							// Move one position to the right, and repeat
							currentParent = currentParent.rightSibling;
							currentChild = nextChild;
						}
					}
					line = in.readLine();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Search the so-far existing structure, and return the left-most node on the given (1-based)
	 * row
	 */
	private Node getFirstNodeOnRow(int row) {
		int rowsToDescend = row - 1;
		Node currentNode = root;
		while (rowsToDescend > 0) {
			currentNode = currentNode.leftChild;
			rowsToDescend--;
		}
		return currentNode;
	}
	
	private void printTriangle() {
		Node n = root;
		while (n != null) {
			Node n2 = n;
			while (n2 != null) {
				System.out.print(n2.value + " ");
				n2 = n2.rightSibling;
			}
			System.out.println();
			n = n.leftChild;
		}
	}

	/**
	 * Class to represent one node in the tree. For purposes of this problem, we only need to move
	 * downwards (leftChild, rightChild). However, the ability to move across a row (rightSibling)
	 * helps us traverse the structure. For completeness, we also keep track of the parent nodes.
	 */
	private static class Node {
		Node leftChild = null;
		Node rightChild = null;
		Node leftParent = null;
		Node rightParent = null;
		Node rightSibling = null;
		int pathValue = 0; // See explanation at top of class
		int value;

		public Node(int value) {
			this.value = value;
		}
	}
}
