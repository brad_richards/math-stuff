package AdventOfCode.aoc_2021.day08;

public class Data {
	String[] patterns;
	String[] outputs;
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (String s : patterns) sb.append(s + " ");
		sb.append("| ");
		for (String s : outputs) sb.append(s + " ");
		return sb.toString();
	}
}
