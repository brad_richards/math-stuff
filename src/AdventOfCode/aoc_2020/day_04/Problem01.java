package AdventOfCode.aoc_2020.day_04;

import java.io.IOException;
import java.util.ArrayList;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader1 ir = new InputReader1("input.txt");
		ArrayList<Passport1> passports = ir.getPassports();
		
		int count = 0;
		for (Passport1 p : passports) {
			if (p.isValid()) count++;
		}
		System.out.println(count + " of " + passports.size() + " passports are valid");
	}

}
