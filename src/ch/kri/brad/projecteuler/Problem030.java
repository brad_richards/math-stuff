package ch.kri.brad.projecteuler;
import ch.kri.brad.utility.Digits;


public class Problem030 {
	private static final int n = 5;
	
	/**
	 * Generic solution that should work for any value n > 1, as long as the limit is representable as a long.
	 * 
	 * Note: The limit is the maximum value that we need to check, which is n * 9^n 
	 * 
	 */
	public static void main(String[] args) {
		long limit = n * power(9l, n);
		
		long sum = 0;
		for (int value = 2; value <= limit; value++) {
			if (value == calcValue(Digits.toDigits(value))) {
				sum += value;
				System.out.println("Value = " + value);
			}
		}
		System.out.println("Answer = " + sum);
	}

	/**
	 * power - calculate the integer power of a long
	 */
	private static long power(long base, int exponent) {
		long value = base;
		for (int i = 1; i < exponent; i++) {
			value *= base;
		}
		return value;
	}
	
	/**
	 * Calculate the sum of the powers of the digits
	 */
	private static long calcValue(int[] digits) {
		long value = 0;
		for (int digit : digits) {
			value += power(digit, n);
		}
		return value;
	}
}
