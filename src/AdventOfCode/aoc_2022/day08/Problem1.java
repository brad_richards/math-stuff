package AdventOfCode.aoc_2022.day08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static int[][] trees;
	
	public static void main(String[] args) throws Exception {
		readInput();
		createTrees();
		
		// Number of trees visible is initially all the outside trees
		int visible = 2 * trees.length + 2 * trees[0].length - 4;
		visible += interiorTrees();
		System.out.println("Part 1: " + visible);
		
		int maxScenic = 0;
		for (int row = 1; row < trees.length-1; row++) {
			for (int col = 1; col < trees[0].length-1; col++) {
				int tmpScenic = scenic(row, col);
				if (tmpScenic > maxScenic) maxScenic = tmpScenic;
			}
		}
		System.out.println("Part 2: " + maxScenic);
	}
	
	private static int scenic(int row, int col) {
		// Check left
		int numLeft = 0;
		for (int c = col-1; c >= 0; c--) {
			numLeft++;
			if (trees[row][c] >= trees[row][col]) break;
		}
		// Check right
		int numRight = 0;
		for (int c = col+1; c < trees[0].length; c++) {
			numRight++;
			if (trees[row][c] >= trees[row][col]) break;
		}
		// Check top
		int numTop = 0;
		for (int r = row-1; r >= 0; r--) {
			numTop++;
			if (trees[r][col] >= trees[row][col]) break;
		}
		// Check bottom
		int numBottom = 0;
		for (int r = row+1; r < trees.length; r++) {
			numBottom++;
			if (trees[r][col] >= trees[row][col]) break;
		}
		
		int scenic = numLeft * numRight * numTop * numBottom;
//		System.out.format("Tree at %d, %d has scenic rating %d%n", row, col, scenic);
		return scenic;
	}	
	
	private static void createTrees() {
		trees = new int[lines.size()][lines.get(0).length()];
		for (int row = 0; row < lines.size(); row++) {
			String line = lines.get(row);
			for (int col = 0; col < line.length(); col++) {
				trees[row][col] = line.charAt(col) - '0';
			}
		}
	}
	
	private static int interiorTrees() {
		int answer = 0;
		for (int row = 1; row < trees.length-1; row++) {
			for (int col = 1; col < trees[0].length-1; col++) {
				if (isVisible(row, col)) answer++;
			}
		}
		return answer;
	}
	
	private static boolean isVisible(int row, int col) {
		// Check left
		boolean blockedLeft = false;
		for (int c = 0; c < col; c++) if (trees[row][c] >= trees[row][col]) blockedLeft = true;
		// Check right
		boolean blockedRight = false;
		for (int c = col+1; c < trees[0].length; c++) if (trees[row][c] >= trees[row][col]) blockedRight = true;
		// Check top
		boolean blockedTop = false;
		for (int r = 0; r < row; r++) if (trees[r][col] >= trees[row][col]) blockedTop = true;
		// Check bottom
		boolean blockedBottom = false;
		for (int r = row+1; r < trees.length; r++) if (trees[r][col] >= trees[row][col]) blockedBottom = true;
		
		return !blockedLeft || !blockedRight || !blockedTop || !blockedBottom;
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
