package LeetCode.reverse_nodes_in_k_group;

public class Solution {
    public static void main(String[] args) {
        ListNode head = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));


        Solution s = new Solution();
        ListNode newList = s.reverseKGroup(head, 3);

        ListNode cursor = newList;
        while (cursor != null) {
            System.out.print(cursor.val + " ");
            cursor = cursor.next;
        }
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        if (k < 2) return head; // Eliminate edge cases

        ListNode cursor = head;
        ListNode newHead = null;
        ListNode newCursor = null;
        while (cursor != null) {
            ListNode[] nodeRefs = new ListNode[k];
            // Get the next k nodes
            for (int i = 0; cursor != null && i < k; i++) {
                nodeRefs[i] = cursor;
                cursor = cursor.next;
            }

            if (newHead == null) { // First batch - set the new head
                if (nodeRefs[k-1] == null) {
                   newHead = head; // Whole list remains unchanged
                } else {
                    newHead = nodeRefs[k-1]; // Will be new first node
                    for (int i = k-2; i >= 0; i--) {
                        nodeRefs[i+1].next = nodeRefs[i];
                    }
                    newCursor = nodeRefs[0];
                    newCursor.next = null; // Might be end of list
                }
            } else { // All subsequent batches
                if (nodeRefs[k-1] == null) {
                    newCursor.next = nodeRefs[0]; // Incomplete set, do not reverse
                } else {
                    for (int i = k-1; i >= 0; i--) {
                        newCursor.next = nodeRefs[i];
                        newCursor = newCursor.next;
                    }
                    newCursor.next = null; // might be end of list
                }
            }
        }
        return newHead;
    }
}
