package ch.kri.brad.projecteuler;

/**
 * First approach: Take numbers, calculate their digit-sum, raise that sum to various
 * powers and see if we get the number back. Even with some optimizaitons, way too slow.
 * 
 * See solution 119b
 */
public class Problem119a {

	public static void main(String[] args) {
		int count = 1;
		long minPower = 1;
		for (long i = 10; i < Long.MAX_VALUE; i++) {
			long digitSum = sumOfDigits(i);
			if (lastDigitPossible(digitSum, i)) {
				boolean toBig = false;
				for (long j = minPower; j < 20 && !toBig; j++) {
					long value = power(digitSum, j);
					if (value == i) {
						System.out.println(count + ": " + value + " = " + digitSum + "^" + j);
						minPower = j-1;
						count++;
					} else {
						toBig = (value > i);
					}
				}
			}
		}
	}
	
	/**
	 * If n1 is raised to some power, can the result possibly be n2?
	 * We can eliminate a lot of possibilities just by looking at the
	 * last digits.
	 */
	private static boolean lastDigitPossible(long n1, long n2) {
		long d1 = n1 % 10;
		long d2 = n2 % 10;
		
		boolean answer = false;
		if (d1 == d2 && (d1 < 2 || d1 == 5 || d1 == 6)) answer = true;
		else if  (d1 == 3 && (d2 == 3 || d2 == 9 || d2 == 7 || d2 == 1)) answer = true;
		else if (d1 == 4 && ( d2 == 4 || d2 == 6) ) answer = true;
		else if (d1 == 7 && ( d2 == 7 || d2 == 9 || d2 == 3 || d2 == 1)) answer = true;
		else if ( (d1 == 2 || d1 == 8) && (d2 == 2 || d2 == 4 || d2 == 6 || d2 == 8)) answer = true;
		else  if (d2 == 1 || d2 == 9) answer = true;
		return answer;
	}

	private static long sumOfDigits(long n) {
		long sum = 0;
		while (n > 0) {
			sum += n % 10;
			n /= 10;
		}
		return sum;
	}

	private static long power(long n, long e) {
		long prod = 1;
		for (int i = 1; i <= e; i++)
			prod *= n;
		return prod;
	}
}
