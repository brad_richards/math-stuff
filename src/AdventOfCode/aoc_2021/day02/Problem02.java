package AdventOfCode.aoc_2021.day02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import AdventOfCode.aoc_2021.day02.Command.Direction;

public class Problem02 {
	private static List<Command> commands;

	public static void main(String[] args) throws IOException {
		readInput();
		
		long horizontal = 0;
		long depth = 0;
		long aim = 0;
		for (Command c : commands) {
			if (c.direction == Command.Direction.forward) {
				horizontal += c.distance;
				depth += aim * c.distance;
			}
			else if (c.direction == Command.Direction.down) aim += c.distance;
			else if (c.direction == Command.Direction.up) aim -= c.distance;
		}
		
		System.out.println(horizontal * depth);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			commands = stream.map(Problem02::parseCommand).collect(Collectors.toList());
		}
	}
	
	private static Command parseCommand(String in) {
		String[] parts = in.split(" ");
		Command c = new Command();
		c.direction = Direction.valueOf(parts[0]);
		c.distance = Integer.parseInt(parts[1]);
		return c;
	}

}
