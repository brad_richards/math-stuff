package ch.kri.brad.misc;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A pretty rectangle is a rectangle with integer sides whose surface area is equal to its volume.
 * Similarly, a pretty cube is a cube with integer edges whose surface equals its volume. One
 * Olympiad problem was to find all of the pretty rectangles and cubes. This program carries the
 * problem to hyperrectangle arbitrary number of dimensions.
 * 
 * The surface area of a "side" of a hyperrectangle of dimension n is calculated the same as the
 * volume of a hyperrectangle of one fewer dimensions. For example, consider a 3-dimensional block:
 * The area of one "side" of the block is x*y where x and y are two of the dimensions x, y and z of
 * the 3-dimensional block. There are a total of 3 pairs of sides, so the total surface area is 2*xy
 * + 2*xz + 2*yz.
 * 
 * The number of pairs of sides is equal to the dimensions - logically, since each pair of sides
 * omits one dimension. Hence, the surface area of a 4-dimensional hyperrectangle with volume
 * w*x*y*z is 2*wxy + 2*wxz + 2*wyz + 2*xyz.
 * 
 * This program works by trial-and-error, and could be substantially optimized. For example, the
 * value of the smallest dimension never needs to exceed 2 * numDimensions; this is implemented.
 * However, further restrictions can be derived for the second, third, and higher dimensions.
 * 
 * Even better, of course, would be to write a completely different program that is aware of the
 * equations for surface and volume, and uses these equations to impose contraints on possible
 * values; then one merely needs to test if the values are integers.
 * 
 * @author brad
 * 
 */
public class PrettyHypercubes {
	static int maxNumDimensions = -1;
	static int numDimensions;
	static long[] dimensions;
	static ArrayList<String> solutions;
	static final long MAX_DIM_VALUE = 2000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		while (maxNumDimensions <= 1) {
			System.out.println("Calculate pretty hyperrects for what dimension (must be >= 2)?");
			maxNumDimensions = s.nextInt();
		}
		for (numDimensions = 4; numDimensions <= maxNumDimensions; numDimensions++) {
			dimensions = new long[numDimensions];
			solutions = new ArrayList<String>();
			addDimension(1);
			System.out.println("\n\nSolutions for " + numDimensions + " dimensions\n");
			for (String solution : solutions) {
				System.out.println(solution);
			}
		}
	}

	private static void addDimension(int dimension) {
		long maxValue = getMaxValue(dimension);
		if (dimension == numDimensions) { // This is the last dimension - try for solutions
			long minValue = dimensions[dimension - 2];
			for (long dimValue = minValue; dimValue <= maxValue; dimValue++) {
				dimensions[dimension - 1] = dimValue;
				long surfaceArea = surfaceArea();
				long volume = volume();
				if (surfaceArea == volume) {
					String solution = newSolution();
					solutions.add(solution);
				}
			}

		} else { // Generate numbers for this dimension, then recurse for the remaining
			long minValue = 3;
			if (dimension > 1) minValue = dimensions[dimension - 2];
			for (long dimValue = minValue; dimValue <= maxValue; dimValue++) {
				dimensions[dimension - 1] = dimValue;
				addDimension(dimension + 1);
			}
			
		}
	}
	
	private static long getMaxValue(long dimension) {
		long maxValue;
		if (dimension == 1) {
			maxValue = numDimensions*2;
		} else if (dimension == 2) {
			maxValue = 2 * dimensions[0] * (numDimensions-1) / (dimensions[0] - 2);
		} else if (dimension == 3) {
			maxValue = 126; // nonsense
		} else if (dimension == 4) {
			maxValue = 3612; // nonsense
		} else {
			maxValue = MAX_DIM_VALUE;
		}
		return maxValue;
	}

	private static String newSolution() {
		StringBuffer sb = new StringBuffer();
		for (long dim : dimensions) {
			if (dim < 100) sb.append(" ");
			if (dim < 10) sb.append(" ");
			sb.append(" " + dim + ",");
		}
		String solution = sb.toString();
		return solution;
	}

	private static long surfaceArea() {
		long surfaceArea = 0;
		for (long omit = 0; omit < numDimensions; omit++) {
			long product = 1;
			for (int dim = 0; dim < numDimensions; dim++) {
				if (dim != omit) {
					product *= dimensions[dim];
				}
			}
			surfaceArea += 2 * product;
		}
		return surfaceArea;
	}

	private static long volume() {
		long volume = 1;
		for (long dim : dimensions) {
			volume *= dim;
		}
		return volume;
	}
}
