package ch.kri.brad.projecteuler;

import java.util.TreeSet;

import ch.kri.brad.utility.Primes;

/**
 * Due to the massive number of primes we put into a TreeSet, this requires
 * extra memory. Use the parameter -Xmx4G should do...
 * 
 * PROBLEM: The solutions are all correct, but contain duplicates.
 * Solution: Originally, I wanted to generate a simple hash-value for each
 * solution. However, MathBlog has a better idea: just insist that each
 * prime in the number is larger than the previous one. This works nicely,
 * the way we have defined the partitions.
 */
public class Problem118 {
	private static TreeSet<Long> primes;
	private static int[] permutations = new int[362880]; // All permutations of digits 1-9
	private static int position; // Position within the list of permutations
	private static int count = 0;
	
	// Small optimization: There are only 4 single-digit primes, so we omit the
	// following partitions:
	// {1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,2},{1,1,1,1,1,2,2},{1,1,1,1,1,1,3},{1,1,1,1,1,4},
	private static int[][] partitions = { 
			                       {1,1,1,2,2,2},
			                       {1,2,2,2,2},
			                       {1,1,1,1,2,3},
			                       {1,1,2,2,3},
			                       {2,2,2,3},
			                       {1,1,1,2,4},
			                       {1,2,2,4},
			                       {1,1,1,1,5},
			                       {1,1,2,5},
			                       {2,2,5},
			                       {1,1,1,6},
			                       {1,2,6},
			                       {1,1,1,3,3},
			                       {1,2,3,3},
			                       {1,1,7},
			                       {2,7},
			                       {1,1,3,4},
			                       {2,3,4},
			                       {1,8},
			                       {1,3,5},
			                       {1,4,4},
			                       {9},
			                       {3,6},
			                       {4,5},
			                       {3,3,3} };
	

	public static void main(String[] args) {
		// First, generate all primes containing up to 9 digits
		Primes.generatePrimesErastothenes(1000000000, true, false);
		primes = Primes.getPrimeTree();
		
		System.out.println("Primes generated");

		// Now generate all permutations of the available digits
		generatePermutations();
		System.out.println("Permutations generated");
		
		// For each permutation, check all possible partitionings into primes
		int n = checkPartitions();
		System.out.println(n + " solutions");
	}

	private static int checkPartitions() {
		int count = 0;
		for (int i : permutations) {
			for (int[] partition : partitions) {
				if (allPrime(i, partition)) {
					count++;
					System.out.print(i + "   ");
					for (int p : partition) System.out.print(p + " ");
					System.out.println();
				}
			}
		}
		return count;
	}
	
	/**
	 * As a cheap "hash" of the result, we sum all the primes found.
	 * If the numbers are not all prime, we return 0. This allows us
	 * to eliminate duplicates (as described at the top of the class)
	 */
	private static boolean allPrime(long value, int[] partition) {
		boolean allPrimes = true;
		long lastPrime = 0;
		for (int i = 0; allPrimes && i < partition.length-1; i++) {
			int numDigits = partition[i];
			long smallValue;
			if (numDigits == 1) {
				smallValue = value % 10; value /= 10;
			} else if (numDigits == 2) {
				smallValue = value % 100; value /= 100;
			} else if (numDigits == 3) {
				smallValue = value % 1000; value /= 1000;
			} else { // must be 4
				smallValue = value % 10000; value /= 10000; 
			}
			if (lastPrime > smallValue || !primes.contains(smallValue)) allPrimes = false;
			lastPrime = smallValue;
		}
		if (allPrimes && (lastPrime > value || !primes.contains(value))) allPrimes = false;
		return allPrimes;
	}
	
	private static void generatePermutations() {
		int[] digits = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		position = 0;
		permute(0, digits);
	}

	private static void permute(int number, int[] digits) {
		if (digits.length == 0) {
			// Add final result to collection
			permutations[position++] = number;
		} else {
			for (int i = 0; i < digits.length; i++) {
				int newNumber = number * 10 + digits[i];
				int[] newDigits = new int[digits.length - 1];
				for (int j = 0; j < i; j++) newDigits[j] = digits[j];
				for (int j = i + 1; j < digits.length; j++)	newDigits[j - 1] = digits[j];
				permute(newNumber, newDigits);
			}
		}
	}
}
