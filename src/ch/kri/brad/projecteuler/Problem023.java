package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Factoring;



public class Problem023 {
	private static final int LIMIT = 28123;
	private static ArrayList<Long> abundantNumbers;
	private static boolean[] values = new boolean[LIMIT+1]; // one higher because of zero

	/**
	 * General approach:
	 * - Find all abundant numbers <= limit
	 * - Use a "sieve" to find all possible sums of two abundant numbers
	 * - Add up all unmarked numbers
	 */
	public static void main(String[] args) {
		for (int i = 0; i < values.length; i++) values[i] = false;
		
		Factoring.generateAbundantNumbers(LIMIT);
		abundantNumbers = Factoring.getAbundantNumbers();
		sieveValues();
		System.out.println("Answer = " + sumValues());
	}
	
	private static void sieveValues() {
		for (int i = 0; i < abundantNumbers.size(); i++) {
			for (int j = i; j < abundantNumbers.size(); j++) {
				int sum = (int) (abundantNumbers.get(i) + abundantNumbers.get(j));
				if (sum <= LIMIT) {
					values[sum] = true;
				}
			}
		}
	}
	
	private static long sumValues() {
		long sum = 0;
		for (int i = 0; i < values.length; i++) {
			if (!values[i]) {
				sum += i;
			}
		}
		return sum;
	}

}
