package AdventOfCode.aoc_2020.day_15;

import java.io.IOException;

public class Problem2 {
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		NumberList2 numbers = new NumberList2(30000000, ir.getInitialNumbers());
		numbers.fillNumbers();
		System.out.println("Answer = " + numbers.getByPos(30000000-1)); // because 0-based
	}
}
