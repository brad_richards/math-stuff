package ch.kri.brad.projecteuler;

public class Problem014 {

	public static void main(String[] args) {
		int maxChainLength = 0;
		long bestNum = 0;
		for (long num = 1; num < 1000000; num++) {
			int chainLength = 1;
			long val = num;
			while (val != 1) {
				if (val % 2 == 0) {
					val = val / 2;
				} else {
					val = 3 * val + 1;
				}
				chainLength++;
			}
			if (chainLength > maxChainLength) {
				System.out.println(num + " produces a chain of length " + chainLength);
				maxChainLength = chainLength;
				bestNum = num;
			}
		}
		
		System.out.println("Answer = " + bestNum);
	}

}
