package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ch.kri.brad.utility.Digits;

/**
 * Our approach: calculate cubes, sort the digits, and use the sorted digits as a key into a
 * HashMap. The HashMap maps the sorted digits (as a string) to an ArrayList containing the values leading to the
 * cube. So, for example, 345 would be stored under the key 01234566, as would 384 and 405.
 */
public class Problem062 {
	static HashMap<String, ArrayList<Long>> cubeMap = new HashMap<>(10000);
	static final long MAX_VALUE = 2097148; // higher cubes exceed Long.MAX_VALUE
	
	public static void main(String[] args) {
		for (long i = 1; i < 100000; i++) {
			long cube = i * i * i;
			int[] digits = Digits.toDigits(cube);
			Arrays.sort(digits);
			String digitString = new String();
			for (int digit : digits) digitString += digit;
			
			ArrayList<Long> valueList;
			if (cubeMap.containsKey(digitString)) {
				valueList = cubeMap.get(digitString);
			} else {
				valueList = new ArrayList<>();
			}
			valueList.add(i);
			cubeMap.put(digitString, valueList);
		}
		
		long smallestValue = Long.MAX_VALUE;
		for (ArrayList<Long> valueList : cubeMap.values()) {
			if (valueList.size() == 5) {
				for (long value : valueList) {
					System.out.print(value + " ");
					long cube = value * value * value;
					if (cube < smallestValue) smallestValue = cube;
				}
				System.out.println();
			}
		}
		System.out.println("\nAnswer = " + smallestValue);
	}
}
