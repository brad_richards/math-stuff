package ch.kri.brad.projecteuler;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.JFileChooser;


public class Problem042 {
	private static TreeSet<Integer> triangleNumbers = new TreeSet<Integer>();
	private static int lastAddedNumber = 0;
	private static int lastTriangleNumber = 0;
	private static String[] words;
	
	public static void main(String[] args) {
		words = readWords();
		int numFound = 0;
		if (words != null) {
			for (String word : words) {
				if (isTriangleNumber(stringToValue(word))) {
					numFound++;
				}
			}
		}
		System.out.println(numFound);
	}
	
	private static String[] readWords() {
		String[] words = null;
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showOpenDialog(null);
		File inFile = fileChooser.getSelectedFile();
		LineNumberReader in;
		try {
			in = new LineNumberReader(new FileReader(inFile));
			String wordsIn = in.readLine();
			words = wordsIn.split(",");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return words;
	}

	private static boolean isTriangleNumber(int value) {
		// Extend triangle numbers as needed
		while (value > lastTriangleNumber) {
			lastAddedNumber++;
			lastTriangleNumber += lastAddedNumber;
			triangleNumbers.add(lastTriangleNumber);
		}
		
		// Check membership
		return triangleNumbers.contains(value);
	}

	private static int stringToValue(String word) {
		word = word.toUpperCase();
		char[] chars = word.toCharArray();
		int value = 0;
		for (char c : chars) {
			if (c >= 'A' & c <= 'Z') { // Ignore quotation marks, etc.
				value += 1 + (int) c - (int) 'A';
			}
		}
		return value;
	}
	
}
