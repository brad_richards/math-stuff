package AdventOfCode.aoc_2022.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	
	private static final ArrayList<Instruction> program = new ArrayList<>();
	
	private static int cycle = 0;
	private static int Xregister = 1;
	private static int answer = 0;
	
	private static boolean[][] display = new boolean[6][40];

	public static void main(String[] args) throws Exception {
		readInput();

		int pos = 0;
		Instruction current = program.get(0);
		while (pos < program.size()-1 || current.getCycles() < current.getOp().ticks) {
			if (current.getCycles() >= current.getOp().ticks) { 
				System.out.format("Executed %s %d: Cycle %d, X = %d%n", current.getOp(), current.getValue(), cycle, Xregister);
				current = program.get(++pos); 
			}
			
			// Part 2: draw on display
			int row = cycle / 40;
			int col = cycle % 40;
			if (  Math.abs(Xregister - col) <= 1 ) display[row][col] = true;
			
			cycle++;
			if ( (cycle - 20) % 40 == 0) {
				int signalStrength = cycle * Xregister;
				System.out.println("Signal strength = " + signalStrength);
				answer += signalStrength;
			}
			Xregister = current.tick(Xregister);
		}
		System.out.println("Answer to part 1 = " + answer);
		
		// Part 2: show display
		for (int i = 0; i < 240; i++) {
			int row = i / 40;
			int col = i % 40;
			if (col == 0) System.out.println();
			System.out.print( display[row][col] ? '#' : '.');
			
		}
		
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		
		for (String line : lines) program.add(new Instruction(line));
	}
}