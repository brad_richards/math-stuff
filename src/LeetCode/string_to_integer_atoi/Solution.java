package LeetCode.string_to_integer_atoi;

class Solution {

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.myAtoi("9223372036854775808"));
        System.out.println(s.myAtoi(""));
    }

    public int myAtoi(String s) {
        long value = 0;
        long sign = 1;
        int pos = 0;
        boolean finished = false;
        s = s.trim();
        if (s.isEmpty()) return 0;

        if (!Character.isDigit(s.charAt(0))) {
            if (s.charAt(0) == '-')
                sign = -1;
            else if (s.charAt(0) == '+')
                sign = 1;
            else
                finished = true;
            pos++;
        }
        while (!finished && pos < s.length()) {
            if (Character.isDigit(s.charAt(pos))) {
                value = value * 10 + (s.charAt(pos) - '0');
            } else {
                finished = true;
            }
            pos++;
            if (value > Integer.MAX_VALUE) finished = true; // stop, just stop.
        }
        value *= sign;
        if (value < Integer.MIN_VALUE) value = Integer.MIN_VALUE;
        else if (value > Integer.MAX_VALUE) value = Integer.MAX_VALUE;
        return (int) value;
    }
}
