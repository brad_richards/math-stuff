package ch.kri.brad.projecteuler.Problem096;

public class Utilities {

	/**
	 * Get the unique value from a possible-values array, or return 0 if the value is not definite
	 */
	public static int getDefiniteVal(int[] possVals) {
		if (possVals[0] != 1) return 0;
		for (int i = 1; i <= 9; i++)
			if (possVals[i] == 1) return i;
		return 0;
	}

	/**
	 * Return true if a possible value is present
	 */
	public static boolean hasPossVal(int[] possVals, int value) {
		return (possVals[value] == 1);
	}
	
	/**
	 * Remove a possible value from a possible-values array, updating the number of possible values as needed
	 */
	public static boolean removeVal(int[] possVals, int value) {
		boolean progress = false;
		if (possVals[value] == 1) {
			possVals[value] = 0;
			possVals[0]--;
			progress = true;
		}
		return progress;
	}
	
	/**
	 * Set a definite value for this possible-values array
	 */
	public static boolean setDefiniteVal(int[] possVals, int value) {
		boolean progress = false;
		if (possVals[0] != 1) {
			for (int i = 1; i <= 9; i++) possVals[i] = 0;
			possVals[value] = 1;
			possVals[0] = 1;
			progress = true;
		}
		return progress;
	}
	
	/**
	 * Return true if the given value is present in the given row.
	 */
	public static boolean valueInRow(int[][][] puzz, int row, int possibleValue) {
		boolean found = false;
		for (int col = 0; !found && col < 9; col++) {
			if (getDefiniteVal(puzz[row][col]) == possibleValue) found = true;
		}
		return found;
	}

	/**
	 * Return true if the given value is present in the given column.
	 */
	public static boolean valueInCol(int[][][] puzz, int col, int possibleValue) {
		boolean found = false;
		for (int row = 0; !found && row < 9; row++) {
			if (getDefiniteVal(puzz[row][col]) == possibleValue) found = true;
		}
		return found;
	}	
	
	/**
	 * Remove a given number from the possible-values in an entire row,
	 * with the exception of the block containing the specified column
	 * 
	 * @param num - number to remove
	 * @param row - row to remove from
	 * @param except - column to leave as-is (-1 for no exception)
	 * @return true if any change was made
	 */
	public static boolean removePossValFromRow(int[][][] puzz, int num, int row, int except) {
		boolean possValsChanged = false;
		// CONSTS.log("removeNum " + num + " FromRow " + row);
		for (int n = 0; n < 9; n++) {
			if ((n / 3) * 3 != except) {// skip numbers in the same block
				if (puzz[row][n][num] == 1) {
					puzz[row][n][num] = 0;
					puzz[row][n][0]--;
					possValsChanged = true;
				}
			}
		}
		return possValsChanged;
	}
	
	/**
	 * Remove a given number from the possible-values in an entire column,
	 * with the exception of the block containing the specified row
	 * 
	 * @param num - number to remove
	 * @param col - col to remove from
	 * @param except - row to leave as-is (-1 for no exception)
	 * @return true if any change was made
	 */
	public static boolean removePossValFromCol(int[][][] puzz, int num, int col, int except) {
		boolean possValsChanged = false;
		// CONSTS.log("removeNum " + num + " FromCol " + col);
		for (int n = 0; n < 9; n++) {
			if ((n / 3) * 3 != except) { // skip numbers in the same block
				if (puzz[n][col][num] == 1) {
					puzz[n][col][num] = 0;
					puzz[n][col][0]--;
					possValsChanged = true;
				}
			}
		}
		return possValsChanged;
	}
	
	/**
	 * Remove a given number from the box containing the given row and column,
	 * with the exception of the specified square
	 * 
	 * @param num - number to remove
	 * @param i - row identifying box
	 * @param j = column identifying box
	 * @param excludeRow - row identifying exception-square
	 * @param excludeCol - column identifying exception-square
	 * @return true if any change was made
	 */
	public static boolean removePossValFromBox(int[][][] puzz, int num, int i, int j, int excludeRow, int excludeCol) {
		boolean possValsChanged = false;
		for (int x = i - i % 3; x < i - i % 3 + 3; x++) {
			if (x != excludeRow) {
				for (int y = j - j % 3; y < j - j % 3 + 3; y++) {
					if (y != excludeCol && puzz[x][y][num] != 0) {
						puzz[x][y][num] = 0;
						puzz[x][y][0]--;
						possValsChanged = true;
					}
				}
			}
		}
		return possValsChanged;
	}
}
