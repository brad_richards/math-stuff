package AdventOfCode.aoc_2018.day06;

import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * General approach:
 * 
 * - Create a bounding box defined by the largest and smallest X and Y coordinates.
 * 
 * - Within this box, allow all points to propagate via the Manhattan algorithm
 * 
 * - Create a list of candidate nodes
 * 
 * - Eliminate from this list all nodes that have an entry on the edge of the bounding box, as these
 * would extend to infinity
 * 
 * - For the remaining nodes, count the number of entries they occupy (possibly this can be done
 * during generation)
 */
public class Day06_part1 {
	private static final ArrayList<Point> points = new ArrayList<>();
	private static Point topLeft = new Point();
	private static Point bottomRight = new Point();
	
	private static final int CONFLICT = -99;

	public static void main(String[] args) {
		readPoints();
		findBoundingBox();

		// We now actually create the bounding box. The array indices will be offset by the values
		// of "topLeft"
		// Points are identified by their position in "points", hence their IDs are integers
		// beginning with 0.
		// Cells in the bounding box are identifed by the point that they belong to - and are
		// initially -1.
		// Cells where the areas of two points collide are marked with CONFLICT
		int[][] box = new int[bottomRight.y + 1 - topLeft.y][bottomRight.x + 1 - topLeft.x];
		for (int row = 0; row < box.length; row++) {
			for (int col = 0; col < box[0].length; col++)
				box[row][col] = -1;
		}

		// Place the points into the box
		for (int id = 0; id < points.size(); id++) {
			Point p = points.get(id);
			box[p.y - topLeft.y][p.x - topLeft.x] = id;
		}

		// Propagate until no more changes are made
//		printBox(box);
		box = propagate(box);
//		printBox(box);

		// Create list of candidates as a HashMap, where the ID is the key and the count is the
		// value
		HashMap<Integer, Integer> candidates = new HashMap<>();
		for (int id = 0; id < points.size(); id++) {
			candidates.put(id, 0);
		}

		// Remove candidates on edge of box
		for (int row = 0; row < box.length; row++) {
			if (box[row][0] >= 0) candidates.remove(box[row][0]);
			if (box[row][box[0].length - 1] >= 0) candidates.remove(box[row][box[0].length - 1]);
		}
		for (int col = 0; col < box[0].length; col++) {
			if (box[0][col] >= 0) candidates.remove(box[0][col]);
			if (box[box.length - 1][col] >= 0) candidates.remove(box[box.length - 1][col]);
		}

		// Count candidate sizes
		for (int row = 0; row < box.length; row++) {
			for (int col = 0; col < box[0].length; col++) {
				if (box[row][col] >= 0 && candidates.containsKey(box[row][col])) {
					int count = candidates.get(box[row][col]);
					count++;
					candidates.put(box[row][col], count);
				}
			}
		}

		// Print candidates, their sizes, and their positions, and find largest
		int largest = 0;
		for (int key : candidates.keySet()) {
			System.out.println(
					"id " + key + " has " + candidates.get(key) + " squares, and is located at row "
							+ points.get(key).y + ", col " + points.get(key).x);
			if (candidates.get(key) > largest) largest = candidates.get(key);
		}
		
		// Print answer
		System.out.println("\nLargest is " + largest);

	}

	/**
	 * Parse the points
	 */
	private static void readPoints() {
		InputStream fis = Day06_part1.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String line = in.nextLine();
				String[] nums = line.split(",");
				assert (nums.length == 2);
				Point p = new Point();
				p.x = Integer.parseInt(nums[0].trim());
				p.y = Integer.parseInt(nums[1].trim());
				points.add(p);
			}
		}
		System.out.println(points.size() + " points read");
	}

	private static void findBoundingBox() {
		topLeft.x = Integer.MAX_VALUE;
		topLeft.y = Integer.MAX_VALUE;
		bottomRight.x = 0;
		bottomRight.y = 0;
		for (Point p : points) {
			if (p.x < topLeft.x) topLeft.x = p.x;
			if (p.y < topLeft.y) topLeft.y = p.y;
			if (p.x > bottomRight.x) bottomRight.x = p.x;
			if (p.y > bottomRight.y) bottomRight.y = p.y;
		}
	}

	/**
	 * For each cell in the box, if that cell has a value of -1 (unclaimed) then we examine the four
	 * surrounding cells. If the surrounding cells...
	 * 
	 * - contain one or more occurrences of the same id, then this cell is claimed.
	 * 
	 * - contain multiple IDs, or if any neighboring cell is a conflict, then this cell is marked as
	 * a conflict.
	 * 
	 * Otherwise, this cell remains unchanged
	 * 
	 * We repeat this process until we make a complete pass without making any changes.
	 */
	private static int[][] propagate(int[][] box) {
		int numChanges = -1;
		while (numChanges != 0) {
			int[][] newBox = new int[box.length][box[0].length];
			numChanges = 0;
			for (int row = 0; row < box.length; row++) {
				for (int col = 0; col < box[0].length; col++) {
					newBox[row][col] = box[row][col]; // copy, then overwrite with any change
					if (box[row][col] == -1) { // unclaimed
						int newCellValue = -1;

						// Up
						if (row > 0) newCellValue = newCellValue(newCellValue, box[row - 1][col]);

						// Down
						if (row < box.length - 1)
							newCellValue = newCellValue(newCellValue, box[row + 1][col]);

						// Left
						if (col > 0)
							newCellValue = newCellValue(newCellValue, box[row][col - 1]);

						// Right
						if (col < box[0].length - 1)
							newCellValue = newCellValue(newCellValue, box[row][col + 1]);

						if (newCellValue != -1) {
							newBox[row][col] = newCellValue;
							numChanges++;
						}
					}
				}
			}
			box = newBox;
//			printBox(box);
		}
		return box;
	}

	/**
	 * Given two cell values: If either of them is a conflict, then we have a conflict. If it is
	 * _not_ the case that at least one of them is -1, then we have a conflict.
	 * 
	 * Otherwise, we take the maximum value of the two cells as the new value: this will either be
	 * -1 or an ID.
	 */
	private static int newCellValue(int cell1, int cell2) {
		if (cell1 == CONFLICT || cell2 == CONFLICT)
			return CONFLICT;
		if (cell1 != -1 && cell2 != -1 && cell1 != cell2) {
			return CONFLICT;
		} else {
			return Math.max(cell1, cell2);
		}
	}

	private static void printBox(int[][] box) {
		for (int row = 0; row < box.length; row++) {
			for (int col = 0; col < box[0].length; col++) {
				System.out.printf("%4d", box[row][col]);
			}
			System.out.println();
		}
		System.out.println();
	}
}
