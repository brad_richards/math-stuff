package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Fibonacci;



public class Problem002 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Fibonacci.generateFibonacci(4000000);
		ArrayList<Long> fibonacci = Fibonacci.getFibonacciNumbers();
		long sum = 0;
		for (Long val : fibonacci) {
			if (val%2 == 0) sum += val;
		}
		System.out.println("Sum = " + sum);
	}

}
