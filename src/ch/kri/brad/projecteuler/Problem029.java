package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.HashSet;


public class Problem029 {

	public static void main(String[] args) {
		HashSet<BigInteger> values = new HashSet<BigInteger>();
		for (int a = 2; a <= 100; a++) {
			for (int b = 2; b <= 100; b++) {
				BigInteger value = BigInteger.valueOf(a).pow(b);
				values.add(value);
			}
		}
		System.out.println("Answer = " + values.size());
	}

}
