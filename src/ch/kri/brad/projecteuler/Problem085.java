package ch.kri.brad.projecteuler;

public class Problem085 {
	private static final int target = 2000000;
	private static int bestDelta = Integer.MAX_VALUE;
	private static int bestAnswer = 0;
	private static int bestX = 0;
	private static int bestY = 0;

	/**
	 * The number of rectangles across the grid is always X = (x+1)*x/2. This is true independent of the height
	 * of the rectangles. For rectangles of height 1, the total number is then X * y. For rectangles of height 2,
	 * it is then X * (y-1). Etc. Hence, the total number of rectangles is X * Y or (x+1)*x*(y+1)*y/4. We want this
	 * value to be as close as possible to a given "target" value.
	 * 
	 * We will iterate y, from 1 until it exceeds x (stopping, to avoid redundancy). For each iteration, we
	 * solve the remaining quadratic equation for x. This will (probably) not be an integer, so we calculate
	 * the number of squares for the floor and ceiling of this value.
	 */
	public static void main(String[] args) {		
		int y = 0;
		int xCeiling = 0;
		do {
			y++;
			int yVal = (y+1) * y / 2;
			
			double dx = (Math.sqrt(8.0d * yVal * target + yVal * yVal) - yVal) / (2 * yVal);
			int xFloor = (int) Math.floor(dx);
			xCeiling = (int) Math.ceil(dx);
			
			trySolution(xFloor, y);
			trySolution(xCeiling, y);
		} while(y < xCeiling);
		System.out.println("Best answer = " + bestAnswer + " from " + bestX + ", " + bestY + " = aread of " + bestX*bestY);
	}
	
	private static void trySolution(int x, int y) {
		int num = numRects(x, y);
		int delta = Math.abs(target - num);
		if (delta < bestDelta) {
			bestDelta = delta;
			bestAnswer = num;
			bestX = x;
			bestY = y;
		}
	}

	private static int numRects(int x, int y) {
		return (x+1)*x*(y+1)*y/4;
	}
}
