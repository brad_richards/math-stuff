package ch.kri.brad.projecteuler.Problem054;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * This program uses the classes PlayingCard and PokerHand to solve ProjectEuler problem 54.
 * 
 * File contains 1000 hands; each line is 10 cards, five for player 1 followed by five for player 2.
 * Using the standard poker rules, determine how many hands player 1 wins.
 * 
 * We make use of an enumeration for hand types, in order of increasing value. We also define an
 * inner class for valuing hands; one attribute is the hand type, the other is a list of card ranks
 * used for tie-breaking when the hand-type is the same.
 */
public class Problem054 {
	private static class HandPair {
		PokerHand player1 = new PokerHand();
		PokerHand player2 = new PokerHand();
	}
	
	private static ArrayList<HandPair> handPairs = new ArrayList<>();
	private static int player1Wins;
	
	public static void main(String[] args) {
		readHandPairs();
		System.out.print("Hands read in. ");

		for (HandPair pair : handPairs) {
			if (pair.player1.compareTo(pair.player2) > 0) {
				player1Wins++;
			}
		}
		System.out.println("Answer: " + player1Wins);
	}

	/**
	 * Read in the hands from the text file
	 */
	private static void readHandPairs() {
		File f = new File("Problem054.txt");
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(f));
			String line;
			while ((line = in.readLine()) != null) {
				String[] cardStrings = line.split(" ");
				HandPair handPair = new HandPair();
				handPairs.add(handPair);
				for (int i = 0; i < 5; i++) {
					handPair.player1.addCard(parseCard(cardStrings[i]));
					handPair.player2.addCard(parseCard(cardStrings[5+i]));
				}
			}
			if (handPairs.size() != 1000) throw new Exception("Didn't find 1000 hands");
		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			if (in != null) try {in.close();} catch (Exception e) {}
		}
	}
	
	private static PlayingCard parseCard(String cardIn) {
		char rankIn = cardIn.charAt(0);
		char suitIn = cardIn.charAt(1);
		PlayingCard.CardRank rank = null;
		PlayingCard.CardSuit suit = null;;
		
		switch(rankIn) {
		case '2': rank = PlayingCard.CardRank.Two; break;
		case '3': rank = PlayingCard.CardRank.Three; break;
		case '4': rank = PlayingCard.CardRank.Four; break;
		case '5': rank = PlayingCard.CardRank.Five; break;
		case '6': rank = PlayingCard.CardRank.Six; break;
		case '7': rank = PlayingCard.CardRank.Seven; break;
		case '8': rank = PlayingCard.CardRank.Eight; break;
		case '9': rank = PlayingCard.CardRank.Nine; break;
		case 'T': rank = PlayingCard.CardRank.Ten; break;
		case 'J': rank = PlayingCard.CardRank.Jack; break;
		case 'Q': rank = PlayingCard.CardRank.Queen; break;
		case 'K': rank = PlayingCard.CardRank.King; break;
		case 'A': rank = PlayingCard.CardRank.Ace; break;
		}
		
		switch(suitIn) {
		case 'C': suit = PlayingCard.CardSuit.Club; break;
		case 'D': suit = PlayingCard.CardSuit.Diamond; break;
		case 'H': suit = PlayingCard.CardSuit.Heart; break;
		case 'S': suit = PlayingCard.CardSuit.Spade; break;
		}
		
		return new PlayingCard(suit, rank);
	}
}
