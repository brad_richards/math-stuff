package ch.kri.brad.projecteuler;

/**
 * Parameterized version of problem 114
 */
public class Problem115 {
	public static final int m = 50;
	public static final int start_n = 100;

	public static void main(String[] args) {
		long sum = 0;
		long n = start_n;
		while (sum < 1000000) {
			n++;
			sum = 0;
			for (long nBlocks = 1; nBlocks <= (n + 1) / (m + 1); nBlocks++) {
				long start = n + 1 - (m+1) * nBlocks;
				long product = 1;
				for (long i = 1; i <= nBlocks * 2; i++) {
					System.out.print((start + i) + " ");
					product *= (start + i);
					product /= i;
				}
				System.out.println();
				sum += product;
			}
			sum += 1;
			System.out.println("for n = " + n + " the total is " + sum);
		}
	}
}
