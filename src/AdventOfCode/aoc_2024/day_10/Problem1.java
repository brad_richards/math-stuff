package AdventOfCode.aoc_2024.day_10;

import AdventOfCode.utility.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// For part 1, in class TrailHead store the ends as a HashSet.
// For part 2, store them as a List (i.e. allow duplicates reached by different paths)
public class Problem1 {
	private static int[][] map;
	private static final List<TrailHead> trailHeads = new ArrayList<>();

	private static class TrailHead {
		private Point start;
		private final ArrayList<Point> ends = new ArrayList<>();
		public TrailHead(Point start) { this.start = start; }
	}

	public static void main(String[] args) throws Exception {
		readInput();
		findAllTrailheads();
		for (TrailHead t : trailHeads) {
			findAllEnds(t);
		}
		int sum = 0;
		for (TrailHead t : trailHeads) sum += t.ends.size();
		System.out.println(sum);
	}

	// A path is just an ArrayList of points.
	// altitude is the next height that we are searching for
	private static void findAllEnds(TrailHead t) {
		List<List<Point>> paths = new ArrayList<>();
		List<Point> path = new ArrayList<>();
		path.add(t.start);
		paths.add(path);
		int altitude = 1;
		while (altitude <= 9) {
			List<List<Point>> newPaths = new ArrayList<>();
			for (List<Point> p : paths) {
				Point end = p.getLast();
				if (end.row() > 0 && map[end.row()-1][end.col()] == altitude) {
					List<Point> newPath = new ArrayList<>(p);
					newPath.add(new Point(end.row()-1, end.col()));
					newPaths.add(newPath);
				}
				if (end.row() < map.length-1 && map[end.row()+1][end.col()] == altitude) {
					List<Point> newPath = new ArrayList<>(p);
					newPath.add(new Point(end.row()+1, end.col()));
					newPaths.add(newPath);
				}
				if (end.col() > 0 && map[end.row()][end.col()-1] == altitude) {
					List<Point> newPath = new ArrayList<>(p);
					newPath.add(new Point(end.row(), end.col()-1));
					newPaths.add(newPath);
				}
				if (end.col() < map[0].length-1 && map[end.row()][end.col()+1] == altitude) {
					List<Point> newPath = new ArrayList<>(p);
					newPath.add(new Point(end.row(), end.col()+1));
					newPaths.add(newPath);
				}
			}
			paths = newPaths;
			altitude++;
		}
		for (List<Point> p : paths) {
			t.ends.add(p.getLast());
		}
	}

	private static void findAllTrailheads() {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				if (map[row][col] == 0)
					trailHeads.add(new TrailHead(new Point(row, col)));
			}
		}
	}

	private static void readInput() throws IOException {
		List<String> lines;
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.toList();
		}
		int rows = lines.size();
		int cols = lines.get(0).length();
		map = new int[rows][cols];
		for (int row = 0; row < rows; row++) {
			char[] chars = lines.get(row).toCharArray();
			for (int col = 0; col < cols; col++) {
				map[row][col] = chars[col] - '0';
			}
		}
	}
}