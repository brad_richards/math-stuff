package kattis.b_datatypes.integers;

import java.util.Scanner;

public class spavanac {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int hours = in.nextInt();
		int minutes = in.nextInt();
		
		minutes = minutes - 45; // 45 minutes earlier
		
		if (minutes < 0) { // If needed, adjust hour
			minutes = minutes + 60;
			hours = hours - 1;
		}
		
		if (hours < 0) { // If necessary, wrap hour
			hours = hours + 24;
		}
		
		System.out.println(hours + " " + minutes);
	}

}
