package AdventOfCode.aoc_2021.day19;

public class Beacon {
	// Initial coordinates
	public final int x;
	public final int y;
	public final int z; 
	
	// Final coordinates (relative to Scanner 0)
	
	public Beacon(int[] coords) {
		this.x = coords[0]; this.y = coords[1]; this.z = coords[2];
	}
}
