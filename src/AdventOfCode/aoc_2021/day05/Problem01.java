package AdventOfCode.aoc_2021.day05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Inspecting the data, the maximum coordinate is < 1000. So we use a map key of 1000 * x + y
 */
public class Problem01 {
	private static List<HydroLine> lines;
	private static HashMap<Integer, Integer> ventMap = new HashMap<>();

	public static void main(String[] args) throws IOException {
		readInput();

		// for (HydroLine hl : lines) System.out.println(hl.end1x + "," + hl.end1y + " -> " + hl.end2x + "," + hl.end2y);
		
		// For problem 1, only consider lines where end1x==end2x or end1y==end2y
		for (HydroLine hl : lines) {
			if (hl.end1x==hl.end2x) {
				for (int y = Math.min(hl.end1y, hl.end2y); y <= Math.max(hl.end1y, hl.end2y); y++) {
					mapIncrement(hl.end1x * 1000 + y);
				}
			} else if (hl.end1y==hl.end2y) {
				for (int x = Math.min(hl.end1x, hl.end2x); x <= Math.max(hl.end1x, hl.end2x); x++) {
					mapIncrement(x * 1000 + hl.end1y);
				}
			} else { // diagonal -- Problem 2 only !!!
				int x = hl.end1x;
				int y = hl.end1y;
				int xInc = (int) Math.signum(hl.end2x - hl.end1x);
				int yInc = (int) Math.signum(hl.end2y - hl.end1y);
				while ( xInc > 0 && x <= hl.end2x || xInc < 0 && x >= hl.end2x) {
					mapIncrement(x * 1000 + y);
					x += xInc;
					y += yInc;
				}
			}
		}
		
		// Count vents with value 2 or greater
		int count = 0;
		for (int key : ventMap.keySet())
			if (ventMap.get(key) >= 2) count++;
		
		System.out.println(count);
	}
	
	private static void mapIncrement(int key) {
		if (ventMap.containsKey(key)) {
			ventMap.put(key, ventMap.get(key)+1);
		} else {
			ventMap.put(key, 1);
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.map(HydroLine::parseHydroLine).collect(Collectors.toList());
		}
	}

}
