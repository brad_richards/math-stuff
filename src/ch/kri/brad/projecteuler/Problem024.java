package ch.kri.brad.projecteuler;
import java.util.ArrayList;


public class Problem024 {
	static ArrayList<Integer> availableDigits;
	static ArrayList<Integer> usedDigits;
	static int[] factorials;
	static int goalIteration = 1000000;
	
	/**
	 * Analysis:
	 * 
	 * - Ten Digits can be arranged in 10! different ways
	 * 
	 * - If we regard only the rightmost 9 digits, these can be arranged 9! or 362880 different ways
	 * 
	 * - Hence, the millionth permutation occurs on the third iteration of nine digits. The leftmost
	 * digit is therefore a 2.
	 * 
	 * - In this third iteration, we are looking for iteration 1000000 - 2 * 362880 = 274240
	 * 
	 * - Looking at the rightmost 8 digits, these can be arranged 8! or 40320 different ways
	 * 
	 * - Hence, the desired permutation occurs on the 7th iteration. The leftmost digit is 7
	 * 
	 * In the end, finishing the analysis by hand would be faster than writing a program. However,
	 * here we go anyway. Important: our calculations are zero-based; hence, we immediately substract
	 * one from the goalIteration!
	 */
	public static void main(String[] args) {
		init();
		goalIteration--; // Our calculations are zero-based!
		for (int i = 0; i < 10; i++) {
			int digitToUse = goalIteration / factorials[9-i];
			Integer digit = availableDigits.remove(digitToUse);
			usedDigits.add(digit);
			goalIteration = goalIteration - digitToUse * factorials[9-i];
		}
		
		System.out.print("Answer = ");
		for (Integer i : usedDigits) {
			System.out.print(i);
		}
	}
	
	private static void init() {
		usedDigits = new ArrayList<Integer>();
		availableDigits = new ArrayList<Integer>();
		availableDigits.add(0);
		availableDigits.add(1);
		availableDigits.add(2);
		availableDigits.add(3);
		availableDigits.add(4);
		availableDigits.add(5);
		availableDigits.add(6);
		availableDigits.add(7);
		availableDigits.add(8);
		availableDigits.add(9);
		
		factorials = new int[10];
		int factorial = 1;
		factorials[0] = factorial;
		for (int i = 1; i < 10; i++) {
			factorial *= i;
			factorials[i] = factorial;
		}
	}
}
