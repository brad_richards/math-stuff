package AdventOfCode.aoc_2023.day_04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<Card> cards;

	public static void main(String[] args) throws Exception {
		readInput();

		long sum = 0;
		for (Card c : cards) {
			sum += (c.numMatches == 0) ? 0 : (long) (Math.pow(2, c.numMatches-1) + 0.0001);
		}
		System.out.println(sum);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			cards = stream.map(s -> new Card(s)).collect(Collectors.toList());
		}
	}
}