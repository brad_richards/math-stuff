package AdventOfCode.aoc_2020.day_06;

import java.util.Arrays;

public class Group02 {
	private boolean[] answers = new boolean[26];
	
	public Group02() {
		Arrays.fill(answers, true);
	}
	
	public void addPerson(String line) {
		boolean[] lineAnswers = new boolean[26];
		Arrays.fill(lineAnswers, false);
		for (char c : line.toCharArray()) lineAnswers[c - 'a'] = true;
		for (int i = 0; i < answers.length; i++) answers[i] &= lineAnswers[i];
	}
	
	public int getCount() {
		int count = 0;
		for (boolean b : answers) if (b) count++;
		return count;
	}
}
