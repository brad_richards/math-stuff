package hackerRank.java.strings;

import java.util.Scanner;

public class JavaStringTokens {

	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        scan.close();
        
        String[] tokens = s.split("[ !,?\\._'@]+");
        
        // Unfortunately, split sometimes produces empty tokens, for example, on the string ".a"
        // We must count and skip these tokens
        
        int emptyTokens = 0;
        for (String t : tokens) if (t.equals("")) emptyTokens++;
        
        System.out.println(tokens.length - emptyTokens);
        for (String t : tokens) if (!t.equals("")) System.out.println(t);
	}

}
