package AdventOfCode.aoc_2023.day_04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card {
		final int cardNumber;
		final List<Integer> winningNumbers = new ArrayList<>();
		final List<Integer> myNumbers = new ArrayList<>();
		final int numMatches;

		public Card(String line) {
			String[] parts = line.split(": +");
			cardNumber = Integer.parseInt(parts[0].split(" +")[1]);
			parts = parts[1].split(" \\| +");
			String[] winNum = parts[0].split(" +");
			for (String s : winNum) winningNumbers.add(Integer.parseInt(s.trim()));
			Collections.sort(winningNumbers);
			String[] myNum = parts[1].split(" +");
			for (String s : myNum) myNumbers.add(Integer.parseInt(s.trim()));
			Collections.sort(myNumbers);
			
			this.numMatches = this.getMatches();
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Card: "); sb.append(cardNumber); sb.append(": ");
			for (Integer i : winningNumbers) { sb.append(i); sb.append(" "); }
			sb.append("| ");
			for (Integer i : myNumbers) { sb.append(i); sb.append(" "); }
			return sb.toString();
		}
		
		// Since lists are sorted, could be a lot more efficient. But this is easier,
		// and the amount of data isn't all that big, so...
		private int getMatches() {
			int numMatches = 0;
			for (Integer win : winningNumbers) {
				for (Integer my : myNumbers) {
					if (win == my) numMatches++;
				}
			}
			return numMatches;
		}
}
