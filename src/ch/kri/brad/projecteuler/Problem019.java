package ch.kri.brad.projecteuler;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class Problem019 {

	/**
	 * Brute force - why be dainty?
	 */
	public static void main(String[] args) {
		int numSundays = 0;
		for (int year = 1901; year <= 2000; year++) {
			for (int month = 1; month <= 12; month++) {
				GregorianCalendar date = new GregorianCalendar(year, month, 1);
				int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek == Calendar.SUNDAY) {
					numSundays++;				
				}
			}
		}
		System.out.println("Number of Sundays = " + numSundays);
	}
}
