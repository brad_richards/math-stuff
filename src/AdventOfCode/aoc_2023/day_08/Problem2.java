package AdventOfCode.aoc_2023.day_08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * All paths have a cycle-length, going through the end-node over and over again.
 * As it turns out, the cycle length is constant --> even the first time!
 * 
 * So we just need for each path to reach its end node once, and note the
 * cycle length. Then we calculate the LCM (least-common-multiple) of all
 * cycle lengths.
 */
public class Problem2 {
	// Directions - for the moment, at least, just the original string
	static String directions;
	
	// Maintain the list of nodes in a HashMap
	static final HashMap<String, Node> nodes = new HashMap<>();
	
	// The list of nodes we are at (initially, all ending with 'A')
	static final List<String> current = new ArrayList<>();
	static final List<Long> cycleLengths = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		readInput();
		
		findStartingNodes();
		
		long count = 0;
		while (!finished()) {
			char direction = directions.charAt((int) (count % directions.length()));
			for (int i = 0; i < current.size(); i++) {
				Node node = nodes.get(current.get(i));
				if (direction == 'R')
					current.set(i, node.right);
				else
					current.set(i, node.left);		
			}
			count++;

			for (int i = 0; i < current.size(); i++) {
				String s = current.get(i);
				if (s.charAt(s.length()-1) == 'Z') {
					if (cycleLengths.get(i) == 0) cycleLengths.set(i, count);			
				}
			}
		}
		
		long lcm = getLCM();
		System.out.println(lcm);
	}
	
	private static long getLCM() {
		long current = cycleLengths.get(0);
		for (int i = 1; i < cycleLengths.size(); i++) {
			current = lcm(current, cycleLengths.get(i));
		}
		return current;
	}
	
	private static long lcm(long a, long b) {
		long gcd = gcd(a, b);
		return a / gcd * b;
	}
	
	private static long gcd(long a, long b) {
		if (a < b) { long tmp = a; a = b; b = tmp; }
		long tmp = a % b;
		if (tmp == 0) return b;
		return gcd(b, tmp);
	}
	
	private static void findStartingNodes() {
		for (String name : nodes.keySet()) {
			if (name.charAt(name.length()-1) == 'A') {
				current.add(name);
				cycleLengths.add(0l);
			}
		}
	}
	
	// We are finished when we have all cycle lengths
	private static boolean finished() {
		boolean finished = true;
		for (Long cycleLength : cycleLengths) {
			if (cycleLength == 0) finished = false;
		}		
		return finished;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lines = stream.collect(Collectors.toList());
			
			directions = lines.get(0);
			for (int i = 2; i < lines.size(); i++) {
				Node node = new Node(lines.get(i));
				nodes.put(node.name, node);
			}
		}
	}
}