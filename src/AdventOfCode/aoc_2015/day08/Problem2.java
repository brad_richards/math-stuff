package AdventOfCode.aoc_2015.day08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int numChars = 0;
		int numEncoded = 0;
		for (String line : lines) {
			numChars += line.length();
//			System.out.println(process(line));
			numEncoded += process(line).length();
		}
		System.out.format("Part 2 answer = %d - %d = %d%n", numEncoded, numChars, (numEncoded - numChars));
	}
	
	private static String process(String in) {
		char[] chars = in.toCharArray();
		String result = "\"";
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '\\') {
				result += "\\\\";
			} else if (chars[i] == '"') {
				result += "\\\"";
			} else result += chars[i];
		}
		result += "\"";
		return result;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}