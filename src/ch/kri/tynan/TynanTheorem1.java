package ch.kri.tynan;

import java.math.BigInteger;

/**
 * Theorem: x^y - x is always divisible by 24, where x and y are odd numbers greater than 3
 */
public class TynanTheorem1 {
	private static final int limit = 10000;

	public static void main(String[] args) {

		for (int i = 3; i <= limit; i += 2) {
			BigInteger bi = BigInteger.valueOf(i);
			for (int j = i; j <= limit; j += 2) {
				BigInteger bResult = bi.pow(j);
				bResult = bResult.subtract(bi);				
				BigInteger bmod = bResult.mod(BigInteger.valueOf(24));
				
				if (!bmod.equals(BigInteger.ZERO)) {
					System.out.println("Exception found: " + i + ", " + j);
				}
			}
		}
		System.out.println("All values through " + limit + " tested");
	}
}
