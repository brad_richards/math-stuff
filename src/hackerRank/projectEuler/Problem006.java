package hackerRank.projectEuler;

import java.util.Scanner;

/**
 * Given the potentially large number of test cases, but the relative low maximum value, we will just calculate all 10000 values in advance.
 */
public class Problem006 {
	static long[] squareOfSum = new long[10000]; // position 0 is the number 1
	static long[] sumOfSquares = new long[10000]; // position 0 is the number 1

	public static void main(String[] args) {
		int[] testCases = readData();
		
		prepareData();
		
		for (int value : testCases)
			System.out.println(doOneCase(value));
	}

	public static int[] readData() {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		int[] data = new int[size];
		for (int i = 0; i < size; i++) {
			data[i] = s.nextInt();
		}
		s.close();
		return data;
	}

	public static long doOneCase(int valToTest) {
		return squareOfSum[valToTest-1] - sumOfSquares[valToTest-1];
	}

	public static void prepareData() {
		long sumOfSquare = 0;
		long sum = 0;
		for (int i = 1; i <= 10000; i++) {
			sum += i;
			sumOfSquare += i * i;
			sumOfSquares[i-1] = sumOfSquare;
			squareOfSum[i-1] = sum * sum;
		}
	}
}
