package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Digits;

/**
 * There is lots of potential for optimization, for example, if we have a
 * 5-digit number and the first three digits are "bouncy", then we can increment
 * by 100 instead of by 1.
 * 
 * @author brad
 *
 */
public class Problem112 {
	private static enum Trend {RISING, FLAT, FALLING};

	// We specify the goal as the ratio of bouncy to non-bouncy numbers.
	// Hence, 1 = 50%, 3 = 75%, 9 = 90%, etc.
	private static final long GOAL = 99;

	public static void main(String[] args) {
		long countBouncy = 0;

		long num = 100;
		while (countBouncy / (num - countBouncy) < GOAL) {
			if (isBouncy(++num)) countBouncy++;
		}
		System.out.println(num);
	}

	/**
	 * Scan the digits from left to right, and find the trend: rising, falling,
	 * or flat. If we are rising, a bouncy number will eventually fall, and vice
	 * versa.
	 */
	private static boolean isBouncy(long num) {
		int[] digits = Digits.toDigits(num);
		Trend trend = Trend.FLAT;
		boolean bouncy = false;
		for (int i = 0; !bouncy && i < digits.length-1; i++) {
			if (trend == Trend.FLAT) {
				if (digits[i] < digits[i+1]) trend = Trend.RISING;
				else if (digits[i] > digits[i+1]) trend = Trend.FALLING;
			} else if (trend == Trend.RISING && digits[i] > digits[i+1]) bouncy = true;
			else if (trend == Trend.FALLING && digits[i] < digits[i+1]) bouncy = true;
		}
		return bouncy;
	}
}
