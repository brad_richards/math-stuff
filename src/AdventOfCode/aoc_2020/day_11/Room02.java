package AdventOfCode.aoc_2020.day_11;

import java.util.List;

/**
 * A room contains an array that represents the status of the seating. For efficiency, this is a
 * char-array, using the three characters from the problem description.
 * 
 * This class offers a factory method that produces a new room configuration after applying all
 * rules to this room.
 */
public class Room02 {
	private char[][] room;

	public Room02(List<String> input) {
		room = new char[input.size()][input.get(0).length()];
		for (int i = 0; i < input.size(); i++) {
			room[i] = input.get(i).toCharArray();
		}
	}

	private Room02(char[][] room) {
		this.room = room;
	}

	/**
	 * Apply all rules to create a new room
	 */
	public Room02 applyRules() {
		char[][] newRoom = new char[room.length][room[0].length];

		for (int r = 0; r < room.length; r++) {
			for (int c = 0; c < room[0].length; c++) {
				if (room[r][c] == '.') {
					newRoom[r][c] = '.';
				} else {
					int occupiedNeighbors = occupiedNeighbors(r, c);
					if (room[r][c] == 'L') if (occupiedNeighbors == 0) newRoom[r][c] = '#';
					else newRoom[r][c] = 'L';
					else if (room[r][c] == '#') // must be the case
						if (occupiedNeighbors >= 5) newRoom[r][c] = 'L';
						else newRoom[r][c] = '#';
					else System.out.println("Error in seating found!");
				}
			}
		}

		return new Room02(newRoom);
	}

	/**
	 * The biggest change from problem 1. No longer a compact solution: We define a movement vector,
	 * and apply this repeatedly in each directly, getting back a true if we see an occupied seat in
	 * that direction. Eight directions.
	 */
	private int occupiedNeighbors(int row, int col) {
		int count = 0;
		for (int y = -1; y <= 1; y++) {
			for (int x = -1; x <= 1; x++) {
				if ((x != 0 || y != -0) && seatVisible(row, col, y, x)) count++;
			}
		}
		return count;
	}

	/**
	 * Follow a movement vector from row/col until we run off the map or hit a seat.
	 * 
	 * @return true if we hit an occupied seat
	 */
	private boolean seatVisible(int row, int col, int y, int x) {
		int xPos = col + x;
		int yPos = row + y;
		boolean seatOccupied = false;
		boolean seatSeen = false;
		while (seatSeen == false && xPos >= 0 && xPos < room[0].length && yPos >= 0
				&& yPos < room.length) {
			seatOccupied = room[yPos][xPos] == '#';
			seatSeen = room[yPos][xPos] == '#' || room[yPos][xPos] == 'L';
			xPos += x;
			yPos += y;
		}
		return seatOccupied;
	}

	public int occupiedSeats() {
		int count = 0;
		for (char[] row : room) {
			for (char c : row) {
				if (c == '#') count++;
			}
		}
		return count;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) return false;
		Room02 roomIn = (Room02) o;
		boolean same = true;
		for (int r = 0; same && r < room.length; r++) {
			for (int c = 0; same && c < room[0].length; c++) {
				same = room[r][c] == roomIn.room[r][c];
			}
		}
		return same;
	}

	public void printRoom() {
		for (char[] row : room) {
			for (char c : row)
				System.out.print(c);
			System.out.println();
		}
		System.out.println();
	}
}
