package AdventOfCode.aoc_2023.day_03;

import java.util.ArrayList;
import java.util.List;

public class Symbol {
	int x, y; // position
	char symbol;
	List<Value> values = new ArrayList<>();
	
	Symbol(int x, int y, char c) {
		this.x = x;
		this.y = y;
		this.symbol = c;
	}
	
	void addValue(Value value) {
		values.add(value);
	}
}
