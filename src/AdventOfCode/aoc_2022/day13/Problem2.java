package AdventOfCode.aoc_2022.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;
	static List<XmasList> packets = new ArrayList<>();
	
	private static class TestCase {
		public XmasList left;
		public XmasList right;
	}

	public static void main(String[] args) throws Exception {
		readInput();
		
		for (String line : lines) {
			if (!line.isBlank()) packets.add(new XmasList(line));
		}
		
		XmasList p1 = new XmasList("[[2]]"); 
		XmasList p2 = new XmasList("[[6]]"); 
		
		packets.add(p1);
		packets.add(p2);
		
		Collections.sort(packets);
		
		for (XmasList packet : packets) System.out.println(packet);

		int i1 = packets.indexOf(p1)+1;
		int i2 = packets.indexOf(p2)+1;
		
		System.out.println("Part 2 answer: " + (i1 * i2));
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}