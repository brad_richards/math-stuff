package AdventOfCode.aoc_2021.day16;

public class Binary {

	/**
	 * Retrieve up to 57 bits from a hex-string, as a long.
	 * 
	 * @param in The input string of hex characters
	 * @param nBits The number of bits to retrieve (1-57)
	 * @param offset The first bit to retrieve (0-based)
	 * @return A long containing the bits, right-justified 
	 */
	public static long getBits(String in, int nBits, int offset) {
		int skipChars = offset / 4;
		int skipBits = offset % 4;
		int bitsToShift = 6 - (nBits + 2) % 4 - offset % 4;
		String sBlock = in.substring(skipChars, skipChars + (nBits+6)/4);
		long block = Long.parseLong(sBlock, 16);
		for (int i = 0; i < bitsToShift; i++) block /= 2; // Shift right as needed
		
		// Mask unwanted bits
		long mask = 1;
		for (int i = 1; i < nBits; i++) mask = mask * 2 + 1;
		
		return block & mask;
	}
}
