package ch.kri.brad;

public class Solera {

	public static final double Transfer_proportion = 0.333333;
	public static final double Angels_share = 0.03;
	public static final int Num_layers = 20;
	
	public static Criadera[] layers = new Criadera[Num_layers];
	
	private static  class Criadera {
		private double aroba;
		private double average_age;
		
		private Criadera(double aroba, double average_age) {
			this.aroba = aroba;
			this.average_age = average_age;
		}
		
		/**
		 * Transfer the given increment into this criadera
		 */
		public void add(Criadera input) {
			double new_quantity = aroba + input.aroba;
			double new_age;
			if (new_quantity == 0) {
				new_age = 0;
			} else {
				new_age = (aroba * average_age + input.aroba * input.average_age) / new_quantity;
			}
			this.aroba = new_quantity;
			this.average_age = new_age;
		}
		
		public void age() {
			average_age++;
//			aroba = aroba * (1 - Angels_share);
		}
		
		public Criadera transfer() {
			Criadera transfer_amount = new Criadera(aroba * Transfer_proportion, average_age);
			aroba = (1 - Transfer_proportion) * aroba;
			return transfer_amount;
		}
		
		public double getAroba() {
			return aroba;
		}
		
		public double getAverageAge() {
			return average_age;
		}
	}
	
	/**
	 * Simple simulation of a solera system, to see what ages the various levels converge on. Ages are expresses as multiples of the period in which wine is transferred. For example, if wine is transferred
	 * every 4 months, then the age represents the "average" age in each level, in multiples of 4 months. The age given is at the end of a period, just before the next transfer occurs.
	 * 
	 * Assumption: transfers happen at the same frequency and at the same time for all layers in the solera/criadera
	 * 
	 * Layer 0 is the solera, layers 1 through n are the criadera in decreasing age. We deliberately work with too many layers...
	 */
	public static void main(String[] args) {
		// Initialization
		for (int i = 0; i < layers.length; i++) layers[i] = new Criadera(0,0);

		for (int time = 1; time <= 100; time++) {
			// transfer downwards
			for (int layer = 0; layer < Num_layers; layer++) {
				Criadera transfer = layers[layer].transfer();
				if ( layer > 0 ) {
					layers[layer-1].add(transfer);
				}
			}
			
			// add new wine to top layer
			layers[Num_layers-1].add(new Criadera(10,0));
			
			// age one time-unit
			for (Criadera layer : layers) layer.age();
		}
		
		// Print the results
		for (int layer = Num_layers-1; layer >= 0; layer--) {
			String strOut = "Criadera %2d contains %3.1f aroba with an average age of %3.1f time-units";
			System.out.printf(strOut, layer, layers[layer].getAroba(), layers[layer].getAverageAge());
			System.out.println();
		}
	}
}
