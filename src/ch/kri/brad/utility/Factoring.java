package ch.kri.brad.utility;

import java.math.BigInteger;
import java.util.ArrayList;

public class Factoring {
	public static class AmicablePair {
		long value1;
		long value2;

		public AmicablePair(long value1, long value2) {
			this.value1 = value1;
			this.value2 = value2;
		}

		public long getValue1() {
			return value1;
		}

		public long getValue2() {
			return value2;
		}
	}

	private static ArrayList<AmicablePair> amicablePairs = new ArrayList<AmicablePair>();
	private static ArrayList<Long> abundantNumbers = new ArrayList<Long>();

	public static void generateAmicablePairs(int maxValue) {
		for (long value1 = 1; value1 <= maxValue; value1++) {
			long value2 = getSumDivisors(value1);
			if (getSumDivisors(value2) == value1) {
				// Amicable; avoid duplicates and check maxValue against value2
				if (value1 < value2 && value2 <= maxValue) {
					amicablePairs.add(new AmicablePair(value1, value2));
				}
			}
		}
	}

	public static void generateAbundantNumbers(int maxValue) {
		for (long i = 2; i <= maxValue; i++) {
			if (Factoring.getSumDivisors(i) > i) {
				abundantNumbers.add(i);
			}
		}
	}

	/**
	 * Unsorted list of all divisors *including* the number
	 */
	public static ArrayList<Long> getDivisors(long valIn) {
		ArrayList<Long> divisors = new ArrayList<Long>();
		for (long divisor = 1; divisor <= Math.sqrt(valIn); divisor++) {
			if ((valIn % divisor) == 0) {
				divisors.add(divisor);
				long dividend = valIn / divisor;
				if (dividend != divisor) divisors.add(dividend);				
			}
		}
		return divisors;
	}
	
	/**
	 * Sorted list of all divisors up to and including sqrt(number)
	 */
	public static ArrayList<Long> getSmallDivisors(long valIn) {
		ArrayList<Long> divisors = new ArrayList<Long>();
		for (long divisor = 1; divisor <= Math.sqrt(valIn); divisor++) {
			if ((valIn % divisor) == 0) {
				divisors.add(divisor);
			}
		}
		return divisors;
	}

	/**
	 * Sum of all divisors *excluding* the number
	 */
	public static long getSumDivisors(long valIn) {
		long sum = 1;
		for (long divisor = 2; divisor <= Math.sqrt(valIn); divisor++) {
			if ((valIn % divisor) == 0) {
				long dividend = valIn / divisor;
				if (dividend != divisor) sum += dividend;
				sum += divisor;
			}
		}
		return sum;
	}

	public static ArrayList<AmicablePair> getAmicablePairs() {
		return amicablePairs;
	}

	public static ArrayList<Long> getAbundantNumbers() {
		return abundantNumbers;
	}

	public static ArrayList<Integer> getCommonPrimeFactors(long a, long b) {
		ArrayList<Integer> primeFactors = new ArrayList<Integer>();
		int maxPrimeRequired;
		if (a > b) {
			maxPrimeRequired = (int) Math.sqrt(a);
		} else {
			maxPrimeRequired = (int) Math.sqrt(b);
		}
		// Generate primes
		Primes.generatePrimesErastothenes(maxPrimeRequired);

		ArrayList<Long> primes = Primes.getPrimes();
		for (Long prime : primes) {
			if ((a % prime == 0) & (b % prime == 0)) {
				boolean divisible = true;
				while (divisible) {
					divisible = (a % prime == 0) & (b % prime == 0);
					if (divisible) {
						a = a / prime;
						b = b / prime;
					}
				}
				primeFactors.add(new Integer(prime.intValue()));
				if (a == 1 | b == 1) break; // ugly...
			}
		}
		return primeFactors;
	}
}
