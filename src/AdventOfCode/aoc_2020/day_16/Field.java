package AdventOfCode.aoc_2020.day_16;

import java.util.ArrayList;

public class Field {
	private final String name;
	private final ArrayList<Range> ranges = new ArrayList<>();
	private int column; // The identified column for this field
	
	public class Range {
		public int min;
		public int max;
		
		public Range(int min, int max) {
			this.min = min;
			this.max = max;
		}
		
		public boolean inRange(int value) {
			return (value >= min && value <= max);
		}
	}
	
	public Field(String name, int[] values) {
		this.name = name;
		for (int i = 0; i < values.length; i+=2) {
			Range range = new Range(values[i], values[i+1]);
			this.ranges.add(range);
		}
		this.column = -1;
	}
	
	public boolean inRange(int value) {
		boolean inRange = false;
		for (int i = 0; !inRange && i < ranges.size(); i++) {
			inRange = ranges.get(i).inRange(value);
		}
		return inRange;
	}

	public String getName() {
		return name;
	}

	public ArrayList<Range> getRanges() {
		return ranges;
	}
	
	public int getColumn() {
		return column;
	}
	
	public void setColumn(int column) {
		this.column = column;
	}
}
