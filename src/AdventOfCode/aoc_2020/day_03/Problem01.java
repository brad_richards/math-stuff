package AdventOfCode.aoc_2020.day_03;

import java.io.IOException;

public class Problem01 {
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		boolean[][] landscape = ir.getLandscape();
		
		long a1 = countTrees(landscape, 1, 1);
		long a2 = countTrees(landscape, 3, 1);
		long a3 = countTrees(landscape, 5, 1);
		long a4 = countTrees(landscape, 7, 1);
		long a5 = countTrees(landscape, 1, 2);
		
		System.out.println("Part 1 answer is " + a2);
		System.out.println("Part 2 answer is " + (a1 * a2 * a3 * a4 * a5));
	}
	
	private static int countTrees(boolean[][] landscape, int deltaX, int deltaY) {
		int x = 0;
		int y = 0;
		int trees = 0;
		int maxX = landscape[0].length;

		do {
			if (landscape[y][x]) trees++;
			y += deltaY;
			x += deltaX;
			x = x % maxX;
		} while (y < landscape.length);
		
		return trees;
	}
}
