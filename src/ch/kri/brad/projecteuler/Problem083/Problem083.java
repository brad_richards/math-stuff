package ch.kri.brad.projecteuler.Problem083;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Arrays;

/**
 * This differs from problem 82 not only in the ability to move left, but also in the fixed start and end cells.
 */
public class Problem083 {
	private static final String FILE_NAME = "p083_matrix.txt";
	static final int SIZE = 80;
	static int[][] matrix = new int[SIZE][SIZE];
	private static int[][] bestValues = new int[SIZE][SIZE];

//	private static final int[][] testMatrix =
//		{ { 131, 673, 234, 103, 18 },
//		  { 201, 96, 342, 965, 150 },
//		  { 630, 803, 746, 422, 111 },
//		  { 537, 699, 497, 121, 956 },
//		  { 805, 732, 524, 37, 331 } };

	public static void main(String[] args) {
		 readFile();

//		matrix = testMatrix;
	
		// Initialize bestValues
		for (int row = 0; row < SIZE; row++) {
			Arrays.fill(bestValues[row], Integer.MAX_VALUE/10);
		}
		bestValues[0][0] = matrix[0][0];
//		printBestValues();
		
		// Propagate to each subsequent column. Since we can also move left, if we make changes to a
		// column, we then backtrack to the left, to see if we have triggered any changes in the
		// previous column.
		int col = 0;
		while (col < SIZE) {
			boolean anyChanges = bestValuesForCol(col);
			if (!anyChanges || col == 0) col++;
			else col--;
			
		}
		
		// Print the value in the bottom-right cell
		System.out.println("Answer: " + bestValues[SIZE-1][SIZE-1]);
	}

	private static boolean bestValuesForCol(int col) {
//		System.out.println("Processing column " + col);
		boolean anyChanges = false;
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int row = 0; row < SIZE; row++) {
				int possVal;
				if (col > 0) {
					possVal = bestValues[row][col-1] + matrix[row][col];
					if (betterValue(row, col, possVal)) { changed = true; anyChanges = true; }
				}
				
				if (col < SIZE-1) {
					possVal = bestValues[row][col+1] + matrix[row][col];
					if (betterValue(row, col, possVal)) { changed = true; anyChanges = true; }
				}
				
				if (row > 0) {
					possVal = bestValues[row-1][col] + matrix[row][col];
					if (betterValue(row, col, possVal)) { changed = true; anyChanges = true; }
				}
				
				if (row < SIZE-1) {
					possVal = bestValues[row+1][col] + matrix[row][col];
					if (betterValue(row, col, possVal)) { changed = true; anyChanges = true; }
				}
			}
//			printBestValues();
		}
		return anyChanges;
	}
	
	private static void printBestValues() {
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				System.out.printf("%11d", bestValues[row][col]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	private static boolean betterValue(int row, int col, int value) {
		boolean result = bestValues[row][col] > value;
		if (result) bestValues[row][col] = value;
		return result;
	}
	
	/**
	 * Method to read the input file and create the internal representation
	 */
	private static void readFile() {
		URL url = Problem083.class.getResource(FILE_NAME);
		File f = new File(url.getPath());
		if (f.exists()) {
			try (BufferedReader in = new BufferedReader(new FileReader(f))) {
				String line = in.readLine();
				int row = -1;
				while (line != null) {
					row++;
					String[] nodeValues = line.split(",");
					if (nodeValues.length != SIZE)
						System.out.println("Error in input file: wrong row length");
					for (int col = 0; col < nodeValues.length; col++) {
						matrix[row][col] = Integer.parseInt(nodeValues[col]);
					}
					line = in.readLine();
				}
				if (row != SIZE - 1)
					System.out.println("Error in input file: wrong number of rows");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
