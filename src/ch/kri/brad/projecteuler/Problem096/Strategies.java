package ch.kri.brad.projecteuler.Problem096;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Each strategy must return true if it made any progress on the puzzle, or false if it did not.
 * 
 * A call to a strategy may make one or many changes to a puzzle.
 */
public class Strategies {

	/**
	 * Comment for <code>groups</code> There are 27 groups of 9 cells; 9 rows + 9 columns + 9
	 * 3x3_blocks For each group we can apply the different strategies. Each position is just a
	 * pointer to a possVals element.
	 */
	private static int[][][] groups = null;

	public static void init(int[][][] puzz) {
		groups = new int[27][9][];
		int cont = 0;

		for (int n = 0; n < 9; n++) {
			// add rows
			for (int m = 0; m < 9; m++) {
				groups[cont][m] = puzz[n][m];
			}
			cont++;

			// add cols
			for (int m = 0; m < 9; m++) {
				groups[cont][m] = puzz[m][n];
			}
			cont++;
		}

		// add boxes
		for (int i = 0; i < 9; i += 3) {
			for (int j = 0; j < 9; j += 3) {
				groups[cont][0] = puzz[i][j];
				groups[cont][1] = puzz[i][j + 1];
				groups[cont][2] = puzz[i][j + 2];
				groups[cont][3] = puzz[i + 1][j];
				groups[cont][4] = puzz[i + 1][j + 1];
				groups[cont][5] = puzz[i + 1][j + 2];
				groups[cont][6] = puzz[i + 2][j];
				groups[cont][7] = puzz[i + 2][j + 1];
				groups[cont][8] = puzz[i + 2][j + 2];
				cont++;
			}
		}
	}

	/**
	 * Remove possible-values from a cell, when those values appear (as definite values) in other
	 * cells in the same row, column or box.
	 * 
	 * We check all 81 positions, and then return.
	 */
	public static boolean strategy0(int[][][] puzz) {
		boolean progress = false;
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				int[] possValues = puzz[row][col];
				if (possValues[0] != 1) { // if cell is not already definite
					// Eliminate other values in row
					for (int c = 0; c < 9; c++) {
						int[] possValsOtherCell = puzz[row][c];
						if (c != col && possValsOtherCell[0] == 1) progress |= Utilities
								.removeVal(possValues, Utilities.getDefiniteVal(possValsOtherCell));
					}
					// Eliminate other values in column
					for (int r = 0; r < 9; r++) {
						int[] possValsOtherCell = puzz[r][col];
						if (r != row && possValsOtherCell[0] == 1) progress |= Utilities
								.removeVal(possValues, Utilities.getDefiniteVal(possValsOtherCell));
					}
					// Eliminate other values in box
					int rowBox = row / 3;
					for (int r = rowBox * 3 + 0; r < rowBox * 3 + 3; r++) {
						int colBox = col / 3;
						for (int c = colBox * 3 + 0; c < colBox * 3 + 3; c++) {
							int[] possValsOtherCell = puzz[r][c];
							if ((r != row || c != col) && possValsOtherCell[0] == 1)
								progress |= Utilities.removeVal(possValues,
										Utilities.getDefiniteVal(possValsOtherCell));
						}
					}
				}
			}
		}
		return progress;
	}

	/**
	 * Look for a value that must be in a certain certain cell, because the other rows/columns for
	 * that box are occupied by other copies of the value.
	 * 
	 * We check all 81 positions, and then return.
	 */
	public static boolean strategy1(int[][][] puzz) {
		boolean progress = false;
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				int[] possValues = puzz[row][col];
				if (possValues[0] != 1) { // if cell not definite, look at each possibility
					for (int possibleValue = 1; possibleValue <= 9; possibleValue++) {
						if (possValues[possibleValue] == 1) {
							boolean valueFoundFourTimes = true;

							// Check other rows in box
							int rowBox = row / 3;
							for (int r = rowBox * 3; r < rowBox * 3 + 3; r++) {
								valueFoundFourTimes &= (row == r
										|| Utilities.valueInRow(puzz, r, possibleValue));
							}

							// Check other columns in box
							int colBox = col / 3;
							for (int c = colBox * 3; c < colBox * 3 + 3; c++) {
								valueFoundFourTimes &= (col == c
										|| Utilities.valueInCol(puzz, c, possibleValue));
							}

							if (valueFoundFourTimes) {
								progress |= Utilities.setDefiniteVal(possValues, possibleValue);
							}
						}
					}
				}
			}
		}
		return progress;
	}

	/**
	 * Look for places where one of the possible values is unique. For example, if only one cell in
	 * a row allows the possibility of being a 3, then that cell must be a 3. We check rows, columns
	 * and boxes.
	 */
	public static boolean strategy2(int[][][] puzz) {
		boolean progress = false;
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				int[] possValues = puzz[row][col];
				if (possValues[0] != 1) { // if cell not definite, look at each possibility
					for (int possibleValue = 1; possibleValue <= 9; possibleValue++) {
						if (possValues[possibleValue] == 1) {
							boolean found = false;

							// Check the row for other cells that could have this value
							for (int c = 0; !found && c < 9; c++) {
								if (c != col) {
									found = Utilities.hasPossVal(puzz[row][c], possibleValue);
								}
							}

							// Check the col for other cells that could have this value
							if (found) {
								found = false;
								for (int r = 0; !found && r < 9; r++) {
									if (r != row) {
										found = Utilities.hasPossVal(puzz[r][col], possibleValue);
									}
								}
							}

							// Check the box for other cells that could have this value
							if (!found) {
								int rowBox = row / 3;
								for (int r = rowBox * 3 + 0; r < rowBox * 3 + 3; r++) {
									int colBox = col / 3;
									for (int c = colBox * 3 + 0; c < colBox * 3 + 3; c++) {
										if (r != row || c != col) {
											found = Utilities.hasPossVal(puzz[r][c], possibleValue);
										}
									}
								}
							}

							// If not found, then this cell must have the value
							if (!found)
								progress |= Utilities.setDefiniteVal(possValues, possibleValue);
						}
					}
				}
			}
		}
		return progress;
	}

	/**
	 * Within a box, a possible-value appears only on a single row; then that possible value can be
	 * eliminated from the row in the neighboring blocks.
	 * 
	 * The identical logic applies to columns
	 */
	public static boolean strategy3(int[][][] puzz) {
		boolean progress = false;

		// check rows - is this possible value in one row, but not in the others?
		for (int row = 0; row < 9; row += 3) {
			for (int col = 0; col < 9; col += 3) {
				for (int possVal = 1; possVal <= 9; possVal++) {
					int changeRow = -1;
					if ((puzz[row][col + 0][possVal] == 1 || puzz[row][col + 1][possVal] == 1
							|| puzz[row][col + 2][possVal] == 1)
							&& (puzz[row + 1][col + 0][possVal] != 1
									&& puzz[row + 1][col + 1][possVal] != 1
									&& puzz[row + 1][col + 2][possVal] != 1
									&& puzz[row + 2][col + 0][possVal] != 1
									&& puzz[row + 2][col + 1][possVal] != 1
									&& puzz[row + 2][col + 2][possVal] != 1)) {
						changeRow = row;
					} else if ((puzz[row + 1][col + 0][possVal] == 1
							|| puzz[row + 1][col + 1][possVal] == 1
							|| puzz[row + 1][col + 2][possVal] == 1)
							&& (puzz[row][col + 0][possVal] != 1 && puzz[row][col + 1][possVal] != 1
									&& puzz[row][col + 2][possVal] != 1
									&& puzz[row + 2][col + 0][possVal] != 1
									&& puzz[row + 2][col + 1][possVal] != 1
									&& puzz[row + 2][col + 2][possVal] != 1)) {
						changeRow = row + 1;
					} else if ((puzz[row + 2][col + 0][possVal] == 1
							|| puzz[row + 2][col + 1][possVal] == 1
							|| puzz[row + 2][col + 2][possVal] == 1)
							&& (puzz[row][col + 0][possVal] != 1 && puzz[row][col + 1][possVal] != 1
									&& puzz[row][col + 2][possVal] != 1
									&& puzz[row + 1][col + 0][possVal] != 1
									&& puzz[row + 1][col + 1][possVal] != 1
									&& puzz[row + 1][col + 2][possVal] != 1)) {
						changeRow = row + 2;
					}
					// did we find something to try?
					if (changeRow >= 0) {
						progress |= Utilities.removePossValFromRow(puzz, possVal, changeRow, col);
					}
				}
			}
		}

		// check cols - is this possible value in one column, but not in the others?
		for (int col = 0; col < 9; col += 3) {
			for (int row = 0; row < 9; row += 3) {
				for (int possVal = 1; possVal <= 9; possVal++) {
					int changeCol = -1;
					if ((puzz[row + 0][col][possVal] == 1 || puzz[row + 1][col][possVal] == 1
							|| puzz[row + 2][col][possVal] == 1)
							&& (puzz[row + 0][col + 1][possVal] != 1
									&& puzz[row + 1][col + 1][possVal] != 1
									&& puzz[row + 2][col + 1][possVal] != 1
									&& puzz[row + 0][col + 2][possVal] != 1
									&& puzz[row + 1][col + 2][possVal] != 1
									&& puzz[row + 2][col + 2][possVal] != 1)) {
						changeCol = col;
					} else if ((puzz[row + 0][col + 1][possVal] == 1
							|| puzz[row + 1][col + 1][possVal] == 1
							|| puzz[row + 2][col + 1][possVal] == 1)
							&& (puzz[row + 0][col + 0][possVal] != 1
									&& puzz[row + 1][col + 0][possVal] != 1
									&& puzz[row + 2][col + 0][possVal] != 1
									&& puzz[row + 0][col + 2][possVal] != 1
									&& puzz[row + 1][col + 2][possVal] != 1
									&& puzz[row + 2][col + 2][possVal] != 1)) {
						changeCol = col + 1;
					} else if ((puzz[row + 0][col + 2][possVal] == 1
							|| puzz[row + 1][col + 2][possVal] == 1
							|| puzz[row + 2][col + 2][possVal] == 1)
							&& (puzz[row + 0][col + 1][possVal] != 1
									&& puzz[row + 1][col + 1][possVal] != 1
									&& puzz[row + 2][col + 1][possVal] != 1
									&& puzz[row + 0][col + 0][possVal] != 1
									&& puzz[row + 1][col + 0][possVal] != 1
									&& puzz[row + 2][col + 0][possVal] != 1)) {
						changeCol = col + 2;
					}
					if (changeCol >= 0) {
						progress |= Utilities.removePossValFromCol(puzz, possVal, changeCol, row);
					}
				}
			}
		}

		return progress;
	}

	/**
	 * If a number appears in only a single box within a line, then the number cannot appear on
	 * other lines within that box.
	 * 
	 * The identical logic applies to columns
	 */
	public static boolean strategy4(int[][][] puzz) {
		boolean progress = false;

		for (int num = 1; num <= 9; num++) {

			// check rows
			for (int row = 0; row < 9; row++) {
				int changeCol = -1;
				if ((puzz[row][0][num] == 1 || puzz[row][1][num] == 1 || puzz[row][2][num] == 1)
						&& (puzz[row][3][num] != 1 && puzz[row][4][num] != 1
								&& puzz[row][5][num] != 1 && puzz[row][6][num] != 1
								&& puzz[row][7][num] != 1 && puzz[row][8][num] != 1)) {
					changeCol = 0;
				} else if ((puzz[row][3][num] == 1 || puzz[row][4][num] == 1
						|| puzz[row][5][num] == 1)
						&& (puzz[row][0][num] != 1 && puzz[row][1][num] != 1
								&& puzz[row][2][num] != 1 && puzz[row][6][num] != 1
								&& puzz[row][7][num] != 1 && puzz[row][8][num] != 1)) {
					changeCol = 3;
				} else if ((puzz[row][6][num] == 1 || puzz[row][7][num] == 1
						|| puzz[row][8][num] == 1)
						&& (puzz[row][0][num] != 1 && puzz[row][1][num] != 1
								&& puzz[row][2][num] != 1 && puzz[row][3][num] != 1
								&& puzz[row][4][num] != 1 && puzz[row][5][num] != 1)) {
					changeCol = 6;
				}
				if (changeCol >= 0) {
					progress |= Utilities.removePossValFromBox(puzz, num, row, changeCol, row, -1);
				}
			}

			// check cols
			for (int col = 0; col < 9; col++) {
				int changeRow = -1;
				if ((puzz[0][col][num] == 1 || puzz[1][col][num] == 1 || puzz[2][col][num] == 1)
						&& (puzz[3][col][num] != 1 && puzz[4][col][num] != 1
								&& puzz[5][col][num] != 1 && puzz[6][col][num] != 1
								&& puzz[7][col][num] != 1 && puzz[8][col][num] != 1)) {
					changeRow = 0;
				} else if ((puzz[3][col][num] == 1 || puzz[4][col][num] == 1
						|| puzz[5][col][num] == 1)
						&& (puzz[0][col][num] != 1 && puzz[1][col][num] != 1
								&& puzz[2][col][num] != 1 && puzz[6][col][num] != 1
								&& puzz[7][col][num] != 1 && puzz[8][col][num] != 1)) {
					changeRow = 3;
				} else if ((puzz[6][col][num] == 1 || puzz[7][col][num] == 1
						|| puzz[8][col][num] == 1)
						&& (puzz[0][col][num] != 1 && puzz[1][col][num] != 1
								&& puzz[2][col][num] != 1 && puzz[3][col][num] != 1
								&& puzz[4][col][num] != 1 && puzz[5][col][num] != 1)) {
					changeRow = 6;
				}
				if (changeRow >= 0) {
					progress |= Utilities.removePossValFromBox(puzz, num, changeRow, col, -1, col);
				}
			}
		}
		return progress;
	}

	/**
	 * Look for two identical pairs of possible values within a group. If found, then neither of
	 * these values can appear elsewhere in the same group.
	 */
	public static boolean strategy5(int[][][] puzz) {
		boolean progress = false;
		for (int n = 0; n < 27; n++) { // for each of the 27 groups
			progress |= checkGroupForNakedPairs(groups[n]);
		}
		return progress;
	}

	private static boolean checkGroupForNakedPairs(int[][] group) {
		boolean possibleValsChanged = false;

		for (int n = 0; n < 8; n++) { // for each square in the group
			if (group[n][0] == 2) { // if there are exactly two possible values
				for (int m = n + 1; m < 9; m++) { // compare to remaining squares
					if (group[m][0] == 2) { // if also two possible values here
						boolean pairFound = true;
						int val1 = 0;
						int val2 = 0;

						// all possible values must be the same for m and n
						// also save the pair of values in val1/val2, so that
						// we can remove them from other squares
						for (int k = 1; k <= 9 & pairFound; k++) {
							if (group[n][k] != group[m][k]) {
								pairFound = false;
							} else if (group[n][k] == 1) {
								if (val1 == 0) val1 = k;
								else val2 = k;
							}
						}

						if (pairFound) { // remove pair-values from other cells
							for (int x = 0; x < group.length; x++) {
								if (x != n && x != m) {
									if (group[x][val1] == 1) {
										group[x][val1] = 0;
										group[x][0]--;
										possibleValsChanged = true;
									}
									if (group[x][val2] == 1) {
										group[x][val2] = 0;
										group[x][0]--;
										possibleValsChanged = true;
									}
								}
							}
						}
					}
				}
			}
		}
		return possibleValsChanged;
	}

	/**
	 * Look for two identical pairs of possible values within a group, neither of which appear in
	 * any other square in the group. The pair may be "hidden" by the presence of other possible
	 * values in the squares.
	 * 
	 * If found, then remove any other possible values from these two squares.
	 */
	public static boolean strategy6(int[][][] puzz) {
		boolean progress = false;
		for (int n = 0; n < 27; n++) { // for each of the 27 groups
			progress |= checkGroupForHiddenPairs(groups[n]);
		}
		return progress;
	}

	private static boolean checkGroupForHiddenPairs(int[][] group) {
		// iterate through all possible pairs val1/val2
		for (int val1 = 1; val1 <= 8; val1++) {
			for (int val2 = val1 + 1; val2 <= 9; val2++) {
				for (int n = 0; n < 8; n++) { // for each square in the group
					if (group[n][0] >= 2 // if at least these two possible values
							&& (group[n][val1] == 1) & (group[n][val2] == 1)) {
						for (int m = n + 1; m < 9; m++) { // compare to remaining squares
							if (group[m][0] >= 2 && (group[m][val1] == 1) & (group[m][val2] == 1)) { // if
																										// at
																										// least
																										// these
																										// two
																										// possible
																										// values
								boolean pairFound = true;

								// The values cannot exist for any other square in the group
								for (int x = 0; x < 9 & pairFound; x++) {
									if (x != n & x != m) {
										if (group[x][val1] == 1 || group[x][val2] == 1) {
											pairFound = false;
										}
									}
								}

								if (pairFound) { // remove other possible values from these squares
									// CONSTS.log("HIDDEN PAIR found: " + val1 + " " + val2);
									if (group[n][0] > 2 || group[m][0] > 2) {
										Arrays.fill(group[n], 0); // clear array
										group[n][val1] = 1;
										group[n][val2] = 1;
										group[n][0] = 2;
										Arrays.fill(group[m], 0); // clear array
										group[m][val1] = 1;
										group[m][val2] = 1;
										group[m][0] = 2;
										// CONSTS.log("HIDDEN PAIR. Pos val removed!");
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Look for sets of three squares that each contain two or three of a set of three values (e.g.:
	 * 46, 48, 468). If found, then none of these values can appear elsewhere in the same group.
	 */
	public static boolean strategy7(int[][][] puzz) {
		boolean progress = false;
		for (int n = 0; n < 27; n++) { // for each of the 27 groups
			progress |= checkGroupForNakedTriples(groups[n]);
		}
		return progress;
	}

	private static boolean checkGroupForNakedTriples(int[][] group) {
		boolean possibleValsChanged = false;

		ArrayList<int[]> triples = getAllTriples(group);

		int[] triple;
		int[] found = new int[3];
		int f = 0;
		for (int t = 0; t < triples.size(); t++) {
			triple = (int[]) triples.get(t);
			f = 0;
			for (int cell = 0; cell < 9; cell++) {
				if (group[cell][0] <= 3) { // either 2 or 3
					// Check that the only values present are members of the triple
					if (group[cell][triple[0]] + group[cell][triple[1]]
							+ group[cell][triple[2]] == group[cell][0]) {
						found[f++] = cell;
						if (f == 3) break; // optimization - can never be more than 3
					}
				}
			}

			if (f == 3) { // naked triple found
				// CONSTS.log("Naked triple found: " + triple[0] + triple[1] + triple[2]);
				for (int cell2 = 0; cell2 < 9; cell2++) { // remove triple-values from other cells
					if (cell2 != found[0] & cell2 != found[1] & cell2 != found[2]) {
						if (group[cell2][triple[0]] == 1 || group[cell2][triple[1]] == 1
								|| group[cell2][triple[2]] == 1) {
							group[cell2][0] -= group[cell2][triple[0]] + group[cell2][triple[1]]
									+ group[cell2][triple[2]];
							group[cell2][triple[0]] = group[cell2][triple[1]] = group[cell2][triple[2]] = 0;
							possibleValsChanged = true;
						}
					}
				}
			}
		}
		return possibleValsChanged;
	}

	private static ArrayList<int[]> getAllTriples(int[][] group) {
		ArrayList<int[]> res = new ArrayList<int[]>();

		// Find all possible-values used in the entire group
		int[] numbers = new int[9];
		int count = 0;
		for (int n = 1; n <= 9; n++) {
			for (int i = 0; i < 9; i++) {
				if (group[i][n] == 1) {
					numbers[count++] = n;
					break;
				}
			}
		}

		// Find all possible triples with these possible-values
		int[] aux = null;
		for (int t1 = 0; t1 < count; t1++) {
			for (int t2 = t1 + 1; t2 < count; t2++) {
				for (int t3 = t2 + 1; t3 < count; t3++) {
					aux = new int[3];
					aux[0] = numbers[t1];
					aux[1] = numbers[t2];
					aux[2] = numbers[t3];
					res.add(aux);
				}
			}
		}
		return res;
	}

	/**
	 * Look for triplets of possible values within a group, none of which appear in any other square
	 * in the group. The triplet may be "hidden" by the presence of other possible values in the
	 * squares.
	 * 
	 * If found, then remove any other possible values from these three squares.
	 */
	public static boolean strategy8(int[][][] puzz) {
		boolean progress = false;
		for (int n = 0; n < 27; n++) { // for each of the 27 groups
			progress |= checkGroupForHiddenTriples(groups[n]);
		}
		return progress;
	}

	private static boolean checkGroupForHiddenTriples(int[][] group) {
		boolean possibleValsChanged = false;
		boolean hiddenTripleFound = false;
		ArrayList<int[]> triples = getAllTriples(group);
		int[] triple = null;
		int[] found = new int[3];
		int f = 0;

		for (int t = 0; t < triples.size(); t++) {
			hiddenTripleFound = false;
			triple = (int[]) triples.get(t);
			f = 0;
			for (int cell = 0; cell < 9; cell++) {
				if (group[cell][triple[0]] + group[cell][triple[1]] + group[cell][triple[2]] >= 1) {
					found[f++] = cell;
				}
				if (f == 3) break; // possible triple found
			}

			if (f == 3) // we have to check that the triple is not in any other cell
			{
				hiddenTripleFound = true;
				for (int cell2 = 0; cell2 < 9; cell2++) {
					if (cell2 == found[0] || cell2 == found[1] || cell2 == found[2]) continue;
					if (group[cell2][triple[0]] == 1 || group[cell2][triple[1]] == 1
							|| group[cell2][triple[2]] == 1) {
						// is not a hidden triple
						hiddenTripleFound = false;
						break;
					}
				}
			}

			if (hiddenTripleFound) { // hidden triple found -> remove other pos vals from these 3
										// cells
				// CONSTS.log("********* Hidden triple found: " + triple[0] + triple[1] +
				// triple[2]);
				for (int n = 1; n <= 9; n++) {
					if (triple[0] != n & triple[1] != n & triple[2] != n) {
						for (int fnd = 0; fnd < 3; fnd++) {
							if (group[found[fnd]][n] == 1) {
								// CONSTS.log("Hidden triple. Remove number: " + n);
								group[found[fnd]][0]--;
								group[found[fnd]][n] = 0;
								possibleValsChanged = true;
							}
						}
					}
				}
			}
		} // for t
		return possibleValsChanged;
	}

	/**
	 * Look for x-wings. These are groups of four cells forming a rectangle, in which the same
	 * number is possible in all four cells, but nowhere else in the row. If such a pattern is
	 * found, then this number can be removed as a possible value for all other cells in the two
	 * columns.
	 * 
	 * The same check can be done for columns, eliminating candidates in rows.
	 * 
	 * @return true if we found something, false otherwise
	 */
	public static boolean strategy9(int[][][] puzz) {
		boolean progress = false;

		// For each possible value
		for (int possVal = 1; possVal <= 9; possVal++) {

			// Check for horizontal X-Wings

			// Find a row where this possible value occurs exactly twice
			for (int row1 = 0; row1 < 8; row1++) {
				int[] row1cols = findPairInGroup(groups[row1 * 2], possVal);
				if (row1cols != null) {
					// Find a second row where this possible value occurs exactly twice
					for (int row2 = row1 + 1; row2 < 9; row2++) {
						int[] row2cols = findPairInGroup(groups[row2 * 2], possVal);
						if (row2cols != null) {
							// we have two candidate rows - the columns must be identical
							// if the columns are the same, we have an x-wing!
							if ((row1cols[0] == row2cols[0]) & (row1cols[1] == row2cols[1])) {
								// CONSTS.log("Horizontal X-wing on " + possVal + " rows " + row1 +
								// "/" + row2 + ", cols " + row1cols[0] + "/" + row1cols[1]);
								int[] rows = new int[] { row1, row2 };
								progress |= removeNumFromCol(puzz, possVal, row1cols[0], rows);
								progress |= removeNumFromCol(puzz, possVal, row1cols[1], rows);
							}
						}
					}
				}
			}

			// check for vertical X-Wings
			// Find a column where this possible value occurs exactly twice
			for (int col1 = 0; col1 < 8; col1++) {
				int[] col1rows = findPairInGroup(groups[col1 * 2 + 1], possVal);
				if (col1rows != null) {
					// Find a second column where this possible value occurs exactly twice
					for (int col2 = col1 + 1; col2 < 9; col2++) {
						int[] col2rows = findPairInGroup(groups[col2 * 2 + 1], possVal);
						if (col2rows != null) {
							// we have two candidate columns - the rows must be identical
							// if the rows are the same, we have an x-wing!
							if ((col1rows[0] == col2rows[0]) & (col1rows[1] == col2rows[1])) {
								// CONSTS.log("Vertical X-wing on " + possVal + " cols " + col1 +
								// "/" + col2 + ", rows " + col1rows[0] + "/" + col1rows[1]);
								int[] cols = new int[] { col1, col2 };
								progress |= removeNumFromRow(puzz, possVal, col1rows[0], cols);
								progress |= removeNumFromRow(puzz, possVal, col1rows[1], cols);
							}
						}
					}
				}
			}
		}
		return progress;
	}

	/**
	 * Find the same possible value in exactly two locations within a group If the value occurs too
	 * few or two many times, return null
	 * 
	 * @param Group
	 *            - the group to search
	 * @param possVal
	 *            - the possible-Value to find
	 * @return an integer array with two entries: the locations of the values (0-based), or null if
	 *         no pair was found
	 */
	private static int[] findPairInGroup(int[][] group, int possVal) {
		boolean havePair = false;
		int loc1 = -1;
		int loc2 = -1;

		// look through all cols, and hope to find exactly 2 occurrences...
		for (int loc = 0; loc < 9; loc++) {
			if (group[loc][possVal] == 1) {
				if (loc1 == -1) {
					loc1 = loc;
				} else if (loc2 == -1) {
					loc2 = loc;
					havePair = true;
				} else { // too many
					havePair = false;
				}
			}
		}
		if (havePair) {
			return new int[] { loc1, loc2 };
		} else {
			return null;
		}
	}

	/**
	 * Remove a given number from the possible-values in an entire row, with the exception of the
	 * specified exceptions
	 * 
	 * @param num
	 *            - number to remove
	 * @param row
	 *            - row to remove from
	 * @param exception
	 *            - colums to leave as-is
	 * @return true if any change was made
	 */
	private static boolean removeNumFromRow(int[][][] puzz, int num, int row, int[] except) {
		boolean possValsChanged = false;
		for (int n = 0; n < 9; n++) {
			boolean exception = false;
			for (int e = 0; e < except.length; e++) {
				if (n == except[e]) exception = true;
			}
			if (!exception) {
				if (puzz[row][n][num] == 1) {
					puzz[row][n][num] = 0;
					puzz[row][n][0]--;
					possValsChanged = true;
					// CONSTS.log("removed " + num + " from row " + row + ", col " + n);
				}
			}
		}
		return possValsChanged;
	}

	/**
	 * Remove a given number from the possible-values in an entire column,
	 * with the exception of the specified exceptions
	 * 
	 * @param num - number to remove
	 * @param col - col to remove from
	 * @param except - rows to leave as-is
	 * @return true if any change was made
	 */
	private static boolean removeNumFromCol(int[][][] puzz, int num, int col, int[] except) {
		boolean possValsChanged = false;
		for (int n = 0; n < 9; n++) {
			boolean exception = false;
			for (int e = 0; e < except.length; e++) {
				if ( n == except[e] ) exception = true;
			}
			if (!exception) {
				if (puzz[n][col][num] == 1) {
					puzz[n][col][num] = 0;
					puzz[n][col][0]--;
					possValsChanged = true;
					//CONSTS.log("removed " + num + " from row " + n + ", col " + col);
				}
			}
		}
		return possValsChanged;
	}
}
