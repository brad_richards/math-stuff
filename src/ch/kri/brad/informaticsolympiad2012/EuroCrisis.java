package ch.kri.brad.informaticsolympiad2012;
import java.util.Scanner;


public class EuroCrisis {
	private static final int NUMBER_OF_MICE = 3;
	private static final int CUSTOMS_LIMIT = 300;
	private static int[] prices; // The prices of each item
	
	public static void main(String[] args) {
		readPrices();
		boolean possible = ! isImpossible();
		if (possible) {
			possible = findSolution();
		}
		if (possible) {
			System.out.println("Yes - this is possible");
		} else {
			System.out.println("No - this is not possible");
		}
	}

	/**
	 * Read the prices from the console
	 */
	private static void readPrices() {
		Scanner s = new Scanner(System.in);
		int numPrices = s.nextInt();
		prices = new int[numPrices];
		for (int i = 0; i < numPrices; i++) {
			prices[i] = s.nextInt();
		}
	}
	
	/**
	 * Check for situations that are completely impossible. This method is only here for efficiency.
	 * 
	 * @return true if the problem is impossible to solve
	 */
	private static boolean isImpossible() {
		boolean impossible = false;
		
		// If any single price exceeds the customs limit, the problem is impossible.
		// If the total of all prices exceed the total customs-limit for all mice, the problem is impossible/
		int sum = 0;
		for (int price : prices) {
			if (price > CUSTOMS_LIMIT) impossible = true;
		}
		if (sum > (CUSTOMS_LIMIT * NUMBER_OF_MICE)) impossible = true;
		
		// Other checks could also be made...
		
		return impossible;
	}
	
	/**
	 * We are reduced to the packing problem. This is NP-complete, so we do not try
	 * to be overly clever - just try the various permutations.
	 */
	private static boolean findSolution() {		
		int[] shoppingBagTotal = new int[NUMBER_OF_MICE]; // The total purchases of each mouse
		int[] inShoppingBag = new int[prices.length]; // For each item, which shopping bag is it in?
		
		// Initialize the arrays we will use
		for (int i = 0; i < shoppingBagTotal.length; i++) shoppingBagTotal[i] = 0;
		for (int i = 0; i < inShoppingBag.length; i++) inShoppingBag[i] = -1;

		// Fill the shopping bags of the mice, beginning with mouse 0; if successful, we have a solution
		return fillBags(shoppingBagTotal, inShoppingBag);
	}
	
	private static boolean fillBags(int[] shoppingBagTotals, int[] inShoppingBag) {
		boolean solutionFound = false;

		// If all items have been placed, we have found a solution
		boolean baseCaseDone = true;
		for (int itemPlaced : inShoppingBag) {
			if (itemPlaced == -1) baseCaseDone = false;
		}
		if (baseCaseDone) {
			printShoppingBags(inShoppingBag);
			solutionFound = true;
		} else {
			// We will try placing each item in turn into a shopping bag. After placing one item, we recurse.
			for (int item = 0; item < inShoppingBag.length & !solutionFound; item++) {
				if (inShoppingBag[item] == -1) { // not yet placed
					for (int mouse = 0; mouse < NUMBER_OF_MICE & !solutionFound; mouse++) {
						int newShoppingBagTotal = shoppingBagTotals[mouse] + prices[item];
						if (newShoppingBagTotal <= CUSTOMS_LIMIT) {
							// We need new copies to enable backtracking
							int[] shoppingBagTotals_new = copyArray(shoppingBagTotals);
							int[] inShoppingBag_new = copyArray(inShoppingBag);
							
							inShoppingBag_new[item] = mouse;
							shoppingBagTotals_new[mouse] += prices[item];
							solutionFound = fillBags(shoppingBagTotals_new, inShoppingBag_new);
						}						
					}
				}
			}
		}
		return solutionFound;
	}
	
	private static int[] copyArray(int[] arrayIn) {
		int[] arrayOut = new int[arrayIn.length];
		for (int i = 0; i < arrayIn.length; i++) {
			arrayOut[i] = arrayIn[i];
		}
		return arrayOut;
	}
	
	private static void printShoppingBags(int[] inShoppingBag) {
		for (int mouse = 0; mouse < NUMBER_OF_MICE; mouse++) {
			System.out.print("Mouse " + mouse + " buys items ");
			for (int item = 0; item < inShoppingBag.length; item++) {
				if (inShoppingBag[item] == mouse) {
					System.out.print(prices[item] + " ");
				}
			}
			System.out.println();
		}
	}
}
