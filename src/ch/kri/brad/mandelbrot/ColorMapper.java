package ch.kri.brad.mandelbrot;

import java.awt.Color;

/**
 * We want to offer different options for mapping colors. Any instance of this
 * class has the task of mapping an integer to a color. This may ultimately be
 * a class hierarchy, or else a single class that accepts various options.
 * 
 * For the moment, it is just a very simple class with a static mapping
 */
public class ColorMapper {
	static final Color[] colors = new Color[100];
	
	static {
		for (int i = 0; i < 100; i++) {
			colors[i] = rainbow(i);
		}
	}
	
	public Color map(boolean psychedelic, long value, long iterations) {
		Color c;
		if (value < 0) {
			c = Color.black;
		} else {
			long colorDivisor = (value + 200) / 200;
			if (psychedelic) {
				if ((value % 2) == 1) value += 50;
			}
			long colorValue = (value / colorDivisor) % 100;
			c = colors[(int) colorValue];
		}
		return c;
	}
	
	/**
	 * Produce a color in the rainbow, from 0 (violet) to 99 (red)
	 */
	private static Color rainbow(double i) {
		double frequency = 0.055;
		double offset = 0.76;
		double oneThirdCycle = 2 * Math.PI/3;
		
		i = i * frequency + offset;		
		
		int red = (int) (Math.cos(i) * 127 + 128);
		int green = (int) (Math.cos(i + oneThirdCycle) * 127 + 128);
		int blue = (int) (Math.cos(i + 2 * oneThirdCycle) * 127 + 128);
		
		return new Color(red, green, blue);
	}
}
