package AdventOfCode.aoc_2019.IntCodeComputer;

import java.util.ArrayList;

public class IntCodeComputer {
	private int[] memory;
	private int instructionPointer = 0;
	private ArrayList<Integer> inputs = new ArrayList<>();
	private ArrayList<Integer> outputs = new ArrayList<>();

	/**
	 * Create and initialize memory. The input array is copied, so it will not
	 * be changed by execution
	 */
	public IntCodeComputer(int[] memory) {
		this.memory = new int[memory.length];
		this.reset(memory);
	}

	/**
	 * Used to reset memory to a known state. The input array is copied, so it
	 * will not be modified. The array must be the correct length;
	 * 
	 * @return true if the reset was successful
	 */
	public boolean reset(int[] memory) {
		boolean ok = (memory.length == this.memory.length);
		if (ok) {
			for (int i = 0; i < memory.length; i++)
				this.memory[i] = memory[i];
		}
		instructionPointer = 0;
		inputs = new ArrayList<>();
		outputs = new ArrayList<>();
		return ok;
	}

	public boolean setMemoryLocation(int index, int value) {
		boolean ok = (index >= 0 && index < memory.length);
		if (ok) memory[index] = value;
		return ok;
	}

	public int getMemoryLocation(int index) {
		return memory[index];
	}

	public void addInput(int input) {
		inputs.add(input);
	}

	public ArrayList<Integer> getOutputs() {
		return outputs;
	}

	public void executeProgram() {
		Instruction instruction;
		do {
			instruction = new Instruction(memory[instructionPointer]);
			int operand1, operand2;

			switch (instruction.getOpCode()) {
			case 1:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				setOperand(3, operand1 + operand2);
				break;
			case 2:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				setOperand(3, operand1 * operand2);
				break;
			case 3:
				int input = inputs.remove(0);
				setOperand(1, input);
				break;
			case 4:
				operand1 = getOperand(1, instruction);
				outputs.add(operand1);
				break;
			case 5:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				if (operand1 != 0) instructionPointer = operand2 - instruction.getSize();
				break;
			case 6:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				if (operand1 == 0) instructionPointer = operand2 - instruction.getSize();
				break;
			case 7:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				setOperand(3, (operand1 < operand2) ? 1 : 0);
				break;
			case 8:
				operand1 = getOperand(1, instruction);
				operand2 = getOperand(2, instruction);
				setOperand(3, (operand1 == operand2) ? 1 : 0);
				break;
			case 9:
				break;
			case 10:
				break;
			case 11:
				break;
			case 12:
				break;
			case 13:
				break;
			case 99:
				break; // STOP
			default:
				System.out.println("Invalid opcode " + instruction.getOpCode() + " at position " + instructionPointer);
			}

			instructionPointer += instruction.getSize();
		} while (instruction.getOpCode() != 99);
	}

	/**
	 * Get the specified operand (1-based)
	 */
	private int getOperand(int operand, Instruction instruction) {
		if (instruction.isPositionMode(operand)) {
			return memory[memory[instructionPointer + operand]];
		} else {
			return memory[instructionPointer + operand];
		}
	}

	/**
	 * Set the specified operand (1-based). Always position-based
	 */
	private void setOperand(int operand, int value) {
		memory[memory[instructionPointer + operand]] = value;
	}
}
