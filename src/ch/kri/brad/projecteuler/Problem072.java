package ch.kri.brad.projecteuler;

import java.util.ArrayList;

import ch.kri.brad.utility.Primes;

/**(
 * Based on the solution to problem 70
 */
public class Problem072 {

	public static void main(String[] args) {
		long fractionCount = 0;

		Primes.generatePrimeFactorsErastothenes(1000000);
		for (int i = 0; i < Primes.getPrimeFactorSieve().size(); i++) {
			int n = i + 2; // Actual value being checked
			ArrayList<Integer> primeFactors = Primes.getPrimeFactorSieve().get(i);
			if (primeFactors == null) { // prime number
				fractionCount += n - 1;
			} else {// not a prime number
				int numFactors = primeFactors.size();

				// Calculate total numbers less than n, with a common factor (opposite of
				// totient-function)
				int[] factorCounts = new int[numFactors];
				for (int j = 0; j < numFactors; j++)
					factorCounts[j] = n / primeFactors.get(j) - 1;

				for (int j = 0; j < numFactors - 1; j++) {
					for (int k = j + 1; k < numFactors; k++) {
						factorCounts[k] = factorCounts[k] - (factorCounts[k] / primeFactors.get(j));
					}
				}

				int numNotMutuallyPrime = 0;
				for (int j = 0; j < numFactors; j++)
					numNotMutuallyPrime += factorCounts[j];

				// Calculate totient function
				int totient = (n - 1) - numNotMutuallyPrime;

				fractionCount += totient;
			}
		}
		System.out.println(fractionCount);
	}
}
