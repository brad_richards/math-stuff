package LeetCode.add_two_numbers;

class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    	ListNode a1 = new ListNode();
    	ListNode prevNode = null;
    	ListNode currentNode = a1;
    	int carry = 0;
    	while (l1 != null || l2 != null) {
    		int sum = carry;
    		if (l1 != null) {
    			sum += l1.val;
        		l1 = l1.next;
    		}
    		if (l2 != null) {
    			sum += l2.val;
        		l2 = l2.next;
    		}
    		currentNode.val = sum % 10;
    		carry = sum / 10;
    		prevNode = currentNode;
    		currentNode = new ListNode();
    		prevNode.next = currentNode;
    	}
    	// If carry is 0, then we have one node too many
    	if (carry == 0) { // we have one node too many
    		prevNode.next = null;
    	} else { // We put the carry into this last node
    		currentNode.val = carry;
    	}
    	return a1;
    }
}
