package AdventOfCode.aoc_2019.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	public static void main(String[] args) {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			Integer fuel = stream
					.map(Integer::parseInt)
					.collect(Collectors.summingInt(Problem02::moduleFuel));

			System.out.println(fuel);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static int moduleFuel(int moduleWeight) {
		int total = 0;
		int fuel = moduleWeight / 3 - 2;
		while (fuel > 0) {
			total += fuel;
			fuel = fuel / 3 - 2;
		}
		return total;
	}
}
