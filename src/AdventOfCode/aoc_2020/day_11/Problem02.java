package AdventOfCode.aoc_2020.day_11;

import java.io.IOException;
import java.util.List;

public class Problem02 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> lines = ir.getValues();
		
		Room02 newRoom = new Room02(lines);
		Room02 room = null;
		while (!newRoom.equals(room)) {
			room = newRoom;
			newRoom = room.applyRules();
			// room.printRoom();
		}
		
		System.out.println("Seats occupied: " + room.occupiedSeats());
	}

}
