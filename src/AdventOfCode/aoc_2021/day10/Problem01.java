package AdventOfCode.aoc_2021.day10;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class Problem01 {
	private static ArrayList<String> lines = new ArrayList<>();
	private static ArrayList<Long> completionScores = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		readInput();

		// Part 1: Use a stack to parse each line. If corrupted, add to score and remove line
		long score = 0;
		for (Iterator<String> i = lines.iterator(); i.hasNext();) {
			String line = i.next();
			Stack<Character> stack = new Stack<>();
			long lineScore = 0;
			for (char c : line.toCharArray()) {
				if (c == '(' || c == '[' || c == '{' || c == '<') stack.push(c);
				else {
					char d = stack.peek();
					if (c == ')' && d == '(' ||
						c == ']' && d == '[' ||
						c == '}' && d == '{' ||
						c == '>' && d== '<') {
						stack.pop();
					} else { // Mismatch - corrupted line
						lineScore = charScore(c);
						score += lineScore;
						break;
					}
				}
			}
			if (lineScore > 0) { // Corrupted line
				i.remove(); // Delete corrupted line
			} else { // incomplete line: complete and score the line
				lineScore = 0;
				while (!stack.empty()) {
					char c = stack.pop();
					lineScore *= 5;
					if (c == '(') lineScore += 1;
					if (c == '[') lineScore += 2;
					if (c == '{') lineScore += 3;
					if (c == '<') lineScore += 4;
				}
				completionScores.add(lineScore);
				// System.out.println(lineScore);
			}
		}	
		System.out.println("Part 1: " + score);
		
		Collections.sort(completionScores);
		System.out.println("Part 2: " + completionScores.get(completionScores.size()/2));
	}
	
	private static  int charScore(char c) {
		if (c == ')') return 3;
		else if (c == ']') return 57;
		else if (c == '}') return 1197;
		else if (c == '>') return 25137;
		else return 0;
	}
	
	private static void readInput() throws IOException {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			lines.add(in.nextLine());
		}
	}
}
