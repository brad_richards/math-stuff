package ch.kri.brad.projecteuler;

import java.util.ArrayList;

import ch.kri.brad.utility.Primes;

/**
 * Much the same as problem 76, but we can no longer generate values ourselves; we much choose
 * values from the list of prime numbers.
 */
public class Problem077 {
	private static final int MAX_VALUE = 100;
	private static ArrayList<Long> primes;

	public static void main(String[] args) {
		Primes.generatePrimesErastothenes(MAX_VALUE);
		primes = Primes.getPrimes();
		
		for (int targetValue = 10; targetValue <= MAX_VALUE; targetValue++) {
			int numSolutions = getNumSolutions(targetValue);
			System.out.println("Value " + targetValue + " has solutions " + numSolutions);
			if (numSolutions > 5000) break;			
		}	
	}
	
	
	/**
	 * Method to count solutions for a particular value
	 */
	private static int getNumSolutions(int targetValue) {
		int solutionCount = 0;
		for (int numValues = 2; numValues <= targetValue; numValues++) {
			// Initiate the recursion, using prime values up to TARGET/numValues
			for (int i = 0; i < primes.size() && primes.get(i) <= targetValue / numValues; i++) {
				int firstValue = primes.get(i).intValue();
				solutionCount += countSolutions(targetValue, numValues-1, firstValue, i);
			}
		}
		return solutionCount;
	}
	
	/**
	 * Recursive method to generate all possible solutions. We follow the principle that each value must be >= the previous value; this avoids any duplicates, because each solution is then sorted from first to last
	 */
	private static int countSolutions(int targetValue, int numValuesLeft, int sumSoFar, int lastIndex) {
		int solutionCount = 0;
		int lastValue = primes.get(lastIndex).intValue();
		if (numValuesLeft == 1) { // base case
			int valueRemaining = targetValue - sumSoFar;
			if (valueRemaining >= lastValue) { // Solution valid, if prime
				if (containsPrime(primes, valueRemaining)) solutionCount++;
			}
		} else { // inductive case
			for (int i = lastIndex; i < primes.size() && primes.get(i) <= (targetValue-sumSoFar) / numValuesLeft; i++) {
				int value = primes.get(i).intValue();
				solutionCount += countSolutions(targetValue, numValuesLeft-1, sumSoFar+value, i);
			}
		}
		return solutionCount;
	}

	private static boolean containsPrime(ArrayList<Long> primes, long potentialPrime) {
		boolean found = false;
		for (long prime : primes) {
			if (prime == potentialPrime) {
				found = true;
				break;
			}
		}
		return found;
	}
}
