package kattis.f_arrays;

import java.util.Scanner;

/**
 * Solve this using the String.toCharArray() method
 */
public class classfieldtrip {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String list1 = in.nextLine();
		String list2 = in.nextLine();

		char[] chars1 = list1.toCharArray();
		char[] chars2 = list2.toCharArray();
		
		int c1 = 0;
		int c2 = 0;
		String result = "";		
		while (c1 < chars1.length && c2 < chars2.length) {
			if (chars1[c1] < chars2[c2])
				result += chars1[c1++];
			else
				result += chars2[c2++];
		}
		
		// At the end, one of the lists will have characters left
		if (c1 < chars1.length) {
			for (int i = c1; i < chars1.length; i++) result += chars1[i];
		} else {
			for (int i = c2; i < chars2.length; i++) result += chars2[i];
		}
		
		System.out.println(result);
	}

}
