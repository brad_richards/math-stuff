package ch.kri.brad.primes;

import java.util.ArrayList;
import java.util.TreeSet;

public class BradSieve {

	/**
	 * Simple test program for the Brad's enhanced sieve . The program times itself generating prime
	 * numbers, proceeding by orders of magnitude. That is, it generates all the prime numbers to
	 * 10, then to 100, then to 1000 - up to 10,000,000 - starting over each time.
	 * 
	 * Each step is repeated 100 times, and the times are averaged. The program prints the total
	 * run-time (in milliseconds), as well as the number of primes found. The latter serves as
	 * verification that the program is running correctly.
	 */
	public static void main(String[] args) {
		int limit = 100000001;
		long start, end, millis;
		boolean primes[] = null;
		int numPrimes;
		for (int roundLimit = 1000; roundLimit < limit; roundLimit *= 10) {
			start = System.currentTimeMillis();
			primes = enhancedSieve(roundLimit);
			end = System.currentTimeMillis();
			millis = end - start;
			numPrimes = countPrimes(primes, roundLimit);

			System.out.println("Generated primes to " + roundLimit + " in " + millis
					+ "ms and found " + numPrimes + " primes");
		}
	}

	public static boolean[] enhancedSieve(int roundLimit) {
		boolean primes[] = new boolean[roundLimit * 2];
		BradSieveN n_generator = new BradSieveN();
		int n;

		// Initialize: primes[0] represents 0, primes[1] represents 1, etc.
		primes[0] = false;
		primes[1] = false;
		primes[2] = true;
		primes[3] = true;
		primes[4] = false;
		primes[5] = true;
		primes[6] = false;

		// Fold and re-fold and re-fold and...
		for (n = n_generator.getValue(); n < roundLimit; n = n_generator.next()) {
			primes[n * 2 - 1] = true;
			for (int m = 2; m < n; m++) {
				if (primes[m] && !n_generator.isFactor(m)) {
					primes[n * 2 - m] = true;
				} else {
					primes[n * 2 - m] = false;
				}
			}

			// ----- Special rules -----

			// Get all primes P < N that are not factors of N
			// Limit to primes where P <= 2 * N / smallest-prime
			ArrayList<Integer> specialPrimes = new ArrayList<Integer>();
			int smallestPrime = 0;
			int upperLimit = Integer.MAX_VALUE;
			for (int p = 2; p < n && p <= upperLimit; p++) {
				if (primes[p] && !n_generator.isFactor(p)) {
					specialPrimes.add(p);
					if (upperLimit == Integer.MAX_VALUE) {
						smallestPrime = p;
						upperLimit = 2 * n / p;
					}
				}
			}

			// Create all products of the given primes that are < 2N, up to the maximum possible
			// number of factors. Here we determine how many factors we need, at most.
			int maxFactors = 0;
			int tmpProduct = smallestPrime;
			while (tmpProduct < (2 * n)) {
				maxFactors++;
				tmpProduct *= smallestPrime;
			}

			// For efficient access, convert our ArrayList of factors to an int-array
			int[] factors = new int[specialPrimes.size()];
			int i = 0;
			for (int factor : specialPrimes) {
				factors[i++] = factor;
			}

			// Finally, generate products for all numbers of factors from 2 to the maximum
			TreeSet<Integer> primeProducts = new TreeSet<Integer>();
			for (int numFactors = 2; numFactors <= maxFactors; numFactors++) {
				generateFactors(primeProducts, factors, 2 * n, 1, 0, numFactors);
			}

			// For each product Q: if Q < N, then 2N-Q is prime, else Q is non-prime
			for (int product : primeProducts) {
				if (product < n) {
					primes[2 * n - product] = true;
				} else {
					primes[product] = false;
				}
			}
		}

		return primes;
	}

	/**
	 * Recursive method to generate all possible products of the given factors, subject to a
	 * depth-limit (number of factors) and a value limit (2 * n). There is no return value, because
	 * the products are added directly to the given ArrayList
	 * 
	 * @param primes
	 */
	public static void generateFactors(TreeSet<Integer> products, int[] factors, int valueLimit,
			long productIn, int highestFactorPos, int depthLimit) {
		for (int i = highestFactorPos; i < factors.length; i++) {
			long tmpProduct = productIn * factors[i]; // some too-large products will exceed MaxInt
			if (tmpProduct > valueLimit) break; // factors sorted; remaining will be too large
			if (depthLimit <= 1) { // at our depth limit
				products.add((int) tmpProduct);
			} else {
				generateFactors(products, factors, valueLimit, tmpProduct, i, depthLimit - 1);
			}
		}
	}

	private static int countPrimes(boolean[] primes, int roundLimit) {
		int numPrimes = 0;
		for (int i = 0; i <= roundLimit; i++) {
			if (primes[i]) numPrimes++;
		}
		return numPrimes;
	}

	/**
	 * Represents "N" in the enhanced sieve. N is a member of the sequence 3, 6, 12, 24, 30, 60,
	 * 120, 210, ... as explained in the article. It's is a small cheat, but for efficiency we
	 * provide this class with a list of the first few primes. It would otherwise be required to
	 * derive these from the first primes found.
	 */
	public static class BradSieveN {
		private static final int PRIMES[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 };
		private ArrayList<Integer> factors;
		private int value;

		// Create the first N, which has the value 6
		public BradSieveN() {
			value = 6;
			factors = new ArrayList<Integer>();
			factors.add(2);
			factors.add(3);
		}

		/**
		 * Calculate the next value of N
		 */
		public int next() {
			int lastProduct = 1;
			int product = 1;
			factors = new ArrayList<Integer>();
			for (int i = 0; product <= value; i++) {
				lastProduct = product;
				product *= PRIMES[i];
				factors.add(PRIMES[i]);
			}
			if (product > (2 * value)) {
				// back one step
				product = lastProduct;
				factors.remove(factors.size() - 1);

				// Multiply by 2 until big enough
				while (product <= value)
					product *= 2;
			}
			value = product;
			return value;
		}

		public int getValue() {
			return value;
		}

		public boolean isFactor(int x) {
			for (Integer factor : factors) {
				if (factor.equals(x)) return true;
			}
			return false;
		}
	}
}