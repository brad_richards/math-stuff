package AdventOfCode.aoc_2015.day07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;
	static HashMap<String, Character> wires = new HashMap<>();

	public static void main(String[] args) throws Exception {
		readInput();
		
		// A bit of a cheat, but the inputs use the constant 1
		wires.put("1", (char) 1);

		// Repeatedly go through all inputs, processing the ones for which we
		// have all preconditions. Initially, those are only the ones that have
		// constant values on the
		// left.
		//
		// Each time we encounter an operation, we create a new "wire" with the
		// appropriate value.
		//
		// We stop, as soon as we have a value for Wire "a"
		//
		// Difference from part 1 - we use "char" because it is unsigned. Something,
		// somewhere was going wrong with all the implicit numeric conversions.
		boolean found = false;
		while (!lines.isEmpty() && !found) {
			Iterator<String> i = lines.iterator();
			while (i.hasNext()) {
				String line = i.next();				
				if (process(line)) {
					i.remove();
					found = wires.containsKey("a");
				}
			}
		}
		if (found) {
			System.out.println("Part 1 answer = " + (int) wires.get("a"));
		} else {
			System.out.println("No answer found");
		}
	}
	
	private static boolean process(String in) {
		boolean processed = false;
		String[] parts = in.split(" ");
		
		if (parts[1].equals("->")) { // NOP
			try { // Just a constant?
				char value = (char) Integer.parseInt(parts[0]);
				
				// For Part 2, we override wire b to 956
				if (parts[2].equals("b")) value = (char) 956;
				
				wires.put(parts[2], value);
				processed = true;
			} catch (NumberFormatException e) {
				// Do we have the value on the left?
				if (wires.containsKey(parts[0])) {
					wires.put(parts[2], wires.get(parts[0]));
					processed = true;
				}
			}
		} else if (parts[0].equals("NOT")) {
			if (wires.containsKey(parts[1])) {
				wires.put(parts[3], (char) ~ wires.get(parts[1]));
				processed = true;
			}
		} else if (parts[1].equals("LSHIFT")) {
			if (wires.containsKey(parts[0])) {
				wires.put(parts[4], (char) (wires.get(parts[0]) << Integer.parseInt(parts[2])));
				processed = true;
			}
		} else if (parts[1].equals("RSHIFT")) {
			if (wires.containsKey(parts[0])) {
				wires.put(parts[4], (char) (wires.get(parts[0]) >> Integer.parseInt(parts[2])));
				processed = true;
			}
		} else if (parts[1].equals("AND")) {
			if (wires.containsKey(parts[0]) && wires.containsKey(parts[2])) {
				wires.put(parts[4], (char) (wires.get(parts[0]) & wires.get(parts[2])));
				processed = true;
			}
		} else if (parts[1].equals("OR")) {
			if (wires.containsKey(parts[0]) && wires.containsKey(parts[2])) {
				wires.put(parts[4], (char) (wires.get(parts[0]) | wires.get(parts[2])));
				processed = true;
			}
		} else {
			System.out.println("Unidentified operation: " + in);
		}
		return processed;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}