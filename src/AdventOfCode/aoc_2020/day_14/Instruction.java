package AdventOfCode.aoc_2020.day_14;

public class Instruction {

	public static Instruction parseInstruction(String input) {
		if (input.substring(0, 4).equals("mask")) {
			return new Mask(input.substring(7));
		} else { // assignment
			String[] parts = input.split("\\[|\\]|=");
			return new Assignment(parts[1], parts[3].trim());
		}
	}
}
