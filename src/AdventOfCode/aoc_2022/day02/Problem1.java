package AdventOfCode.aoc_2022.day02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	
	static List<String> lines;
	
	public static void main(String[] args) throws Exception {
		readInput();
		long score = 0;
		
		for (String line : lines) {
			char opp = line.charAt(0);
			char me = (char) (line.charAt(2) - 23);
			
			long roundScore = 0;
			roundScore += playScore(me);
			roundScore += roundResult(opp, me);
//			System.out.println(line + "  " + roundScore);
			
			score += roundScore;
		}
		
		System.out.println(score);
	}
	
	private static long playScore(char played) {
		return played + 1 - 'A';
	}
	
	private static long roundResult(char opp, char me) {
		if (opp == me) return 3;
		if (opp == 'A' && me == 'C') return 0;
		if ((opp - me) == 1) return 0;
		return 6;
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
