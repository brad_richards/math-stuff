package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Primes;



public class Problem007 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Generate far too many primes
		Primes.generatePrimesErastothenes(1000000);

		// Look up the 10001st element (remember: 0-based)
		ArrayList<Long> primes = Primes.getPrimes();
		long answer = primes.get(10000);
		System.out.println("Answer = " + answer);
	}

}
