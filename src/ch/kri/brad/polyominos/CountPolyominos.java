package ch.kri.brad.polyominos;

import java.util.ArrayList;

public class CountPolyominos {


	public static void main(String[] args) {
		Polyomino one = new Polyomino(1);
		one.setField(0,0);
		ArrayList<Polyomino> lastSize = new ArrayList<>();
		lastSize.add(one);

		long lastMillis = System.currentTimeMillis();
		for (int size = 2; size <= 10; size++) {
			ArrayList<Polyomino> newSize = new ArrayList<>();
			for (Polyomino o : lastSize) {
				Polyomino.extendPolyomino(newSize, o);
			}
			
			long currentMillis = System.currentTimeMillis();
			long diff = currentMillis - lastMillis;
			System.out.println("Size " + size + " contains " + newSize.size() + " polyominos in " + diff + "ms");
			lastMillis = currentMillis;
			
			lastSize = newSize;
		}
	}
}
