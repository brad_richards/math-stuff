package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Brute-force approach: Find all pairs such that x > y, x+y and x-y are both perfect squares. Then
 * search for triplets from these pairs.
 */
public class Problem142_brute_force {
	private static final long LIMIT = 30000;
	private static TreeSet<Long> perfectSquares = new TreeSet<>();
	private static HashMap<Long, ArrayList<Pair>> pairs = new HashMap<>();

	public static void main(String[] args) {
		createPerfectSquares();
		findPairs();
		findTriplets();
	}

	/**
	 * Calculate all squares up to 2 * LIMIT - 1
	 */
	private static void createPerfectSquares() {
		for (long num = 1; num < LIMIT * 2; num++) {
			perfectSquares.add(num * num);
		}
	}

	/**
	 * Find all pairs up to LIMIT
	 */
	private static void findPairs() {
		long time = System.currentTimeMillis();
		long numPairs = 0;
		for (long larger = 2; larger <= LIMIT; larger++) {
			for (long smaller = 1; smaller < larger; smaller++) {
				if (perfectSquares.contains(smaller + larger)
						&& perfectSquares.contains(larger - smaller)) {
					ArrayList<Pair> pairList;
					if (pairs.containsKey(smaller)) {
						pairList = pairs.get(smaller);
					} else {
						pairList = new ArrayList<>();
						pairs.put(smaller, pairList);
					}
					Pair newPair = new Pair(smaller, larger);
					pairList.add(newPair);
					numPairs++;
				}
			}
		}
		time = (System.currentTimeMillis() - time) / 1000;
		System.out.println(numPairs + " pairs generated in " + time + " seconds");
	}

	/**
	 * Search for triplets
	 */
	private static void findTriplets() {
		// First, we need two pairs with the same smaller element
		for (Iterator<Long> i = pairs.keySet().iterator(); i.hasNext();) {
			ArrayList<Pair> pairList = pairs.get(i.next());
			
			for (int j = 0; j < pairList.size(); j++) {
				Pair pair1 = pairList.get(j);
				for (int k = j + 1; k < pairList.size(); k++) {
					Pair pair2 = pairList.get(k);

					// Now a third pair containing the larger elements from the first two pairs
					Pair pair3 = new Pair(pair1.larger, pair2.larger);
					ArrayList<Pair> pair3List = pairs.get(pair3.smaller);
					if (pair3List != null && pair3List.contains(pair3)) {
						System.out.println("Solution found: " + pair1.smaller + ", " + pair1.larger + ", " + pair2.larger);
					}
				}
			}
		}

	}

	/**
	 * Pairs contain two longs, and should be indexed by their smaller element
	 */
	private static class Pair {
		public long smaller;
		public long larger;

		public Pair(long a, long b) {
			if (a < b) {
				this.smaller = a;
				this.larger = b;
			} else {
				this.smaller = b;
				this.larger = a;
			}
		}
		
		@Override
		public boolean equals(Object o) {
			boolean equal = false;
			if (o instanceof Pair) {
				Pair p = (Pair) o;
				equal = (p.smaller == this.smaller && p.larger == this.larger);
			}
			return equal;
		}
	}
}
