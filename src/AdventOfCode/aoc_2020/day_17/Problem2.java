package AdventOfCode.aoc_2020.day_17;

import java.io.IOException;

/**
 * Trivial adaptation of part 1, just adding an extra dimension onto the front
 */
public class Problem2 {
	private static final int NUM_CYCLES = 6;
	private static boolean[][][][] grid;

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		boolean[][] grid2d = ir.getGrid();
		int xySize = 2 * NUM_CYCLES + grid2d.length;
		int wzSize = 2 * NUM_CYCLES + 1;
		grid = new boolean[wzSize + 2][wzSize + 2][xySize + 2][xySize + 2];

		// Copy grid2d into place in grid
		copyIntoGrid(grid2d);

		// Print the grid
		// printGrid(-1);

		// Execute the required number of cycles
		for (int cycle = 0; cycle < NUM_CYCLES; cycle++) {
			executeCycle();

			// Print the grid
			// printGrid(cycle);
		}

		// Print the number of active cells
		System.out.println("Number of active cells = " + countActive());
	}

	private static void copyIntoGrid(boolean[][] grid2d) {
		int wz = NUM_CYCLES + 1;
		for (int y = 0; y < grid2d.length; y++) {
			for (int x = 0; x < grid2d[0].length; x++) {
				grid[wz][wz][y + NUM_CYCLES][x + NUM_CYCLES] = grid2d[y][x];
			}
		}
	}

	/**
	 * We create a new grid, calculate the new activations, and then replace the existing grid. Our
	 * grid is deliberately 1 position larger than needed on all sides, so we do not have to worry
	 * about cells on the edges.
	 */
	private static void executeCycle() {
		boolean[][][][] newGrid = new boolean[grid.length][grid[0].length][grid[0][0].length][grid[0][0][0].length];

		for (int w = 1; w < grid.length - 1; w++) {
			for (int z = 1; z < grid[0].length - 1; z++) {
				for (int y = 1; y < grid[0][0].length - 1; y++) {
					for (int x = 1; x < grid[0][0][0].length - 1; x++) {
						int count = countNeighbors(w, z, y, x);
						newGrid[w][z][y][x] = (grid[w][z][y][x] && (count == 2 || count == 3))
								|| (!grid[w][z][y][x] && count == 3);
					}
				}
			}
		}
		grid = newGrid;
	}

	private static int countNeighbors(int w, int z, int y, int x) {
		int count = grid[w][z][y][x] ? -1 : 0;
		for (int cw = w - 1; cw <= w + 1; cw++) {
			for (int cz = z - 1; cz <= z + 1; cz++) {
				for (int cy = y - 1; cy <= y + 1; cy++) {
					for (int cx = x - 1; cx <= x + 1; cx++) {
						if (grid[cw][cz][cy][cx]) count++;
					}
				}
			}
		}
		return count;
	}

	// private static void printGrid(int cycle) {
	// System.out.println("\n===== Cycle " + (cycle + 1) + " =====\n");
	// for (int z = 1; z < grid.length - 1; z++) {
	// System.out.println("----- Layer " + z + " -----");
	// for (int y = 1; y < grid[0].length - 1; y++) {
	// for (int x = 1; x < grid[0][0].length - 1; x++) {
	// System.out.print(grid[z][y][x] ? "#" : ".");
	// }
	// System.out.println();
	// }
	// System.out.println();
	// }
	// }

	private static int countActive() {
		int count = 0;
		for (int w = 1; w < grid.length - 1; w++) {
			for (int z = 1; z < grid[0].length - 1; z++) {
				for (int y = 1; y < grid[0][0].length - 1; y++) {
					for (int x = 1; x < grid[0][0][0].length - 1; x++) {
						if (grid[w][z][y][x]) count++;
					}
				}
			}
		}
		return count;
	}
}
