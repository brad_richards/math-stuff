package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.Digits;



public class Problem020 {

	public static void main(String[] args) {
		// Calculate 100!
		BigInteger value = BigInteger.ONE;
		for (int i = 2; i <= 100; i++) {
			BigInteger factor = BigInteger.valueOf(i);
			value = value.multiply(factor);
		}
		
		ArrayList<Integer> digits = Digits.toDigits(value);
		
		// Add up the digits
		int sum = 0;
		for (int digit : digits) {
			sum += digit;
		}
		
		System.out.println("Answer = " + sum);
	}

}
