package AdventOfCode.aoc_2022.day05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	
	static List<String> lines;
	static ArrayList<Stack<Character>> crates = new ArrayList<>();
	
	private static class Move {
		int number;
		int from;
		int to;
	}
	
	public static void main(String[] args) throws Exception {
		readInput();
		
		// Parse the initial crate stacks
		parseCrates(lines);
		
//		// Only movements now remain
		for (String line : lines) {
			Move move = parseMove(line);
			
			for (int i = 0; i < move.number; i++) {
				Character c = crates.get(move.from - 1).pop();
				crates.get(move.to - 1).push(c);
			}
		}

		String answer = createAnswer();
		System.out.println(answer);
	}
	
	private static String createAnswer() {
		String answer = "";
		for (Stack<Character> s : crates) {
			answer += s.pop();
		}
		return answer;
	}
	
	private static Move parseMove(String line) {
		Move move = new Move();
		String[] parts = line.split(" ");
		move.number = Integer.parseInt(parts[1]);
		move.from = Integer.parseInt(parts[3]);
		move.to = Integer.parseInt(parts[5]);
		return move;
	}
	
	private static void parseCrates(List<String> lines) {
		// Find the line containing the stack numbers
		int numberLine = 0;
		while (!Character.isDigit(lines.get(numberLine).charAt(1))) numberLine++;
		
		// Find the total number of stacks
		String nLine = lines.get(numberLine);
		int numStacks = nLine.charAt(nLine.length()-2) - '0';

		// Create the stacks
		for (int i = 0; i < numStacks; i++) {
			crates.add(new Stack<>());
		}
		
		// Work backwards through the lines, adding crates to the stacks
		for (int i = numberLine-1; i >= 0; i--) {
			String cLine = lines.get(i);
			for (int stackNum = 1, charPos = 1; stackNum <= numStacks; stackNum++, charPos += 4) {
				char c = cLine.charAt(charPos);
				if (c != ' ') crates.get(stackNum-1).push(c);
			}
		}
		
		// Delete the initial lines from the input
		for (int i = 0; i <= numberLine+1; i++) lines.remove(0);
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
