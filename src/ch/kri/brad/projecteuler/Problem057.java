package ch.kri.brad.projecteuler;

import java.math.BigInteger;

import ch.kri.brad.utility.Digits;

/**
 * Replace the 2 with (1+1), and we discover an easy way to use each iteration as the basis for the
 * next.
 * 
 * 1 + 1 / (1 + 1)
 * 
 * 1 + 1 / (1 + [ 1 + 1 / (1 + 1) ] )
 * 
 * In other words, if x is the result of the last iteration, then we add 1, invert the result, and
 * add 1 again.
 * 
 * Since the problem requires us to work with explicit fractions, we will maintain numerator and
 * denominator as BigIntegers.
 * 
 * We could make this more efficient by *not* using objects, but hey...it's only 1000 objects
 */
public class Problem057 {
	static int ITERATIONS = 1000;
	static BigInteger numerator = BigInteger.ONE;
	static BigInteger denominator = BigInteger.ONE;
	
	public static void main(String[] args) {
		int count = 0;
		for (int i = 0; i < ITERATIONS; i++) {
			iterate();
			if (Digits.numDigits(numerator) > Digits.numDigits(denominator)) count++;
		}		
		System.out.println("Answer = " + count);
	}
	
	private static void iterate() {
		numerator = numerator.add(denominator); // add one
		BigInteger tmp = numerator; // invert
		numerator = denominator;
		denominator = tmp;
		numerator = numerator.add(denominator); // add one
	}
}
