package hackerRank.projectEuler;

import java.util.Scanner;

/**
 * Maximum factor is given as 40; we only need to consider the primes
 */
public class Problem005 {
	static long[] primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};

	public static void main(String[] args) {
		long[] testCases = readData();
		
		for (long value : testCases)
			System.out.println(doOneCase(value));
	}

	public static long[] readData() {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		long[] data = new long[size];
		for (int i = 0; i < size; i++) {
			data[i] = s.nextLong();
		}
		s.close();
		return data;
	}


	/**
	 * For each prime
	 */
	public static long doOneCase(long limit) {
		long answer = 1;
		for (int i = 0; i <= limit && primes[i] <= limit; i++) {
			long prime = primes[i];
			long thisFactor = 1;
			while (thisFactor <= limit/prime) {
				thisFactor *= prime;
			}
			answer *= thisFactor;
		}
		return answer;
	}

}
