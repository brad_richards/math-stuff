package AdventOfCode.aoc_2020.day_11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	private List<String> values;
	
	public InputReader(String fileName) throws IOException {
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			values = stream.collect(Collectors.toList());
		}
	}
	
	public List<String> getValues() {
		return values;
	}
}
