package AdventOfCode.aoc_2023.day_05;

import java.util.ArrayList;
import java.util.List;

public class Mapper {
	private final List<Range> ranges = new ArrayList<>();
	
	private static final class Range {
		final long destStart;
		final long sourceStart;
		final long range;
		
		public Range(String line) {
			String[] parts = line.split(" ");
			destStart = Long.parseLong(parts[0]);
			sourceStart = Long.parseLong(parts[1]);
			range = Long.parseLong(parts[2]);
		}
		
		public boolean inRange(long value) {
			return (value >= sourceStart  && value < sourceStart + range);
		}
		
		public long map(long in) {
			return in - sourceStart + destStart;
		}
	}
	
	public void addRange(String line) {
		Range r = new Range(line);
		ranges.add(r);
	}
	
	public long map(long in) {
		Range range = null;
		for (Range r : ranges) {
			if (r.inRange(in)) {
				range = r;
				break;
			}
		}
		return (range != null) ? range.map(in) : in;
	}
	
	public int size() {
		return ranges.size();
	}
}
