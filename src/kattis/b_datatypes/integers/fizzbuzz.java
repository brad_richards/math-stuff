package kattis.b_datatypes.integers;

import java.util.Scanner;

public class fizzbuzz {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int fizz = in.nextInt();
		int buzz = in.nextInt();
		int max = in.nextInt();
		
		for (int i = 1; i <= max; i++) {
			String out = "";
			if (i % fizz == 0) out += "Fizz";
			if (i % buzz == 0) out += "Buzz";
			if (out.isEmpty()) out = Integer.toString(i);
			System.out.println(out);
		}
	}

}
