package AdventOfCode.aoc_2020.day_04;

public class Passport2 {
	private Integer birthYear;
	private Integer issueYear;
	private Integer expiryYear;
	private Integer height;
	private String hairColor;
	private EyeColor eyeColor;
	private Long passportID;
	private String countryID; // not parsed
	
	private String input;

	public enum EyeColor {
		amb, blu, brn, gry, grn, hzl, oth
	};

	public Passport2(String input) {
		this.input = input;
		String[] fields = input.split(" ");
		for (String field : fields) {
			String[] parts = field.split(":");
			switch (parts[0]) {
			case "byr":
				try {
					birthYear = Integer.parseInt(parts[1]);
					if (birthYear < 1920 || birthYear > 2002) birthYear = null;
				} catch (NumberFormatException e) {
				}
				break;
			case "iyr":
				try {
					issueYear = Integer.parseInt(parts[1]);
					if (issueYear < 2010 || issueYear > 2020) issueYear = null;
				} catch (NumberFormatException e) {
				}
				break;
			case "eyr":
				try {
					expiryYear = Integer.parseInt(parts[1]);
					if (expiryYear < 2020 || expiryYear > 2030) expiryYear = null;
				} catch (NumberFormatException e) {
				}
				break;
			case "hgt": // remove "cm" from end
				String num = parts[1].substring(0, parts[1].length() - 2);
				String units = parts[1].substring(parts[1].length() - 2);
				try {
					height = Integer.parseInt(num);
					if (units.equals("cm") && (height < 150 || height > 193)) height = null;
					if (units.equals("in") && (height < 59 || height > 76)) height = null;
					if (!units.equals("cm") && !units.equals("in")) height = null;
				} catch (NumberFormatException e) {
				}
				break;
			case "hcl":
				hairColor = parts[1];
				if (!hairColor.matches("#[a-f0-9]{6}")) hairColor = null;
				break;
			case "ecl":
				try {
					eyeColor = EyeColor.valueOf(parts[1]);
				} catch (IllegalArgumentException e) {
				}
				break;
			case "pid":
				try {
					passportID = Long.parseLong(parts[1]);
					if (parts[1].length() != 9) passportID = null;
				} catch (NumberFormatException e) {
				}
				break;
			case "cid":
					countryID = parts[1];
				break;
			}
		}
		if (this.isValid()) System.out.println(input);
	}

	public boolean isValid() {
		return birthYear != null && issueYear != null && expiryYear != null && height != null && hairColor != null
				&& eyeColor != null && passportID != null;
	}
}
