package AdventOfCode.aoc_2022.day01;

import java.io.InputStream;
import java.util.Scanner;

public class Problem1_v2 {

	public static void main(String[] args) {
		InputStream file = Problem1_v2.class.getResourceAsStream("sample.txt");
		try (Scanner in = new Scanner(file) ) {
			while (in.hasNextLine()) {
				String line = in.nextLine();
				System.out.println(line);
			}
		}
	}

}
