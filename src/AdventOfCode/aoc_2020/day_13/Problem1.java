
package AdventOfCode.aoc_2020.day_13;

import java.io.IOException;
import java.util.ArrayList;

public class Problem1 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		int timestamp = ir.getTimestamp();
		ArrayList<Integer> busIDs = ir.getBusIDs();

		int bestBus = -1;
		int bestTimeToBus = Integer.MAX_VALUE;
		for (int busID : busIDs) {
			int mod = timestamp % busID;
			int timeToBus = (mod == 0) ? 0 : busID - mod;
			if (timeToBus < bestTimeToBus) {
				bestTimeToBus = timeToBus;
				bestBus = busID;
			}
		}
		
		System.out.println(bestBus * bestTimeToBus);
	}

}
