package ch.kri.brad.utility;

import static ch.kri.brad.utility.Fibonacci.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class FibonacciTest {
	private final long[] tenFibonacciNumbers = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55};

	@Test
	public void testGenerateFibonacci() {
		generateFibonacci(60);
		ArrayList<Long> fibonacciNumbers = getFibonacciNumbers();
		assertEquals(fibonacciNumbers.size(), 10);
		for (Long fn : tenFibonacciNumbers) {
			fibonacciNumbers.remove(fn);
		}
		assertEquals(fibonacciNumbers.size(), 0);
	}

}
