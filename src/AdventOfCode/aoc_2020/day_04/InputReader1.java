package AdventOfCode.aoc_2020.day_04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader1 {
	ArrayList<Passport1> passports = new ArrayList<>();
	
	public InputReader1(String fileName) throws IOException {
		List<String> lines;
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		
		String passportString = "";
		for (String line : lines) {
			if (!line.isBlank()) {
				passportString += " " + line;
			} else {
				passports.add(new Passport1(passportString));
				passportString = "";
			}
		}
		passports.add(new Passport1(passportString));
	}
	
	public ArrayList<Passport1> getPassports() {
		return passports;
	}
}
