package ch.kri.brad.projecteuler;
import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.Primes;


public class Problem035 {
	static boolean[] rawPrimeSieve; // position 0 represents number 2
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int totalCircularPrimes = 0;
		
		// Generate primes to 1000000
		Primes.generatePrimesErastothenes(1000000);
		rawPrimeSieve = Primes.getRawSieve();

		// For each prime below 1000000
		for (int value = 2; value < 1000000; value++) {
			if (rawPrimeSieve[value-2]) {
				// Convert digits to int[]
				int[] digits = Digits.toDigits(value);
				
				boolean ok = true;
				// Check primeness of each rotation of digits
				for (int i = 1; ok && i < digits.length; i++) {
					rotateDigits(digits);
					int check = digitsToNum(digits);
					ok = rawPrimeSieve[check-2];
				}
				
				if (ok) totalCircularPrimes++;
			}
		}
		
		// Print answer
		System.out.println("Answer = " + totalCircularPrimes);
	}
	
	static void rotateDigits(int[] digits) {
		int tmpDigit = digits[0];
		for (int i = 1; i < digits.length; i++) {
			digits[i-1] = digits[i];
		}
		digits[digits.length-1] = tmpDigit;
	}
	
	static int digitsToNum(int[] digits) {
		int num = 0;
		for (int digit : digits) {
			num = num * 10 + digit;
		}
		return num;
	}

}
