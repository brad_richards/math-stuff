package LeetCode.zigzag_conversion;

// We are actually going to make the conversion into a 2-dim array
public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.convert("A", 1));
        System.out.println(s.convert("AB", 1));
        System.out.println(s.convert("AB", 2));
        System.out.println(s.convert("PAYPALISHIRING", 1));
        System.out.println(s.convert("PAYPALISHIRING", 3));
        System.out.println(s.convert("PAYPALISHIRING", 4));
    }

    public String convert(String s, int numRows) {
        if (s.length() == 0) return "";

        int numCols = 0;
        if (numRows == 1) numCols = s.length();
        else if (numRows == 2) numCols = (s.length() + 1) / 2;
        else numCols = (numRows - 1) * s.length() / ((3 * numRows)/2);
        numCols++; // Covers some border cases...

        char[][] target = new char[numRows][numCols];
        int row = 0;
        int col = 0;
        int pos = 0;
        while (pos < s.length()) {
            if (row == 0) {
                for (int i = 0; i < numRows && pos < s.length(); i++) {
                    target[row++][col] = s.charAt(pos++);
                }
                col++;
                row = Math.max(row-2, 0);
            } else {
                target[row--][col++] = s.charAt(pos++);
            }
        }

        char[] out = new char[s.length()];
        pos = 0;
        for (int j = 0; j < numRows; j++) {
            for (int i = 0; i < numCols; i++) {
                if (target[j][i] != 0) out[pos++] = target[j][i];
            }
        }
        return new String(out);
    }
}
