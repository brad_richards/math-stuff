package AdventOfCode.aoc_2020.day_02;

public class Password {
	private char required;
	private int value1;
	private int value2;
	private String password;

	public Password(String input) {
		String[] parts = input.split(":");
		this.password = parts[1].trim();
		parts = parts[0].split(" ");
		this.required = parts[1].charAt(0);
		parts = parts[0].split("-");
		this.value1 = Integer.parseInt(parts[0]);
		this.value2 = Integer.parseInt(parts[1]);
	}
	
	/**
	 * Valid according to problem 1
	 */
	public boolean isValid_1() {
		char[] chars = password.toCharArray();
		int count = 0;
		for (char c : chars) {
			if (c == required) count++;
		}
		return (count >= value1 && count <= value2);
	}
	
	/**
	 * Valid according to problem 2
	 */
	public boolean isValid_2() {
		char[] chars = password.toCharArray();
		int count = 0;
		if (chars[value1-1] == required) count++;
		if (chars[value2-1] == required) count++;
		return (count == 1);
	}
}
