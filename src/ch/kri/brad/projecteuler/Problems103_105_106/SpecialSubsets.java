package ch.kri.brad.projecteuler.Problems103_105_106;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SpecialSubsets {

	public static void main(String[] args) {
		System.out.println("Problem 103");
		findOptimalSolution(3, 5);
		findOptimalSolution(4, 8);
		findOptimalSolution(5, 14);
		findOptimalSolution(6, 25);
		findOptimalSolution(7, 46);
		
		// Note: For problem 105, our "optimized" code is inadequate, as it only performs equality checks on subsets up to size 3
		System.out.println("\n\nProblem 105");
		InputReader105 in = new InputReader105();
		ArrayList<int[]> sets = in.readSets();
		long startTime = System.currentTimeMillis();
		int sum = 0;
		for (int[] set : sets) {
			if (Nonoptimized.testSet(set)) sum += Arrays.stream(set).sum();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Sum is " + sum + " (should be 73702) in " + (endTime - startTime) + "ms");

	}

	public static void findOptimalSolution(int setSize, int MAX_VALUE) {
		long startTime = System.currentTimeMillis();
		
		Set<int[]> setSolutions = new HashSet<>();
		int[] elements = new int[setSize]; // elements incremented, to create all possible sets
		searchSets(0, elements, MAX_VALUE, setSolutions);

		// Find set with lowest sum
		int bestSum = Integer.MAX_VALUE;
		int[] bestSet = null;
		for (int[]set : setSolutions) {
			int sum = Arrays.stream(set).sum();
			if (sum < bestSum) {
				bestSum = sum;
				bestSet = set;
			}
		}

		long endTime = System.currentTimeMillis();
		// Print answer
		System.out.print("Optimal set of size " + setSize + " is: ");
		if (bestSet == null) {
			System.out.println("NOT FOUND");
		} else {
			for (int elt : bestSet)
				System.out.print(elt + " ");
			System.out.println(" (" + (endTime-startTime) + "ms)");
		}
	}

	public static void searchSets(int index, int[] elements, int MAX_VALUE,
			Set<int[]> setSolutions) {
		if (index >= elements.length) { // Set is complete - test it
			if (Optimized.testSet(elements)) setSolutions.add(elements.clone());
		} else { // Still building set
			int startValue = (index == 0 ? 1 : elements[index - 1] + 1);
			int endValue = MAX_VALUE + index + 1 - elements.length;
			for (int eltValue = startValue; eltValue <= endValue; eltValue++) {
				elements[index] = eltValue;
				searchSets(index + 1, elements, MAX_VALUE, setSolutions);
			}
		}
	}


}
