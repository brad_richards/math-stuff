package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.Digits;


public class Problem016 {

	public static void main(String[] args) {
		BigInteger two_power_10 = new BigInteger("1024");

		// Calculate 2 to the 100th
		BigInteger two_power_100 = BigInteger.ONE;
		for (int i = 1; i <= 10; i++) {
			two_power_100 = two_power_100.multiply(two_power_10);
		}

		// Calculate 2 to the 1000th
		BigInteger two_power_1000 = BigInteger.ONE;
		for (int i = 1; i <= 10; i++) {
			two_power_1000 = two_power_1000.multiply(two_power_100);
		}
		
		ArrayList<Integer> digits = Digits.toDigits(two_power_1000);

		int sum = 0;
		for (int digit : digits) {
			sum += digit;
		}

		System.out.println("Answer = " + sum);
	}

}
