package AdventOfCode.aoc_2020.day_07;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Problem02 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> lines = ir.getLines();
		
		// Create the collection of all bags
		BagCollection allBags = new BagCollection(lines);
		
		// Find target bag-type
		Bag targetBag = allBags.get("shiny gold");
		
		// Count the bags we see, as we work down the bag hierarchy
		List<Bag> pendingBags = targetBag.getContents();
		int totalBags = pendingBags.size();
		while (!pendingBags.isEmpty()) {
			Bag b = pendingBags.remove(0);
			List<Bag> contents = b.getContents();
			totalBags += contents.size();
			pendingBags.addAll(contents);
		}
		
		System.out.println("Total bags = " + totalBags);
	}
}
