package AdventOfCode.aoc_2018.day02;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Part1 {
	private static ArrayList<String> inputValues = new ArrayList<>();
	
	public static void main(String[] args) {
		readInputs();
		int checksum = calcChecksum();
		System.out.println("Checksum is " + checksum); // Solution to part 1
	}

	private static void readInputs() {
		InputStream fis = Part1.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis) ) {
			while (in.hasNext()) {
				inputValues.add(in.nextLine());
			}
		}
	}
	
	private static int calcChecksum() {
		int numTwos = 0;
		int numThrees = 0;
		for (String value : inputValues) {
			int[] letterCounts = getCounts(value);
			boolean hasTwo = false;
			boolean hasThree = false;
			for (int count : letterCounts) {
				hasTwo = hasTwo || count == 2;
				hasThree = hasThree || count == 3;
			}
			if (hasTwo) numTwos++;
			if (hasThree) numThrees++;
		}
		return numTwos * numThrees;
	}
	
	/**
	 * The inputs consist solely of lower case letters. We create a 26-place array, and count the occurrences of each letter.
	 */
	private static int[] getCounts(String value) {
		int[] letterCounts = new int[26];
		for (char c : value.toCharArray()) {
			letterCounts[ c - 'a']++;
		}
		return letterCounts;
	}
}
