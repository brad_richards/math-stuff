package AdventOfCode.aoc_2020.day_02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem01 {

	private static List<Password> passwords;
	
	public static void main(String[] args) throws IOException {
		readInput();

		int count = 0;
		for (Password p : passwords) {
			if (p.isValid_1()) count++;
		}
		System.out.println("Number of valid passwords: " + count);
	}


	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			passwords = stream.map(s -> new Password(s)).collect(Collectors.toList());
		}
	}
}
