package ch.kri.brad.utility;

import java.util.Collection;
import java.util.Iterator;

/**
 * A RecursableList implements a Prolog-like list that can be easily used in recursive methods. The list
 * can be passed in a recursive call, where elements can be added to or removed from the head of the list
 * <i>without altering the list in the calling instance</i>. Hence, when the recursive call returns, the
 * changes it made to the head of the list disappear.
 * 
 * Elements in a RecursableList may not be null, because an explicit null element is always used to mark the end
 * of a RecursableList.
 * 
 * A bit uncomfortable, but adding a new element to the head can only be done with a static method.
 */
public class RecursableList<T> {
	private T head;
	private RecursableList<T> tail;

	/**
	 * Public constructor for creating a new, empty list
	 */
	public RecursableList() {
		this.head = null;
		this.tail = null;
	}
	
	public static <T> RecursableList<T> addHead(T data, RecursableList<T> list) {
		return new RecursableList<T>(data, list);
	}
	
	/**
	 * Private constructor for lengthening a list by adding a new head element
	 */
	private RecursableList(T head, RecursableList<T> tail) {
		if (head == null) throw new NullPointerException("Element may not be null!");
		this.head = head;
		this.tail = tail;
	}

	public T head() {
		return head;
	}
	
	public RecursableList<T> tail() {
		return tail;
	}
	
	public int size() {
		if (head == null)
			return 0;
		else
			return 1 + tail.size();
	}

	public boolean isEmpty() {
		return (head == null);
	}

	public boolean contains(Object o) {
		return head.equals(o) || tail.contains(o);
	}
}
