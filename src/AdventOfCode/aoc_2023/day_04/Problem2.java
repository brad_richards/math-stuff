package AdventOfCode.aoc_2023.day_04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<Card> cards;

	public static void main(String[] args) throws Exception {
		readInput();

		// Array to track how many of each card we have
		int[] cardQty = new int[cards.size()];
		Arrays.fill(cardQty, 1);
		
		for (int i = 0; i < cards.size(); i++) {
			Card c = cards.get(i);
			// Add one to "numMatches" following cards
			for (int j = i+1; j < i + 1 + c.numMatches && j < cards.size(); j++) {
				cardQty[j] += cardQty[i];
			}
		}
		
		int sum = 0;
		for (int qty : cardQty) sum += qty;
		System.out.println(sum);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			cards = stream.map(s -> new Card(s)).collect(Collectors.toList());
		}
	}
}