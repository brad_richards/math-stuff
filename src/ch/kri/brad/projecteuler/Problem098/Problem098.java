package ch.kri.brad.projecteuler.Problem098;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Problem098 {
	private static ArrayList<AnagramPair> anagrams;
	private static HashMap<Long, AnagramPair> squareAnagrams;
	

	public static void main(String[] args) {
		InputReader in = new InputReader();
		ArrayList<String> words = in.readWords();
		System.out.println("Words read: " + words.size());

		AnagramFinder finder = new AnagramFinder();
		anagrams = finder.findAnagrams(words);
		
		ArrayList<String> squares = getSquaresAsStrings();
		ArrayList<AnagramPair> squareAnagramsAsList = finder.findAnagrams(squares);
		
		// Convert square-anagrams into hashtable, indexed by *both* values of each pair
		squareAnagrams = indexByValues(squareAnagramsAsList);
		
		// For each pair of anagram words, see if we can find a matching pair of square-values
		// We are only looking for the maximum square value that we can find. Note that we look
		// under both possible index values, and it may well be that only one of these matches a
		// square-pair.
		int maxValue = 0;
		for (AnagramPair wordPair : anagrams) {
			Long value = wordPair.getValue1();
			if (squareAnagrams.containsKey(value)) {
				AnagramPair squarePair = squareAnagrams.get(value);
				
				System.out.println("Match found: " + wordPair.getWord1() + " " + wordPair.getWord2() + " " + squarePair.getWord1() + " " + squarePair.getWord2());
				
				int a1 = Integer.parseInt(squarePair.getWord1());
				int a2 = Integer.parseInt(squarePair.getWord2());
				maxValue = Math.max(maxValue, Math.max(a1, a2) );
			}
		}
		System.out.println("Overall highest value found is " + maxValue);
	}

	/**
	 * Return all squares (with 2-9 digits) as strings
	 */
	private static ArrayList<String> getSquaresAsStrings() {
		ArrayList<String> squaresAsStrings = new ArrayList<>();
		long n = 4;
		long square = n * n;
		while (square < 10000000000l) {
			squaresAsStrings.add(Long.toString(square));
			n++;
			square = n * n;
		}
		return squaresAsStrings;
	}
	
	private static HashMap<Long, AnagramPair> indexByValues(ArrayList<AnagramPair> squareAnagramsAsList) {
		HashMap<Long, AnagramPair> indexedAnagrams = new HashMap<>();
		for (AnagramPair pair : squareAnagramsAsList) {
			Long value = pair.getValue1();
			indexedAnagrams.put(value,  pair);
			value = pair.getValue2();
			indexedAnagrams.put(value, pair);
		}
		return indexedAnagrams;
	}

}
