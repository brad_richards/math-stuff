package kattis.d_characters_and_digits;

import java.util.Scanner;

public class digitswap {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int value = in.nextInt();
		int tens = value / 10;
		int ones = value % 10;
		int swapped = ones * 10 + tens;
		System.out.println(swapped);
	}

}
