package AdventOfCode.aoc_2019.day03;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Derived from problem 1, but we use an explicit comparator and we have more
 * searching to do...
 */
public class Problem02 {
	private static ArrayList<Wire> wires = new ArrayList<>();

	public static void main(String[] args) {
		readInput();
		
		// Find all common points
		Iterator<Point> i0 = wires.get(0).getPoints().iterator();
		Iterator<Point> i1 = wires.get(1).getPoints().iterator();
		ArrayList<Point> commonPoints = new ArrayList<>();
		Point p0 = i0.next();
		Point p1 = i1.next();
		boolean done = false;
		while (!done) {
			if (p0.compareTo(p1) < 0)
				if (i0.hasNext()) p0 = i0.next();
				else done = true;
			else if (p0.compareTo(p1) > 0)
				if (i1.hasNext()) p1 = i1.next();
				else done = true;
			else { // match found
				commonPoints.add(p0);
				p0.addSteps(p1.getSteps());
				if (i0.hasNext() && i1.hasNext()) {
					p0 = i0.next(); p1 = i1.next();
				} else {
					done = true;
				}
			}
		}
		
		// Find point with lowest number of steps
		Point bestPoint = commonPoints.get(0);
		for (Point p : commonPoints)
			if (p.getSteps() < bestPoint.getSteps()) bestPoint = p;

		System.out.print("Solution is the point: " + bestPoint);
	}

	private static void readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNextLine()) {
				String wire = in.nextLine();
				String[] wireMoves = wire.split(",");
				wires.add(parseWire(wireMoves));
			}
		}
	}

	private static Wire parseWire(String[] wireMoves) {
		Wire wire = new Wire();
		for (String move : wireMoves) {
			char direction = move.charAt(0);
			int distance = Integer.parseInt(move.substring(1));
			for (int i = 0; i < distance; i++)
   				wire.move(direction);
		}
		return wire;
	}
}
