package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.Digits;



public class Problem055 {

	public static void main(String[] args) {
		int count = 0;
		for (int i = 1; i < 10000; i++) {
			if (isLychrel(i, 50)) count++;
		}
		System.out.println(count);
	}
	
	/**
	 * Test a number to see if it is a Lychrel number
	 * 
	 * @param n - the number to test
	 * @param limit - the maximum number of iterations
	 * @return
	 */
	public static boolean isLychrel(int n, int limit) {
		ArrayList<Integer> digits;
		ArrayList<Integer> reversedDigits;
		
		BigInteger bigN = BigInteger.valueOf(n);
		reversedDigits = Digits.toReversedDigits(bigN);
		boolean isPalindrome = false;
		int count = 0;
		do {
			BigInteger reversedValue = Digits.fromDigits(reversedDigits);
			bigN = bigN.add(reversedValue);
			
			digits = Digits.toDigits(bigN);
			reversedDigits = Digits.toReversedDigits(bigN);
			isPalindrome = checkIdentity(digits, reversedDigits);
			count++;
		} while (!isPalindrome & count <= limit);
		return !isPalindrome;
	}

	private static boolean checkIdentity(ArrayList<Integer> a, ArrayList<Integer> b) {
		boolean same = true;
		for (int i = 0; i < a.size(); i++) {
			if (! a.get(i).equals(b.get(i))) same = false;
		}
		return same;
	}	
}
