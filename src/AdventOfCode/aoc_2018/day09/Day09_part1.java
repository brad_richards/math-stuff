package AdventOfCode.aoc_2018.day09;

import java.util.Arrays;

/**
 * Note: We will index our players starting with 0. To match results given in the problem, we must
 * add one to the player indices.
 * 
 * Maybe there would be some clever shortcut to determine the results, but we will just implement
 * the game as defined. For this purpose, we define a new class "Circle", which is an ArrayList with
 * a couple of special operations.
 * 
 * This turns out to be too slow for part 2
 */
public class Day09_part1 {
	public static int NUM_PLAYERS = 426;
	public static int LAST_MARBLE = 72058;

	public static void main(String[] args) {
		int[] playerScores = new int[NUM_PLAYERS];
		Circle circle = new Circle(); // Starts with marble 0 in place
		
		long start = System.currentTimeMillis();
		
		int currentPlayer = 0;
		for (int marble = 1; marble <= LAST_MARBLE; marble++) {
			playerScores[currentPlayer] += circle.addMarble(marble);
			currentPlayer++;
			if (currentPlayer >= NUM_PLAYERS) currentPlayer -= NUM_PLAYERS;
		}
		
		long end = System.currentTimeMillis();
		System.out.println("Time elapsed = " + (end - start) + "ms");
		System.out.println("Maximum score is " + Arrays.stream(playerScores).max().getAsInt());
	}
}
