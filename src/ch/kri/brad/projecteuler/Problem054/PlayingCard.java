package ch.kri.brad.projecteuler.Problem054;

/**
 * We use the Comparable interface to provide sorting by rank
 */
public class PlayingCard implements Comparable<PlayingCard> {
	public static enum CardRank {Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};
	public static enum CardSuit {Club, Diamond, Heart, Spade};

	private CardRank rank;
	private CardSuit suit;
	
	public PlayingCard(CardSuit suit, CardRank rank) {
		this.suit = suit;
		this.rank = rank;
	}

	public CardRank getRank() {
		return rank;
	}

	public CardSuit getSuit() {
		return suit;
	}

	@Override
	public int compareTo(PlayingCard o) {
		return rank.ordinal() - o.rank.ordinal();
	}
}
