package kattis.b_datatypes.floats;

import java.util.Scanner;

public class qaly {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		double QALY = 0.0;
		for (int i = 0; i < num; i++) {
			double quality = in.nextDouble();
			double length = in.nextDouble();
			QALY += quality * length;
		}
		System.out.println(QALY);
	}

}
