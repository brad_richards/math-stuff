package AdventOfCode.aoc_2023.day_03;

public class Value {
	int x, y; // Leftmost position
	long value = 0;
	boolean symbol = false;

	Value(int x, int y, long value) {
		this.x = x;
		this.y = y;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return Long.toString(value)  + (symbol? "s":"");
	}
}
