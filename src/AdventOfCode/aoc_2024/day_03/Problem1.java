package AdventOfCode.aoc_2024.day_03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	static class Model {
		String input = null;
		int cursor = 0;
		int total = 0;
	}

	public static void main(String[] args) throws Exception {
		readInput();
		Model model = new Model();
		StringBuilder sb = new StringBuilder();
		for (String line : lines) sb.append(line); // We may lose some CR characters
		model.input = sb.toString();
		while (model.cursor > -1 && model.cursor < sb.length()) {
			model.cursor = model.input.indexOf("mul", model.cursor);
			if (model.cursor > -1) {
				model.cursor += 3;
				// Check for format
				if (model.input.charAt(model.cursor) == '(') {
					model.cursor++;
					int op1 = getOperand(model);
					if (model.input.charAt(model.cursor) == ',') {
						model.cursor++;
						int op2 = getOperand(model);
						if (model.input.charAt(model.cursor) == ')') {
							model.cursor++;
							model.total += op1 * op2;
						}
					}
				}
			}
		}
		System.out.println(model.total);
	}

	private static int getOperand(Model model) {
		int operand = 0;
		if (Character.isDigit(model.input.charAt(model.cursor))) {
			operand = operand * 10 + (model.input.charAt(model.cursor++) - '0');
			if (Character.isDigit(model.input.charAt(model.cursor))) {
				operand = operand * 10 + (model.input.charAt(model.cursor++) - '0');
				if (Character.isDigit(model.input.charAt(model.cursor))) {
					operand = operand * 10 + (model.input.charAt(model.cursor++) - '0');
				}
			}
		}
		return operand;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}