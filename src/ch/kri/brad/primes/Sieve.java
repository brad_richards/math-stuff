package ch.kri.brad.primes;

public class Sieve {

	/**
	 * Simple test program for the Sieve of Eratosthenes. The program times
	 * itself generating prime numbers, proceeding by orders of magnitude. That
	 * is, it generates all the prime numbers to 10, then to 100, then to 1000 -
	 * up to 10,000,000 - starting over each time.
	 * 
	 * The program prints the total run-time (in milliseconds), as well as the
	 * number of primes found. The latter serves as verification that the
	 * program is running correctly.
	 */
	public static void main(String[] args) {
		int limit = 100000001;
		long start, end, millis;
		boolean primes[] = null;
		int numPrimes;
		for (int roundLimit = 10; roundLimit < limit; roundLimit *= 10) {
			start = System.currentTimeMillis();
			primes = sieveOfEratosthenes(roundLimit);
			end = System.currentTimeMillis();
			millis = end - start;
			numPrimes = countPrimes(primes);

			System.out.println("Generated primes to " + roundLimit + " in "
					+ millis + "ms and found " + numPrimes + " primes");
		}
	}

	public static boolean[] sieveOfEratosthenes(int roundLimit) {
		boolean primes[] = new boolean[roundLimit];

		// Initialize
		for (int i = 0; i < primes.length; i++)
			primes[i] = true;
		primes[0] = false;
		primes[1] = false;

		// Sieve
		for (int check = 2; check < primes.length / check; check++) {
			if (primes[check]) {
				// found the next prime - mark all multiples in the array
				for (int flip = check * 2; flip < primes.length; flip += check) {
					primes[flip] = false;
				}
			}
		}

		return primes;
	}

	private static int countPrimes(boolean[] primes) {
		int numPrimes = 0;
		for (boolean p : primes) {
			if (p)
				numPrimes++;
		}
		return numPrimes;
	}
}
