package ch.kri.brad.projecteuler.Problem098;

/**
 * An anagram pair is a set of two words (or numbers) that are anagrams. They are indexed by a
 * number that represents the patter of letters (or digits) in the pair. Since there are two
 * possible orderings, each pair has two possible values that may be different.
 * 
 * For example, consider the word "anagram". We begin numbering unique letters with the digit "1".
 * The pattern for this word is therefore "1 2 1 3 4 1 5", giving us the number 1213415.
 * 
 * Now consider the pair "male" and "meal". These concatenate to either "malemeal" or "mealmale",
 * leading to two possible values 12341423 and 12341342.
 * 
 * The point of these two possible values is to allow us to find two pairs of anagrams that map to
 * the same value. In the case of Euler problem 98, we are seeking a number-pair that map to a
 * word-pair.
 */
public class AnagramPair {
	private String word1;
	private String word2;

	public AnagramPair(String word1, String word2) {
		this.word1 = word1;
		this.word2 = word2;
	}

	public String getWord1() {
		return word1;
	}

	public String getWord2() {
		return word2;
	}

	public long getValue1() {
		return getValue(word1 + word2);
	}

	public long getValue2() {
		return getValue("word2 + word1");
	}

	private long getValue(String in) {
		char[] chars = in.toCharArray();
		int nextCharCode = 1;
		int[] charCodes = new int[chars.length];
		for (int i = 0; i < chars.length; i++) {
			if (charCodes[i] == 0) { // not yet assigned
				charCodes[i] = nextCharCode++;
				for (int j = i + 1; j < chars.length; j++) { // other occurrences
					if (charCodes[j] == 0 && chars[i] == chars[j]) charCodes[j] = charCodes[i];
				}
			}
		}

		// Concatenate digits into a long
		int value = 0;
		for (int digit : charCodes)
			value = value * 10 + digit;
		return value;
	}
}
