package kattis.h_methods;

import java.util.Scanner;

/**
 * Use a method to run each test-case. This method should be declared:
 * 
 * private static void doTestCase(Scanner in)
 *
 * It uses the scanner created in the main method.
 */
public class electricaloutlets {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numTestCases = in.nextInt();
        
        int i = 0;
        while (i < numTestCases) {
            
            doTestCase(in);
            
            i++;
        }
    }
    
    private static void doTestCase(Scanner in) {
        int numStrips = in.nextInt();
        int i = 0;
        int total = 1;
        while (i < numStrips) {
            total += in.nextInt() - 1;
            i++;
        }
        System.out.println(total);
    }

}