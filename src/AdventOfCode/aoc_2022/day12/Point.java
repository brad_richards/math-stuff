package AdventOfCode.aoc_2022.day12;

public class Point {
	public final int y;
	public final int x;
	
	public Point(int y, int x) {
		this.y = y;
		this.x = x;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass()==this.getClass()) {
			Point p = (Point) o;
			return (this.x == p.x) && (this.y == p.y);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
