package ch.kri.brad.projecteuler.Problem081;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

/**
 * We use breadth-first search, but only saving the best path to any given node.
 * The value of a path is the accumulated cost (sum of traversed cells).
 * Although we expand all paths (breadth-first), we then sort them, so that we
 * can eliminate suboptimal paths, retaining only the best one for each node.
 * 
 * We note the following characteristics of the matrix: - Values range from 1 to
 * 9999 - The matrix contains duplicate values
 * 
 * To keep track of our path, we define an inner class Node, noting row, column
 * and value. Equality is based on row and column. We also have the class Path,
 * which contains a sequence of Nodes plus the path value as defined above.
 */
public class Problem081 {
	private static final String FILE_NAME = "p081_matrix.txt";
	private static final int SIZE = 80;
	private static int[][] matrix = new int[SIZE][SIZE];

	public static void main(String[] args) {
		readFile();

		ArrayList<Path> paths = new ArrayList<>();
		paths.add(new Path()); // Initial path to start the search

		while (!paths.get(0).goalReached()) {

			// for (Path path : paths) System.out.println(path);
			// System.out.println();

			ArrayList<Path> newPaths = new ArrayList<>();
			for (Path path : paths) {
				newPaths.addAll(expandPaths(path));
			}
			Collections.sort(newPaths);
			deleteRedundantPaths(newPaths);
			paths = newPaths;
		}
		System.out.println("Minimum cost is " + paths.get(0).totalCost);
	}

	/**
	 * Each path can be expanded in two direction: right and down
	 */
	private static ArrayList<Path> expandPaths(Path path) {
		ArrayList<Path> newPaths = new ArrayList<>();
		Node lastNode = path.getLastNode();
		if (lastNode.row < SIZE - 1) {
			Path newPath = path.clone();
			newPath.appendNode(new Node(lastNode.row + 1, lastNode.col));
			newPaths.add(newPath);
		}
		if (lastNode.col < SIZE - 1) {
			Path newPath = path.clone();
			newPath.appendNode(new Node(lastNode.row, lastNode.col + 1));
			newPaths.add(newPath);
		}
		return newPaths;
	}

	/**
	 * Remove any paths that are redundant, i.e., if two paths both have reached
	 * the same final node, keep only the path with the lower cost.
	 */
	private static void deleteRedundantPaths(ArrayList<Path> paths) {
		for (int i = 0; i < paths.size() - 1; i++) {
			Node endNode = paths.get(i).getLastNode();
			for (int j = paths.size() - 1; j > i; j--) {
				if (endNode.equals(paths.get(j).getLastNode()))
					paths.remove(j);
			}
		}
	}

	/**
	 * Method to read the input file and create the internal representation
	 */
	private static void readFile() {
		URL url = Problem081.class.getResource(FILE_NAME);
		File f = new File(url.getPath());
		if (f.exists()) {
			try (BufferedReader in = new BufferedReader(new FileReader(f))) {
				String line = in.readLine();
				int row = -1;
				while (line != null) {
					row++;
					String[] nodeValues = line.split(",");
					if (nodeValues.length != SIZE)
						System.out.println("Error in input file: wrong row length");
					for (int col = 0; col < nodeValues.length; col++) {
						matrix[row][col] = Integer.parseInt(nodeValues[col]);
					}
					line = in.readLine();
				}
				if (row != SIZE - 1)
					System.out.println("Error in input file: wrong number of rows");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static class Path implements Comparable {
		private ArrayList<Node> nodeList;
		private int totalCost = 0; // Sum of the values of all nodes in the list

		public Path() {
			nodeList = new ArrayList<>();
			this.appendNode(new Node(0, 0));
		}

		private Path(ArrayList<Node> nodeList) {
			this.nodeList = nodeList;
		}

		private boolean goalReached() {
			Node n = this.getLastNode();
			return (n.row == 79 && n.col == 79);
		}

		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			for (Node n : nodeList) {
				sb.append("(" + n.row + "," + n.col + ") ");
			}
			sb.append("Total cost " + totalCost);
			return sb.toString();
		}

		@Override
		public Path clone() {
			ArrayList<Node> nodeListCopy = (ArrayList<Node>) nodeList.clone();
			Path newPath = new Path(nodeListCopy);
			newPath.totalCost = this.totalCost;
			return newPath;
		}

		public void appendNode(Node n) {
			nodeList.add(n);
			totalCost += n.value;
		}

		public Node getLastNode() {
			return nodeList.get(nodeList.size() - 1);
		}

		@Override
		public int compareTo(Object o) {
			Path p = (Path) o;
			return totalCost - p.totalCost;
		}
	}

	private static class Node {
		int row, col, value;

		public Node(int row, int col) {
			this.row = row;
			this.col = col;
			this.value = matrix[row][col];
		}

		@Override
		public boolean equals(Object o) {
			Node n = (Node) o;
			return (n.row == row && n.col == col);
		}
	}
}
