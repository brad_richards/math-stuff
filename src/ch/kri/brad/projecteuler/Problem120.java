package ch.kri.brad.projecteuler;

public class Problem120 {

	public static void main(String[] args) {
		long sum = 0;
		for (int a = 3; a <= 1000; a++) {
			if (a % 2 == 0) sum += a * (a-2);
			else sum += a * (a-1);
		}
		System.out.println(sum);

	}

}
