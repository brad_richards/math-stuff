package AdventOfCode.aoc_2021.day15;

public class Point {
	public int x;
	public int y;
	
	public Point() {
		this.x = -1;
		this.y = -1;
	}
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) return false;
		Point p = (Point) o;
		return (this.x == p.x && this.y == p.y);
	}
}
