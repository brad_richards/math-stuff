package ch.kri.brad.projecteuler.Problems103_105_106;

import java.util.Arrays;
import java.util.Set;

/**
 * In this optimized version, we avoid created Set objects, and just work with the base array.
 *
 * We test all equalities for subsets of equal size. The code necessary to filter these (as per
 * Problem 106) would take as much time as just checking them all. We do at least generate the
 * subsets in a way that eliminates duplication (e.g. <{a,d},{b,c}> vs. <{b,c},{a,d}>)
 * 
 * We only test the greater-than relationship on worst-case subsets (i.e. n smallest elements vs.
 * n-1 largest elements).
 */
public class Optimized {
	public static boolean testSet(int[] set) {
		int setSize = set.length; // Convenience, because we use it a lot

		// Test equalities; in this section, just for subsets of size 2
		// For sets of size 2, both elements of the second set are "inside" the first set
		for (int s1a = 0; s1a < set.length - 3; s1a++) { // lower elt of set 1
			for (int s1b = s1a + 3; s1b < set.length; s1b++) { // upper elt of set 1
				for (int s2a = s1a + 1; s2a < s1b - 1; s2a++) { // lower elt of set 2
					for (int s2b = s2a + 1; s2b < s1b; s2b++) { // upper elt of set 2
						if (set[s1a] + set[s1b] == set[s2a] + set[s2b]) return false;
					}
				}
			}
		}

		// Test equalities for subsets of size 3, which is all we need for problem 103.
		// For larger subsets, a recursive solution would be better. Note: we do not
		// optimize the sets we generate (except to say that the first element is in set 1), as it's
		// likely just as fast to generate all of them
		for (int s1a = 0; s1a < set.length - 2; s1a++) { // lower elt of set 1
			for (int s1b = s1a + 1; s1b < set.length-1; s1b++) { // middle elt of set 1
				for (int s1c = s1b + 1; s1c < set.length; s1c++) { // upper elt of set 1
					for (int s2a = s1a + 1; s2a < set.length-2; s2a++) { // lower elt of set 2
						if (s2a != s1b && s2a != s1c) {
							for (int s2b = s2a+1; s2b < set.length-1; s2b++) { // middle elt of set 2
								if (s2b != s1b && s2b != s1c) {
									for (int s2c = s2b+1; s2c < set.length; s2c ++ ) {
										if (s2c != s1b && s2b != s1c) {
											if (set[s1a] + set[s1b] + set[s1c] == set[s2a] + set[s2b] + set[s2c]) return false;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// Test greater-than relationships
		for (int subsetSize = 2; subsetSize <= (setSize + 1) / 2; subsetSize++) {
			int lowerSum = sumArraySubrange(set, 0, subsetSize);
			int uppersum = sumArraySubrange(set, setSize - (subsetSize - 1), setSize);
			if (lowerSum <= uppersum) return false;
		}
		return true;
	}

	private static boolean contains(int[][] array, int value) {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				if (array[i][j] == value) return true;
			}
		}
		return false;
	}

	private static boolean subsetsEqual(int[][] subsets, int[] set) {
		int[] sums = new int[2];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < subsets[0].length; j++) {
				sums[i] += set[subsets[i][j]];
			}
		}
		return sums[0] == sums[1];
	}

	/**
	 * Add all elements in the given subrange of the array, including "from" but excluding "to"
	 */
	private static int sumArraySubrange(int[] array, int from, int to) {
		int sum = array[from];
		for (int i = from + 1; i < to; i++)
			sum += array[i];
		return sum;
	}
}
