package AdventOfCode.aoc_2021.day06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Let's try the really stupid approach, and see how bad it is...
 */
public class Problem01 {
	private static List<Integer> numbers;
	private static final int simLength = 80;

	public static void main(String[] args) throws IOException {
		readInput();
		for (int day = 1; day <= simLength; day++) {
			int num8s = 0;
			for (int i = 0; i < numbers.size(); i++) {
				Integer value = numbers.get(i);
				if (value == 0) {
					value = 6;
					num8s++;
				} else {
					value = value -1;
				}
				numbers.set(i, value);
			}
			for (int i = 0; i < num8s; i++) numbers.add(8);
			System.out.println(numbers.size());
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		List<String> lines;
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		numbers = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(",");
			for (String part : parts) numbers.add(Integer.parseInt(part));
		}
	}

}
