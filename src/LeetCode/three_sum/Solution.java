package LeetCode.three_sum;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Works, but "time limit exceeded" for very long test cases
 */
public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
        System.out.println(s.threeSum(new int[]{0, 1, 1}));
        System.out.println(s.threeSum(new int[]{0}));
        System.out.println(s.threeSum(new int[]{0, 0, 0}));
    }

    public List<List<Integer>> threeSum(int[] nums) {
        TreeMap<Long, int[]> triplets = new TreeMap<>();
        for (int i = 0; i < nums.length - 2; i++) {
            for (int j = i + 1; j < nums.length - 1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        int[] triplet = sort(nums[i], nums[j], nums[k]);
                        long hash = hash(triplet);
                        triplets.put(hash, triplet);
                    }
                }
            }
        }
        List<List<Integer>> answers = new ArrayList<>();
        for (int[] triplet : triplets.values()) {
            List<Integer> answer = new ArrayList<>();
            for (int num : triplet) { answer.add(num); }
            answers.add(answer);
        }
        return answers;
    }

    private int[] sort(int... triplet) {
        if (triplet[1] < triplet[0]) swap(triplet, 0, 1);
        if (triplet[2] < triplet[0]) swap(triplet, 0, 2);
        if (triplet[2] < triplet[1]) swap(triplet, 1, 2);
        return triplet;
    }

    private void swap(int[] triplet, int i, int j) {
        int tmp = triplet[i];
        triplet[i] = triplet[j];
        triplet[j] = tmp;
    }

    private long hash(int[] triplet) {
        return 11939 * triplet[0] + 19391 * triplet[1] + 19937 * triplet[2];
    }
}
