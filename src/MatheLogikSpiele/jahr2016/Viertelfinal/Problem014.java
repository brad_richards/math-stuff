package MatheLogikSpiele.jahr2016.Viertelfinal;

public class Problem014 {

	/**
	 * Find all 4-digit numbers whose square has the form aabbccaa.
	 * Pure brute force, because I am missing something.
	 * 
	 * Note: Nope, seems that there really are no solutions ?!
	 */
	public static void main(String[] args) {
		for (long i = 1000; i <= 9999; i++) {
			long specialResult = i * i + 1;
			if (hasForm(i, specialResult)) System.out.println("SOLUTION: " + i + ", " + specialResult);
		}
	}

	private static boolean hasForm(long i, long x) {
		boolean hasForm = false;
		if (x >= 10000000 && x <= 99999999) {
			int[] digits = getDigits(x);
			if (digits[0] == digits[1] && digits[0] == digits[6] && digits[6] == digits[7]) {
				System.out.println("first and last digits work: " + i + ", " + x);
				if (digits[2] == digits[3] && digits[4] == digits[5]) hasForm = true;
			}
		}
		return hasForm;
	}

	/**
	 * We already know that x has exactly 8 digits
	 */
	private static int[] getDigits(long x) {
		int[] digits = new int[8];
		for (int pos = 7; pos >= 0; pos--) {
			digits[pos] = (int) (x % 10);
			x = x / 10;
		}
		return digits;
	}

}
