package AdventOfCode.aoc_2018.day08;

import java.io.InputStream;
import java.util.Scanner;

public class InputReader {
	private static final String INPUT_FILE = "input.txt";

	public Node readTree() {
		Node root = null;
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			root = readNode(in);
		}
		return root;
	}

	private Node readNode(Scanner in) {
		Node node = new Node();
		int numChildren = in.nextInt();
		int numMetadata = in.nextInt();
		for (int i = 0; i < numChildren; i++) {
			node.addChild(readNode(in));
		}
		for (int i = 0; i < numMetadata; i++) {
			node.addMetadata(in.nextInt());
		}		
		return node;
	}

}
