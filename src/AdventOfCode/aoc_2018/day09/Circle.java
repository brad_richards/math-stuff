package AdventOfCode.aoc_2018.day09;

import java.util.ArrayList;

public class Circle {
	private final ArrayList<Integer> marbles = new ArrayList<>();
	private int currentMarble;

	public Circle() {
		marbles.add(0);
		currentMarble = 0;
	}

	/**
	 * This method implements both rules: If a marble's value is not divisible by 23, it adds the marble into the circle, and returns 0. If the marble is divisible by 23,
	 * it removes the appropriate marble from the circle and returns a positive value to be added to the player's score. For details, see the problem descriptions.
	 */
	public int addMarble(int marble) {
		int score = 0;
		int index;
		if ( marble%23 != 0) {
			index = currentMarble+2;
			if (index > marbles.size()) index -= marbles.size();
			marbles.add(index, marble);
		} else {
			index = currentMarble-7;
			if (index < 0) index += marbles.size();
			score = marble + marbles.remove(index);
			if (index > marbles.size()) index = 0; // Could possibly be one too large now
		}
		currentMarble = index;
		
//		printMarbles();
		return score;
	}
	
	private void printMarbles() {
		for (int marble : marbles) System.out.print(marble + " ");
		System.out.println();
	}
}
