package LeetCode.minimum_obstacle_removal_to_reach_corner;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

// Just use Best-First search, until the best path is already at the destination.
// We represent a path as a list of nodes. A note is represented by a Pair object.
// This solution worked, but exceeded the time limit for some test cases.
//
// A* was the next obvious choice, but only after implementing it did I realize
// that the Manhattan distance is not an admissible heuristic, since it can
// exceed the actual path cost (since some transitions are free).
//
// So the obvious next choice is Dijkstra's algorithm: Recording the best path to
// each node, and dropping any other paths that reach a known node without improving
// the path cost.
//
// Even with Dijkstra, the time limit is exceeded. One could screw up the representations
// to make things faster (not retaining paths, for example, just the costs), but
// that would be ugly. Not gonna do it.
class Solution {
    public static void main(String[] args) {
        int[][] grid = new int[3][3];
        grid[0][1] = 1; grid[0][2] = 1;
        grid[1][0] = 1; grid[1][1] = 1;
        grid[2][0] = 1; grid[2][1] = 1;

        Solution s = new Solution();
        System.out.println(s.minimumObstacles(grid));
    }

    record Pair(int x, int y) {}
    static class Path {
        public final List<Pair> nodes;
        public int cost = 0;
        public int value = 0;
        public Path() {
            nodes = new LinkedList<>();
        }
        public Path(Path p) {
            nodes = new LinkedList<>(p.nodes);
            cost = p.cost;
        }
    }

    public int minimumObstacles(int[][] grid) {
        Pair goal = new Pair(grid.length-1, grid[0].length-1);
        Path[][] bestPaths = new Path[grid.length][grid[0].length];
        List<Path> paths = new LinkedList<>();

        Path startingPath = new Path();
        startingPath.nodes.add(new Pair(0, 0));
        paths.add(startingPath);
        bestPaths[0][0] = startingPath;
        while (!paths.getFirst().nodes.getFirst().equals(goal)) {
            // Remove first path, and expand in all possible ways
            Path firstPath = paths.removeFirst();
            List<Path> newPaths = expand(grid, firstPath, bestPaths);
            paths.addAll(newPaths);
            paths.sort(new Comparator<Path>() {
                @Override
                public int compare(Path o1, Path o2) {
                    return o1.cost - o2.cost;
                }
            });
        }
        return paths.getFirst().cost;
    }

    // Expand by adding all nodes not already in the path
    private List<Path> expand(int[][] grid, Path path, Path[][] bestPaths) {
        List<Path> newPaths = new LinkedList<>();
        Pair currentNode = path.nodes.getFirst();
        if (currentNode.x > 0 && !path.nodes.contains(new Pair(currentNode.x-1, currentNode.y))) {
            addPath(grid, currentNode.x-1, currentNode.y, path, bestPaths, newPaths);
        }
        if (currentNode.y > 0 && !path.nodes.contains(new Pair(currentNode.x, currentNode.y-1))) {
            addPath(grid, currentNode.x, currentNode.y-1, path, bestPaths, newPaths);
        }
        if (currentNode.x < grid.length-1 && !path.nodes.contains(new Pair(currentNode.x+1, currentNode.y))) {
            addPath(grid, currentNode.x+1, currentNode.y, path, bestPaths, newPaths);
        }
        if (currentNode.y < grid[0].length-1 && !path.nodes.contains(new Pair(currentNode.x, currentNode.y+1))) {
            addPath(grid, currentNode.x, currentNode.y+1, path, bestPaths, newPaths);
        }
        return newPaths;
    }

    private void addPath(int[][] grid, int x, int y, Path path, Path[][] bestPaths, List<Path> newPaths) {
        int newCost = (grid[x][y] == 1) ? path.cost + 1 : path.cost;
        if (bestPaths[x][y] == null || newCost < bestPaths[x][y].cost) {
            Path newPath = new Path(path);
            newPath.nodes.addFirst(new Pair(x, y));
            newPath.cost = newCost;
            newPaths.add(newPath);
            bestPaths[x][y] = newPath;
        }
    }
}
