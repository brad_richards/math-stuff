package AdventOfCode.aoc_2018.day10;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class InputReader {
	private static final String INPUT_FILE = "input.txt";

	public ArrayList<Dot> readDots() {
		ArrayList<Dot> dots = new ArrayList<>();
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String line = in.nextLine();
				Dot dot = parseDot(line);
				dots.add(dot);
			}
		}
		return dots;
	}

	private Dot parseDot(String line) {
		// Parse positions
		int posLeft = line.indexOf('<', 0);
		int posComma = line.indexOf(',', posLeft);
		int posRight = line.indexOf('>', posComma);
		int x = Integer.parseInt(line.substring(posLeft+1, posComma).trim());
		int y = Integer.parseInt(line.substring(posComma+1, posRight).trim());

		// Parse velocity
		posLeft = line.indexOf('<', posRight);
		posComma = line.indexOf(',', posLeft);
		posRight = line.indexOf('>', posComma);
		int vx = Integer.parseInt(line.substring(posLeft+1, posComma).trim());
		int vy = Integer.parseInt(line.substring(posComma+1, posRight).trim());

		return new Dot(x, y, vx, vy);
	}

}

