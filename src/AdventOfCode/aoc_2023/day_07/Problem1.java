package AdventOfCode.aoc_2023.day_07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<Hand> hands = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		readInput();
		Collections.sort(hands);
		long value = 0;
		for (int i = 0; i < hands.size(); i++) {
			value += (i+1) * hands.get(i).bid;
		}
		System.out.println(value);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lines = stream.collect(Collectors.toList());
			for (String line : lines) hands.add(new Hand(line));
		}
	}
}