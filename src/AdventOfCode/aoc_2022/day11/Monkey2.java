package AdventOfCode.aoc_2022.day11;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Monkey2 {
	public static long allDivs = 1;
	
	private final int ID; // Corresponds to position in the "monkeys" list
	private final Queue<Long> items = new LinkedList<>();
	private final char operator;
	private final String operand;
	private final long divBy;
	private final int trueTarget;
	private final int falseTarget;
	private int count = 0;

	public Monkey2(int ID, char operator, String operand, long divBy, int trueTarget, int falseTarget, long[] initialItems) {
		this.ID = ID;
		this.operator = operator;
		this.operand = operand;
		this.divBy = divBy;
		this.trueTarget = trueTarget;
		this.falseTarget = falseTarget;
		for (long item : initialItems) items.add(item);
	}
	
	public void throwItems(List<Monkey2> monkeys) {
		while (!items.isEmpty()) {
			long item = items.remove();
			item = updateWorry(item);
			if (item % divBy == 0) {
				monkeys.get(trueTarget).catchItem(item);
			} else {
				monkeys.get(falseTarget).catchItem(item);
			}
			count++;
		}
	}
	
	private long updateWorry(long item) {
		long operand;
		try {
			operand = Integer.parseInt(this.operand);
		} catch (NumberFormatException e) { // "old * old" in input file
			operand = item;
		}
		if (operator == '*')
			item *= operand;
		else // +
			item += operand;
		return item % allDivs;
	}
	
	@Override
	public String toString() {
		String out = String.format("Monkey %d (%3d): ",ID, count);
//		for (long item : items) out += item + ", ";
		return out;
	}
	
	public void catchItem(long item) {
		items.add(item);
	}

	public int getCount() {
		return count;
	}
}
