package AdventOfCode.aoc_2021.day09;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * For part 2: It is not clearly stated, but we assume there is a one-to-one relationship between low-points and basins.
 */
public class Problem01 {
	private static int[][] caves;
	private static ArrayList<Point> lowPoints = new ArrayList<>();
	private static ArrayList<Integer> basinSizes = new ArrayList<>(); // For part 2
	private static int xSize;
	private static int ySize;

	public static void main(String[] args) throws IOException {
		readInput();
		
		// Part 1
		findLowPoints();

		int sum = 0;
		for (Point p : lowPoints) {
			sum += (caves[p.y][p.x] + 1);
		}
		System.out.println("Part 1: " + sum);
		
		// Part 2
		findBasinSizes();
		
		Collections.sort(basinSizes);
		long answer = 1;
		for (int i = 1; i <= 3; i++) {
			answer *= basinSizes.get(basinSizes.size() - i);
		}
		System.out.println("Part 2: " + answer);
		
	}
	
	/**
	 * It is not clearly stated, but we assume there is a one-to-one relationship between low-points and basins.
	 * We add 10 to each point in a basin, as we identify it.
	 */
	private static void findBasinSizes() {
		for (Point p : lowPoints) {
			int basinSize = basinPoint(p.x, p.y);
			basinSizes.add(basinSize);
		}
	}
	
	private static int basinPoint(int x, int y) {
		caves[y][x] += 10;
		int count = 1;
		if (y > 0 && caves[y-1][x] < 9) count += basinPoint(x, y-1); 
		if (y < ySize-1 && caves[y+1][x] < 9) count += basinPoint(x, y+1);
		if (x > 0 && caves[y][x-1] < 9) count += basinPoint(x-1, y);
		if (x < xSize-1 && caves[y][x+1] < 9) count += basinPoint(x+1, y);
		return count;
	}
	
	private static void findLowPoints() {
		for (int y = 0; y < ySize; y++) {
			for (int x = 0; x < xSize; x++) {
				boolean isLowPoint = true;
				if (y > 0 && caves[y][x] >= caves[y-1][x]) isLowPoint = false;
				if (y < ySize-1 && caves[y][x] >= caves[y+1][x]) isLowPoint = false;
				if (x > 0 && caves[y][x] >= caves[y][x-1]) isLowPoint = false;
				if (x < xSize-1 && caves[y][x] >= caves[y][x+1]) isLowPoint = false;
				
				if (isLowPoint) lowPoints.add(new Point(x,y));
			}
		}
	}

	private static void readInput() throws IOException {
		ArrayList<String> lines = new ArrayList<>();
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			lines.add(in.nextLine());
		}
		
		xSize = lines.get(0).length();
		ySize = lines.size();
		
		caves = new int[ySize][xSize];
		for (int y = 0; y < ySize; y++) {
			String line = lines.get(y);
			for (int x = 0; x < xSize; x++) {
				caves[y][x] = line.charAt(x) - '0';
			}
		}
	}
}
