package AdventOfCode.aoc_2022.day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static HashMap<String, Valve> valves;

	public static void main(String[] args) throws Exception {
		readInput();
		Solution1.valves = valves; // For easy access by the Solution class

		// Stupid, exhaustive search of all possible solutions
		// We always extend the first path in the list. However, if we
		// cannot find a path with less than Solution.timeLimit steps, we set
		// found = false
		PriorityQueue<Solution1> allPaths = new PriorityQueue<>();
		allPaths.add(new Solution1());
		Solution1 bestSolution = null;
		int printCounter = 0;
		
		while (allPaths.size() > 0) {
			// Periodically show current status
			if (printCounter++ % 100000 == 0) {
				Solution1 tmp = allPaths.peek();
				System.out.format("%2d  %4d  (%d)%n", tmp.numMoves, tmp.prognosis, allPaths.size());
			}

			Solution1 s = allPaths.remove();
			
			if (s.numMoves >= Solution1.timeLimit) {
				if (bestSolution == null || s.total > bestSolution.total) bestSolution = s;
			} else {
				// Only process paths that could lead to a better solution
				if (s.maxValue >= Solution1.highestPrognosis) {
					s.getNextPaths(allPaths);
				}
			}
		}

		// Find the path with the highest value
		System.out.println("Part 1 answer = " + bestSolution.total);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}

		// Create valves with flow-rates, but not yet tunnels
		valves = new HashMap<>();
		for (String line : lines) {
			String name = line.substring(6, 8);
			String tmp = line.substring(23, 26); // Flow rate with semicolon
			int flowRate = Integer.parseInt(tmp.split(";")[0]);
			valves.put(name, new Valve(name, flowRate));
		}

		// Create tunnel connections
		for (String line : lines) {
			Valve valve = valves.get(line.substring(6, 8));
			String tmp = line.split(" to valve")[1].substring(1);
			String[] parts = tmp.split(",");
			for (String part : parts) {
				String name = part.trim();
				Valve tunnel = valves.get(name);
				valve.addTunnel(tunnel);
			}
		}
	}
}