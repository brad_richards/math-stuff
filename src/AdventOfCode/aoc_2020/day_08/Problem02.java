package AdventOfCode.aoc_2020.day_08;

import java.io.IOException;
import java.util.List;

import AdventOfCode.aoc_2020.day_08.Instruction.InstructionType;

public class Problem02 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Instruction> program = ir.getProgram();
		Processor proc = new Processor(program);
		
		boolean success = false;
		for (int n = 0; !success && n < program.size(); n++) {
			Instruction i = program.get(n); 
			if (i.getType() != InstructionType.ACC) {
				swap_NOP_JMP(i);
				success = proc.execute();
				if (!success) swap_NOP_JMP(i);
			}
		}
		System.out.println("Answer = " + proc.getAccumulator());
	}

	private static void swap_NOP_JMP(Instruction i) {
		if (i.getType() == InstructionType.JMP)
			i.setType(InstructionType.NOP);
		else if (i.getType() == InstructionType.NOP)
			i.setType(InstructionType.JMP);
	}
		
}
