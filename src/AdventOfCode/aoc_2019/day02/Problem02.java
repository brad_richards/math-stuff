package AdventOfCode.aoc_2019.day02;

import java.io.InputStream;
import java.util.Scanner;

import AdventOfCode.aoc_2019.IntCodeComputer.IntCodeComputer;

public class Problem02 {
	private static int[] originalMemory;

	public static void main(String[] args) {
		// read memory
		originalMemory = readInput();
		IntCodeComputer computer = new IntCodeComputer(originalMemory);
		
		int targetResult = 19690720;
		int answer = -1;
		for (int noun = 0; noun <= 99 && answer == -1; noun++) {
			for (int verb = 0; verb <= 99 && answer == -1; verb++) {
				computer.reset(originalMemory);
				computer.setMemoryLocation(1, noun);
				computer.setMemoryLocation(2, verb);
				computer.executeProgram();
				if (computer.getMemoryLocation(0) == targetResult) answer = 100 * noun + verb;
			}
		}
		
		// Print answer
		System.out.println(answer);
	}

	private static int[] readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		String line;
		try (Scanner in = new Scanner(fis) ) {
			line = in.nextLine();
		}
		String[] parts = line.split(",");
		int[] intParts = new int[parts.length];
		for (int i = 0; i < intParts.length; i++) intParts[i] = Integer.parseInt(parts[i]);
		return intParts;
	}
}
