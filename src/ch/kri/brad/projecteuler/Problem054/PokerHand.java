package ch.kri.brad.projecteuler.Problem054;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

public class PokerHand implements Comparable<PokerHand> {
	public static enum HandType {HighCard, Pair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush};
	
	/**
	 * The value of a hand is primarily determined by the HandType. If this is equal, then we break
	 * the tie by looking at card ranks. Precisely which ranks are considered for the tie-break
	 * depends on the HandType; these are listed from most to least important.
	 */
	private static class HandValue {
		private HandType handType = null;
		private ArrayList<PlayingCard.CardRank> tieBreakRanks = new ArrayList<>();
	}
	
	private ArrayList<PlayingCard> cards = new ArrayList<>();
	private HandValue handValue = null;

	public void addCard(PlayingCard card) throws Exception {
		if (cards.size() < 5) {
			cards.add(card);
		} else {
			throw new Exception("Too many cards");
		}
	}
	
	/**
	 * If the value has been calculated, return it. Otherwise, evaluate the hand and then return the value.
	 */
	private HandValue getValue() {
		if (handValue == null) evaluateHand();
		return handValue;
	}
	
	public HandType getHandType() {
		HandValue value = this.getValue();
		return value.handType;
	}
	
	private void evaluateHand() {
		handValue = new HandValue();
		
		// Sort the cards by rank
		Collections.sort(cards);

		// If we have a straight or a flush, then we cannot have a pair, 3-of-a-kind, etc.
		boolean isStraight = isStraight();
		boolean isFlush = isFlush();
		if (isStraight) {
			handValue.tieBreakRanks.add(cards.get(4).getRank()); // highest rank
			if (isFlush) {
				handValue.handType = HandType.StraightFlush;  // includes royal flush
			} else {
				handValue.handType = HandType.Straight;
			}
		} else if (isFlush) {
			handValue.handType = HandType.Flush;
			for (int i = 4; i >= 0; i--) handValue.tieBreakRanks.add(cards.get(i).getRank());
		} else { // Check x-of-a-kind options
			TreeSet<RankGroup> rankGroups = groupByRank();
			if (rankGroups.last().cards.size() == 4) {
				handValue.handType = HandType.FourOfAKind;
			} else if (rankGroups.last().cards.size() == 3 && rankGroups.size() == 2) {
				handValue.handType = HandType.FullHouse;
			} else if (rankGroups.last().cards.size() == 3 && rankGroups.size() == 3) {
				handValue.handType = HandType.ThreeOfAKind;
			} else if (rankGroups.last().cards.size() == 2 && rankGroups.size() == 3) {
				handValue.handType = HandType.TwoPair;
			} else if (rankGroups.last().cards.size() == 2 && rankGroups.size() == 4) {
				handValue.handType = HandType.Pair;
			} else {
				handValue.handType = HandType.HighCard;
			}
			// The correct tie breaks are given by the ranks of the groups in reverse order
			for (RankGroup group : rankGroups) handValue.tieBreakRanks.add(0, group.cards.get(0).getRank());
		}
	}
	
	private boolean isStraight() {
		boolean isStraight = true;
		for (int i = 0; i < 4; i++) {
			isStraight &= ((cards.get(i).getRank().ordinal()+1) == cards.get(i+1).getRank().ordinal());
		}
		return isStraight;
	}
	
	private boolean isFlush() {
		boolean isFlush = true;
		for (int i = 0; i < 4; i++) {
			isFlush &= (cards.get(i).getSuit() == cards.get(i+1).getSuit());
		}
		return isFlush;
	}

	/**
	 * This method groups the cards by their ranks, and returns a TreeSet of RankGroups.
	 * Each RankGroup contains one or more cards of the same rank. By placing these into
	 * a TreeSet, they are automatically sorted by the criteria of RankGroup: this is the
	 * number of cards; if this is tied (two-pair), it is ordered by rank.
	 */
	private TreeSet<RankGroup> groupByRank() {
		TreeSet<RankGroup> rankGroups = new TreeSet<>();
		RankGroup group = new RankGroup();
		for (PlayingCard card : cards) {
			if (group.cards.size() == 0 || group.cards.get(0).getRank() == card.getRank()) {
				group.cards.add(card);
			} else {
				rankGroups.add(group);
				group = new RankGroup();
				group.cards.add(card);
			}
		}
		rankGroups.add(group);
		return rankGroups;
	}

	private static class RankGroup implements Comparable<RankGroup> {
		ArrayList<PlayingCard> cards = new ArrayList<>();

		@Override
		public int compareTo(RankGroup group) {
			int value = cards.size() - group.cards.size();
			if (value == 0) value = cards.get(0).compareTo(group.cards.get(0));
			return value;
		}
	}
	
	@Override
	public int compareTo(PokerHand opponent) {
		// Must access handValue via getter, to ensure that it has been calculated
		int value = this.getValue().handType.ordinal() - opponent.getValue().handType.ordinal();
		if (value == 0) {
			int i = 0;
			while (value == 0 && i < handValue.tieBreakRanks.size() && i < opponent.handValue.tieBreakRanks.size()) {
				value = handValue.tieBreakRanks.get(i).ordinal() - opponent.handValue.tieBreakRanks.get(i).ordinal();
				i++;
			}
		}
		return value;
	}
}
