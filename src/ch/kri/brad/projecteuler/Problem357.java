package ch.kri.brad.projecteuler;

import java.util.ArrayList;

import ch.kri.brad.utility.Factoring;
import ch.kri.brad.utility.Primes;

/**
 * Still way too slow - too many combinations are generated and tested. Further optimization must be possible!
 */
public class Problem357 {
	static boolean[] primeSieve;
	static ArrayList<Long> primes;
	static ArrayList<Long> solutions = new ArrayList<Long>();
	static final int LIMIT = 100000001;

	/**
	 * Note that paired divisors generate the same result. This would help by searching, since we
	 * would not need to examine primes over 10000. However, searching will be far too slow - we
	 * need a generative algorithm!
	 * 
	 * 1 and 2 are special cases - they trivially meet the criteria.
	 * 
	 * Because 1 is considered a divisor, it must be the case that n+1 is a prime number.
	 * 
	 * A brief analysis shows that any prime factor can occur in n only once. E.g. 6 (2*3) meets the
	 * criteria, but 18 (2*3*3) does not.
	 * 
	 * Unfortunately, it is not the case that all remaining numbers meet the criteria.
	 * Example: 46 has divisors 1, 2, 23. 2 + 46/2 = 25, which is non-prime.
	 * 
	 * Hence: our generator function: - generate a list of all primes up to 50,000,000 (because
	 * 2*maxPrime is a candidate number)
	 * 
	 * - find the maximum number of primes that can be used and still remain under 100,000,000
	 * (2*3*5*...)
	 * 
	 * - for numbers of primes from two to maxNumPrimes
	 * 
	 *   -- do
	 *   
	 *      --- Initialize an array of indices into the array of primes
	 *      
	 *      --- test this number; record it if it passes
	 *      
	 *      --- increment the array of indices
	 *      
	 *   -- while increment is successful
	 */
	public static void main(String[] args) {
		Primes.generatePrimesErastothenes(LIMIT + 1);
		primes = Primes.getPrimes();
		primeSieve = Primes.getRawSieve();
		System.out.println("have primes");
		
		solutions.add(1l); // Special case
		solutions.add(2l); // Special case

		int maxNumPrimes = calcMaxNumPrimes();

		// For all numbers of primes
		for (int numPrimes = 2; numPrimes <= maxNumPrimes; numPrimes++) {
			System.out.println(numPrimes + " : " + maxNumPrimes);
			// Init array of indices into the list of primes
			int[] indices = new int[numPrimes];
			for (int i = 0; i < numPrimes; i++) indices[i] = i;
			
			// For all combinations of primes whose product does not exceed limit
			// Check the combination to see if it is a solution
			do {
				long prod = calcProduct(indices);
				if (isSolution(prod)) solutions.add(prod);
			} while(increment(indices));
		}
		solutions.stream().sorted().forEach(System.out::println);
		long sum = solutions.stream().mapToLong(Long::longValue).sum();
		System.out.println("sum = "  + sum);
	}

	private static int calcMaxNumPrimes() {
		long product = 1;
		int numPrimes = 0;
		while (product < LIMIT) {
			product *= primes.get(numPrimes++);
		}
		return --numPrimes; // One less, because we exceeded limit with last prime
	}

	/**
	 * Increment the indices, trying all combinations where the product does not exceed LIMIT
	 * 
	 * Since odd numbers cannot be solutions, the first index must remain 0 (prime factor = 2)
	 */
	private static boolean increment(int[] indices) {
		int idx = indices.length - 1; // The index we are currently changing
		boolean found = false; // found becomes true when we have found the next valid combination
		while (!found && idx > 0) {
			// max value for this index
			int max = primes.size() - (indices.length - idx);
			
			if (indices[idx] < max) {
				indices[idx]++;
				
				// Reset all higher indices
				for (int i = idx+1; i < indices.length; i++) indices[i] = indices[i-1] + 1;

				if (calcProduct(indices) < LIMIT) found = true;
			}
			if (!found) idx--; // Move down to previous index
		}
		return found;
	}
	
	private static long calcProduct(int[] indices) {
		long product = 1;
		for (int index : indices) {
			product *= primes.get(index);
		}
		return product;
	}
	
	private static boolean isSolution(long value) {
		ArrayList<Long> factors = Factoring.getSmallDivisors(value);
		boolean isSolution = true;
		for (int i = 0; i < factors.size() && isSolution; i++) {
			long factor = factors.get(i);
			long potentialPrime = factor + value / factor;
			isSolution = primeSieve[((int) potentialPrime)-2];
		}
		return isSolution;
	}
}
