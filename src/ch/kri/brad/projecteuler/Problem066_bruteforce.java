package ch.kri.brad.projecteuler;

/**
 * A direct solution is impractical: many of the numbers exceed the maximum value of a long, and
 * anyway the blind search is too slow.
 */
public class Problem066_bruteforce {
	public static void main(String[] args) {
		int missingSolutions = 0;
		for (long D = 2; D <= 1000; D++) {
			if (!isPerfectSquare(D)) {
				boolean solved = false;
				for (long x = 2; !solved && x < Integer.MAX_VALUE; x++) {
					long x2 = x * x;
					if (x2 % D == 1) {
						long y2 = (x2 - 1) / D;
						if (isPerfectSquare(y2)) {
							solved = true;
							System.out.println(D + ": " + x);
						}
					}
				}
				if (!solved) {
					System.out.println("No solution for " + D);
					missingSolutions++;
				}
			}
		}
		System.out.println("Total missing solutions: " + missingSolutions);
	}
	
	/**
	 * http://stackoverflow.com/questions/295579/fastest-way-to-determine-if-an-integers-square-root-is-an-integer
	 */
	private final static boolean isPerfectSquare(long n)
	{
	  if (n < 0)
	    return false;

	  switch((int)(n & 0x3F))
	  {
	  case 0x00: case 0x01: case 0x04: case 0x09: case 0x10: case 0x11:
	  case 0x19: case 0x21: case 0x24: case 0x29: case 0x31: case 0x39:
	    long sqrt;
	    if(n < 410881L)
	    {
	      //John Carmack hack, converted to Java.
	      // See: http://www.codemaestro.com/reviews/9
	      int i;
	      float x2, y;

	      x2 = n * 0.5F;
	      y  = n;
	      i  = Float.floatToRawIntBits(y);
	      i  = 0x5f3759df - ( i >> 1 );
	      y  = Float.intBitsToFloat(i);
	      y  = y * ( 1.5F - ( x2 * y * y ) );

	      sqrt = (long)(1.0F/y);
	    }
	    else
	    {
	      //Carmack hack gives incorrect answer for n >= 410881.
	      sqrt = (long)Math.sqrt(n);
	    }
	    return sqrt*sqrt == n;

	  default:
	    return false;
	  }
	}
}
