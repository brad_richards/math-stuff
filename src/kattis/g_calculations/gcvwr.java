package kattis.g_calculations;

import java.util.ArrayList;
import java.util.Scanner;

public class gcvwr {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int g = in.nextInt();
        int t = in.nextInt();
        int numThings = in.nextInt();
        
        ArrayList<Integer> things = new ArrayList<>();
        int count = 0;
        int thingsWeight = 0;
        while (count < numThings) {
            int thing = in.nextInt();
            thingsWeight += thing;
            count++;
        }

        int maxWeight = 9 * (g-t) / 10;
        int trailerWeight = maxWeight - thingsWeight;
        System.out.println(trailerWeight);
    }

}
