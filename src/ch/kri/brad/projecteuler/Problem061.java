package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.HashMap;

public class Problem061 {
	// Define the precise problem
	private static final int FIRST_DEGREE = 3;
	private static final int LAST_DEGREE = 8;
	private static final int NUM_MAPPINGS = LAST_DEGREE + 1 - FIRST_DEGREE;

	// Lists of the polygonal numbers from 3 through 8
	private static ArrayList<ArrayList<Integer>> polygonNumbers = new ArrayList<>();
	private static ArrayList<HashMap<Integer, ArrayList<Integer>>> digitMapping = new ArrayList<>();

	public static void main(String[] args) {

		createMappings(FIRST_DEGREE, LAST_DEGREE);

		// Search for a circular path through the mappings
		int[] currentSolution = new int[NUM_MAPPINGS];
		for (Integer firstKey : digitMapping.get(0).keySet()) {
			Integer currentKey = firstKey;
			if (findSolution(currentKey, 0, digitMapping, currentSolution,
					firstKey)) {
				
				// Reconstruct original numbers for solution
				int numberInSolution = currentSolution[currentSolution.length-1] * 100 + currentSolution[0];
				System.out.print("Solution: " + numberInSolution);
				int sum = numberInSolution;
				for (int i = 1; i < currentSolution.length; i++) {
					numberInSolution = currentSolution[i-1] * 100 + currentSolution[i];
					sum += numberInSolution;
					System.out.print(" " + numberInSolution);
				}
				System.out.println("   Sum = " + sum);
			}
		}
	}

	private static void createMappings(int firstDegree, int lastDegree) {
		// Create lists of the polygon numbers
		for (int degree = firstDegree; degree <= lastDegree; degree++) {
			ArrayList<Integer> numbers = generatePolygonNumbers(degree);
			polygonNumbers.add(numbers); // We don't actually need to save it...

			// Create digit mappings for this list
			HashMap<Integer, ArrayList<Integer>> digitMap = new HashMap<>();
			for (Integer num : numbers) {
				int firstDigits = num / 100;
				int lastDigits = num % 100;

				ArrayList<Integer> possibilities;
				possibilities = digitMap.get(firstDigits);
				if (possibilities == null) {
					possibilities = new ArrayList<>();
					digitMap.put(firstDigits, possibilities);
				}
				possibilities.add(lastDigits); // guaranteed unique in this list
			}
			digitMapping.add(digitMap);
		}
	}

	private static boolean findSolution(Integer currentKey, int step,
			ArrayList<HashMap<Integer, ArrayList<Integer>>> maps,
			int[] currentSolution, Integer firstKey) {
		if (step == NUM_MAPPINGS) { // base case
			return (currentKey == firstKey);
		} else { // recursive case
			for (int i = 0; i < maps.size(); i++) {
				// Clone the mappings, before extracting one. Remaining maps will
				// be passed on in the recursion
				ArrayList<HashMap<Integer, ArrayList<Integer>>> mapsClone =
						(ArrayList<HashMap<Integer, ArrayList<Integer>>>) maps.clone();
				// Get the map to try
				HashMap<Integer, ArrayList<Integer>> digitMap = mapsClone.remove(i);
				
				// Find the current key in the mapping
				ArrayList<Integer> possibilities = digitMap.get(currentKey);
				if (possibilities != null) {
					for (Integer possibility : possibilities) {
						currentSolution[step] = possibility;
						if (findSolution(possibility, step + 1, mapsClone,
								currentSolution, firstKey)) {
							return true; // solution found
						}
					}
				}
			}
			return false; // possibilities exhausted without success
		}
	}

	/**
	 * This method is custom tailored to this problem: it only saves numbers
	 * consisting of four digits.
	 * 
	 * @param degree
	 *            determines what type of polygon number to generate
	 * @return an ArrayList containing the generated numbers
	 */
	private static ArrayList<Integer> generatePolygonNumbers(int degree) {
		ArrayList<Integer> numbers = new ArrayList<>();
		int lastNumber = -1;
		int n = 1;
		while (lastNumber < 10000) {
			switch (degree) {
			case 3:
				lastNumber = n * (n + 1) / 2;
				break;
			case 4:
				lastNumber = n * n;
				break;
			case 5:
				lastNumber = n * (3 * n - 1) / 2;
				break;
			case 6:
				lastNumber = n * (2 * n - 1);
				break;
			case 7:
				lastNumber = n * (5 * n - 3) / 2;
				break;
			case 8:
				lastNumber = n * (3 * n - 2);
			}
			if (lastNumber >= 1000 && lastNumber < 10000) {
				numbers.add(lastNumber);
			}
			n++;
		}
		return numbers;
	}
}
