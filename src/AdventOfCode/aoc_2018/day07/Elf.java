package AdventOfCode.aoc_2018.day07;

public class Elf {
	private int timeLeftOnTask = 0;
	private Character task;
	
	private final int EXTRA_TIME = 60;
	
	/**
	 * One unit of time passes. If we complete a task in this time,
	 * return it. Else return null.
	 */
	public Character tick() {
		Character finishedTask = null;
		if (task != null) {
			timeLeftOnTask--;
			if (timeLeftOnTask <= 0) {
				finishedTask = task;
				task = null;
			}
		}
		return finishedTask;
	}
	
	public Character getTask() {
		return task;
	}
	
	public void setTask(Character task) {
		if (task != null) {
			this.task = task;
			timeLeftOnTask = EXTRA_TIME + task + 1 - 'A';
		}
	}
	
	public boolean isBusy() {
		return task != null;
	}
}
