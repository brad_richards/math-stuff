package AdventOfCode.aoc_2020.day_16;

import java.io.IOException;
import java.util.ArrayList;

import AdventOfCode.aoc_2020.day_16.Field.Range;

public class Problem1 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		ArrayList<Field> fields = ir.getFields();
		ArrayList<Ticket> nearbyTickets = ir.getNearbyTickets();
		
		ArrayList<Range> allRanges = new ArrayList<>();
		for (Field field : fields) {
			allRanges.addAll(field.getRanges());
		}
		
		long sum = 0;
		for (Ticket t : nearbyTickets) {
			int[] fieldValues = t.getFieldValues();
			for (int value : fieldValues) {
				boolean ok = false;
				for (int i = 0; !ok && i < allRanges.size(); i++) {
					ok = allRanges.get(i).inRange(value);
				}
				
				if (!ok) sum += value;
			}
		}
		
		System.out.println("TSER = " + sum);
	}

}
