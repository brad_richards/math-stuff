package MatheLogikSpiele.jahr2015;

import ch.kri.brad.utility.Digits;

/**
 * The square of 11 is a palindrome; the next one is 22. Both squares have an odd number of digits.
 * 
 * The first number whose square is a palindrome with an even number of digits is 836. What is the next one?
 */
public class PalindromicSquares {
	static final long MAX = (long) Math.sqrt(Long.MAX_VALUE);

	public static void main(String[] args) {
		// First, a naive approach - just check everything
		for (long n = 11; n < MAX; n++) {
			long square = n * n;
			int[] digits = Digits.toDigits(square);
			if (isPalindrome(digits) && digits.length % 2 == 0) {
				System.out.println(n);
			}
		}
	}

	private static boolean isPalindrome(int[] digits) {
		boolean isPal = true;
		for (int i = 0; i <= digits.length/2; i++) {
			if (digits[i] != digits[digits.length - i-1]) {
				isPal = false;
				break;
			}
		}
		return isPal;
	}
}