package LeetCode.remove_nth_node_from_end_of_list;

// Is it me, or is this pretty trivial for a "medium" problem?
public class Solution {
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        ListNode list = new ListNode(1, new ListNode(2, new ListNode(3)));
        list = s.removeNthFromEnd(list, 3);
        while (list != null) {
            System.out.println(list.val);
            list = list.next;
        }
    }

    // The problem guarantees that n is a valid position in the list
    public ListNode removeNthFromEnd(ListNode head, int n) {
        int size = 0;
        ListNode cur = head;
        while (cur != null) {
            cur = cur.next;
            size++;
        }
        n = size + 1 - n;

        if (n == 1) return head.next;

        ListNode cursor = head;
        for (int i = 0; i < n-2; i++) { cursor = cursor.next; }
        cursor.next = cursor.next.next;
        return head;
    }
}
