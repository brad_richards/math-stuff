package AdventOfCode.aoc_2021.day16;

import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter hex string");
		String s = in.nextLine();
		while (!s.isEmpty()) {
			Packet p = Packet.parsePacket(s);
			System.out.println(p);
			System.out.println("Part 1 answer: " + Packet.versionSum);
			System.out.println("Part 2 answer: " + p.evaluate());
			System.out.println("Enter hex string");
			s = in.nextLine();
		}
		in.close();
	}
}
