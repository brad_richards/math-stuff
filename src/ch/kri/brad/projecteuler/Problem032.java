package ch.kri.brad.projecteuler;
import java.util.HashSet;

/**
 * The only possible combinations (in terms of number of digits) are:
 * 
 * D x DDDD = DDDD DD x DDD = DDDD DDD x DD = DDDD DDDD x D = DDDD
 * 
 * In the absence of any constraints, the first two and the last two are
 * redundant, meaning that there are essentially 2 * 9! possible arrangements of
 * digits.
 * 
 * We could derive various restrictions (such as: neither 1 nor 5 can be the
 * last digit of either the multiplicand or the multiplier). However, it will be
 * simpler to work with more dynamic restrictions. We will start by placing the
 * final digits, and ensuring that the multiplication works. E.g. D2 x DD3 =
 * DDD6. There are only 17 possibilities, so this reduces the possibilities to 4
 * * 17 * 6! = 48960. The possible final digits are hard-coded in the given
 * array.
 * 
 * We could find other restrictions, for example, on the first digits. However,
 * this number is already small enough to allow brute-force - see the recursive
 * method placeRemainingDigits(...).
 */
public class Problem032 {
	private static enum Pattern {
		p144, p234, p324, p414
	};

	private static HashSet<Integer> solutions = new HashSet<Integer>();

	private static int[][] finalDigitCombinations = { { 2, 3, 6 }, { 2, 4, 8 },
			{ 2, 7, 4 }, { 2, 8, 6 }, { 3, 4, 2 }, { 3, 6, 8 }, { 3, 7, 1 },
			{ 3, 8, 4 }, { 3, 9, 7 }, { 4, 7, 8 }, { 4, 8, 2 }, { 4, 9, 6 },
			{ 6, 7, 2 }, { 6, 9, 4 }, { 7, 8, 6 }, { 7, 9, 3 }, { 8, 9, 2 } };

	public static void main(String[] args) {
		pattern();
		
		long sum = 0;
		for (Integer solution : solutions) {
			sum += solution;
		}
		System.out.println("Answer = " + sum);
	}

	/**
	 * We represent all three numbers together in a single, 9-place array. This
	 * array is then divided into one of the four patterns above.
	 */
	private static void pattern() {
		int[] numbers = new int[9];
		boolean[] digits = new boolean[9];

		for (int[] finalDigits : finalDigitCombinations) {
			// Arrangement one
			initSolution(finalDigits, digits, numbers);
			placeFinalDigits(Pattern.p144, finalDigits, digits, numbers);
			placeRemainingDigits(Pattern.p144, digits, numbers, 6);

			// Arrangement two
			initSolution(finalDigits, digits, numbers);
			placeFinalDigits(Pattern.p234, finalDigits, digits, numbers);
			placeRemainingDigits(Pattern.p234, digits, numbers, 6);

			// Arrangement three
			initSolution(finalDigits, digits, numbers);
			placeFinalDigits(Pattern.p324, finalDigits, digits, numbers);
			placeRemainingDigits(Pattern.p324, digits, numbers, 6);

			// Arrangement four
			initSolution(finalDigits, digits, numbers);
			placeFinalDigits(Pattern.p414, finalDigits, digits, numbers);
			placeRemainingDigits(Pattern.p414, digits, numbers, 6);
		}
	}

	private static void initSolution(int[] finalDigits, boolean[] digits,
			int[] numbers) {
		for (int i = 0; i < 9; i++) {
			digits[i] = false;
			numbers[i] = 0;
		}
	}

	private static void placeFinalDigits(Pattern pattern, int[] finalDigits,
			boolean[] digits, int[] numbers) {
		for (int i : finalDigits)
			digits[i - 1] = true; // digit used

		switch (pattern) {
		case p144:
			numbers[0] = finalDigits[0];
			numbers[4] = finalDigits[1];
			numbers[8] = finalDigits[2];
			break;
		case p234:
			numbers[1] = finalDigits[0];
			numbers[4] = finalDigits[1];
			numbers[8] = finalDigits[2];
			break;
		case p324:
			numbers[2] = finalDigits[0];
			numbers[4] = finalDigits[1];
			numbers[8] = finalDigits[2];
			break;
		case p414:
			numbers[3] = finalDigits[0];
			numbers[4] = finalDigits[1];
			numbers[8] = finalDigits[2];
			break;
		}
	}

	private static void placeRemainingDigits(Pattern pattern, boolean[] digits,
			int[] numbers, int digitsLeft) {
		
		if (digitsLeft == 0) { // Base case - check the answer
			int num1 = arrayToInt(pattern, numbers, 1);
			int num2 = arrayToInt(pattern, numbers, 2);
			int num3 = arrayToInt(pattern, numbers, 3);
			if (num1 * num2 == num3) {
				solutions.add(num3);
			}
		} else { // Place the next digit, recurse, undo the placement and iterate
			int emptyDigit = 0;
			while (numbers[emptyDigit] > 0) emptyDigit++;
			for (int i = 0; i < 9; i++) {
				if (digits[i] == false) {
					// place the digit
					digits[i] = true;
					numbers[emptyDigit] = i+1;
					
					// recurse
					placeRemainingDigits(pattern, digits, numbers, digitsLeft-1);
					
					// undo, so the next iteration starts afresh
					digits[i] = false;
					numbers[emptyDigit] = 0;
				}
			}
		}
	}

	private static int arrayToInt(Pattern pattern, int[] numbers, int whichNumber) {
		int startPos=0;
		int endPos=0;
		
		// easy stuff first
		if (whichNumber == 1) startPos = 0;
		if (whichNumber == 2) endPos = 4;
		if (whichNumber == 3) {startPos = 5; endPos = 8;}
		
		switch (pattern) {
		case p144:
			if (whichNumber == 1) endPos = 0;
			if (whichNumber == 2) startPos = 1;
			break;
		case p234:
			if (whichNumber == 1) endPos = 1;
			if (whichNumber == 2) startPos = 2;
			break;
		case p324:
			if (whichNumber == 1) endPos = 2;
			if (whichNumber == 2) startPos = 3;
			break;
		case p414:
			if (whichNumber == 1) endPos = 3;
			if (whichNumber == 2) startPos = 4;
			break;
		}
		
		int value = 0;
		for (int i = startPos; i <= endPos; i++) {
			value = value * 10 + numbers[i];
		}
		return value;
	}
}
