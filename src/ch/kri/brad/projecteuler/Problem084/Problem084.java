package ch.kri.brad.projecteuler.Problem084;

import java.util.Arrays;

/**
 * To solve this problem, we will simulate a large number of random rolls, accumulate the
 * statistics, and find the three most common squares.
 */
public class Problem084 {
	private static final int DIE_SIZE = 4;
	private static final int REPETITIONS = 1000000;

	private static int[] boardCounts = new int[40];

	private static int doublesCounter = 0;

	public static void main(String[] args) {
		int pos = 0;
		for (int i = 0; i < REPETITIONS; i++) {
			pos = roll(pos);
			if (pos == 30) pos = 10; // Go to jail
			else if (pos == 2 || pos == 17 || pos == 33) pos = doCC(pos);
			else if (pos == 7 || pos == 22 || pos == 36) pos = doChance(pos);
			boardCounts[pos]++;
		}

//		for (int i = 0; i < 40; i++) {
//			System.out.printf("%3d %5.3f\n", i, boardCounts[i] / 10000.0);
//		}
		
		// Find three highest, and print them
		for (int j = 0; j < 3; j++) {
			int maxPos = -1;
			int maxCount = 0;
			for (int i = 0; i < 40; i++) {
				int count = boardCounts[i];
				if (count > maxCount) {
					maxCount = count;
					maxPos = i;
				}
			}
			
			boardCounts[maxPos] = 0;
			System.out.printf("%3d", maxPos);
		}
	}

	/**
	 * Takes the current position and returns the new position.
	 */
	private static int roll(int pos) {
		int die1 = (int) (Math.random() * DIE_SIZE) + 1;
		int die2 = (int) (Math.random() * DIE_SIZE) + 1;
		if (die1 == die2) {
			doublesCounter++;
			if (doublesCounter == 3) {
				doublesCounter = 0;
				return 10; // Go to jail
			}
		} else {
			doublesCounter = 0;
		}
		pos = pos + die1 + die2;
		if (pos >= 40) pos -= 40;
		return pos;
	}

	private static int doCC(int pos) {
		int rand = (int) (Math.random() * 16);
		if (rand == 0) return 0;
		else if (rand == 1) return 10;
		else return pos;
	}

	private static int doChance(int pos) {
		int rand = (int) (Math.random() * 16);
		if (rand == 0) return 0;
		else if (rand == 1) return 10;
		else if (rand == 2) return 11;
		else if (rand == 3) return 24;
		else if (rand == 4) return 39;
		else if (rand == 5) return 5;
		else if (rand == 6 || rand == 7) { // next railroad
			if (pos == 7) return 15;
			else if (pos == 22) return 25;
			else return 5;
		} else if (rand == 8) { // next utility
			if (pos == 22) return 28;
			else return 12;
		} else if (rand == 9) return pos - 3;
		else return pos;
	}
}
