package AdventOfCode.aoc_2021.day06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Not seeing how to come up with a sensible mathematical model. However, there are only 5 input values in use (1-5).
 * If we could calculate the number of fish each of these numbers would generate, we could then count the number of
 * input fish with each value, and get the total.
 * 
 * Unfortunately, even that would exceed max-int, which would make the calculation below impossible. So: we will simulate
 * the fish for - let's say - 80 days. Then take the current values (there will be 0-8), and simulate each of these values
 * for the remaining 256-80 = 176 days. And then calculate the total.
 * 
 * Horrible, but there we are...
 */
public class Problem02 {
	private static List<Integer> numbers;
	private static final int firstSimLength = 80;
	private static final int secondSimLength = 176;
	private static final long[] numFishEachValue = new long[9];
	private static final long[] numFishGenerated = new long[9];

	public static void main(String[] args) throws IOException {
		readInput();
		
		// First simulation
		simulate(firstSimLength);
		for (int number : numbers) numFishEachValue[number]++;
		
		// Answer to part 1
		int count = 0;
		for (long n : numFishEachValue) count+=n;
		System.out.println("Answer to part 1: " + count);
		
		// Second simulation
		secondSimulation();
		
		// Calculate total
		long total = 0;
		for (int i = 0; i < 9; i++) {
			total += numFishEachValue[i] * numFishGenerated[i];
		}
		
		// Answer to part 2
		System.out.println("Answer to part 1: " + total);
	}
	
	private static void secondSimulation() {
		for (int initVal = 0; initVal < 9; initVal++) {
			numbers = new ArrayList<>();
			numbers.add(initVal);
			simulate(secondSimLength);
			numFishGenerated[initVal] = numbers.size();
			System.out.println("Value " + initVal + " gives " + numbers.size() + " fish");
		}
	}
	
	private static void simulate(int simLength) {
		for (int day = 1; day <= simLength; day++) {
			int num8s = 0;
			for (int i = 0; i < numbers.size(); i++) {
				Integer value = numbers.get(i);
				if (value == 0) {
					value = 6;
					num8s++;
				} else {
					value = value -1;
				}
				numbers.set(i, value);
			}
			for (int i = 0; i < num8s; i++) numbers.add(8);
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		List<String> lines;
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		numbers = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(",");
			for (String part : parts) numbers.add(Integer.parseInt(part));
		}
	}

}
