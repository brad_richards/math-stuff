package AdventOfCode.aoc_2022.day09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Same as problem one, but now we have an array of knots, and we track the positions of the last one
 */
public class Problem2 {
	static List<String> lines;
	static final int numKnots = 10;
	static final Point[] knots = new Point[numKnots];
	static TreeSet<Point> tailVisited = new TreeSet<>();

	public static void main(String[] args) throws Exception {
		readInput();
		
		for (int i = 0; i < numKnots; i++) {
			knots[i] = new Point(0,0);
		}
		
		tailVisited.add(knots[numKnots-1]);

		for (String line : lines) {
			doMove(line);
		}

		System.out.println("Answer to part 2: " + tailVisited.size());
	}

	private static void doMove(String line) {
		String[] parts = line.split(" ");
		char direction = parts[0].charAt(0);
		int distance = Integer.parseInt(parts[1]);

		for (int i = 0; i < distance; i++) {
			switch (direction) {
			case 'R' -> knots[0] = new Point(knots[0].getX() + 1, knots[0].getY());
			case 'L' -> knots[0] = new Point(knots[0].getX() - 1, knots[0].getY());
			case 'U' -> knots[0] = new Point(knots[0].getX(), knots[0].getY() + 1);
			case 'D' -> knots[0] = new Point(knots[0].getX(), knots[0].getY() - 1);
			}
			
			for (int j = 1; j < numKnots; j++) {
				moveKnot(j);
			}
			tailVisited.add(knots[numKnots-1]);
//			System.out.println(tail);
		}
	}

	private static void moveKnot(int i) {
		if (knots[i-1].getY() == knots[i].getY()) { // In same row
			int diff = knots[i-1].getX() - knots[i].getX();
			if (Math.abs(diff) > 1) knots[i] = new Point(knots[i].getX() + getSign(diff), knots[i].getY());
		} else if (knots[i-1].getX() == knots[i].getX()) { // In same col
			int diff = knots[i-1].getY() - knots[i].getY();
			if (Math.abs(diff) > 1) knots[i] = new Point(knots[i].getX(), knots[i].getY() + getSign(diff));
		} else { // Diagonal move may be needed
			int xdiff = knots[i-1].getX() - knots[i].getX();
			int ydiff = knots[i-1].getY() - knots[i].getY();
			if (Math.abs(xdiff) > 1 || Math.abs(ydiff) > 1)knots[i] = new Point(knots[i].getX() + getSign(xdiff), knots[i].getY() + getSign(ydiff));
		}
	}

	private static int getSign(int i) {
		if (i < 0)
			return -1;
		else
			return 1;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}