package LeetCode.integer_to_roman;

public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();

        System.out.println(s.intToRoman(3749));
    }

    // Let's just go for it naively... Lots of if-statements
    // Because of the lack of 5000 and 10000 symbols, this code
    // only works for values up to 3999
    public String intToRoman(int num) {
        StringBuilder sbWhole = new StringBuilder();
        int digitNum = 0;
        while (num > 0) {
            int digit = num % 10;
            num = num / 10;
            digitNum++;

            char TEN = 'X';
            char FIVE = 'V';
            char ONE = 'I';
            if (digitNum == 4) {
                TEN = ' '; FIVE = ' '; ONE = 'M';
            } else if (digitNum == 3) {
                TEN = 'M'; FIVE = 'D'; ONE = 'C';
            } else if (digitNum == 2) {
                TEN = 'C'; FIVE = 'L'; ONE = 'X';
            }

            StringBuilder sb = new StringBuilder();
            switch (digit) {
//                case 0 -> do nothing
                case 1 -> sb.append(ONE);
                case 2 -> { sb.append(ONE); sb.append(ONE); }
                case 3 -> { sb.append(ONE); sb.append(ONE); sb.append(ONE); }
                case 4 -> { sb.append(ONE); sb.append(FIVE); }
                case 5 -> sb.append(FIVE);
                case 6 -> { sb.append(FIVE); sb.append(ONE); }
                case 7 -> { sb.append(FIVE); sb.append(ONE); sb.append(ONE); }
                case 8 -> { sb.append(FIVE); sb.append(ONE); sb.append(ONE); sb.append(ONE); }
                case 9 -> { sb.append(ONE); sb.append(TEN); }
            }
            sbWhole.insert(0, sb);
        }
        return sbWhole.toString();
    }
}
