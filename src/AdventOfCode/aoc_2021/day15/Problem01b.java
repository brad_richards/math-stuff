package AdventOfCode.aoc_2021.day15;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Parallel array with lowest cost to reach a cell as the attribute. Parallel
 * boolean array: true is visited, false is unvisited.
 * 
 * Initially, all cells have infinite cost except the start-cell, which is zero.
 * All cells are unvisited.
 * 
 * While there are unvisited cells - Choose the unvisited cell with the lowest
 * cost - Calculate the cost of all surrounding, unvisited cells. If the
 * calculated cost is lower than their current cost, keep the lower cost
 */
public class Problem01b {
	private static int[][] map;
	private static int[][] costs;
	private static boolean[][] visited;
	private static int mapSize;
	private static Point startPoint = new Point(0, 0);
	private static Point endPoint;

	public static void main(String[] args) {
		readInput();

		Point p = findUnvisited();
		while (p != null) {
			if (p.y > 0 && !visited[p.y - 1][p.x])
				costs[p.y - 1][p.x] = Math.min(costs[p.y - 1][p.x], costs[p.y][p.x] + map[p.y - 1][p.x]);
			if (p.x > 0 && !visited[p.y][p.x - 1])
				costs[p.y][p.x - 1] = Math.min(costs[p.y][p.x - 1], costs[p.y][p.x] + map[p.y][p.x - 1]);
			if (p.y < endPoint.y && !visited[p.y + 1][p.x])
				costs[p.y + 1][p.x] = Math.min(costs[p.y + 1][p.x], costs[p.y][p.x] + map[p.y + 1][p.x]);
			if (p.x < endPoint.x && !visited[p.y][p.x + 1])
				costs[p.y][p.x + 1] = Math.min(costs[p.y][p.x + 1], costs[p.y][p.x] + map[p.y][p.x + 1]);
			visited[p.y][p.x] = true;

			p = findUnvisited();
		}

		System.out.println("Cost to end-point: " + costs[endPoint.y][endPoint.x]);
	}

	private static Point findUnvisited() {
		Point p = null;
		for (int y = 0; y < mapSize; y++) {
			for (int x = 0; x < mapSize; x++) {
				if (!visited[y][x])
					if (p == null || costs[y][x] < costs[p.y][p.x]) p = new Point(x, y);
			}
		}
		return p;
	}

	/**
	 * Read input and initialize our data structures. We know that our map is
	 * square.
	 */
	private static void readInput() {
		Scanner in = new Scanner(Problem01b.class.getResourceAsStream("input.txt"));
		ArrayList<String> lines = new ArrayList<>();
		while (in.hasNextLine()) {
			lines.add(in.nextLine());
		}

		mapSize = lines.size();
		map = new int[mapSize][mapSize];
		costs = new int[mapSize][mapSize];
		visited = new boolean[mapSize][mapSize];
		for (int y = 0; y < lines.size(); y++) {
			String line = lines.get(y);
			for (int x = 0; x < lines.get(0).length(); x++) {
				map[y][x] = line.charAt(x) - '0';
				costs[y][x] = Integer.MAX_VALUE;
				visited[y][x] = false;
			}
		}

		costs[0][0] = 0; // Starting position
		endPoint = new Point(mapSize - 1, mapSize - 1);
	}

}
