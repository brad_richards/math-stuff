package hackerRank.projectEuler;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem008HR {
	private static class TestCase {
		int numDigits;
		int[] digits;
		
		public TestCase(int numDigits, String number) {
			this.numDigits = numDigits;
			char[] chars = number.toCharArray();
			digits = new int[chars.length];
			for (int i = 0; i < chars.length; i++) digits[i] = chars[i] - 0x30;
		}
	}
	
	private static ArrayList<TestCase> testCases = new ArrayList<>();

	public static void main(String[] args) {
		readData();
		
		for (TestCase tc : testCases)
			System.out.println(doOneCase(tc));
	}

	private static void readData() {
		Scanner s = new Scanner(System.in);
		int numTestCases = s.nextInt();
		for (int i = 0; i < numTestCases; i++) {
			s.nextInt(); // unneeded
			int numDigits = s.nextInt();
			s.nextLine(); // skip end of line
			String number = s.nextLine();
			testCases.add(new TestCase(numDigits, number));
		}
		s.close();
	}
	
	/**
	 * No need to be fancy - direct solution
	 */
	private static int doOneCase(TestCase tc) {
		int maxProduct = 0;
		for (int start = 0; start <= tc.digits.length - tc.numDigits; start++) {
			int product = 1;
			for (int i = start; i < start+tc.numDigits; i++) {
				product *= tc.digits[i];
			}
			if (product > maxProduct) maxProduct = product;
		}
		return maxProduct;
	}
}
