package AdventOfCode.aoc_2022.day12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Dijkstra's algorithm...
 * 
 * For part 2, we are going to cheat. Looking at the input, the 'b' squares are
 * very limited, and every 'b' is adjacent to an 'a. So we try all the 'b'
 * squared, find the minimum, and add 1.
 */
public class Problem2 {
	static List<String> lines;
	static char[][] map;
	static int[][] steps;
	static Point start;
	static Point end;
	static LinkedList<Point> points = new LinkedList<>(); // Last points
															// processed

	public static void main(String[] args) throws Exception {
		readInput();
		end = findPoint('E');
		map[end.y][end.x] = 'z';
		start = findPoint('S');
		map[start.y][start.x] = 'a';
		
		int overallMin = 99999;
		for (int startY = 0; startY < map.length; startY++) {
			Point start = new Point(startY, 1);
			
			for (int row = 0; row < map.length; row++) {
				for (int col = 0; col < map[0].length; col++) {
					steps[row][col] = -1;
				}
			}
			steps[start.y][start.x] = 1;
	
			points.add(start);
			while (!points.isEmpty()) {
				Point next = points.remove();
	
				// Try adding the four adjacent points
				Point p = tryPoint(next, next.y - 1, next.x);
				if (p != null) points.add(p);
				p = tryPoint(next, next.y + 1, next.x);
				if (p != null) points.add(p);
				p = tryPoint(next, next.y, next.x + 1);
				if (p != null) points.add(p);
				p = tryPoint(next, next.y, next.x - 1);
				if (p != null) points.add(p);
			}
			System.out.println(steps[end.y][end.x]);
			if (steps[end.y][end.x] < overallMin) overallMin = steps[end.y][end.x];
		}
		System.out.println("Final answer = " + overallMin);
	}

	/**
	 * The proposed point is ok, if: - It exists in the bounds of the map - It
	 * has not already been added - It is at most one step higher than the
	 * current point
	 */
	public static Point tryPoint(Point prev, int y, int x) {
		Point p = null;
		if (y >= 0 && y < map.length && x >= 0 && x < map[0].length) {
			if (steps[y][x] < 0) {
				if (map[y][x] - map[prev.y][prev.x] <= 1) {
					p = new Point(y, x);
					steps[y][x] = steps[prev.y][prev.x] + 1;
				}
			}
		}
		return p;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		int height = lines.size();
		int width = lines.get(0).length();
		map = new char[height][width];

		for (int i = 0; i < height; i++) {
			map[i] = lines.get(i).toCharArray();
		}

		steps = new int[height][width];
	}

	private static Point findPoint(char c) {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[0].length; col++) {
				if (map[row][col] == c) return new Point(row, col);
			}
		}
		return null;
	}
}