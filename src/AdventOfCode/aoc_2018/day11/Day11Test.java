package AdventOfCode.aoc_2018.day11;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Day11Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCalculateFuelLevel() {
		assertEquals(Day11.calculateFuelLevel(2, 4, 8), 4);
		assertEquals(Day11.calculateFuelLevel(121, 78, 57), -5);
		assertEquals(Day11.calculateFuelLevel(216, 195, 39), 0);
		assertEquals(Day11.calculateFuelLevel(100, 152, 71), 4);
	}

}
