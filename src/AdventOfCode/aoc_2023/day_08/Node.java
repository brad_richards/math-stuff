package AdventOfCode.aoc_2023.day_08;

public class Node {
	String name;
	String left;
	String right;
	
	public Node(String line) {
		String[] parts = line.split(" = ");
		this.name = parts[0];
		parts = parts[1].split(", ");
		this.left = parts[0].substring(1);
		this.right = parts[1].substring(0, parts[1].length()-1);
	}
	
	@Override
	public String toString() {
		return name + " = (" + left + ", " + right + ")";
	}
};
