package AdventOfCode.aoc_2024.day_08;

import AdventOfCode.utility.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static char[][] map;
	static int mapRows;
	static int mapCols;
	static HashSet<Character> frequencies = new HashSet<>();
	static HashSet<Point> antinodes = new HashSet<>();

	public static void main(String[] args) throws Exception {
		readInput();

		// For each frequency: create a list of all nodes with that frequency
		for (Character c : frequencies) {
			List<Point> nodes = new ArrayList<>();
			for (int row = 0; row < map.length; row++) {
				for (int col = 0; col < map[row].length; col++) {
					if (map[row][col] == c) nodes.add(new Point(row, col));
				}
			}

			// For each pair of nodes, identify all antinodes
			for (int i = 0; i < nodes.size()-1; i++) {
				for (int j = i+1; j < nodes.size(); j++) {
					int rowDiff = nodes.get(i).row() - nodes.get(j).row();
					int colDiff = nodes.get(i).col() - nodes.get(j).col();
					Point p = new Point(nodes.get(i).row() + rowDiff, nodes.get(i).col() + colDiff);
					if (inBounds(p)) antinodes.add(p);
					p = new Point(nodes.get(j).row() - rowDiff, nodes.get(j).col() - colDiff);
					if (inBounds(p)) antinodes.add(p);
				}
			}
		}

		for (Point p : antinodes) System.out.println(p);
		System.out.println(antinodes.size());
	}

	private static boolean inBounds(Point p) {
		return (p.row() >= 0 && p.col() >= 0 && p.row() < mapRows && p.col() < mapCols);
	}

	private static void readInput() throws IOException {
		List<String> lines;		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		mapRows = lines.size();
		mapCols = lines.get(0).length();
		map = new char[mapRows][mapCols];
		for (int i = 0; i < lines.size(); i++) {
			map[i] = lines.get(i).toCharArray();
			for (char c : map[i]) if (c != '.') frequencies.add(c);
		}
	}
}