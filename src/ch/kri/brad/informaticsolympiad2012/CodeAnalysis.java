package ch.kri.brad.informaticsolympiad2012;
public class CodeAnalysis {

	public static void main(String[] args) {
		for (int i = 0; i < 99; i++) {
			int n = (int) (Math.random() * 1000.0 + 2);
			int a = (int) (Math.random() * 10000.0);

			int res1 = Optimized(a, n);
			int res2 = Calculate(a, n);
			if (res1 != res2) {
				System.out.println("Differing results for (" + a + ", " + n + "): "
						+ res1 + " and " + res2);
			}
		}
		System.out.println("Done!");
	}

	/**
	 * Die Funktion "Operation" berechnet ein XOR für jeden Divisor von j, für alle Werte j von 2 bis
	 * n.
	 * 
	 * Diese möchten wir auf die Seite kehren: wir generieren die möglichen Divisoren für alle Werte
	 * von 2 bis n, und berechnen wie oft diese vorkommen (Konzept ähnlich zum Sieb von Erastothenes).
	 * Beispiel: ist n=10, so gibt es drei Zahlen im Bereich 2-10 mit dem Divisor 3. Dies berechnen
	 * wir so: (10 / 3) bzw. (n/j).
	 * 
	 * Zweimal ein XOR ergibt den ursprünglichen Wert. Deshalb müssen wir ein XOR nur dann berechnen,
	 * wenn der Divisor eine ungerade Anzahl Vorkommen hat. Dies berechnen wir dann mit (n/j)%2.
	 */
	static int Optimized(int a, int n) {
		int j, res;
		res = a;
		for (j = 2; j <= n; j++) {
			if (((n / j) % 2) != 0) {
				res = res ^ j;
			}
		}
		return res;
	}

	/*
	 * Diese Funktion ist lediglich eine rekursive Implementation von Integer-Division. Es wird von
	 * "Calculate" verwendet, zu prüfen, ob eine Zahl ein Divisor einer anderen ist.
	 */
	static int Operation(int c, int d) {
		if (c < d) {
			return 0;
		} else {
			return Operation(c - d, d) + 1;
		}
	}

	static int Calculate(int a, int n) {
		int i, j, x, res;
		res = a;
		for (i = 1; i <= n; i++) {
			for (j = 2; j <= i; j++) {
				x = Operation(i, j);
				if (x * j == i) {
					res = res ^ j; // A bitwise XOR-operation.
				}
			}
		}
		return res;
	}
}
