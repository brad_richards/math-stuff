package ch.kri.brad.projecteuler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Print the first lines of Pascal's triangle to the console, to allow us to examine the patterns.
 * All numbers modulo 7, to suit this problem. For efficiency, we re-use two arrays for the entire
 * calculation; if we later decide to work with a billion lines, this will be the only possible
 * approach.
 */
public class Problem148_printTriangle {
	private static final int linesToPrint = 1200;
	private static int[] lastRow = new int[linesToPrint];
	private static int[] nextRow = new int[linesToPrint];
	private static FileWriter out;

	public static void main(String[] args) throws IOException {
		out = new FileWriter(new File("Problem148.txt"));
		
		// Initialize first line
		lastRow[0] = 1;
		printLine(lastRow, 0);

		// To keep the math simpler, line is the number of the *last* line
		for (int line = 1; line < linesToPrint; line++) {
			nextRow[0] = 1;
			nextRow[line] = 1;
			for (int j = 1; j < line; j++) {
				nextRow[j] = (lastRow[j - 1] + lastRow[j]) % 7;
			}

			printLine(nextRow, line);

			// Swap the arrays
			int[] tmp = lastRow;
			lastRow = nextRow;
			nextRow = tmp;
		}
	}

	private static void printLine(int[] row, int lastRowSize) throws IOException {
		StringBuffer buff = new StringBuffer();

		// Calculate the number of leading characters required for alignment;
		// Add a bit of extra padding...
		int numLeadingChars = linesToPrint + 10 - lastRowSize;

		if (lastRowSize % 7 == 6) {
			// Every 7th line, print the line number
			String rowNum = Integer.toString(lastRowSize + 1);
			buff.append(rowNum);
			numLeadingChars -= rowNum.length();
			for (int i = 1; i <= numLeadingChars; i++)
				buff.append('.');
		} else {
			// leading spaces based on number of lines we are generating
			for (int i = 1; i <= numLeadingChars; i++)
				buff.append(' ');
		}

		// Print row contents, separated with spaces
		for (int i = 0; i <= lastRowSize; i++) {
			buff.append(Integer.toString(row[i]));
			buff.append(' ');
		}
		
		buff.append('\n');

		out.write(buff.toString());
	}

}
