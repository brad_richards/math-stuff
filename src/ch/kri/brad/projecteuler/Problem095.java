package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import ch.kri.brad.utility.Factoring;

public class Problem095 {
	public static final int maxNumber = 1000000;
	public static final int maxChainLength = 30;
	public static int[] sumsOfDivisors = new int[maxNumber]; // offset by 1
	public static TreeSet<AmicableSet> amicableSets = new TreeSet<>();

	/**
	 * Generate all amicable chains that begin with a number below the limit given
	 * and have a length below the maximum length given.
	 */
	public static void main(String[] args) {
		// Prepare the sums of divisors; any values over maxNumber are set to 0
		for (int i = 1; i <= maxNumber; i++) {
			int sumOfDivisors = (int) Factoring.getSumDivisors(i);
			if (sumOfDivisors > maxNumber) sumOfDivisors = 0;
			sumsOfDivisors[i-1] = sumOfDivisors;
		}

		System.out.println("Done with prep work");
		
		// Try to find a chain up to maxChainLength beginning at each value
		for (int i = 1; i <= maxNumber; i++) {
			int nextValue = sumsOfDivisors[i-1];
			
			// Look for a chain, aborting if we ever hit a value <= 1
			for (int j = 1; nextValue > 1 && j <= maxChainLength; j++) {
				if (nextValue == i) {
					// found amicable chain; recreate it as an AmicableSet
					ArrayList<Integer> amicableChain = new ArrayList<>();
					amicableChain.add(i);
					nextValue = sumsOfDivisors[i-1];
					while (nextValue != i) {
						amicableChain.add(nextValue);
						nextValue = sumsOfDivisors[nextValue-1];
					}
					AmicableSet as = new AmicableSet(amicableChain);
					amicableSets.add(as);
					break;
				} else {
					nextValue = sumsOfDivisors[nextValue-1];
				}
			}
		}
		
		for (AmicableSet aSet : amicableSets) {
			System.out.println(aSet.toString());
		}
	}

	/**
	 * An AmicableSet is a set of numbers that form an amicable chain. Stored in this way, the precise sequence is not
	 * of interest; we do this for easy identification of different variations of the same chain.
	 * 
	 * @author brad
	 * 
	 */
	public static class AmicableSet implements Comparable<AmicableSet> {
		TreeSet<Integer> elements = new TreeSet<Integer>();

		/**
		 * Constructor to create an enlarged PrimeSet
		 */
		public AmicableSet(ArrayList<Integer> a) {
			for (Integer i : a) {
				elements.add(i);
			}
		}
		
		@Override
		public String toString() {
			StringBuffer b = new StringBuffer();
			for (Integer e : elements) {
				b.append(e.toString() + ", ");
			}
			return b.toString();
		}

		@Override
		public int compareTo(AmicableSet o) {
			Integer thisSize = elements.size();
			Integer oSize = o.elements.size();
			int compare = thisSize.compareTo(oSize);
			if (compare == 0) {
				Integer thisSum = 0;
				for (int e : elements) thisSum += e;
				Integer oSum = 0;
				for (int e : o.elements) oSum += e;
				compare = thisSum.compareTo(oSum);
			}
			return compare;
		}
	}
}
