package kattis.e_loops;

import java.util.Scanner;

public class stopwatch {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int presses = in.nextInt(); // always even
		
		// Check immediately, if the watch will still be running
		if (presses % 2 == 1) {
			System.out.println("still running");
		} else { // even number of presses
			int count = 0;
			int time = 0;
			while (count < presses) {
				int start = in.nextInt();
				int stop = in.nextInt();
				time += stop - start;
				count += 2;
			}
			System.out.println(time);
		}
	}

}
