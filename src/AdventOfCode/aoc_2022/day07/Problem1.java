package AdventOfCode.aoc_2022.day07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static int pos = 0; // Next line to process
	
	// Define classes for files and directories.
	// Store direct data from the input in these classes
	// Beginning at root, recursively calculate the total size of the directories
	// Search out the desired directories for the answer
	public static void main(String[] args) throws Exception {
		readInput();
		Directory root = processInput();
		System.out.println(root);
		
		root.calculateTotalSize(); // Recursively calculate all directory sizes
		
		System.out.println("Answer to part 1: " + root.part1());
		
		//----------- Part 2
		
		long freeSpace = 70000000 - root.getTotalSize();
		long freeSpaceNeeded = 30000000 - freeSpace;
		
		// Get totalSizes of all directories (recursively)
		ArrayList<Long> sizes = new ArrayList<>();
		root.getDirSizes(sizes);
		Collections.sort(sizes);
		
		// Fine the first size that equals or exceeds freeSpaceNeeded
		for (long size : sizes) {
			if (size >= freeSpaceNeeded) {
				System.out.println("Answer to part 2: " + size);
				break;
			}
		}
	}
	
	private static Directory processInput() {
		Directory root = new Directory("", null);
		Directory currentDir = root;
		// Special handling for first input, which creates the root directory
		pos += 2; // First two lines are always "cd /" and "ls"
		processDirectory(currentDir);
		while (pos < lines.size()) {
			// At this level, we always have a command
			String line = lines.get(pos++);
			String[] parts = line.split(" ");
			if (parts[1].equals("cd")) {
				if (parts[2].equals(".."))
					currentDir = currentDir.getParent();
				else if (parts[2].equals("/")) 
					currentDir = root;
				else // name of subdirectory
					currentDir = currentDir.getDirectory(parts[2]);
			} else if (parts[1].equals("ls")) {
				processDirectory(currentDir);
			} else {
				System.out.println("Unexpected command: " + line);
			}
		}
		return root;
	}
	
	private static void processDirectory(Directory current) {
		while (pos < lines.size() && lines.get(pos).charAt(0) != '$') {
			String line = lines.get(pos++);
			String[] parts = line.split(" ");
			if (line.charAt(0) == 'd') { // directory
				current.addEntry(new Directory(parts[1], current));
			} else { // file
				current.addEntry(new File(parts[1], current, Long.parseLong(parts[0]), current.getDepth()+1));
			}
		}
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
