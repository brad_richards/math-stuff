package AdventOfCode.aoc_2019.day06;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem02 {
	private static ArrayList<Body> bodies = new ArrayList<>();
	public static void main(String[] args) {
		readInput();
		
		Body you = bodies.get(bodies.indexOf(new Body("YOU")));
		ArrayList<Body> youAncestors = getAncestors(you);
		
		Body santa = bodies.get(bodies.indexOf(new Body("SAN")));
		ArrayList<Body> santaAncestors = getAncestors(santa);

		int cursor = 0;
		while (youAncestors.get(cursor) == santaAncestors.get(cursor)) cursor++;
		
		int totalTransfers = youAncestors.size() + santaAncestors.size() - 2 * cursor;
		System.out.println(totalTransfers);
	}

	private static ArrayList<Body> getAncestors(Body b) {
		ArrayList<Body> ancestors = new ArrayList<>();
		b = b.orbiting;
		while (b != null) {
			ancestors.add(0, b);
			b = b.orbiting;
		}
		return ancestors;
	}
	
	private static void readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		String line;
		try (Scanner in = new Scanner(fis) ) {
			while (in.hasNextLine()) {
				line = in.nextLine();
				String[] parts = line.split("\\)");
				Body orbitee = new Body(parts[0]);
				int index = bodies.indexOf(orbitee);
				if (index == -1)
					bodies.add(orbitee);
				else
					orbitee = bodies.get(index);
				
				Body orbiter = new Body(parts[1]);
				index = bodies.indexOf(orbiter);
				if (index == -1)
					bodies.add(orbiter);
				else {
					orbiter = bodies.get(index);
				    if (orbiter.orbiting != null) System.out.println(parts[1] + " is already orbiting something!");
				}
				
				orbitee.orbiters.add(orbiter);
				orbiter.orbiting = orbitee;
			}
		}
	}
}
