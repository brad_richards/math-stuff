package ch.kri.brad.projecteuler;

/**
 * Starting with a simple, array-based simulation. Representation: 1 or -1 is a ball, 0 is an empty
 * box. Each iteration, we swap which sign we're working with, so the first time all balls are 1,
 * until moved when they become -1. The next time, they are -1 until moved, when they become 1.
 * 
 * This is far too slow to solve the real problem...
 */
public class Problem426 {
	private static int[] bbs = new int[100];
	private static int numElements = 10;
	private static int ballValue;

	// To know when we are finished, we keep track of (a) how much of the system
	// is finished and (b) the position of the farthest ball. A portion of the system is finished
	// when a segment of balls is followed by a segment of empty boxes <i>larger</i> than the
	// segment of balls. We only need to continue simulating the BBS system to the right of this
	// point.
	private static int finished; // How much of the BBS is finished. Currently, this is just the leftmost ball
	private static int rightmost; // Position of the rightmost ball
	
	private static int segmentsSkipped;
	private static long score = 0;

	public static void main(String[] args) {
		initBBS();
		runBBS();
		long score = scoreFinalState();
		System.out.println("Score = " + score + " and rightmost position is " + rightmost);
	}

	/**
	 * The last cursor position used is the
	 * initial value of "rightmost": the ball farthest to the right in the BBS
	 */
	private static void initBBS() {
		long s = 290797;
		int cursor = 0;
		for (int i = 0; i <= numElements; i++) {
			long t = s % 8 + 1;
			for (int j = 0; j < t; j++) {
				bbs[cursor++] = (i % 2 == 0) ? 1 : 0; // 1 is a ball, 0 is empty
			}
			s = (s * s) % 50515093;
		}
		ballValue = 1; // Balls are initially +1
		finished = -1; // Initially, nothing is finished.
		rightmost = cursor - 1; // Position of rightmost ball
	}

	/**
	 * Run the BBS system.
	 */
	private static void runBBS() {
		 printBBS();
		while (finished < rightmost) {
			shiftBBS();
			moveBalls();
			checkFinished();
			printBBS();
		}
	}

	private static void moveBalls() {
		int newRightmost = 0;
		for (int cursor = finished + 1; cursor <= rightmost; cursor++) {
			if (bbs[cursor] == ballValue) { // Ball to move
				boolean found = false;
				for (int i = cursor + 1; !found && i < bbs.length; i++) {
					found = bbs[i] == 0;
					if (found) {
						bbs[cursor] = 0;
						bbs[i] = -ballValue;
						newRightmost = i;
					}
				}
				if (!found) {
					System.out.println("BBS is too small!");
					finished = Integer.MAX_VALUE;
					break;
				}
			}
		}
		ballValue = -ballValue;
		rightmost = newRightmost;
	}

	/**
	 * It is possible that this could be optimized - as segments are left behind, we may be able to
	 * skip over them. What is currently uncertain: will larger segments generate smaller segments
	 * when they collide? For the moment, we are cautious: we only advance "finished" past the
	 * initial, empty box segment, and any following 1-ball segments.
	 */
	private static void checkFinished() {
		// move finished to one position left of first ball
		while (bbs[finished + 1] == 0) finished++;
		
		// Assume we are finished, unless we find (a) a segment of boxes shorter than the preceding
		// segment of balls, or (b) a segment of balls shorter than the largest segment of balls seen so far.
		boolean isFinished = true;
		int cursor = finished+1;
		int largestBallSegment = 0;
		segmentsSkipped = 0;
		boolean done = false;
		while (isFinished && !done) {
			// Count this ball segment
			int nBalls = 0;
			while (bbs[cursor + nBalls] != 0) nBalls++;
			
			if (nBalls < largestBallSegment) {
				isFinished = false;
			} else {
				largestBallSegment = nBalls;
				cursor += nBalls;
				
				// Are we past all the balls?
				done = (cursor > rightmost);
				
				if (!done) {
					// Count the box segment
					int nBoxes = 0;
					while (bbs[cursor + nBoxes] == 0) nBoxes++;
					cursor += nBoxes;
					
					// Advance "finished" past 1-ball segments
					if (nBalls == 1) {
						finished = cursor - 1;
						segmentsSkipped++;
					}
					
					isFinished = (nBoxes > nBalls);
				}
			}
		}		
		if (isFinished) finished = rightmost+1;
	}
	
	/**
	 * Shift the BBS to the left, so that the first ball is in position 0
	 */
	private static void shiftBBS() {
		if (finished > -1) {
			int offset = finished+1;
			for (int i = offset; i <= rightmost; i++) bbs[i - offset] = bbs[i];
			for (int i = rightmost - offset + 1; i <= rightmost; i++) bbs[i] = 0;
			finished = -1;
			rightmost -= offset;
			
			score += segmentsSkipped;
		}
	}

	private static long scoreFinalState() {
		int cursor = 0;
		while (cursor <= rightmost) {
			// find next ball segment
			while (bbs[cursor] == 0)
				cursor++;

			// count balls
			int nBalls = 0;
			while (bbs[cursor + nBalls] != 0)
				nBalls++;

			// update score and cursor
			cursor += nBalls;
			score += nBalls * nBalls;
		}
		return score;
	}

	/**
	 * Create a small test pattern (instead of normal initialization)
	 */
	private static void testBBS() {
		int[] testArray = new int[] { 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1 };
		for (int i = 0; i < testArray.length; i++)
			bbs[i] = testArray[i];
		ballValue = 1; // Balls are initially +1
		finished = -1; // Initially, nothing is finished.
		rightmost = testArray.length - 1; // Position of rightmost ball
	}

	/**
	 * Only useful for very small test cases
	 */
	private static void printBBS() {
		int maxPos = bbs.length;
		for (int i = 0; i < maxPos; i++) {
			System.out.print(bbs[i] == 0 ? "_" : "*");
		}
		System.out.println();

		if (finished < maxPos) {
			for (int i = 0; i < finished; i++)
				System.out.print(" ");
			System.out.println("|");
		}
		System.out.println();
	}
}
