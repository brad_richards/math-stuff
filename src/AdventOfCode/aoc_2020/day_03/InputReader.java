package AdventOfCode.aoc_2020.day_03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	boolean[][] landscape; // True if tree, false if empty
	
	public InputReader(String fileName) throws IOException {
		List<String> lines;
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		int rows = lines.size();
		int cols = lines.get(0).length();
		landscape = new boolean[rows][cols];
		
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			char[] chars = line.toCharArray();
			for (int j = 0; j < chars.length; j++) {
				landscape[i][j] = chars[j] == '#';
			}
		}
	}
	
	public boolean[][] getLandscape() {
		return landscape;
	}
}
