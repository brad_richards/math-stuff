package AdventOfCode.aoc_2023.day_05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Slow, but optimization would be...difficult. Sorting the ranges in a mapping
 * would help a bit, but what you would really want, would be to compile the
 * seven mappings into a single one. That would be a pile of work!
 */
public class Problem2 {

	public static void main(String[] args) throws Exception {
		InputParser.readInput();
		
		long location = Long.MAX_VALUE;
		for (int i = 0; i < InputParser.seeds.size(); i+=2) {
			System.out.println((i+1) + " of " + InputParser.seeds.size());
			long seedStart = InputParser.seeds.get(i);
			long seedEnd = seedStart + InputParser.seeds.get(i+1);
			for (Long seed = seedStart; seed < seedEnd; seed++) {
				//System.out.print(seed + " ");
				long tmp = seed;
				for (Mapper mapper : InputParser.mappers) {
					tmp = mapper.map(tmp);
				}
				if (tmp < location) location = tmp;				
			}
			//System.out.println();
		}
		
		System.out.println(location);
	}
}