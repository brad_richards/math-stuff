package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Digits;

public class Problem092 {

	/**
	 * Brute force is fast enough...
	 */
	public static void main(String[] args) {
		long n = 2;
		int count = 0;
		while (n < 10000000) {
			if (arrives89(n)) count++;
			n++;
		}
		System.out.println(count);
	}
	
	private static boolean arrives89(long in) {
		while (in != 1 && in != 89) {
			int[] digits = Digits.toDigits(in);
			in = 0;
			for (int i = 0; i < digits.length; i++) in += digits[i] * digits[i];
		}
		return (in == 89);
	}

}
