package ch.kri.tynan;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Theorem: x^y + y^x - x - y is always divisible by 24, where x and y are odd numbers greater than 3
 */
public class TynanTheorem1Original extends Thread {
	private ArrayList<CheckValue> workers = new ArrayList<>();
	private long startTime = System.currentTimeMillis();
	private int lastValueChecked = 1;
	private static int limit;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean goodParm = false;
		if (args.length == 1) {
			try {
				limit = Integer.parseInt(args[0]);
				if (limit > 2) goodParm = true;
			} catch (Exception e) {
			}
		}

		if (!goodParm) {
			System.out.println("Enter limit as command-line parameter: java woof.jar 1234");
		} else {
			TynanTheorem1Original mainProgram = new TynanTheorem1Original();
			mainProgram.start();
		}
	}

	@Override
	public void run() {
		long seconds = 0;
		long minutes = 0;
		while (workers.size() > 0 || lastValueChecked < (limit - 2)) {
			if (workers.size() < 3 && lastValueChecked < (limit - 2)) {
				lastValueChecked += 2;
				CheckValue t = new CheckValue(lastValueChecked);
				addWorker(t);
				t.start();
				seconds = (500 + System.currentTimeMillis() - startTime) / 1000;
				minutes = seconds / 60;
				seconds -= minutes * 60;
				if ((lastValueChecked - 1) % 100 == 0)
					System.out.println("Checking " + lastValueChecked + " Elapsed time" + minutes + ":" + seconds);
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
		System.out.println("Checked all values through " + limit + " Elapsed time" + minutes + ":" + seconds);
	}

	private void addWorker(CheckValue t) {
		synchronized (workers) {
			workers.add(t);
		}
	}

	private void removeWorker(CheckValue t) {
		synchronized (workers) {
			workers.remove(t);
		}
	}

	// Inner class to check one value
	class CheckValue extends Thread {
		private int valueToCheck;

		private CheckValue(int i) {
			this.valueToCheck = i;
		}

		@Override
		public void run() {
			BigInteger bi = BigInteger.valueOf(valueToCheck);

			for (int j = valueToCheck; j <= limit; j += 2) {
				BigInteger bj = BigInteger.valueOf(j);

				BigInteger b1 = bi.pow(j);
				b1 = b1.subtract(bi);
				BigInteger b2 = bj.pow(valueToCheck);
				b2 = b2.subtract(bj);
				BigInteger bvalue = b1.add(b2);
				BigInteger bmod = bvalue.mod(BigInteger.valueOf(24));
				if (!bmod.equals(BigInteger.ZERO)) {
					System.out.println("Exception found: " + valueToCheck + ", " + j);
				}
			}
			removeWorker(this);
		}
	}
}
