package AdventOfCode.aoc_2024.day_01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
    static List<String> lines;

    public static void main(String[] args) throws Exception {
        readInput();
        int[] list1 = new int[lines.size()];
        int[] list2 = new int[lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            String[] parts = lines.get(i).split("   ");
            list1[i] = Integer.parseInt(parts[0]);
            list2[i] = Integer.parseInt(parts[1]);
        }
        Arrays.sort(list1);
        Arrays.sort(list2);

        int pos1 = 0;
        int pos2 = 0;
        int sum = 0;
        while (pos1 < list1.length && pos2 < list2.length) {
            while (pos2 < list2.length && list2[pos2] < list1[pos1]) pos2++;
            int count2 = 0;
            while (pos2 < list2.length && list2[pos2] == list1[pos1]) {
                count2++;
                pos2++;
            }
            int count1 = 1;
            while (pos1 < list1.length-1 && list1[pos1] == list1[pos1+1]) {
                count1++;
                pos1++;
            }
            sum += list1[pos1] * count1 * count2;
            pos1++;
        }
        System.out.println(sum);
    }

    private static void readInput() throws IOException {
        Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
        try (Stream<String> stream = Files.lines(path)) {
            lines = stream.collect(Collectors.toList());
        }
    }
}