package ch.kri.brad.utility;

import java.util.ArrayList;
import java.util.TreeSet;

public class Primes {
	// Methods of this class generate primes as required...
	private static long lastNumTested = 1;
	private static ArrayList<Long> primes = new ArrayList<Long>();
	private static TreeSet<Long> primeTree = new TreeSet<Long>();
	private static boolean[] rawSieve;
	private static ArrayList<ArrayList<Integer>> primeFactorSieve;

	/**
	 * Find the prime factors of an integer value
	 * 
	 * @param valIn
	 *            the value to factor
	 * @return an ArrayList of the prime factors
	 */
	public static ArrayList<Long> primeFactors(long valIn) {
		ArrayList<Long> primeFactors = new ArrayList<Long>();

		// Generate primes, if required
		generatePrimes((long) Math.sqrt(valIn));

		// Divide by all prime numbers <= sqrt(valIn)
		for (int i = 0; valIn > 1 && i < primes.size(); i++) {
			long prime = primes.get(i);
			while (valIn % prime == 0) {
				valIn = valIn / prime;
				primeFactors.add(prime);
			}
		}

		// If the value is still > 1 then it is itself a prime number
		if (valIn > 1) {
			primeFactors.add(valIn);
		}

		return primeFactors;
	}

	/**
	 * Add to the existing list of generated primes. This method is brute-force, and suited only to
	 * problems requiring relatively few primes. For large numbers of primes, use one of the other
	 * methods available in this class
	 * 
	 * @param lastValue
	 *            The highest number to be tested for primeness
	 */
	private static void generatePrimes(long lastValue) {
		for (long testVal = lastNumTested + 1; testVal <= lastValue; testVal++) {
			if (isPrime(testVal)) primes.add(testVal);
		}
	}

	public static boolean isPrime(long testVal) {
		boolean isPrime = false;
		if (testVal > 1) {
			long maxPrime = (long) Math.sqrt(testVal);
			generatePrimes(maxPrime);
			isPrime = true;
			for (int i = 0; isPrime && i < primes.size() && primes.get(i) <= maxPrime; i++) {
				isPrime = (testVal % primes.get(i) != 0);
			}
		}
		return isPrime;
	}

	/**
	 * Generate primes using the Sieve of Erastothenes. Note that this is not an incremental method
	 * - it generates primes afresh, and overwrites any previously generated values.
	 * 
	 * We are, however, clever enough to do nothing, if a previously generated sieve is at least as
	 * large as the present request.
	 * 
	 * @param topValueToTest
	 *            The highest value to test for primeness
	 * @param skips
	 *            Optional booleans; first value says whether or not to place primes in an
	 *            ArrayList, the second says whether or not to place them into a TreeSet. If
	 *            missing, the default Action is to generate the ArrayList only.
	 */
	public static void generatePrimesErastothenes(int topValueToTest, boolean... skips) {
		if (rawSieve == null || (rawSieve != null && rawSieve.length < (topValueToTest - 1))) {

			// Our array: position 0 represents the number 2
			rawSieve = new boolean[topValueToTest - 1];

			// Initialize to "true" for all values
			for (int i = 0; i < rawSieve.length; i++)
				rawSieve[i] = true;

			// Max value for which we must process the array
			int maxValue = (int) Math.sqrt(topValueToTest);

			// Process the array for each value in turn
			for (int val = 2; val <= maxValue; val++) {
				int valPos = val - 2;
				if (rawSieve[valPos]) {
					// Mark all multiples, beginning with val-squared
					for (int pos = val * val - 2; pos < rawSieve.length; pos += val) {
						rawSieve[pos] = false;
					}
				}
			}
		}

		// We normally place the primes in an ArrayList
		if (skips.length == 0 || !skips[0]) {
			// Place the results into our array
			primes = new ArrayList<Long>();
			for (int i = 0; i < rawSieve.length; i++) {
				if (rawSieve[i]) primes.add(new Long(i + 2));
			}
		}

		// We normally do not create a TreeSet of primes
		if (skips.length > 1 && !skips[1]) {
			// Place the results into our tree
			primeTree = new TreeSet<Long>();
			for (int i = 0; i < rawSieve.length; i++) {
				if (rawSieve[i]) primeTree.add(new Long(i + 2));
			}
		}

	}
	
	/**
	 * Generate primes using the Sieve of Erastothenes, accumulating the prime factors for all
	 * non-prime numbers. Note that this is not an incremental method; it generates primes
	 * afresh, and overwrites any previously generated values.
	 * 
	 * We are, however, clever enough to do nothing, if a previously generated sieve is at least as
	 * large as the present request.
	 * 
	 * We cannot use an array, because arrays cannot contain generic types; hence an ArrayList.
	 * Within the outer ArrayList, a value of null indicates a prime number; an inner ArrayList
	 * indicates a compound number, and contains the prime factors.
	 * 
	 * Note that this is much less efficient that the normal Sieve of Erasthothenes, as we must
	 * process many more numbers to ensure we have all prime factors.
	 * 
	 * @param topValueToTest
	 *            The highest value to test for primeness
	 */
	public static void generatePrimeFactorsErastothenes(int topValueToTest) {
		if (primeFactorSieve == null || (primeFactorSieve != null && primeFactorSieve.size() < (topValueToTest - 1))) {

			// Our array: position 0 represents the number 2
			primeFactorSieve = new ArrayList<>(topValueToTest - 1);
			
			// Initialize to null for all values
			for (int i = 0; i < topValueToTest - 1; i++)
				primeFactorSieve.add(null);

			// Max value for which we must process the array
			int maxValue = (int) topValueToTest / 2;

			// Process the array for each value in turn
			for (int val = 2; val <= maxValue; val++) {
				int valPos = val - 2;
				if (primeFactorSieve.get(valPos) == null) {
					// Mark all multiples
					for (int pos = val + val - 2; pos < primeFactorSieve.size(); pos += val) {
						if (primeFactorSieve.get(pos) == null) primeFactorSieve.set(pos,  new ArrayList<Integer>());
						ArrayList<Integer> primeFactorList = primeFactorSieve.get(pos);
						primeFactorList.add(val);
					}
				}
			}
		}
	}

	public static ArrayList<Long> getPrimes() {
		return primes;
	}

	public static TreeSet<Long> getPrimeTree() {
		return primeTree;
	}

	public static boolean[] getRawSieve() {
		return rawSieve;
	}
	
	public static ArrayList<ArrayList<Integer>> getPrimeFactorSieve() {
		return primeFactorSieve;
	}
}
