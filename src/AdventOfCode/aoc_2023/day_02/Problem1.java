package AdventOfCode.aoc_2023.day_02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	
	public static final int[] goal = { 12, 13, 14 };

	public static void main(String[] args) throws Exception {
		List<List<Map<Color, Integer>>> games = readInput();
		
		List<Map<Color, Integer>> gameMaxValues = getMaxValues(games);

		// Finally, compare to the goal values given, and sum the game positions.
		// Note that the game positions begin with 1, not with 0
		int idSum = 0;
		for (int i = 0; i < gameMaxValues.size(); i++) {
			Map<Color, Integer> game = gameMaxValues.get(i);
			if (okGame(game)) idSum += (i + 1);
		}
		System.out.println(idSum);
	}
	
	// For each game, find the maximum number of cubes of each color revealed
	public static List<Map<Color, Integer>> getMaxValues(List<List<Map<Color, Integer>>> games) {
		List<Map<Color, Integer>> gameMaxValues = new ArrayList<>();
		for (List<Map<Color, Integer>> game : games) {
			Map<Color, Integer> maxGame = new HashMap<Color, Integer>();
			gameMaxValues.add(maxGame);
			for (Color color : Color.values()) maxGame.put(color,  0); // Ensure all colors have a value
			for (Map<Color, Integer> reveal : game) {
				for (Color color : Color.values()) {
					Integer number = reveal.get(color);
					if (number != null && number > maxGame.get(color)) maxGame.put(color,  number);
				}
			}
		}
		return gameMaxValues;
	}	
	
	private static boolean okGame(Map<Color, Integer> game) {
		boolean ok = true;
		for (int i = 0; i < goal.length; i++) {
			if (game.get(Color.values()[i]) > goal[i]) ok = false;
		}
		return ok;
	}

	/**
	 * A game is a List of int[3], where the array contains the number of cubes
	 * reveals in the order of the enumeration. The total input is a List of
	 * these Lists, so List<List<int[]>>.
	 * 
	 * Note that a set of revealed cubes may not contain all colors, and that
	 * the order of colors in a revealed set is not always the same.
	 */
	public static List<List<Map<Color, Integer>>> readInput() throws IOException {
		List<List<Map<Color, Integer>>> games = new ArrayList<>();
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lines = stream.collect(Collectors.toList());
			
			for (String line : lines) {
				games.add(parseGame(line));
			}
		}
		return games;
	}
	
	private static List<Map<Color, Integer>> parseGame(String line) {
		List<Map<Color, Integer>> result = new ArrayList<>();
		String[] parts = line.split(": ");
		parts = parts[1].split("; ");
		for (String part : parts) result.add(parseReveal(part));
		return result;
	}
	
	private static Map<Color, Integer> parseReveal(String reveal) {
		Map<Color, Integer> numbers = new HashMap<Color, Integer>();
		String[] cubes = reveal.split(", ");
		for (String cube : cubes) {
			String[] parts = cube.split(" ");
			Integer number = Integer.parseInt(parts[0]);
			Color color = Color.valueOf(parts[1]);
			numbers.put(color, number);
		}
		return numbers;
	}
}