package AdventOfCode.aoc_2023.day_05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem1 {

	public static void main(String[] args) throws Exception {
		InputParser.readInput();
		
		List<Long> locations = new ArrayList<>();
		for (Long seed : InputParser.seeds) {
			for (Mapper mapper : InputParser.mappers) {
				seed = mapper.map(seed);
			}
			locations.add(seed);
		}
		
		Collections.sort(locations);
		System.out.println(locations.get(0));
	}
}