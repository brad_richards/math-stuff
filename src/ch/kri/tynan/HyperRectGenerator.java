package ch.kri.tynan;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Tynan's method to generate hyperrectangles, based on empirical examination of those found through
 * dimension 4 using a blind search program
 */
public class HyperRectGenerator {
	static int maxNumDimensions = -1;
	static int numDimensions;
	static long[] dimensions;
	static ArrayList<String> solutions;

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		while (maxNumDimensions <= 1) {
			System.out.println("Calculate pretty hyperrects for what dimension (must be >= 2)?");
			maxNumDimensions = s.nextInt();
		}
		for (numDimensions = 2; numDimensions <= maxNumDimensions; numDimensions++) {
			dimensions = new long[numDimensions];
			solutions = new ArrayList<String>();
			generateForDimension(numDimensions);
			System.out.println("\n\nSolutions for " + numDimensions + " dimensions\n");
			for (String solution : solutions) {
				System.out.println(solution);
			}
		}
	}

	/**
	 * The smallest value for any dimension is 3 - this is the beginning value for the first
	 * dimension, A.
	 * 
	 * The second dimension begins at the value of (A * x + 1), where x is a factor that will be
	 * successively incremented to generate additional hyperrectangles. The factor x assumes series of
	 * values for successive values of higher dimensions. For example, when generating 3-dimensional
	 * hyperrectangles, we generate four series of hyperrectangles for values of A = 3, 4, 5 and 6.
	 * For A = 3, the hyperrectangles are generates using x = 0.5, 1.0, 1.5, etc. For A = 4, x has the
	 * values 1, 2, 3, .... For A = 5, x has the values 2.5, 5, 7.5, etc. Finally, for A = 6, x has
	 * the value 6.
	 * 
	 * For the moment, we have separate methods for each dimension. The ultimate goal is to have a
	 * single method that can generate the hyperrectangles for any dimension.
	 */
	public static void generateForDimension(int dimension) {
		switch (dimension) {
		case 2:
			generateDim2(dimension);
			break;
		case 3:
			generateDim3(dimension);
			break;
		case 4:
			generateDim4(dimension);
			break;
		case 5:
			generateDim5(dimension);
			break;
		}
	}

	public static void generateDim3(int dimension) {
		dimensions = new long[3];
		double[] factors = { 0.5, 1, 1.5, 2 };
		for (long A = 3; A <= 2 * dimension; A++) {
			double X_base = factors[(int) A - 3];
			double X = X_base;
			long B_min = (long) (A / X_base + 1);
			for (long B = B_min; X <= A; B++) {
				X = X_base * (B + 1 - B_min);
				if (B >= A) {
					double C_test = A * B / X;
					long C = (long) C_test;
					if ((double) C == C_test) { // only valid if the result is an integer
						dimensions[0] = A;
						dimensions[1] = B;
						dimensions[2] = C;
						solutions.add(newSolution());
					}
				}
			}
		}
	}

	public static void generateDim2(int dimension) {
	}

	public static void generateDim4(int dimension) {
		dimensions = new long[4];
		double[] factors = { 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0, 6.5, 7, 7.5, 8, 8.5, 9,
				9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 14.5 };
		for (long A = 3; A <= 2 * dimension; A++) {
			int X_position = (int) A - 3;
			double X_base = factors[X_position];
			double X = X_base;
			long B_min = (long) (A / X_base + 1);
			for (long B = B_min; X_base <= (A * B); B++) {
				if (B >= A) {
					long C_min = (long) (A * B / X + 1);
					for (long C = C_min; X < (A * B); C++) {
						X = X_base * (C + 1 - C_min);
						double D_test = A * B * C / X;
						long D = (long) D_test;
						if ((double) D == D_test) { // only valid if the result is an integer
							dimensions[0] = A;
							dimensions[1] = B;
							dimensions[2] = C;
							dimensions[3] = D;
							solutions.add(newSolution());
						}
					}

				}
				X_position++;
				X_base = factors[X_position];
				X = X_base;
			}
		}
	}

	public static void generateDim5(int dimension) {
	}

	private static String newSolution() {
		StringBuffer sb = new StringBuffer();
		for (long dim : dimensions) {
			if (dim < 100) sb.append(" ");
			if (dim < 10) sb.append(" ");
			sb.append(" " + dim);
		}
		String solution = sb.toString();
		System.out.println(solution);
		return solution;
	}

}