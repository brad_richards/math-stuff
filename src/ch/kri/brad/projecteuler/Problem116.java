package ch.kri.brad.projecteuler;

/**
 * For red tiles (length 2), on a board of length n
 * 
 * - Using 1 tile, we have (n-1)!/[(n-2)! * 1!] possibilities
 * 
 * - Using 2 tiles, we have (n-2)!/[(n-4)! * 2!] possibilities
 * 
 * - Using 3 tiles, we have (n-3)!/[(n-6)! * 3!] possibilities
 * 
 * continuing up to n/2 red tiles. We can easily generate and sum this sequence.
 *
 * 
 * For green tiles (length 3), on a board of length n
 *
 * - Using 1 tile, we have (n-2)!/[(n-3)! * 1!] possibilities
 * 
 * - Using 2 tiles, we have (n-4)!/[(n-6)! * 2!] possibilities
 * 
 * - Using 3 tiles, we have (n-6)!/[(n-9)! * 3!] possibilities
 * 
 * continuing up to n/3 green tiles. We can easily generate and sum this sequence.
 *
 * 
 * For blue tiles (length 4), on a board of length n
 *
 * - Using 1 tile, we have (n-3)!/[(n-4)! * 1!] possibilities
 * 
 * - Using 2 tiles, we have (n-6)!/[(n-8)! * 2!] possibilities
 * 
 * - Using 3 tiles, we have (n-9)!/[(n-12)! * 3!] possibilities
 * 
 * continuing up to n/4 blue tiles. We can easily generate and sum this sequence.
 * 
 * Note: We have to be careful of overflows, even of long-values! We dare not accumulate
 * factorial values.
 */
public class Problem116 {
	private static final int n = 50;

	public static void main(String[] args) {
		long sum = 0;
		
		// red tiles, length 2
		for (long nTiles = 1; nTiles <= n/2; nTiles++) {
			long product = 1;
			for (long i = 1; i <= nTiles; i++) {
				long factor = (n + 1 - nTiles - i);
				product *= factor;
				product /= i;
			}
			sum += product;
		}
		
		// green tiles, length 3
		for (long nTiles = 1; nTiles <= n/3; nTiles++) {
			long product = 1;
			for (long i = 1; i <= nTiles; i++) {
				long factor =(n + 1 - 2 * nTiles - i);
				product *= factor;
				product /= i;
			}
			sum += product;
		}
		
		// blue tiles, length 4
		for (long nTiles = 1; nTiles <= n/4; nTiles++) {
			long product = 1;
			for (long i = 1; i <= nTiles; i++) {
				long factor = (n + 1 - 3 * nTiles - i);
				product *= factor;
				product /= i;
			}
			sum += product;
		}
		System.out.println(sum);
	}

}
