package hackerRank.java.strings;

import java.util.*;

public class JavaStringsIntroduction {

    public static void main(String[] args) {        
        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        String B=sc.next();
        sc.close();
        
        System.out.println(A.length() + B.length());
        
        if (A.compareTo(B) > 0) System.out.println("Yes");
        else System.out.println("No");
        
        char firstCharA = A.charAt(0);
        if (firstCharA >= 'a' && firstCharA <= 'z') firstCharA -= 0x20;

        char firstCharB = B.charAt(0);
        if (firstCharB >= 'a' && firstCharB <= 'z') firstCharB -= 0x20;
        
        String newString = firstCharA + A.substring(1) + " " + firstCharB + B.substring(1);
        System.out.println(newString);
    }
}
