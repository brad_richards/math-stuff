package AdventOfCode.aoc_2022.day03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;
	
	
	public static void main(String[] args) throws Exception {
		readInput();
		long answer = 0;

		for (int i = 0; i < lines.size();) {
			String line1 = lines.get(i++);
			String line2 = lines.get(i++);
			String line3 = lines.get(i++);
			char c = findCommon(line1, line2, line3);
			long priority;
			if (Character.isLowerCase(c))
				priority = (c + 1 - 'a');
			else
				priority = (c + 27 - 'A');
			answer += priority;
		}
		
		System.out.println(answer);
	}
	
	private static char findCommon(String line1, String line2, String line3) {
		char[] chars1 = line1.toCharArray();
		char[] chars2 = line2.toCharArray();
		char[] chars3 = line3.toCharArray();
		
		// We assume that there is exactly one common character
		for (char c : chars1) {
			for (char d : chars2) {
				if (c == d) 
					for (char e : chars3) {
						if (c == e) return c;
					}
			}
		}
		return ' ';
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
