package ch.kri.brad.projecteuler;

/**
 * An attempt at direct calculation - impossibly slow
 */
public class Problem148_direct {
	private static final int linesToPrint = 1000000;
	private static byte[] lastRow = new byte[linesToPrint];
	private static byte[] nextRow = new byte[linesToPrint];
	private static byte[] tmp;

	public static void main(String[] args) {
		// Initialize first line
		lastRow[0] = 1;
		
		// Initialize the count
		int numNonzero = 1;

		// To keep the math simpler, line is the number of the *last* line
		for (int line = 1; line < linesToPrint; line++) {
			if (line%100000 == 0) System.out.print('.');
			
			// Do the ends
			nextRow[0] = 1;
			nextRow[line] = 1;
			numNonzero += 2;
			
			for (int j = 1; j < line; j++) {
				int value = (lastRow[j - 1] + lastRow[j]) % 7;
				if (value != 0) numNonzero++;
				nextRow[j] = (byte) value;
			}

			// Swap the arrays
			tmp = lastRow;
			lastRow = nextRow;
			nextRow = tmp;
		}
		
		System.out.println(numNonzero);
	}
}
