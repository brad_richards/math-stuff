package AdventOfCode.aoc_2015.day03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Using the Point class, we maintain a sparse array in a TreeSet, which
 * automatically removes duplicates
 */
public class Problem1 {
	static List<String> lines;
	static TreeSet<Point> houses = new TreeSet<>();

	public static void main(String[] args) throws Exception {
		readInput();
		Point p = new Point(0,0);
		houses.add(p);
		for (char c : lines.get(0).toCharArray()) {
			p = move(p, c);
			houses.add(p);
		}
		System.out.println(houses.size());
	}
	
	private static Point move(Point p, char c) {
		switch (c) {
		case '^' -> {return new Point(p.getX(), p.getY()+1);}
		case 'v' -> {return new Point(p.getX(), p.getY()-1);}
		case '>' -> {return new Point(p.getX()+1, p.getY());}
		case '<' -> {return new Point(p.getX()-1, p.getY());}
		}
		return null; // never happens
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}