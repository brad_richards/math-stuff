package AdventOfCode.aoc_2020.day_09;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem01 {
	private static final int PreambleLength = 25;
	
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Long> values = ir.getValues();

		// Problem 01
		long answer = problem01(values);
		System.out.println("Problem 1: First invalid entry is " + answer);

		// Problem 02
		answer = problem02(values, answer);
		System.out.println("Problem 2: Weakness is " + answer);
	}
	
	private static boolean isValid(List<Long> values, int pos) {
		boolean valid = false;
		int start = pos - PreambleLength;
		for (int i = start; !valid && i < (pos-1); i++) {
			for (int j = start+1; !valid && j < pos; j++) {
				valid = (values.get(pos).equals((values.get(i) + values.get(j))));
			}
		}
		return valid;
	}
	
	private static long problem01(List<Long> values) {
		boolean found = false;
		long answer = -1;
		for (int i = PreambleLength; !found && i < values.size(); i++) {
			if (!isValid(values, i)) {
				answer = values.get(i);
				found = true;
			}
		}
		return answer;
	}
	
	private static long problem02(List<Long> values, long target) {
		int start = -1;
		int length = 0;
		for (int i = 0; start == -1 && i < values.size(); i++) {
			length = 1;
			long sum = values.get(i);
			while (sum < target && (i + length) < values.size()) {
				sum += values.get(i + length++);
			}
			if (sum == target) { // found sequence
				start = i;
			}
		}
		
		// Copy sequence to temporary list, sort, and add smallest + largest
		List<Long> temp = new ArrayList<>();
		for (int i = start; i < (start + length); i++) {
			temp.add(values.get(i));
		}
		Collections.sort(temp);
		return (temp.get(0) + temp.get(temp.size()-1));
	}
}
