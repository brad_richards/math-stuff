package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.HashSet;

import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.Primes;


/**
 * We proceed on faith that there are, in fact, only 11 truncatable primes.
 * 
 * No idea how we would ever prove such a thing...
 */
public class Problem037 {
	static ArrayList<Long> primes;
	static HashSet<Long> primeSet = new HashSet<Long>();

	public static void main(String[] a) {
		Primes.generatePrimesErastothenes(1000000);
		primes = Primes.getPrimes();

		// Place primes into a HashSet for quick checking
		for (Long prime : primes) {
			primeSet.add(prime);
		}

		// Check all primes
		long sum = 0;
		for (Long prime : primes) {
			if (prime > 10 && truncatable(prime)) {
				System.out.println(prime);
				sum += prime;
			}
		}
		
		System.out.println("Answer = " + sum);
	}

	private static boolean truncatable(long prime) {
		int[] digits = Digits.toDigits(prime);
		boolean isTruncatable = true;
		long value = 0;
		for (int i = 0; isTruncatable && i < digits.length - 1; i++) {
			value = value * 10 + digits[i];
			isTruncatable = primeSet.contains(value);
		}

		value = 0;
		for (int i = 1; isTruncatable && i < digits.length; i++) {
			value = value + (digits[digits.length - i] * (int) Math.pow(10, (i - 1)));
			isTruncatable = primeSet.contains(value);
		}
		return isTruncatable;
	}
}
