package ch.kri.brad.projecteuler.Problems103_105_106;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Nonoptimized {
	/**
	 * Implements a test for the relationship specified in problems 103, 105 and 106.
	 * 
	 * Does not optimize equality-checks, as discussed in problem 106.
	 * 
	 * Anyway, filtering could be massively optimized, by first looking at the set as an array, and
	 * carrying out initial filtering (like the equality comparisons) on the array.
	 */
	public static boolean testSet(int[] setAsArray) {
		// Convert to a true set, for the rest
		Set<Integer> set = new TreeSet<>();
		for (int e : setAsArray) set.add(e);
		return testSet(set);
	}
	public static boolean testSet(Set<Integer> set) {		
		List<SubsetPair> subsetPairs = getAllSubsetPairs(set);
		boolean result = true;
		for (SubsetPair ss : subsetPairs) {
			int sum1 = ss.subset1.stream().mapToInt(Integer::intValue).sum();
			int sum2 = ss.subset2.stream().mapToInt(Integer::intValue).sum();
			if (sum1 == sum2) { // No two subsets may have the same sum
				result = false;
				break;
			}
			if (ss.subset1.size() != ss.subset2.size()) { // Subset size correlates to subset sum
				if (!(sum1 > sum2 && ss.subset1.size() > ss.subset2.size()
						|| sum1 < sum2 && ss.subset1.size() < ss.subset2.size())) {
					result = false;
					break;
				}
			}
		}
		return result;
	}	
	

	
	/**
	 * Generate all combinations of non-empty, disjoint subsets of the given set. Note that our
	 * generation method will include a few duplicates; to avoid this, we need to return a Set
	 * instead of a List, and implement an appropriate "equals" method for SubsetPair.
	 */
	private static List<SubsetPair> getAllSubsetPairs(Set<Integer> set) {
		List<SubsetPair> pairs = new ArrayList<>();

		Set<Set<Integer>> firstSubsets = selectSet(set, null);
		for (Set<Integer> firstSubset : firstSubsets) {
			// Skip empty set and set of all elements
			if (firstSubset.size() > 0 && firstSubset.size() < set.size()) {
				Set<Set<Integer>> secondSubsets = selectSet(set, firstSubset);
				for (Set<Integer> secondSubset : secondSubsets) {
					// Skip empty set
					if (secondSubset.size() > 0) {
						SubsetPair pair = new SubsetPair(firstSubset, secondSubset);
						pairs.add(pair);
					}
				}
			}
		}
		return pairs;
	}
	


	/**
	 * Generate all possible subsets of the set, of the given size, where elements selected are not
	 * in "excludes". We exclude the empty set.
	 */
	private static Set<Set<Integer>> selectSet(Set<Integer> set, Set<Integer> excludes) {
		Set<Integer> candidates = set.stream()
				.filter(e -> excludes == null || !excludes.contains(e)).collect(Collectors.toSet());
		return getPowerSet(candidates);
	}

	/**
	 * Create the power-set of the given set of elements
	 */
	private static Set<Set<Integer>> getPowerSet(Set<Integer> elements) {
		Set<Set<Integer>> powerSet = new HashSet<>();
		if (elements.isEmpty()) { // Base case - emptySet
			powerSet.add(new TreeSet<Integer>());
		} else {
			// Extract one element and recurse; create new sets with and without the extracted
			// element
			Integer firstElt = elements.iterator().next();
			Set<Integer> rest = new TreeSet<>(elements);
			rest.remove(firstElt);

			Set<Set<Integer>> partialSets = getPowerSet(rest);
			powerSet.addAll(partialSets);
			for (Set<Integer> partialSet : partialSets) {
				Set<Integer> newPartialSet = new TreeSet<>(partialSet);
				newPartialSet.add(firstElt);
				powerSet.add(newPartialSet);
			}
		}
		return powerSet;
	}
}
