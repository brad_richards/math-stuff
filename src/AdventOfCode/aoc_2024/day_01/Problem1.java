package AdventOfCode.aoc_2024.day_01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int[] list1 = new int[lines.size()];
		int[] list2 = new int[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			String[] parts = lines.get(i).split("   ");
			list1[i] = Integer.parseInt(parts[0]);
			list2[i] = Integer.parseInt(parts[1]);
		}
		Arrays.sort(list1);
		Arrays.sort(list2);
		int sum = 0;
		for (int i = 0; i < list1.length; i++) {
			sum += Math.abs(list1[i] - list2[i]);
		}
		System.out.println(sum);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}