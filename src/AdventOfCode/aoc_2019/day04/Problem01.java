package AdventOfCode.aoc_2019.day04;

/**
 * Initially taking a simple approach: generate all numbers, and check to see if
 * they meet the criteria. If this is too slow, we could speed it up (at the
 * cost of complexity) by generating only candidates that meet the criteria.
 */
public class Problem01 {
	private static final int START = 130254;
	private static final int END = 678275;
	private static final int numDigits = 6;

	public static void main(String[] args) {
		int count = 0;
		for (int i = START; i <= END; i++) {
			int[] digits = toDigits(i);
			if (meetsCriteria(digits)) count++;
		}
		System.out.println(count);
	}

	private static int[] toDigits(int in) {
		int[] digits = new int[numDigits];
		for (int pos = 0; pos < numDigits; pos++) {
			int digit = in % 10;
			in /= 10;
			digits[numDigits - pos - 1] = digit;
		}
		return digits;
	}
	
	private static boolean meetsCriteria(int[] digits) {
		boolean ascending = true;
		boolean doubleFound = false;
		for (int i = 1; i < numDigits && ascending; i++) {
			ascending = (digits[i-1] <= digits[i]);
			if (digits[i-1]==digits[i]) doubleFound = true;
		}
		return ascending && doubleFound;
	}
}
