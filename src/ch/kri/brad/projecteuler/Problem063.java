package ch.kri.brad.projecteuler;

import java.math.BigInteger;

import ch.kri.brad.utility.Digits;

public class Problem063 {

	/**
	 * We are looking for numbers a = x^n, where a has n digits. The first such
	 * solution is a = 1^1: 1 has one digit and is a power of 1.
	 * 
	 * We need not consider values of x >= 10, as these will always have at
	 * least n+1 digits.
	 * 
	 * 9^21 is the last power of 9^n that still has n digits. Hence, this
	 * problem has no solutions for n > 21.
	 * 
	 * Hence, even with brute force, we only need to check x from 1 to 9, and n
	 * from 1 to 21, for a maximum of 189 possible values. But lower values of x
	 * will cease having enough digits well below 21, so this can be further
	 * optimized.
	 */
	public static void main(String[] args) {
		int numSolutions = 0;
		for (int x = 1; x <= 9; x++) {
			BigInteger bigX = BigInteger.valueOf(x);
			for (int n = 1; n <= 21; n++) {
				BigInteger bigA = bigX.pow(n);
				if (Digits.numDigits(bigA) == n) {
					System.out.println(bigX.toString() + "^" + n + " = " + bigA.toString());
					numSolutions++;
				} else { // As soon as it's too small, stop already
					break;
				}
			}
		}
		System.out.println("Number of solutions: " + numSolutions);
	}

}
