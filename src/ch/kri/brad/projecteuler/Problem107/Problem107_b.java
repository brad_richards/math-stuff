package ch.kri.brad.projecteuler.Problem107;

/**
 * This attempt also uses search, but we work up from zero edges. The graph begins with node 0 as
 * the entire connected graph. From the connected graph, collect all edges that would add a new
 * node. Choose the least expensive, expand the graph, and repeat.
 */
public class Problem107_b {

	public static void main(String[] args) {
		InputReader in = new InputReader();
		int[][] edgeMatrix = in.readMatrix();
		int initialWeight = sum(edgeMatrix) / 2; // All edges present twice
		Graph_b graph = new Graph_b(edgeMatrix.length);
		long startTime = System.currentTimeMillis();
		search(edgeMatrix, graph);
		System.out.println("Initial " + initialWeight + " - final " + graph.weight + " = savings " + (initialWeight - graph.weight));
		long endTime = System.currentTimeMillis();
		System.out.println("Time " + (endTime - startTime) + "ms");
	}

	private static void search(int[][] edgeMatrix, Graph_b graph) {
		while(graph.nextEdge < graph.edges.length) {
			Graph_b.Edge edge = findCheapestEdge(edgeMatrix, graph);
			graph.addEdge(edge);
		}
	}
	
	private static Graph_b.Edge findCheapestEdge(int[][] edgeMatrix, Graph_b graph) {
		Graph_b.Edge bestEdge = new Graph_b.Edge(0,0,Integer.MAX_VALUE);
		
		// Consider all edges that go from a reachable node to a currently unreachable node
		for (int col = 0; col < edgeMatrix.length; col++) {
			if (graph.reachable[col]) {
				for (int row = 0; row < edgeMatrix.length; row++) {
					if (!graph.reachable[row]) {
						if (edgeMatrix[row][col] > 0 && edgeMatrix[row][col] < bestEdge.weight) {
							bestEdge.node1 = col;
							bestEdge.node2 = row;
							bestEdge.weight = edgeMatrix[row][col];
						}
					}
				}
			}			
		}
		return bestEdge;
	}
	
	private static int sum(int[][] matrix) {
		int sum = 0;
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col++) {
				sum+= matrix[row][col];
			}
		}
		return sum;
	}
}
