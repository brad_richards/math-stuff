package AdventOfCode.aoc_2015.day08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		int numChars = 0;
		int numMem = 0;
		for (String line : lines) {
			numChars += line.length();
			numMem += process(line).length();
		}
		System.out.format("Part 1 answer = %d - %d = %d%n", numChars, numMem, (numChars - numMem));
	}
	
	private static String process(String in) {
		char[] chars = in.toCharArray();
		String result = "";
		for (int i = 1; i < chars.length-1; i++) {
			if (chars[i] == '\\' && chars[i+1] == 'x') {
				result += '*'; // Placeholder - not bothering to parse the character
				i += 3;
			} else if (chars[i] == '\\') {
				result += chars[i+1];
				i++;
			} else result += chars[i];
		}
		return result;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}