package ch.kri.brad.utility;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Class to generate all pythagorean triples with a maximum value up to a given
 * limit. The triples are returned in a two-dimensional array, and the values
 * within each triple are sorted. The triples themselves are sorted by their
 * smallest value.
 * 
 * An additional boolean parameter indicates whether only primitive triples or
 * all triples should be returned.
 */
public class PythagoreanTriples {
	public static int[][] pythagoreanTriples(int limit) {
		TreeSet<Triple> triples = generatePrimitiveTriples(limit);

		int[][] triplesArray = new int[triples.size()][];
		int pos = 0;
		for (Triple t : triples) {
			triplesArray[pos++] = t.toArray();
		}
		return triplesArray;
	}

	/**
	 * Generation using Euclid's formula: m is always even, n is always odd, and
	 * the two are co-prime.
	 */
	private static TreeSet<Triple> generatePrimitiveTriples(int limit) {
		TreeSet<Triple> triples = new TreeSet<>();
		for (int m = 2; m * m < limit; m++) {
			int nStart = 1;
			if (m % 2 == 1) nStart = 2;
			for (int n = nStart; n < m; n += 2) {
				if (GenericMath.gcd(m, n) == 1) {
					int a = m * m - n * n;
					int b = 2 * m * n;
					int c = m * m + n * n;

					if (c < limit) {
						Triple t = null;
						if (a < b)
							t = new Triple(a, b, c);
						else
							t = new Triple(b, a, c);
						triples.add(t);
					}
				}
			}
		}
		return triples;
	}

	/**
	 * A triple must be useful in a TreeSet, which means that we need the equals
	 * method (for the Set) and the compareTo method (for sorting)
	 */
	private static class Triple implements Comparable<Triple> {
		private int[] values;

		public Triple(int a, int b, int c) {
			values = new int[3];
			values[0] = a;
			values[1] = b;
			values[2] = c;
		}

		public int[] toArray() {
			return values;
		}

		@Override
		public int compareTo(Triple t) {
			int result = Integer.compare(values[2], t.values[2]);
			if (result == 0) result = Integer.compare(values[1], t.values[1]);
			return result;
		}

		@Override
		public boolean equals(Object o) {
			if (o != null && o instanceof Triple) {
				Triple t = (Triple) o;
				boolean result = true;
				for (int i = 0; i < 3; i++)
					result = result & values[i] == t.values[i];
				return result;
			} else {
				return false;
			}
		}
	}
}
