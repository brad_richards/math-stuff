package AdventOfCode.aoc_2020.day_14;

/**
 * Combines the capabilities required for problem 1 and problem 2
 */
public class Mask extends Instruction {
	private final long AND_mask;
	private final long OR_mask;
	private final long address_AND_mask;
	private final long[] addresses;

	public Mask(String mask) {
		super();
		AND_mask = generateAndMask(mask);
		OR_mask = generateOrMask(mask);
		address_AND_mask = generateAddressAndMask(mask);
		addresses = generateAddresses(mask);
	}
	
	public long applyMask(long value) {
		value &= AND_mask;
		value |= OR_mask;
		return value;
	}
	
	/**
	 * Create addresses required by problem 2. The number of addresses is 2^n
	 * where n is the number of X's in the mask.
	 */
	private long[] generateAddresses(String mask) {
		char[] chars = mask.toCharArray();

		// Size the result
		int numXs = 0;
		for (char c : chars) if (c == 'X') numXs++;
		int size = (int) Math.pow(2,  numXs);
		long[] addresses = new long[size];
		
		// Create an array with the positions of the Xs (their value in powers of 2);
		int[] xPositions = new int[numXs];
		int count = 0;
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == 'X') xPositions[count++] = chars.length - i - 1; 
		}

		// Create all permutations of addresses. We generate the address indices,
		// and use the bits of these values to drive the corresponding X-bits.
		// For example, address number 5 (binary 101) would set the 0th and 2nd
		// X bit. if Xs were in positions 1, 8 and 32, this would generate address 33
		for (int pos = 0; pos < addresses.length; pos++) {
			int tmpPos = pos;
			long value = 0;
			int bit = 0;
			while (tmpPos > 0) {
				if (tmpPos % 2 == 1) value += (long) Math.pow(2,xPositions[bit]);
				tmpPos /= 2;
				bit++;
			}
			addresses[pos] = value;
		}
		
		return addresses;
	}
	
	/**
	 * The "AND" mask forces certain bits to zero. The input mask contains
	 * 0, 1 and X characters. We transform X's to 1's, and create a long
	 * from the resulting value.
	 */
	private long generateAndMask(String mask) {
		long result = 0;
		for (char c : mask.toCharArray()) {
			result *= 2;
			if (c == '1' || c == 'X') result += 1;
		}
		return result;
	}
	
	private long generateAddressAndMask(String mask) {
		long result = 0;
		for (char c : mask.toCharArray()) {
			result *= 2;
			if (c == '1' || c == '0') result += 1;
		}
		return result;
	}
	
	/**
	 * The "OR" mask forces certain bits to one. The input mask contains
	 * 0, 1 and X characters. We transform X's to 0's, and create a long
	 * from the resulting value.
	 */
	private long generateOrMask(String mask) {
		long result = 0;
		for (char c : mask.toCharArray()) {
			result *= 2;
			if (c == '1') result += 1;
		}
		return result;
	}
	
	public long[] getAddresses(long baseAddress) {
		baseAddress |= OR_mask;
		baseAddress &= address_AND_mask;
		long[] finalAddresses = new long[addresses.length];
		for (int i = 0; i < addresses.length; i++) {
			finalAddresses[i] = addresses[i] | baseAddress;
		}
		return finalAddresses;
	}
	
	public long getHighestAddress() {
		return addresses[addresses.length-1] | (int) OR_mask;
	}
}
