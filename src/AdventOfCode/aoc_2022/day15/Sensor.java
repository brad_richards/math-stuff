package AdventOfCode.aoc_2022.day15;

public class Sensor {
	public final int x;
	public final int y;
	public final int distance; // Manhattan Distance to closest beacon
	
	public Sensor(int x, int y, int distance) {
		this.x = x;
		this.y = y;
		this.distance = distance;
	}
}
