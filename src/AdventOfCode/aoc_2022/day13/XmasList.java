package AdventOfCode.aoc_2022.day13;

import java.util.ArrayList;

// Elements can be Integers or XmasLists, so we just avoid using generics to specialize our types.
public class XmasList implements Comparable<XmasList> {
	private final ArrayList contents = new ArrayList();
	
	/**
	 * Our own split-method that splits on commas, but only when those commas are not embedded in internal lists.
	 */
	private static ArrayList<String> split(String in) {
		ArrayList<String> parts = new ArrayList<>();
		int pos = 1; // Skip opening bracket
		int startPart = pos;
		int bracketDepth = 0;
		while (pos < in.length()-1) {
			if (bracketDepth == 0 && in.charAt(pos) == ',') {
				parts.add(in.substring(startPart, pos));
				startPart = pos+1;
			} else if (in.charAt(pos) == '[') {
				bracketDepth++;
			} else if (in.charAt(pos) == ']') {
				bracketDepth--;
			}
			pos++;
		}
		if (startPart < in.length()-1) parts.add(in.substring(startPart, in.length()-1));
		return parts;
	}
	
	public XmasList(Integer elt) {
		contents.add(elt);
	}
	
	public XmasList(String in) {
		ArrayList<String> parts = XmasList.split(in);
		for (String part : parts) {
			if (part.charAt(0)== '[') {
				contents.add(new XmasList(part));
			} else {
				contents.add(Integer.parseInt(part));
			}
		}
	}

	@Override
	public int compareTo(XmasList other) {
		int thisPos = 0;
		int otherPos = 0;

		// As long as both lists have more elements, keep looking
		while (thisPos < contents.size() && otherPos < other.contents.size()) {
			Object thisElt = contents.get(thisPos);
			Object otherElt = other.contents.get(otherPos);
			
			if (thisElt instanceof Integer && otherElt instanceof Integer) {
				int result = ((Integer) thisElt).compareTo((Integer) otherElt);
				if (result != 0) return result; // done!
			} else { // At least one fo the elements is a list
				if  (thisElt instanceof Integer) thisElt = new XmasList((Integer) thisElt);
				if  (otherElt instanceof Integer) otherElt = new XmasList((Integer) otherElt);
				
				int result = ((XmasList) thisElt).compareTo((XmasList) otherElt);
				if (result != 0) return result; // done!
			}
			thisPos++;
			otherPos++;
		}

		// If we are here, one or both lists are out of elements
		if (thisPos >= contents.size()) {
			if (otherPos < other.contents.size())
				return -1;
			else
				return 0;
		} else {
			return 1;
		}
	}
	
	@Override
	public String toString() {
		String s = "[";
		for (int i = 0; i < contents.size()-1; i++) {
			s += contents.get(i) + ",";
		}
		if (contents.size()> 0) s += contents.get(contents.size()-1);
		s += "]";
		return s;
	}
}
