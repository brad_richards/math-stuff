package AdventOfCode.aoc_2020.day_14;

import static org.junit.Assert.*;

import org.junit.Test;

public class MaskTest {

	@Test
	public void test() {
		Mask mask = new Mask("10XX01");
		assertEquals(mask.applyMask(0), 33);
		assertEquals(mask.applyMask(63), 45);
	}

}
