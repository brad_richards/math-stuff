package kattis.g_calculations;

import java.util.Scanner;

public class conundrum {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String msg = in.nextLine();
        
        int count = 0;
        int pos = 0;
        while (pos < msg.length()) {
            char c = msg.charAt(pos);
            
            if ( pos % 3 == 0 && c != 'P') count++;
            else if (pos % 3 == 1 && c != 'E') count++;
            else if (pos % 3 == 2 && c != 'R') count++;
            
            pos++;
        }
        System.out.println(count);
    }

}