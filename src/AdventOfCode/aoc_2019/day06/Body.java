package AdventOfCode.aoc_2019.day06;

import java.util.ArrayList;

public class Body {
	String name;
	Body orbiting = null;
	ArrayList<Body> orbiters = new ArrayList<>();
	
	public Body(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || !o.getClass().equals(this.getClass())) return false;
		Body b = (Body) o;
		return this.name.equals(b.name);
	}
	
	public int countOrbits() {
		if (orbiting == null) return 0;
		else return 1 + orbiting.countOrbits();
	}
}
