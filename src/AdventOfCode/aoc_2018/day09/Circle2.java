package AdventOfCode.aoc_2018.day09;

public class Circle2 {
	private Node currentMarble;
	private Node marble0; // only for debugging purposes

	private class Node {
		int marble;
		Node prev;
		Node next;

		public Node(int marble) {
			this.marble = marble;
			prev = null;
			next = null;
		}

		public Node(int marble, Node prev, Node next) {
			this.marble = marble;
			this.prev = prev;
			this.next = next;
		}
	}

	public Circle2() {
		Node node = new Node(0);
		node.prev = node;
		node.next = node;
		this.currentMarble = node;
		marble0 = node; // only for debugging purposes
	}

	/**
	 * This method implements both rules: If a marble's value is not divisible by 23, it adds the
	 * marble into the circle, and returns 0. If the marble is divisible by 23, it removes the
	 * appropriate marble from the circle and returns a positive value to be added to the player's
	 * score. For details, see the problem descriptions.
	 */
	public int addMarble(int marble) {
		int score = 0;
		if (marble % 23 != 0) {
			Node newNode = new Node(marble, currentMarble.next, currentMarble.next.next);
			currentMarble = newNode;
			currentMarble.prev.next = newNode;
			currentMarble.next.prev = newNode;
		} else {
			for (int i = 0; i < 7; i++) currentMarble = currentMarble.prev;
			currentMarble.prev.next = currentMarble.next;
			currentMarble.next.prev = currentMarble.prev;
			score = marble + currentMarble.marble;
			currentMarble = currentMarble.next;
		}

//		 printMarbles();
		return score;
	}

	private void printMarbles() {
		System.out.print(marble0.marble + " ");
		Node ptr = marble0.next;
		while (ptr != marble0) {
			System.out.print(ptr.marble + " ");
			ptr = ptr.next;
		}
		System.out.println();
	}
}
