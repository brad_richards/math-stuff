package AdventOfCode.aoc_2022.day07;

public class FileSystemEntry {
	private final String name;
	private final Directory parent;
	private final long size;
	private final int depth; // root is 0

	protected FileSystemEntry(String name, Directory parent, long size, int depth) {
		this.name = name;
		this.parent = parent;
		this.size = size;
		this.depth = depth;
	}

	public String getName() {
		return name;
	}

	public Directory getParent() {
		return parent;
	}

	public long getSize() {
		return size;
	}
	
	public int getDepth() {
		return depth;
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass().equals(this.getClass())) {
			FileSystemEntry fe = (FileSystemEntry) o;
			return fe.name.equals(this.name);
		}
		return false;
	}
	
	@Override
	public String toString() {
		String out = "";
		for (int i = 0; i < depth; i++) {
			out += "- ";
		}
		out += name;
		return out;
	}
}
