package AdventOfCode.aoc_2019.day05;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import AdventOfCode.aoc_2019.IntCodeComputer.IntCodeComputer;

public class Problem01_and_02 {
	private static int[] originalMemory = {3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};

	public static void main(String[] args) {
		// read memory
		originalMemory = readInput();
		IntCodeComputer computer = new IntCodeComputer(originalMemory);
		
		computer.reset(originalMemory);
		computer.addInput(5);
		computer.executeProgram();
		ArrayList<Integer> outputs = computer.getOutputs();
		
		// Print answer
		System.out.println(outputs);
	}

	private static int[] readInput() {
		InputStream fis = Problem01_and_02.class.getResourceAsStream("input.txt");
		String line;
		try (Scanner in = new Scanner(fis) ) {
			line = in.nextLine();
		}
		String[] parts = line.split(",");
		int[] intParts = new int[parts.length];
		for (int i = 0; i < intParts.length; i++) intParts[i] = Integer.parseInt(parts[i]);
		return intParts;
	}
}
