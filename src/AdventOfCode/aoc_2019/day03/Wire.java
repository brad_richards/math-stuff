package AdventOfCode.aoc_2019.day03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class Wire {
	private TreeSet<Point> points = new TreeSet<>();
	private int x = 0; // Current x position
	private int y = 0; // Current y position
	private int steps = 0; // Accumulated steps
	
	public void move(char direction) {
		if (direction == 'U') y++;
		else if (direction == 'D') y--;
		else if (direction == 'L') x++;
		else if (direction == 'R') x--;
		else System.out.println("Parse error");
		
		points.add(new Point(x, y, ++steps));
	}
	
	public TreeSet<Point> getPoints() { return points; }
}
