package AdventOfCode.aoc_2019.IntCodeComputer;

/**
 * The index into the tables is the two-digit opcode.
 */
public class OpCodes {
	public static final OpCodes opCodes = new OpCodes();
	
	private String[] names = new String[100];
	private int[] sizes = new int[100];

	public OpCodes() {
		names[00] =    "NOP"; sizes[00] = 1;
		names[01] =    "ADD"; sizes[01] = 4;
		names[02] =   "MULT"; sizes[02] = 4;
		names[03] =  "INPUT"; sizes[03] = 2;
		names[04] = "OUTPUT"; sizes[04] = 2;
		names[05] =  "JUMPT"; sizes[05] = 3;
		names[06] =  "JUMPF"; sizes[06] = 3;
		names[07] =   "LESS"; sizes[07] = 4;
		names[8] =  "EQUAL"; sizes[8] = 4;
		names[99] =   "STOP"; sizes[99] = 1;
	}
	
	public String getName(int opCode) { return names[opCode]; }
	public int getSize(int opCode) { return sizes[opCode]; }
}
