package AdventOfCode.aoc_2020.day_12;

import AdventOfCode.aoc_2020.day_12.Navigation.Instruction;

public class Ship1 {
	private int north = 0;
	private int east = 0;
	private int orientation = 90; // 0 is north, 90 is east
	
	public void navigate(Navigation nav) {
		switch (nav.getInstruction()) {
		case R:
			rotate(nav.getAmount());
			break;
		case L:
			rotate(-nav.getAmount());
			break;
		case F:
			move(nav.getAmount());
			break;
		case N:
			north += nav.getAmount();
			break;
		case S:
			north -= nav.getAmount();
			break;
		case E:
			east += nav.getAmount();
			break;
		case W:
			east -= nav.getAmount();
			break;
		}
	}
	
	private void rotate(int amount) {
		orientation += amount;
		while (orientation < 0) orientation += 360;
		while (orientation >= 360) orientation -= 360;
	}
	
	private void move(int amount) {
		double northDelta = Math.cos(orientation * Math.PI / 180);
		double eastDelta = Math.sin(orientation * Math.PI / 180);
		north += Math.round(amount * northDelta);
		east += Math.round(amount * eastDelta);
	}

	public int getNorth() {
		return north;
	}

	public int getEast() {
		return east;
	}

	public int getOrientation() {
		return orientation;
	}
}
