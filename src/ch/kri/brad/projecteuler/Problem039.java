package ch.kri.brad.projecteuler;
public class Problem039 {

	public static void main(String[] args) {
		int maxNumSolutions = 0;
		int bestMax = 0;
		for (int max = 1; max <= 1000; max++) {
			int numSolutions = 0;
			for (int a = 1; a <= max / 3; a++) {
				for (int b = a; b <= max / 2; b++) {
					int c = max - (a + b);
					if (b <= c) {
						if ((a * a + b * b) == c * c) {
							numSolutions++;
						}
					}
				}
			}
			if (numSolutions > maxNumSolutions) {
				maxNumSolutions = numSolutions;
				bestMax = max;
			}
		}
		System.out.println("Max = " + bestMax + " produces " + maxNumSolutions + " solutions");
	}
}
