package AdventOfCode.aoc_2020.day_06;

import java.io.IOException;
import java.util.List;

public class Problem01 {
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> lines = ir.getLines();
		
		int count = 0;
		Group01 group = new Group01();
		for (String line : lines) {
			if (line.isEmpty()) {
				count += group.getCount();
				group = new Group01();
			} else {
				group.addPerson(line);
			}
		}
		count += group.getCount();
		
		System.out.println("Answer = " + count);
	}

}
