package ch.kri.brad.PiSimulator;

import java.awt.Color;
import java.awt.Graphics;

public class Ball {
	private long mass;
	private int size;
	private double pos;
	private double vel;
	
	public Ball(long mass, int pos, int size, int vel) {
		this.mass = mass;
		this.pos = pos;
		this.size = size;
		this.vel = vel;
	}
	
	public void move(int ticsPerSecond) {
		pos += vel / ticsPerSecond;
	}
	
	public double getLeft() { return pos; }
	
	public double getRight() { return pos + size; }
	
	public void draw(Graphics g) {
		g.setColor(Color.BLUE);
		g.drawRect((int) pos, 10, size, size);
	}
	
	public double getMass() { return mass; }
	public double getVelocity() { return vel; }
	public void setVelocity(double newVel) { this.vel = newVel; }
	
	public double collisionVelocity(Ball other) {
		double otherMass = other.getMass();
		double otherVel = other.getVelocity();
		
		double term1 = vel * (mass - otherMass);
		double term2 = 2 * otherMass * otherVel;
		double term3 = mass + otherMass;
		
		double newVel = (term1 + term2) / term3;
		return newVel;
	}
}
