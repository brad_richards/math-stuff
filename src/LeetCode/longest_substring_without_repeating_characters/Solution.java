package LeetCode.longest_substring_without_repeating_characters;

public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.lengthOfLongestSubstring(""));
        System.out.println(s.lengthOfLongestSubstring("a"));
        System.out.println(s.lengthOfLongestSubstring("au"));
        System.out.println(s.lengthOfLongestSubstring("abcba"));
        System.out.println(s.lengthOfLongestSubstring("abbbbba"));
        System.out.println(s.lengthOfLongestSubstring("abcabcbb"));
    }

    public int lengthOfLongestSubstring(String s) {
        // Special case
        if (s.isEmpty()) return 0;

        char[] chars = s.toCharArray();
        int longest = 1;

        // Now, for each position (up to the next-to-last)
        for (int i = 0; i < chars.length-1; i++) {
            // For each following character, see if there are any duplicates between it and position i
            boolean dupFound = false;
            for (int j = i+1; !dupFound && j < chars.length; j++) {
                for (int k = i; !dupFound && k < j; k++) {
                    dupFound = chars[k] == chars[j];
                }
                if (dupFound) {
                  longest = Math.max(longest, j-i);
                }
            }
            if (!dupFound) { // Ran off end of string - we are done!
                longest = Math.max(longest, chars.length - i);
                break;
            }
        }
        return longest;
    }
}
