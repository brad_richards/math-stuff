package AdventOfCode.aoc_2024.day_11;

import ch.kri.brad.utility.Digits;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ch.kri.brad.utility.Digits.numDigits;

// Any list is suboptimal, because of the linear access time when inserting elements.
// However, all elements are processed independently, so we manage elements in a HashMap,
// where the value of the stone is the first parameter, and the number of such stones is
// the second parameter. Of course, this does not preserve the order, but we do not care.
public class Problem1 {
	static HashMap<Long, Long> stones;

	public static void main(String[] args) throws Exception {
		stones = readInput();

		printStones();
		for (int blink = 1; blink <= 75; blink++) {
			adaptStones();
			//printStones();
		}

		long count = 0;
		for (long key : stones.keySet()) count += stones.get(key);
		System.out.println("Number of stones " + count);
	}

	private static void adaptStones() {
		HashMap<Long, Long> newStones = new HashMap<>();
		Set<Long> stoneValues = stones.keySet();
		for (long key : stoneValues) {
			if (key == 0) {
				addStone(1, stones.get(key), newStones);
			} else if (numDigits(key) % 2 == 0) {
				int numDigits = numDigits(key);
				int[] digits = Digits.toDigits(key);
				long leftHalf = 0;
				long rightHalf = 0;
				for (int j = 0; j < numDigits / 2; j++) {
					leftHalf = leftHalf * 10 + digits[j];
					rightHalf = rightHalf * 10 + digits[numDigits/2 + j];
				}
				addStone(leftHalf, stones.get(key), newStones);
				addStone(rightHalf, stones.get(key), newStones);
			} else {
				addStone(key*2024, stones.get(key), newStones);
			}
		}
		stones = newStones;
	}

	private static void addStone(long value, long quantity, HashMap<Long, Long> stones) {
		if (stones.keySet().contains(value)) {
			stones.put(value, stones.get(value) + quantity);
		} else {
			stones.put(value, quantity);
		}
	}

	private static void printStones() {
		for (Long stone : stones.keySet()) System.out.print(stones.get(stone) + ": " + stone + "   ");
		System.out.println();
	}

	private static HashMap<Long, Long> readInput() throws IOException {
		List<String> lines;
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		String[] parts = lines.get(0).split(" ");
		HashMap<Long, Long> stones = new HashMap<>();
		for (String s : parts) {
			long value = Long.parseLong(s);
			addStone(value, 1, stones);
		}
		return stones;
	}
}