package hackerRank.java.strings;

import java.util.Scanner;

public class JavaStringCompare {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String str = s.next();
		int len = s.nextInt();
		s.close();
		
		String smallest = str.substring(0, len);
		String largest = str.substring(0, len);
		
		for (int start = 1; start <= str.length() - len; start++) {
			int end = start+len;
			String substr = str.substring(start, end);
			if (substr.compareTo(smallest) < 0) smallest = substr;
			if (substr.compareTo(largest) > 0) largest = substr;
		}

		System.out.println(smallest);
		System.out.println(largest);
	}

}
