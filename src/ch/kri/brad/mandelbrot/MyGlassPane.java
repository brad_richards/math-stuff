package ch.kri.brad.mandelbrot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Adapted from: GlassPane tutorial "A well-behaved GlassPane"
 * http://weblogs.java.net/blog/alexfromsun/ by Alexander Potochkin
 */
public class MyGlassPane extends JPanel implements AWTEventListener {
	private final Mandelbrot frame;
	private Point mouseDown, mouseUp, mouseDragPosition;
	private ArrayList<Point> pathPoints;

	public MyGlassPane(Mandelbrot frame) {
		super(null);
		this.frame = frame;
		setOpaque(false);
	}

	protected void paintComponent(Graphics g) {
		// Draw zoom frame (left mouse button)
		if (mouseDown != null && mouseDragPosition != null) {
			int min_x = Math.min(mouseDown.x, mouseDragPosition.x);
			int min_y = Math.min(mouseDown.y, mouseDragPosition.y);
			int max_x = Math.max(mouseDown.x, mouseDragPosition.x);
			int max_y = Math.max(mouseDown.y, mouseDragPosition.y);

			int r_width = max_x - min_x;
			int r_height = max_y - min_y;
			g.setColor(Color.white);
			g.drawRect(min_x, min_y, r_width, r_height);
		}
		
		// Draw path of currently selected point (middle mouse button)
		if (pathPoints != null) {
			g.setColor(Color.CYAN);
			Point fromPoint = pathPoints.get(0);
			for (int i = 1; i < pathPoints.size(); i++) {
				Point toPoint = pathPoints.get(i);
				g.drawLine(fromPoint.x,  fromPoint.y,  toPoint.x,  toPoint.y);
				fromPoint = toPoint;
			}
		}
	}

	public void eventDispatched(AWTEvent event) {
		if (event instanceof MouseEvent) {
			MouseEvent me = (MouseEvent) event;
			if (SwingUtilities.isDescendingFrom(me.getComponent(), frame)) {
				MouseEvent converted = SwingUtilities.convertMouseEvent(me.getComponent(), me,
						frame.getGlassPane());
				if (converted.getY() < frame.canvasHeight) {
					if ((converted.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) { // left button for zoom regions
						if (me.getID() == MouseEvent.MOUSE_PRESSED) {
							mouseDown = converted.getPoint();
						} else if (me.getID() == MouseEvent.MOUSE_DRAGGED) {
							mouseDragPosition = converted.getPoint();
							repaint();
						} else if (me.getID() == MouseEvent.MOUSE_RELEASED) {
							mouseDragPosition = null;
							repaint();
							if (mouseDown != null) {
								mouseUp = converted.getPoint();

								int min_x = Math.min(mouseDown.x, mouseUp.x);
								int min_y = Math.min(mouseDown.y, mouseUp.y);
								int max_x = Math.max(mouseDown.x, mouseUp.x);
								int max_y = Math.max(mouseDown.y, mouseUp.y);

								frame.m_left = frame.m_left + frame.m_pixel * min_x;
								frame.m_bottom = frame.m_bottom + frame.m_pixel * min_y;

								double m_pixel_x = (max_x - min_x) * frame.m_pixel / frame.canvasWidth;
								double m_pixel_y = (max_y - min_y) * frame.m_pixel / frame.canvasHeight;
								frame.m_pixel = Math.max(m_pixel_x, m_pixel_y);

								frame.calculate();
							}
						}
					} else if ((converted.getModifiers() & MouseEvent.BUTTON2_MASK) != 0) { // middle button for paths
						if (me.getID() == MouseEvent.MOUSE_PRESSED || me.getID() == MouseEvent.MOUSE_DRAGGED) {
							pathPoints = frame.calcMandelbrotArray(converted.getPoint());
						} else if (me.getID() == MouseEvent.MOUSE_RELEASED) {
							pathPoints = null;
						}
						repaint();
					} else if ((converted.getModifiers() & MouseEvent.BUTTON3_MASK) != 0) { // right button for zooming out
						frame.reset();
						frame.calculate();
					}
				}
			}
		}
	}

	/**
	 * If someone adds a mouseListener to the GlassPane or set a new cursor we
	 * expect that he knows what he is doing and return the super.contains(x, y)
	 * otherwise we return false to respect the cursors for the underneath
	 * components
	 */
	public boolean contains(int x, int y) {
		if (getMouseListeners().length == 0
				&& getMouseMotionListeners().length == 0
				&& getMouseWheelListeners().length == 0
				&& getCursor() == Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR)) {
			return false;
		}
		return super.contains(x, y);
	}
}
