package AdventOfCode.aoc_2023.day_03;

public class Problem1 {

	public static void main(String[] args) throws Exception {
		InputParser.readInput();

		long sum = 0;
		for (Value value : InputParser.values) {
			if (value.symbol) sum += value.value;
			if (value.symbol) System.out.println(value.value);
		}
		
		System.out.println(sum);
	}


}