package AdventOfCode.aoc_2020.day_16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	private List<String> lines;
	private ArrayList<Field> fields = new ArrayList<>();
	private Ticket yourTicket;
	private ArrayList<Ticket> nearbyTickets = new ArrayList<>();
	
	public InputReader(String fileName) throws IOException {
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		parseInput();
	}

	private void parseInput() {
		int pos = 0;
		while (!lines.get(pos).isBlank()) {
			fields.add(parseField(lines.get(pos++)));
		}
		
		while (lines.get(pos).isBlank()) pos++;
		pos++; // "your ticket"
		
		yourTicket = parseTicket(lines.get(pos++));
		
		while (lines.get(pos).isBlank()) pos++;
		pos++; // "nearby tickets"
		
		while (pos < lines.size()) {
			nearbyTickets.add(parseTicket(lines.get(pos++)));
		}
	}
	
	/**
	 * In practice, there are always exactly two ranges
	 */
	private Field parseField(String line) {
		String[] parts = line.split(":");
		int[] values = new int[4];
		String[] ranges = parts[1].split("or");
		String[] values1 = ranges[0].split("-");
		String[] values2 = ranges[1].split("-");
		values[0] = Integer.parseInt(values1[0].trim());
		values[1] = Integer.parseInt(values1[1].trim());
		values[2] = Integer.parseInt(values2[0].trim());
		values[3] = Integer.parseInt(values2[1].trim());
		
		return new Field(parts[0], values);		
	}
	
	private Ticket parseTicket(String line) {
		String[] parts = line.split(",");
		int[] values = new int[parts.length];
		for (int i = 0; i < parts.length; i++) values[i] = Integer.parseInt(parts[i]);
		return new Ticket(values);
	}

	public ArrayList<Field> getFields() {
		return fields;
	}

	public Ticket getYourTicket() {
		return yourTicket;
	}

	public ArrayList<Ticket> getNearbyTickets() {
		return nearbyTickets;
	}
	
	
}
