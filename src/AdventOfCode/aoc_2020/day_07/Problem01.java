package AdventOfCode.aoc_2020.day_07;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> lines = ir.getLines();
		
		// Create the collection of all bags
		BagCollection allBags = new BagCollection(lines);
		
		// Find target bag-type
		Bag targetBag = allBags.get("shiny gold");
		
		// Probably not efficient, but start from outer bags and find all
		// the ones that ultimately contain the target bag
		List<Bag> possibleBags = new ArrayList<>();
		for (String key : allBags.keySet()) {
			Bag b = allBags.get(key);
			if (allBags.contains(b, targetBag)) possibleBags.add(b);
		}
		
		for (Bag bag : possibleBags) System.out.println(bag.getColor());
		System.out.println("Total options = " + possibleBags.size());
	}

}
