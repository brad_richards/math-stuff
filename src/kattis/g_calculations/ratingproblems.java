package kattis.g_calculations;

import java.util.Scanner;

public class ratingproblems {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int judges = in.nextInt();
		int ratings = in.nextInt();
		
		int totalRating = 0;
		for (int i = 0; i < ratings; i++) 
			totalRating += in.nextInt();
		
		int minAdditional = (judges - ratings) * -3;
		int maxAdditional = (judges - ratings) * 3;
		
		double minRating = (totalRating + minAdditional) / (double) judges;
		double maxRating = (totalRating + maxAdditional) / (double) judges;
		
		System.out.println(minRating + " " + maxRating);
	}

}
