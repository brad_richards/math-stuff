package AdventOfCode.aoc_2022.day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Beam-search

public class Problem2 {
	static List<String> lines;
	static HashMap<String, Valve> valves;

	public static void main(String[] args) throws Exception {
		readInput();
		Solution2.valves = valves; // For easy access by the Solution class

		PriorityQueue<Solution2> allPaths = new PriorityQueue<>();
		allPaths.add(new Solution2());
		int printCounter = 1;
		int beamWidth = 100;
		Solution2 bestSolution = null;
		while (allPaths.size() > 0) {
			List<Solution2> beam = new LinkedList<>();

			// Move the best solutions to the beam for the next round of search
			for (int j = 0; j < beamWidth && allPaths.size() > 0; j++) {
				beam.add(allPaths.remove());
			}

			// Periodically clean up useless paths, and show current status
			if (printCounter++ % 1000 == 0) {
//				int reduced = 0;
//				if (bestSolution != null) {
//					reduced = allPaths.size();
//					PriorityQueue<Solution2> allPathsTmp = new PriorityQueue<>();
//					while (allPaths.size() > 0) {
//						Solution2 tmp = allPaths.remove();
//						if (tmp.maxValue > bestSolution.total) allPathsTmp.add(tmp);
//					}
//					allPaths = allPathsTmp;
//					reduced -= allPaths.size();
//				}

				Solution2 tmp = beam.get(0);
				System.out.format("%2d  %4d  (%d)  ", tmp.numMoves, tmp.prognosis, allPaths.size());
				System.out.println(bestSolution != null ? bestSolution.total : "");
			}

			while (beam.size() > 0) {
				Solution2 s = beam.remove(0);

				// If this is a solution, see if it is the best so far
				if (s.numMoves >= Solution2.timeLimit) {
					if (bestSolution == null || s.total > bestSolution.total) bestSolution = s;
				} else {
					// Only process paths that could lead to a better solution
					if (s.maxValue >= Solution2.highestPrognosis) {
						s.getNextPaths(allPaths);
					}
				}
			}
		}

		System.out.println("Part 2 answer = " + bestSolution.total);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("sample.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}

		// Create valves with flow-rates, but not yet tunnels
		valves = new HashMap<>();
		for (String line : lines) {
			String name = line.substring(6, 8);
			String tmp = line.substring(23, 26); // Flow rate with semicolon
			int flowRate = Integer.parseInt(tmp.split(";")[0]);
			valves.put(name, new Valve(name, flowRate));
		}

		// Create tunnel connections
		for (String line : lines) {
			Valve valve = valves.get(line.substring(6, 8));
			String tmp = line.split(" to valve")[1].substring(1);
			String[] parts = tmp.split(",");
			for (String part : parts) {
				String name = part.trim();
				Valve tunnel = valves.get(name);
				valve.addTunnel(tunnel);
			}
		}
	}
}