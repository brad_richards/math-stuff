package hackerRank.algorithms;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		int[] testCases = readData();

		for (int value : testCases) System.out.println(doOneCase(value));
	}
    
    public static int[] readData() {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] data = new int[size];
        for (int i = 0; i < size; i++) {
        	data[i] = s.nextInt();
        }
        s.close();
        return data;
    }
    
    public static long doOneCase(int limit) {
		long sum = 0;
		
		for (int i = 1; i < limit; i++) {
			if (i % 3 == 0 || i % 5 == 0) sum += i;
		}
		
		return sum;
    }
}
