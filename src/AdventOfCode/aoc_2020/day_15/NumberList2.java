package AdventOfCode.aoc_2020.day_15;

import java.util.HashMap;
import java.util.List;

/**
 * For part 2, we need a more efficient approach - we cannot afford to search for every number.
 * Hence, we have a second data structure - a HashMap - where we store the last position of each
 * number stored.
 * 
 * Tricky: We only store the positions of numbers up-to-but-not-including the last value.
 */
public class NumberList2 {
	int[] numbers;
	HashMap<Integer, Integer> positionMap = new HashMap<>();
	int nextPos = 0; // Next position in array to fill
	
	public NumberList2(int size, List<Integer> initialNumbers) {
		numbers = new int[size];
		for (int pos = 0; pos < initialNumbers.size(); pos++) {
			numbers[pos] = initialNumbers.get(pos);
		}
		for (int pos = 0; pos < initialNumbers.size()-1; pos++) {
			positionMap.put(numbers[pos], pos);
		}
		nextPos = initialNumbers.size();
	}
	
	public void fillNumbers() {
		while (nextPos < numbers.length) {
			numbers[nextPos] = getNextNumber();
			positionMap.put(numbers[nextPos-1], nextPos-1);
			nextPos++;
		}
	}
	
	/**
	 * Return the previous position where the most recent number was used.
	 */
	public int getNextNumber() {
		int target = numbers[nextPos-1];
		Integer lastPos = positionMap.get(target);
		if (lastPos == null) 
			return 0; // Never used before
		else
			return (nextPos - 1) - lastPos;
	}
	
	public int getByPos(int pos) {
		return numbers[pos];
	}
}
