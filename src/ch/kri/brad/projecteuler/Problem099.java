package ch.kri.brad.projecteuler;

import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The obvious solution (working with doubles and Math.log) provides the 1-based answer 709
 * 
 * 
 */
public class Problem099 {

	public static void main(String[] args)  {
		try (Stream<String> numbers = Files.lines(Paths.get(Problem099.class.getResource("Problem099.txt").toURI()));) {
			List<Double> values = numbers.map( s -> calcLog( s.split(",") ) ).collect(Collectors.toList());
			Double largest = Double.MIN_VALUE;
			int index = -1;
			for (int i = 0; i < values.size(); i++) {
				if (values.get(i) > largest) {
					largest = values.get(i);
					index = i;
				}
			}
			System.out.println(index+1); // One-based line numbering
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Does not work, apparently because Math.log has only about 10 digits of precision.
	 */
	private static double calcLog(String[] numbers) {
		double x = Double.parseDouble(numbers[0]);
		double y = Double.parseDouble(numbers[1]);
		double ln = Math.log(x);
		return y * Math.log(x);
	}
}