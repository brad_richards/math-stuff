package ch.kri.brad.projecteuler;

import java.util.ArrayList;

import ch.kri.brad.utility.RecursableList;

/**
 * Forget the exponents, just consider this as addition: 1 + 1 = 2, 2 + 2 = 4.
 * For any given value, we are looking for the lowest-cost means of generating
 * it. However, the cost function is a bit strange. Take 5 as an example: We
 * could generate 5 as 4+1, but we must "pay" for the 4 as 2+2, and we must be
 * for 2 as "1+1". But we only pay for the 2 once! Once we have paid for a
 * number, we can use it as many times as we like.
 * 
 * I tried to come up with a set of rules that would work backwards from a value
 * to its cost, but could not come up with the correct (minimal) answer. Hence,
 * this approach: generating the numbers from the bottom up, and recording the
 * values as we find them.
 * 
 * First, we define an inner class that represents a solution: for a given
 * value, it records the collection of values that make it up. This is then
 * interpreted by the differences. Examples:
 * 
 * 15: {1, 2, 3, 6, 12, 15} = 1 + 1 = 2 + 1 = 3 + 3 = 6 + 6 = 12 + 3 = 15, cost = 5
 * 17: {1, 2, 4, 8, 16, 17} = 1 + 1 = 2 + 2 = 4 + 4 = 8 + 8 = 16 + 1 = 17, cost = 5
 *
 * The core method for generation takes a (partial) list as input, and extends
 * the list by one step using all elements in the list. Each value it generates
 * is then recursively processed, to be extended again. Also, each value it
 * generates is compared against the list of best-so-far solutions.
 * 
 * The recursion terminates in two circumstances: First, when it generates a
 * solution that is worse that a solution already found, there is no point in
 * continuing to develop further solutions on that branch. Second, when the
 * value exceeds the defined limit (200, in the ProjectEuler description).
 */
public class Problem122 {
	private static final int LIMIT = 200;
	static Solution[] best = new Solution[LIMIT]; // off-by-one: position zero is solution for value 1
	
	private static class Solution {
		RecursableList<Integer> sequence;
		int cost = -1; // Cache the cost, since calculating it is O(n)
		
		int getCost() {
			if (cost == -1) cost = sequence.size() -1;
			return cost;
		}
	}

	public static void main(String[] args) {
		// Initialize our list, containing only "1" as a starting value
		RecursableList<Integer> sequence = RecursableList.addHead(1, new RecursableList<>());
		
		// Initialize first solution
		Solution s = new Solution();
		s.sequence = sequence;
		best[0] = s;

		generateSequences(sequence);
		
		int sum = 0;
		for (int i = 0; i < LIMIT; i++) {
			sum += best[i].getCost(); 
		}
		System.out.println(sum);
	}
	
	/**
	 * For each value in our sequence: extend the list using each value in the list.
	 * 
	 * - Check the newest total: if greater than the LIMIT, stop this branch of the search space
	 * 
	 * - Check the new sequence against the list of best solutions: if the current best solution
	 * is better, stop this branch of the search space.
	 * 
	 * Record this as the best solution.
	 * 
	 * Recurse with the new, longer sequence.
	 */
	private static void generateSequences(RecursableList<Integer> sequence) {
		int currentTotal = sequence.head();
		int cost = sequence.size(); // Cost of new, longer sequence is length of unextended sequence
		RecursableList<Integer> cursor = sequence;
		while (!cursor.isEmpty()) {
			int value = cursor.head();
			int newTotal = currentTotal + value;
			if (newTotal <= LIMIT) {
				if (best[newTotal-1] == null || best[newTotal-1].getCost() >= cost) {
					RecursableList<Integer> newSequence = RecursableList.addHead(newTotal, sequence);
					if (best[newTotal-1] == null || best[newTotal-1].getCost() > cost) {
						// Truly a better solution, so save it...
						Solution bestSolution = new Solution();
						bestSolution.sequence = newSequence;
						best[newTotal-1] = bestSolution;
					}
					generateSequences(newSequence);
				}
			}
			cursor = cursor.tail();
		}
	}
}
