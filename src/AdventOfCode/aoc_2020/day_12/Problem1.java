package AdventOfCode.aoc_2020.day_12;

import java.io.IOException;
import java.util.List;

public class Problem1 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Navigation> navs = ir.getValues();
		Ship1 ship = new Ship1();
		for (Navigation nav : navs) {
			ship.navigate(nav);
			System.out.println(ship.getEast() + ", " + ship.getNorth());
		}
		
		int manDist = Math.abs(ship.getNorth()) + Math.abs(ship.getEast());
		System.out.println("Manhattan distance is " + manDist);
	}

}
