package ch.kri.brad.projecteuler;

/**
 * Steps to solution:
 * 
 * - Generate actual sequence of terms (we need 10 of these)
 * 
 * - Consider sequences of these terms: first only one term, then two, then three...up to 10
 * 
 * --- For each such sequence, predict the following term (the FIT)
 * 
 * --- Sum the FITs
 */
public class Problem101 {

	public static void main(String[] args) {
		long[] sequence = actualSequence();
		
		long sum = 0;
		for (int i = 0; i < sequence.length; i++) {
			sum += predictor(sequence, i+1);
		}
		System.out.println("Answer is " + sum);
	}
	
	private static long[] actualSequence() {
		final int numTerms = 10;
		long[] sequence = new long[numTerms];
		for (int n = 1; n <= numTerms; n++) {
			long currentTerm = 1;
			long product = 1;
			for (int i = 1; i <= 10; i++) {
				product *= n;
				currentTerm += (i % 2 == 0 ? product : -product);
			}
			sequence[n-1] = currentTerm;
		}
		return sequence;
	}
	
	private static long predictor(long[] sequence, int numTerms) {
		if (numTerms == 1) { // base case
			return sequence[0];
		} else {
			long[] derived = new long[numTerms-1];
			for (int i = 0; i < derived.length; i++) {
				derived[i] = sequence[i+1] - sequence[i];
			}
			return predictor(derived, numTerms-1) + sequence[numTerms-1];
		}
	}

}
