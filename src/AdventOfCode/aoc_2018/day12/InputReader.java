package AdventOfCode.aoc_2018.day12;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class InputReader {
	private final HashMap<String, Character> rules = new HashMap<>();
	private String initialPosition;
	
	private static final String INPUT_FILE = "input.txt";

	public void readInputs() {
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			initialPosition = "...." + in.nextLine().substring(14).trim() + "....";
			
			boolean noImmaculateConception = false;
			while (in.hasNext()) {
				String line = in.nextLine();
				line = line.trim();
				if (line.length() > 0) { // Skip empty lines
					// Make sure plants cannot spontaneously generate
					noImmaculateConception |= line.equals("..... => .");
					
					String[] parts = line.split("=>");
					String key = parts[0].trim();
					Character value = parts[1].trim().charAt(0);
					if (value.equals('#')) rules.put(key, value);
				}
			}
			if (!noImmaculateConception) System.out.println("INPUT ERROR: Immaculate conception is possible!");
		}
	}

	public HashMap<String, Character> getRules() {
		return rules;
	}
	
	public String getInitialPosition() {
		return initialPosition;
	}
}