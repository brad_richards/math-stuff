package AdventOfCode.aoc_2022.day10;

public class Instruction {
	public static enum OP {
		addx(2), noop(1);

		public final int ticks; // Cycles required for this instruction

		private OP(int ticks) {
			this.ticks = ticks;
		}
	};

	private final OP op;
	private int value = 0;
	private int cycles = 0; // Cycles into this instruction

	public Instruction(String line) {
		this.op = OP.valueOf(line.substring(0, 4));
		if (this.op == OP.addx) this.value = Integer.parseInt(line.substring(4).trim());
	}

	public OP getOp() {
		return op;
	}

	public int getValue() {
		return value;
	}

	public int getCycles() {
		return cycles;
	}

	public int tick(int in) {
		cycles++;
		if (op == OP.addx && cycles == op.ticks) return in + value;
		else return in;
	}
}