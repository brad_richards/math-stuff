package ch.kri.brad.projecteuler;

import java.util.ArrayList;

public class Problem100 {
	/**
	 * Analysis:
	 * 
	 * 1. a * (a+1) / b * (b+1) = 0.5
	 * 
	 * 2. Therefore (b+1)/(a+1) is just over sqrt(2) and b/a is just under
	 * 
	 * The solutions appear cyclically. Analysing the values of "b" below, the
	 * ratios between solutions asymptotically approaches 5.8284271...
	 * 
	 * Therefore, after finding each solutions, we can skip many numbers, to
	 * approach where the next solution must be.
	 */
	public static void main(String[] args) {
		ArrayList<Long> solutions = new ArrayList<Long>();

		double sqrt2 = Math.sqrt(2);
		for (long b = 1l; b < Long.MAX_VALUE; b++) {
			long a = (long) (b / sqrt2);
			long numerator = a * (a + 1);
			long denominator = b * (b + 1);

			// Test for a solution
			if ((2 * numerator) == denominator) {
				System.out.print("Answer = " + (b + 1) + " of which blue " + (a + 1));
				solutions.add(new Long(b));
				if (solutions.size() > 1) System.out.print(", ratio = "
						+ (solutions.get(solutions.size() - 1)) / (double) (solutions.get(solutions.size() - 2)));
				System.out.println();
				b = (long) ((double) b * 5.8284271d);
			}
		}
	}

}
