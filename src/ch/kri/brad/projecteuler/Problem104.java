package ch.kri.brad.projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import ch.kri.brad.utility.Digits;

/**
 * First, we try the simple approach: generate Fibonacci numbers...
 */
public class Problem104 {
	private static final BigInteger CUTOFF = BigInteger.valueOf(1000000000l);

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		BigInteger n1 = BigInteger.ONE;
		BigInteger n2 = BigInteger.ONE;
		BigInteger n3 = null;
		int count = 2;
		boolean found = false;
		while (!found) {
			n3 = n1.add(n2);
			count++;
			if (meetsCriteria(n3)) found = true;
			n1 = n2;
			n2 = n3;
		}
		long end = System.currentTimeMillis();

		System.out.println("Answer to problem 104 is " + count + " (" + (end - start) + "ms)");
		System.out.println(n3);
	}

	private static boolean meetsCriteria(BigInteger num) {
		boolean result = false;
		if (num.compareTo(CUTOFF) > 0) {

			// Check final digits
			long finalDigits = num.mod(CUTOFF).longValue();
			if (panDigital(finalDigits)) {
				// We could use log10, calculate a divisor, etc. - but there is no built-in log10
				// function in Java, so we just divide by 10 until we get there.
				while (num.compareTo(CUTOFF) > 0) num = num.divide(BigInteger.TEN);
				long firstDigits = num.longValue();
				result = panDigital(firstDigits);
			}
		}
		return result;
	}

	/**
	 * Check that the nine digits beginning at "start" contain all of the digits 1-9 exactly once.
	 * We try to be efficient, by first adding them, and only if the total is 45, actually checking
	 * them off a list.
	 */
	private static boolean panDigital(long value) {
		boolean result = (value > 100000000);
		if (result) {
			int[] digits = Digits.toDigits(value);
			int sum = 0;
			for (int i = 0; i < 9; i++) sum += digits[i];
			result = (sum == 45);
			if (result) {
				Arrays.sort(digits);
				for (int i = 0; i < 9; i++)
					if (digits[i] != i + 1) return false;
			}
		}
		return result;
	}
}
