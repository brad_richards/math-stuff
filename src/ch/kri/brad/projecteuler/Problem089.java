package ch.kri.brad.projecteuler;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem089 {

	public static void main(String[] args) {
		try (Stream<String> numbers = Files.lines(Paths.get(Problem099.class.getResource("Problem089.txt").toURI()));) {
			List<String> originalNumbers = numbers.collect(Collectors.toList());
			numbers.close();
			
			int charsSaved = 0;
			for (String number : originalNumbers) {
				String optimalNumber = intToRoman(romanToInt(number));
				charsSaved += number.length() - optimalNumber.length();
			}
			System.out.println("Saved " + charsSaved + " characters");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * We parse out individual digits from the roman numeral. Some digits may
	 * consist of two characters: 4 = IV, 9 = IX, 40 = XL, 90 = XC, 400 = CD and
	 * 900 = CM. We check for these combinations first, and then for single
	 * characters.
	 * 
	 * We assume that the digits appear in descending value, but we make no
	 * other assumptions about validity. For example, we would accept the number
	 * VVVVV as 25, even though this is not a legal roman numeral.
	 */
	public static int romanToInt(String roman) {
		int value = 0;
		int pos = 0;

		while (pos < roman.length() && roman.charAt(pos) == 'M') { value += 1000; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("CM")) { value += 900; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'D') { value += 500; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("CD")) { value += 400; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'C') { value += 100; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("XC")) { value += 90; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'L') { value += 50; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("XL")) { value += 40; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'X') { value += 10; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("IX")) { value += 9; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'V') { value += 5; pos++; }
		while (pos+1 < roman.length() && roman.substring(pos, pos+2).equals("IV")) { value += 4; pos+=2; }
		while (pos < roman.length() && roman.charAt(pos) == 'I') { value += 1; pos++; }
		
		return value;
	}

	public static String intToRoman(int value) {
		StringBuffer sb = new StringBuffer();
		while (value >= 1000) { value -= 1000; sb.append("M"); }
		if (value >= 900) { value -= 900; sb.append("CM"); }
		if (value >= 500) { value -= 500; sb.append("D"); }
		if (value >= 400) { value -= 400; sb.append("CD"); }
		while (value >= 100) { value -= 100; sb.append("C"); }
		if (value >= 90) { value -= 90; sb.append("XC"); }
		if (value >= 50) { value -= 50; sb.append("L"); }
		if (value >= 40) { value -= 40; sb.append("XL"); }
		while (value >= 10) { value -= 10; sb.append("X"); }
		if (value >= 9) { value -= 9; sb.append("IX"); }
		if (value >= 5) { value -= 5; sb.append("V"); }
		if (value >= 4) { value -= 4; sb.append("IV"); }
		while (value >= 1) { value -= 1; sb.append("I"); }
		return sb.toString();
	}
}
