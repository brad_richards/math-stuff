package ch.kri.brad.projecteuler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;

public class Problem059 {

	public static void main(String[] args) {
		BufferedReader reader = null;
		int numChars = 0;
		
		try {
			// Read in file as String
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(null);		
			File f = chooser.getSelectedFile();
			FileReader fr;
				fr = new FileReader(f);
			reader = new BufferedReader(fr);
			String strIn = reader.readLine();
			
			// Split string on commas, then transform into a byte array
			String[] strChars= strIn.split(",");
			numChars = strChars.length;
			int[] intChars = new int[numChars];
			for (int i = 0; i < numChars; i++) {
				intChars[i] = Integer.parseInt(strChars[i]);
			}
			
			// Count frequency of values encrypted by each of the three characters
			int[][] frequency = new int[3][256];
			for (int i = 0; i < intChars.length; i++) {
				int col = i % 3;
				frequency[col][intChars[i]]++;
			}
			
			// Find the three most frequent values in each column (in descending order)
			int[][] key = new int[3][3];
			int[][] frequencies = new int[3][3];
			for (int col = 0; col < 3; col++) {
				for (int i = 0; i < 255; i++) {
					int tmpFreq = frequency[col][i];
					int tmpVal = i;
					int shiftStartPos = 3;
					for (int j = 2; j >= 0; j--) {
						if (tmpFreq > frequencies[col][j]) shiftStartPos = j;
					}
					
					for (int j = shiftStartPos; j < 3; j++) {
						if (tmpFreq > frequencies[col][j]) {
							int tmpint = tmpFreq; tmpFreq = frequencies[col][j]; frequencies[col][j] = tmpint;
							tmpint = tmpVal; tmpVal = key[col][j]; key[col][j] = tmpint;
						}
					}
				}
			}
			
			// Calculate decryption key, assuming the most frequent letters are ' ', 'e', and 't'
			for (int col = 0; col < 3; col++) {
				key[col][0] =  (key[col][0] ^ ' ');
				key[col][1] =  (key[col][1] ^ 'e');
				key[col][2] = (key[col][2] ^ 't');
			}
			
			System.out.print("Key values: ");
			
			for (int col = 0; col < 3; col++) {
				for (int i = 0; i < 3; i++) System.out.print((char) key[col][i]);
				System.out.print("  ");
			}
			System.out.println(" <-- key  \n");
			
			// Decrypt the byte array into a char-array
			char[] decryptedChars = new char[numChars];
			for (int i = 0; i < numChars; i++) {
				int col = i % 3;
					decryptedChars[i] = (char) (key[col][0] ^ intChars[i]);
			}
			
			// Transform the char array into a String and print it
			String decryptedText = new String(decryptedChars);
			System.out.println(decryptedText);
			
			// Sum the char array
			int sum = 0;
			for (char c : decryptedChars) sum += c;
			System.out.println("\nAnswer to Euler Problem 59 is " + sum);
		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
				}
		}
	}

}
