package ch.kri.brad.utility;

import java.util.ArrayList;

public class Fibonacci {
	private static ArrayList<Long> fibonacci = new ArrayList<Long>();
	static {
		fibonacci.add((long) 1);
		fibonacci.add((long) 1);
	}

	/**
	 * Generate numbers in the Fibonacci sequence. This method can be used to sequentially extend the list of already generated numbers.
	 * 
	 * @param maxValue Generate all numbers up to and including this value.
	 */
	public static void generateFibonacci(long maxValue) {
		int numNums = fibonacci.size();
		long beforeLastNum = fibonacci.get(numNums-2);
		long lastNum = fibonacci.get(numNums-1);
		long sum = beforeLastNum + lastNum;
		while (sum <= maxValue) {
			fibonacci.add(sum);
			beforeLastNum = lastNum;
			lastNum = sum;
			sum = beforeLastNum + lastNum;
		}
	}
	
	public static ArrayList<Long> getFibonacciNumbers() {
		return fibonacci;
	}
}
