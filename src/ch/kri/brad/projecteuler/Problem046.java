package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Iterator;

import ch.kri.brad.utility.Primes;



public class Problem046 {
	static ArrayList<Long> primes;
	static ArrayList<Long> squares;
	static boolean[] numberSieve; // position 0 = 0, position 1 = 1, etc.

	/**
	 * We have no idea just where the first number will fall. Hence, we arbitrarily search the first million numbers.
	 * Basically, we take a "sieve" approach: generate all combinations of prime+2*square. Then we look to see which
	 * (if any) numbers have not been generated.
	 */
	public static void main(String[] args) {
		final int LIMIT = 1000000;
		Primes.generatePrimesErastothenes(LIMIT);
		primes = Primes.getPrimes();
		generateSquares(LIMIT);
		
		numberSieve = new boolean[LIMIT];
		for (int i = 0; i < numberSieve.length; i++) numberSieve[i] = false;

		for (Iterator<Long> i = primes.iterator(); i.hasNext();) {
			long prime = i.next();
			numberSieve[(int) prime] = true; // Fill in the primes
			boolean done = false;
			for (Iterator<Long> j = squares.iterator(); j.hasNext() && !done;) {
				long square = j.next();
				long value = prime + 2 * square;
				if (value > LIMIT) {
					done = true;
				} else {
					numberSieve[(int) value] = true;
				}
			}
		}
		
		// Look for an anxwer
		for (int i = 3; i < numberSieve.length; i = i + 2) {
			if (!numberSieve[i]) {
				System.out.println("Impossible for " + i);
			}
		}
	}

	private static void generateSquares(int limit) {
		squares=new ArrayList<Long>();
		for (long i = 1; i <= Math.sqrt(limit); i++) {
			squares.add(new Long(i*i));
		}
	}
}
