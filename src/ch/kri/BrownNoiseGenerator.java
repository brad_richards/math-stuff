package ch.kri;

import java.nio.ByteBuffer;
import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class BrownNoiseGenerator {

	public static void main(String[] args) {
		final int SAMPLE_SIZE = 2;
		final int PACKET_SIZE = 5000;
		
		// These two parameters should be in a GUI, on sliders
		final double heat = 0.1;
		final double maxSDs = 5;

		SourceDataLine line = null;

		try {
			AudioFormat format = new AudioFormat(44100, 16, 1, true, true);
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format, PACKET_SIZE * 2);

			if (!AudioSystem.isLineSupported(info)) {
				throw new LineUnavailableException();
			}

			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();

			ByteBuffer buffer = ByteBuffer.allocate(PACKET_SIZE);

			Random random = new Random();
			double value = 0;
			while (true) {
				buffer.clear();
				for (int i = 0; i < PACKET_SIZE / SAMPLE_SIZE; i++) {
					// The brownian motion
					double move = random.nextGaussian() * heat;
					
					// Cooling to pull values back to the center (prevents audible "pops"
					double cooling = 0 - Math.signum(value) * heat / maxSDs;
					
					value += move + cooling;
					buffer.putShort((short) (value * Short.MAX_VALUE / maxSDs));
				}
				line.write(buffer.array(), 0, buffer.position());
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			line.drain();
			line.close();
		}
	}
}
