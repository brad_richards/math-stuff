package ch.kri.tynan;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Theorem: x und y sind ungerade Zahlen grösser 3; x^y - x is durch 24 teilbar
 */
public class TynanTheorem1Threaded extends Thread {
	private ArrayList<WorkerThread> workers = new ArrayList<>();
	private static final int LIMIT = 10000;
	private static final int MAX_THREADS = 3;

	/**
	 * Die Main-method startet das Manager-Thread
	 */
	public static void main(String[] args) {
		TynanTheorem1Threaded manager = new TynanTheorem1Threaded();
		manager.start();
	}

	@Override
	public void run() {
		int lastValue = 1;
		while (workers.size() > 0 || lastValue < (LIMIT - 2)) {
			if (workers.size() < MAX_THREADS && lastValue < (LIMIT - 2)) {
				lastValue += 2;
				WorkerThread t = new WorkerThread(lastValue);
				addWorker(t);
				t.start();
			}
			try {
				Thread.sleep(100); // 0.1 Sekunden warten
			} catch (InterruptedException e) {
			}
		}
		System.out.println("Checked all values through " + LIMIT);
	}

	/**
	 * Diese Methode "addWorker" nimmt ein WorkerThread als Parameter
	 * und fügt dies die ArrayList "workers" hinzu
	 */
	private void addWorker(WorkerThread t) {
		synchronized (workers) { // Synchronizing the method also ok
			workers.add(t);
		}
	}

	/**
	 * Diese Methode "removeWorker" nimmt ein WorkerThread als Parameter
	 * und entfernt dies von der ArrayList "workers"
	 */
	private void removeWorker(WorkerThread t) {
		synchronized (workers) { // Synchronizing the method also ok
			workers.remove(t);
		}
	}

	/**
	 * Eine Instanze dieser inneren Klasse prüft einen X-Wert
	 */
	class WorkerThread extends Thread {
		private int i;

		private WorkerThread(int i) {
			super("Value " + i); // Name des Threads setzen
			this.i = i;
		}
		
		/**
		 * Diese Methode prüft das Theorem für den X-Wert "i". Sie müssen diese
		 * Methode weder verstehen noch antasten. Allerdings muss diese Methode
		 * aufgerufen werden!
		 */
		private void testOneValue() {
			BigInteger bi = BigInteger.valueOf(i);

			for (int j = i; j <= LIMIT; j += 2) {
				BigInteger bResult = bi.pow(j);
				bResult = bResult.subtract(bi);				
				BigInteger bmod = bResult.mod(BigInteger.valueOf(24));
				
				if (!bmod.equals(BigInteger.ZERO)) {
					System.out.println("Exception found: " + i + ", " + j);
				}
			}
		}

		@Override
		public void run() {
			testOneValue();
			removeWorker(this);
		}
	}
}