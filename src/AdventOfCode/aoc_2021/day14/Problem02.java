package AdventOfCode.aoc_2021.day14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Problem02 {
	private static ArrayList<PairRule> rules = new ArrayList<>();
	private static String startingPolymer;

	private static class PairRule {
		String pIn;
		String pOut1;
		String pOut2;

		public PairRule(String pIn, String pOut1, String pOut2) {
			this.pIn = pIn;
			this.pOut1 = pOut1;
			this.pOut2 = pOut2;
		}
		
		@Override
		public String toString() {
			return pIn + " -> " + pOut1 + " " + pOut2;
		}
	}

	public static void main(String[] args) {
		readInput();

		HashMap<String, Long> pairs = new HashMap<>();
		// Create starting HashMap
		for (int i = 0; i < startingPolymer.length() - 1; i++) {
			String pair = startingPolymer.substring(i, i + 2);
			if (!pairs.keySet().contains(pair)) pairs.put(pair, 0l);
			pairs.put(pair, pairs.get(pair) + 1);
		}
		
		// printPairs(pairs);

		for (int i = 0; i < 40; i++) {
			HashMap<String, Long> newPairs = new HashMap<>();
			for (String pIn : pairs.keySet()) {
				long n = pairs.get(pIn);
				PairRule rule = findRule(pIn);
				if (!newPairs.keySet().contains(rule.pOut1))
					newPairs.put(rule.pOut1, n);
				else
					newPairs.put(rule.pOut1, newPairs.get(rule.pOut1) + n);
				if (!newPairs.keySet().contains(rule.pOut2))
					newPairs.put(rule.pOut2, n);
				else
					newPairs.put(rule.pOut2, newPairs.get(rule.pOut2) + n);
			}
			pairs = newPairs;
			System.out.println(i);
			// printPairs(pairs);
		}

		// Counting elements: every letter occurs twice, except for the start-
		// and end-letters of the initial polymer (which occur once)
		HashMap<Character, Long> elements = new HashMap<>();
		elements.put(startingPolymer.charAt(0), 1l);
		elements.put(startingPolymer.charAt(startingPolymer.length()-1), 1l);
		for (String pair : pairs.keySet()) {
			long value = pairs.get(pair);
			if (!elements.keySet().contains(pair.charAt(0)))
				elements.put(pair.charAt(0), value);
			else
				elements.put(pair.charAt(0), elements.get(pair.charAt(0))+value);
			if (!elements.keySet().contains(pair.charAt(1)))
				elements.put(pair.charAt(1), value);
			else
				elements.put(pair.charAt(1), elements.get(pair.charAt(1))+value);
		}
		
		long highest = 0;
		long lowest = Long.MAX_VALUE;
		for (long value : elements.values()) {
			if (value > highest) highest = value;
			if (value < lowest) lowest = value;
		}
		highest /= 2;
		lowest /= 2;

		System.out.println(highest - lowest);
	}
	
	private static void printPairs(HashMap<String, Long> pairs) {
		for (String pair : pairs.keySet()) {
			System.out.println(pair + " : " + pairs.get(pair));
		}
		System.out.println();
	}

	private static PairRule findRule(String pIn) {
		for (PairRule rule : rules) {
			if (rule.pIn.equals(pIn)) return rule;
		}
		return null;
	}

	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		startingPolymer = in.nextLine();
		in.nextLine(); // skip blank line

		// Read pair-rules
		while (in.hasNextLine()) {
			String line = in.nextLine();
			String pIn = line.substring(0, 2);
			String pOut1 = line.charAt(0) + "" + line.charAt(6);
			String pOut2 = line.charAt(6) + "" + line.charAt(1);
			PairRule rule = new PairRule(pIn, pOut1, pOut2);
			rules.add(rule);
		}
		//for (PairRule rule : rules) System.out.println(rule);
	}
}
