package AdventOfCode.aoc_2022.day09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Solution basically using sparse arrays. Instead, we just define a class
 * representing an array location (X,Y). The head and Tail are individually
 * modelled by XY-points. We keep track of all positions visited by the tail in
 * a TreeSet points (that's the sparse array).
 * 
 * Since TreeSet prohibits duplicates, the size of the TreeSet at the end is
 * already the answer to part 1.
 */
public class Problem1 {
	static List<String> lines;
	static Point head = new Point(0, 0);
	static Point tail = new Point(0, 0);
	static TreeSet<Point> tailVisited = new TreeSet<>();

	public static void main(String[] args) throws Exception {
		readInput();
		tailVisited.add(tail);

		for (String line : lines) {
			doMove(line);
		}

		System.out.println("Answer to part 1: " + tailVisited.size());
	}

	private static void doMove(String line) {
		String[] parts = line.split(" ");
		char direction = parts[0].charAt(0);
		int distance = Integer.parseInt(parts[1]);

		for (int i = 0; i < distance; i++) {
			switch (direction) {
			case 'R' -> head = new Point(head.getX() + 1, head.getY());
			case 'L' -> head = new Point(head.getX() - 1, head.getY());
			case 'U' -> head = new Point(head.getX(), head.getY() + 1);
			case 'D' -> head = new Point(head.getX(), head.getY() - 1);
			}
			moveTail();
			tailVisited.add(tail);
//			System.out.println(tail);
		}
	}

	private static void moveTail() {
		if (head.getY() == tail.getY()) { // In same row
			int diff = head.getX() - tail.getX();
			if (Math.abs(diff) > 1) tail = new Point(tail.getX() + getSign(diff), tail.getY());
		} else if (head.getX() == tail.getX()) { // In same col
			int diff = head.getY() - tail.getY();
			if (Math.abs(diff) > 1) tail = new Point(tail.getX(), tail.getY() + getSign(diff));
		} else { // Diagonal move may be needed
			int xdiff = head.getX() - tail.getX();
			int ydiff = head.getY() - tail.getY();
			if (Math.abs(xdiff) > 1 || Math.abs(ydiff) > 1)tail = new Point(tail.getX() + getSign(xdiff), tail.getY() + getSign(ydiff));
		}
	}

	private static int getSign(int i) {
		if (i < 0)
			return -1;
		else
			return 1;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
