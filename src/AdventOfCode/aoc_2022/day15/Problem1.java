package AdventOfCode.aoc_2022.day15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static List<Sensor> sensors = new ArrayList<>();
	static List<Beacon> beacons = new ArrayList<>();
	static int minX, minY, maxX, maxY;

	public static void main(String[] args) throws Exception {
		readInput();
		createSensors();
		findExtremes();
		
		int answer = possibles(2000000);
		System.out.println("Part 1 answer = " + answer);
	}
	
	private static int possibles(int y) {
		int count = 0;
		for (int x = minX; x <= maxX; x++) {
			boolean inRange = false;
			for (Sensor sensor : sensors) {
				int distance =  Math.abs(x - sensor.x) + Math.abs(y - sensor.y);
				if (distance <= sensor.distance) inRange = true;
			}
			if (inRange) count++;
//			System.out.print(inRange ? '#' : '.');
		}
		
		// Try lower values as well, until we are out of range
		boolean stillInRange = true;
		for (int x = minX-1; stillInRange; x--) {
			boolean inRange = false;
			for (Sensor sensor : sensors) {
				int distance =  Math.abs(x - sensor.x) + Math.abs(y - sensor.y);
				if (distance <= sensor.distance) inRange = true;
			}
			if (inRange) count++;
			else stillInRange = false;
		}
		
		// Try higher values as well, until we are out of range
		stillInRange = true;
		for (int x = maxX+1; stillInRange; x++) {
			boolean inRange = false;
			for (Sensor sensor : sensors) {
				int distance =  Math.abs(x - sensor.x) + Math.abs(y - sensor.y);
				if (distance <= sensor.distance) inRange = true;
			}
			if (inRange) count++;
			else stillInRange = false;
		}
		
		for (Beacon beacon : beacons) {
			if (beacon.y == y) count--;
		}
//		System.out.println();
		return count;
	}
	
	private static void findExtremes() {
		for (Sensor sensor : sensors) {
			if (sensor.x < minX) minX = sensor.x;
			if (sensor.y < minY) minY = sensor.y;
			if (sensor.x > maxX) maxX = sensor.x;
			if (sensor.y > maxY) maxY = sensor.y;
		}
		for (Beacon beacon : beacons) {
			if (beacon.x < minX) minX = beacon.x;
			if (beacon.y < minY) minY = beacon.y;
			if (beacon.x > maxX) maxX = beacon.x;
			if (beacon.y > maxY) maxY = beacon.y;
		}
		System.out.format("X from %d to %d and Y from %d to %d%n", minX, maxX, minY, maxY);
	}
	
	private static void createSensors() {
		for (String line : lines) {
			String[] parts = line.split(" at ");
			String[] coords = parts[1].split(":")[0].split(", ");
			int x = Integer.parseInt(coords[0].substring(2));
			int y = Integer.parseInt(coords[1].substring(2));
			
			coords = parts[2].split(", ");
			int bx = Integer.parseInt(coords[0].substring(2));
			int by = Integer.parseInt(coords[1].substring(2));
			Beacon b = new Beacon(bx, by);
			if (!beacons.contains(b)) beacons.add(b);
			
			int distance = Math.abs(x - bx) + Math.abs(y - by);
			
			sensors.add(new Sensor(x, y, distance));
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}