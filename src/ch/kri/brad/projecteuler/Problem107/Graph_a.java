package ch.kri.brad.projecteuler.Problem107;

/**
 * This class represents a connected graph, consisting of undirected edges. The boolean array
 * "removable" contains an entry for each edge; the entry begins with the value "true", indicated
 * that it *may* be possible to remove the edge without destroying connectivity. If it turns out
 * that removing an edge destroys connectivity, the entry is set to false, so that we never try
 * again to remove the same edge.
 * 
 * Notes are simply continguous integers, from 0 to numNodes-1
 */
public class Graph_a implements Comparable<Graph_a> {
	public static class Edge {
		int node1, node2, weight;

		public Edge(int node1, int node2, int weight) {
			this.node1 = node1;
			this.node2 = node2;
			this.weight = weight;
		}
	}

	Edge[] edges; // undirected edges in this graph
	int numNodes;
	boolean[] removable; // Indicates which edges may be removable
	int weight; // The total weight of all edges in this graph, maintained here for efficiency

	@Override
	public int compareTo(Graph_a other) {
		return Integer.compare(weight, other.weight);
	}

	/**
	 * Returns true if this graph is connected. We check this by brute force - iterating through the
	 * edge list up to (n-2) times. Matrix multiplication might be prettier, but not any faster.
	 */
	public boolean isConnected() {
		boolean[] nodesReachable = new boolean[numNodes];
		nodesReachable[edges[0].node1] = true;
		nodesReachable[edges[0].node2] = true;
		for (int i = 1; i <= edges.length - 2; i++) {
			for (int j = 1; j < edges.length; j++) {
				if (nodesReachable[edges[j].node1]) nodesReachable[edges[j].node2] = true;
				if (nodesReachable[edges[j].node2]) nodesReachable[edges[j].node1] = true;
			}
			if (allTrue(nodesReachable)) return true;
		}
		return false;
	}

	private boolean allTrue(boolean[] in) {
		for (boolean value : in) {
			if (!value) return false;
		}
		return true;
	}

	/**
	 * Create a copy of this graph, omitting one specific edge
	 */
	public Graph_a cloneWithoutEdge(int edgeIndex) {
		Graph_a newGraph = new Graph_a();
		newGraph.numNodes = this.numNodes;
		newGraph.edges = new Edge[this.edges.length - 1];
		newGraph.removable = new boolean[this.edges.length - 1];
		int newIndex = 0;
		for (int i = 0; i < this.edges.length; i++) {
			if (i != edgeIndex) {
				newGraph.edges[newIndex] = this.edges[i];
				newGraph.removable[newIndex] = this.removable[i];
				newIndex++;
			}
		}
		newGraph.weight = this.weight - this.edges[edgeIndex].weight;
		return newGraph;
	}
}
