package AdventOfCode.aoc_2023.day_05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputParser {
	private static final String fileName = "input.txt";
	private static List<String> lines;
	public static List<Long> seeds;
	public static final Mapper[] mappers = new Mapper[7];

	public static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
			seeds = getSeeds(lines.get(0));

			int pos = 3;
			for (int i = 0; i < mappers.length; i++) {
				mappers[i] = getMapper(lines, pos);
				pos += mappers[i].size() + 2;
			}			
		}
	}
	
	private static List<Long> getSeeds(String line) {
		List<Long> seeds = new ArrayList<>();
		String[] parts = line.split(" ");
		for (int i = 1; i < parts.length; i++) seeds.add(Long.parseLong(parts[i]));
		return seeds;
	}
	
	private static Mapper getMapper(List<String> lines, int pos) {
		Mapper mapper = new Mapper();
		while (pos < lines.size() && !lines.get(pos).isBlank()) {
			mapper.addRange(lines.get(pos++));
		}
		return mapper;
	}
}
