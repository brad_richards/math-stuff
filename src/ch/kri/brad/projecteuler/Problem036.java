package ch.kri.brad.projecteuler;

public class Problem036 {

	public static void main(String[] args) {
		int sum = 0;
		// only need to test odd numbers (since binary numbers must begin/end with 1)
		for (int i = 1; i < 1000000; i+=2) {
			if (i == reverseDecimal(i) && i == reverseBinary(i)) {
				sum += i;
			}
		}
		System.out.println("Answer = " + sum);
	}

	static int reverseDecimal(int in) {
		int out = 0;
		while (in > 0) {
			out = 10 * out + in % 10;
			in = in / 10;
		}
		return out;
	}
	
	static int reverseBinary(int in) {
		int out = 0;
		while (in > 0) {
			out = 2 * out + in % 2;
			in = in / 2;
		}
		return out;
	}
}
