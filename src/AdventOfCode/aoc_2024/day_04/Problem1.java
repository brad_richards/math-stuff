package AdventOfCode.aoc_2024.day_04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();
		char[][] chars = new char[lines.size()][];
		for (int i = 0; i < lines.size(); i++) {
			chars[i] = lines.get(i).toCharArray();
		}

		int sum = 0;
		for (int row = 0; row < chars.length; row++) {
			for (int col = 0; col < chars[row].length; col++) {
				if (chars[row][col] == 'X')
					sum += isXmas(chars, row, col);
			}
		}
		System.out.println(sum);
	}

	// There are 8 possibilities. We just create a little method for each of them.
	// We love WET code
	private static int isXmas(char[][] chars, int row, int col) {
		int count = 0;
		if (isXmasUpLeft(chars, row, col)) count += 1;
		if (isXmasUp(chars, row, col)) count += 1;
		if (isXmasUpRight(chars, row, col)) count += 1;
		if (isXmasRight(chars, row, col)) count += 1;
		if (isXmasDownRight(chars, row, col)) count += 1;
		if (isXmasDown(chars, row, col)) count += 1;
		if (isXmasDownLeft(chars, row, col)) count += 1;
		if (isXmasLeft(chars, row, col)) count += 1;
		return count;
	}

	private static boolean isXmasUpLeft(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row >= 3 && col >= 3) {
			isXmas = true;
			if (chars[row-1][col-1] != 'M') isXmas = false;
			if (chars[row-2][col-2] != 'A') isXmas = false;
			if (chars[row-3][col-3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasUp(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row >= 3) {
			isXmas = true;
			if (chars[row-1][col] != 'M') isXmas = false;
			if (chars[row-2][col] != 'A') isXmas = false;
			if (chars[row-3][col] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasUpRight(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row >= 3 && col <= chars[0].length-4) {
			isXmas = true;
			if (chars[row-1][col+1] != 'M') isXmas = false;
			if (chars[row-2][col+2] != 'A') isXmas = false;
			if (chars[row-3][col+3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasRight(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (col <= chars[0].length-4) {
			isXmas = true;
			if (chars[row][col+1] != 'M') isXmas = false;
			if (chars[row][col+2] != 'A') isXmas = false;
			if (chars[row][col+3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasDownRight(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row <= chars.length-4 && col <= chars[0].length-4) {
			isXmas = true;
			if (chars[row+1][col+1] != 'M') isXmas = false;
			if (chars[row+2][col+2] != 'A') isXmas = false;
			if (chars[row+3][col+3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasDown(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row <= chars.length-4) {
			isXmas = true;
			if (chars[row+1][col] != 'M') isXmas = false;
			if (chars[row+2][col] != 'A') isXmas = false;
			if (chars[row+3][col] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasDownLeft(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (row <= chars.length-4 && col >= 3) {
			isXmas = true;
			if (chars[row+1][col-1] != 'M') isXmas = false;
			if (chars[row+2][col-2] != 'A') isXmas = false;
			if (chars[row+3][col-3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static boolean isXmasLeft(char[][] chars, int row, int col) {
		boolean isXmas = false;
		if (col >= 3) {
			isXmas = true;
			if (chars[row][col-1] != 'M') isXmas = false;
			if (chars[row][col-2] != 'A') isXmas = false;
			if (chars[row][col-3] != 'S') isXmas = false;
		}
		return isXmas;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}