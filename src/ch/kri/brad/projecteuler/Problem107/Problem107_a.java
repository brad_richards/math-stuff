package ch.kri.brad.projecteuler.Problem107;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Initial implementation, using best-first search to eliminate unneeded edges
 */
public class Problem107_a {
	public static void main(String[] args) {
		InputReader in = new InputReader();
		Graph_a initialGraph = in.readGraph();
		
		long startTime = System.currentTimeMillis();
		Graph_a finalGraph = search(initialGraph);
		System.out.println("Initial " + initialGraph.weight + " - final " + finalGraph.weight + " = savings " + (initialGraph.weight - finalGraph.weight));
		long endTime = System.currentTimeMillis();
		System.out.println("Time " + (endTime - startTime) + "ms");
	}

	private static Graph_a search(Graph_a in) {
		ArrayList<Graph_a> graphs = new ArrayList<>();
		graphs.add(in);
		Graph_a bestSoFar = null;
		long woof = 0;
		while (bestSoFar == null) { // Accept first solution
			Graph_a tmp = graphs.remove(0);
			ArrayList<Graph_a> extensions = extend(tmp);
			if (extensions.isEmpty()) { // This node is a solution!
				bestSoFar = tmp;
			} else {
				// Would be slightly more efficient to individually insert in correct place
				graphs.addAll(extensions);
				Collections.sort(graphs);
			}
		}
		return bestSoFar;
	}

	private static ArrayList<Graph_a> extend(Graph_a in) {
		ArrayList<Graph_a> extensions = new ArrayList<>();
		for (int i = 0; i < in.edges.length; i++) {
			if (in.removable[i]) { // We can try to remove this edge
				Graph_a extension = in.cloneWithoutEdge(i);
				if (extension.isConnected()) extensions.add(extension);
				else in.removable[i] = false;
			}
		}
		return extensions;
	}	
}
