package ch.kri.brad.projecteuler;
import java.util.Arrays;

import ch.kri.brad.utility.Digits;



public class Problem052 {

	public static void main(String[] args) {
		boolean found = false;
		long value = 0;
		
		// Note that the first digit of the value must be 1
		for (long i = 0; !found && i < Long.MAX_VALUE/10; i++) {
			int numDigits = Digits.numDigits(i);
			value = (int) Math.pow(10, numDigits);
			value += i;
			
			int[] digits = Digits.toDigits(value);
			Arrays.sort(digits);
			found = testValue(digits, value);
		}
		
		if (found) {
			System.out.println("Answer = " + value);
		}
	}

	static boolean testValue(int[] digits, long value) {
		boolean allOk = true;
		for (long mult = 6; allOk && mult >= 2; mult--) {
			long newValue = value * mult;
			int[] newDigits = Digits.toDigits(newValue);
			allOk = (newDigits.length == digits.length);
			if (allOk) {
				Arrays.sort(newDigits);
				for (int i = 0; i < digits.length; i++) {
					if (digits[i] != newDigits[i]) allOk = false;
				}
			}
		}
		return allOk;
	}
}
