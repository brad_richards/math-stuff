package ch.kri.brad.projecteuler;
import ch.kri.brad.utility.Digits;

public class Problem038 {

	public static void main(String[] x) {
		long largestPandigital = 0;
		for (long candidate = 1; candidate < 49999; candidate++) {
			boolean toLarge = false;
			long value = 0;
			for (int multiplier = 1; !toLarge && multiplier <= 9; multiplier++) {
				value = concatenate(value, multiplier * candidate);
				if (isPandigital(value)) {
					if (value > largestPandigital) {
						System.out.println(candidate + " multiplier " + multiplier + " = "
								+ value);
						largestPandigital = value;
					}
				}
				toLarge = (value >= 1000000000);
			}
		}
		System.out.println("Answer = " + largestPandigital);
	}

	public static long concatenate(long left, long right) {
		int numDigitsRight = (int) Math.log10(right) + 1;
		for (int i = 1; i <= numDigitsRight; i++)
			left *= 10;
		return left + right;
	}
	
	private static boolean isPandigital(long value) {
		int[] digits = Digits.toDigits(value);
		boolean answer = false;
		if (digits.length == 9) {
			answer = true;
			for (int i = 0; answer && i < 8; i++) {
				if (digits[i] == 0) answer = false;
				for (int j = i + 1; answer && j < 9; j++) {
					if (digits[i] == digits[j]) {
						answer = false;
					}
				}
			}
		}
		return answer;
	}
}
