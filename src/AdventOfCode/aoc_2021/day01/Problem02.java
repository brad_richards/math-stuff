package AdventOfCode.aoc_2021.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	private static List<Integer> numbers;

	public static void main(String[] args) throws IOException {
		readInput();

		int count = 0;
		for (int i = 2; i < numbers.size()-1; i++) {
			int sum1 = numbers.get(i-2) + numbers.get(i-1) + numbers.get(i);
			int sum2 = numbers.get(i-1) + numbers.get(i) + numbers.get(i+1);
			if (sum2 > sum1) count++;
		}
		System.out.println(count);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			numbers = stream.map(Integer::parseInt).collect(Collectors.toList());
		}
	}

}
