package AdventOfCode.aoc_2022.day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Best-first search

public class Problem3 {
	static List<String> lines;
	static HashMap<String, Valve> valves;

	public static void main(String[] args) throws Exception {
		readInput();
		Solution3.valves = valves; // For easy access by the Solution class

		PriorityQueue<Solution3> allPaths = new PriorityQueue<>();
		allPaths.add(new Solution3());
		int printCounter = 1;
		Solution3 bestSolution = null;
		
		while (allPaths.size() > 0) {
			// Periodically show current status
			if (printCounter++ % 100000 == 0) {
				Solution3 tmp = allPaths.peek();
				System.out.format("%2d  %4d  (%d)  ", tmp.numMoves, tmp.prognosis, allPaths.size());
				System.out.println(bestSolution != null ? bestSolution.total : "");
			}

			Solution3 s = allPaths.remove();
			
			// If this is a solution, see if it is the best so far
			if (s.numMoves >= Solution3.timeLimit) {
				if (bestSolution == null || s.total > bestSolution.total) bestSolution = s;
			} else {
				// Only process paths that could lead to a better solution
				if (s.maxValue >= Solution3.highestPrognosis) {
					s.getNextPaths(allPaths);
				}
			}
		}

		System.out.println("Part 2 answer = " + bestSolution.total);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem3.class.getResource("sample.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}

		// Create valves with flow-rates, but not yet tunnels
		valves = new HashMap<>();
		for (String line : lines) {
			String name = line.substring(6, 8);
			String tmp = line.substring(23, 26); // Flow rate with semicolon
			int flowRate = Integer.parseInt(tmp.split(";")[0]);
			valves.put(name, new Valve(name, flowRate));
		}

		// Create tunnel connections
		for (String line : lines) {
			Valve valve = valves.get(line.substring(6, 8));
			String tmp = line.split(" to valve")[1].substring(1);
			String[] parts = tmp.split(",");
			for (String part : parts) {
				String name = part.trim();
				Valve tunnel = valves.get(name);
				valve.addTunnel(tunnel);
			}
		}
	}
}