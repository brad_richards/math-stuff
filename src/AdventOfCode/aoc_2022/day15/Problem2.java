package AdventOfCode.aoc_2022.day15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * So, we need to find the one and only position outside of all areas commanded
 * by sensors. Our sensors each have a distance. Somewhere, two sensors have a
 * Manhattan distance from each other that is two greater than the sum of their
 * distances.4 Our searched-for point will lie along this line.
 * 
 * So: Given that pair, there will be a diagonal line of candidate positions.
 * For each position, we check if it lies outside the control of all sensors.
 * 
 * Of course, there may be multiple such sensor-pairs. We will try each pair
 * until we find the point.
 */
public class Problem2 {
	static List<String> lines;
	static List<Sensor> sensors = new ArrayList<>();
	static List<Beacon> beacons = new ArrayList<>();
	static int minX, minY, maxX, maxY;

	public static void main(String[] args) throws Exception {
		readInput();
		createSensors();
		findExtremes();

		for (int i = 0; i < sensors.size(); i++) {
			Sensor s1 = sensors.get(i);
			for (int j = 0; j < sensors.size(); j++) {
				Sensor s2 = sensors.get(j);
				int dist = Math.abs(s1.x - s2.x) + Math.abs(s1.y - s2.y);
				if (dist == (s1.distance + s2.distance + 2)) {
					System.out.format("Testing pair %d %d%n", i, j);
					if (testPair(s1, s2)) return;
				}
			}
		}
	}

	private static boolean testPair(Sensor s1, Sensor s2) {
		boolean found = false;
		if (s1.x > s2.x) {
			Sensor tmp = s1;
			s1 = s2;
			s2 = tmp;
		}
		int x = s1.x + s1.distance + 1;
		int y = s1.y;
		int step = (s1.y < s2.y) ? 1 : -1;
		while (!found && x > s1.x) {
			found = testPossible(x,y);
			long answer = x;
			answer *= 4000000;
			answer += y;
			if (found) System.out.format("Part 2 answer = %d, %d, enter %d%n", x, y, answer);
			x--;
			y += step;
		}
		return found;
	}
	
	private static boolean testPossible(int x, int y) {
			boolean inRange = false;
			for (Sensor sensor : sensors) {
				int distance = Math.abs(x - sensor.x) + Math.abs(y - sensor.y);
				if (distance <= sensor.distance) inRange = true;
			}
			return !inRange; // Not in range of any sensor!!
	}

	private static void findExtremes() {
		for (Sensor sensor : sensors) {
			if (sensor.x < minX) minX = sensor.x;
			if (sensor.y < minY) minY = sensor.y;
			if (sensor.x > maxX) maxX = sensor.x;
			if (sensor.y > maxY) maxY = sensor.y;
		}
		for (Beacon beacon : beacons) {
			if (beacon.x < minX) minX = beacon.x;
			if (beacon.y < minY) minY = beacon.y;
			if (beacon.x > maxX) maxX = beacon.x;
			if (beacon.y > maxY) maxY = beacon.y;
		}
		if (minX < 0) minX = 0;
		if (maxX > 4000000) maxX = 4000000;
		if (minY < 0) minY = 0;
		if (maxY > 4000000) maxY = 4000000;
		System.out.format("X from %d to %d and Y from %d to %d%n", minX, maxX, minY, maxY);
	}

	private static void createSensors() {
		for (String line : lines) {
			String[] parts = line.split(" at ");
			String[] coords = parts[1].split(":")[0].split(", ");
			int x = Integer.parseInt(coords[0].substring(2));
			int y = Integer.parseInt(coords[1].substring(2));

			coords = parts[2].split(", ");
			int bx = Integer.parseInt(coords[0].substring(2));
			int by = Integer.parseInt(coords[1].substring(2));
			Beacon b = new Beacon(bx, by);
			if (!beacons.contains(b)) beacons.add(b);

			int distance = Math.abs(x - bx) + Math.abs(y - by);

			sensors.add(new Sensor(x, y, distance));
		}
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}