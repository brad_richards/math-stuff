package AdventOfCode.aoc_2018.day01;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Solved using simple, imperative programming...
 */
public class Problem02 {
	private static ArrayList<Integer> inputValues = new ArrayList<>();
	private static TreeSet<Integer> frequencies = new TreeSet<>();
	
	public static void main(String[] args) {
		readInputValues();
		assert(sumOfValues(inputValues) == 479); // Verify that values are read correctly
		
		// Begin at frequency 0
		int frequency = 0;
		frequencies.add(frequency);
		boolean found = false;
		while (!found) {
			for (int value : inputValues) {
				frequency += value;
				if (frequencies.contains(frequency)) {
					found = true;
					break;
				} else {
					frequencies.add(frequency);
				}
			}
		}
		System.out.println("First frequency duplicated is " + frequency);
	}

	private static void readInputValues() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis) ) {
			while (in.hasNext()) {
				inputValues.add(in.nextInt());
			}
		}
	}

	private static int sumOfValues(ArrayList<Integer> list) {
		int sum = 0;
		for (int value : list) sum += value;
		return sum;
	}
}
