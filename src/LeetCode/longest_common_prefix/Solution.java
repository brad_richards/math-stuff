package LeetCode.longest_common_prefix;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.longestCommonPrefix(new String[]{"", "asdf", "asdf"}));
    }

    public String longestCommonPrefix(String[] strs) {
        int firstMismatch = 0;
        boolean allMatch = true;
        while (allMatch && firstMismatch < strs[0].length()) {
            for (int i = 1; allMatch && i < strs.length; i++) {
                if (firstMismatch >= strs[i].length() || strs[i].charAt(firstMismatch) != strs[0].charAt(firstMismatch)) allMatch = false;
            }
            if (allMatch) firstMismatch++;
        }
        return strs[0].substring(0, firstMismatch);
    }
}