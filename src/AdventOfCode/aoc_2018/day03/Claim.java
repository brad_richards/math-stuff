package AdventOfCode.aoc_2018.day03;

public class Claim {
	private int id;
	private int x;
	private int y;
	private int width;
	private int height;
	
	private Claim(int id, int x, int y, int width, int height) {
		this.id = id; this.x = x; this.y = y; this.width = width; this.height = height;
	}
	
	/**
	 * Factory method to parse a single claim
	 */
	public static Claim parseClaim(String value) {
		Claim claim = null;
		try {
			if (value.startsWith("#")) {
				String[] parts = value.substring(1).split("@");
				int id = Integer.parseInt(parts[0].trim());
				parts = parts[1].split(":");
				String[] positions = parts[0].split(",");
				int x = Integer.parseInt(positions[0].trim());
				int y = Integer.parseInt(positions[1].trim());
				String[] dimensions = parts[1].split("x");
				int width = Integer.parseInt(dimensions[0].trim());
				int height = Integer.parseInt(dimensions[1].trim());
				claim = new Claim(id, x, y, width, height);
			}
		} catch (Exception e) {
			// Any exception just means failure
		}
		return claim;
	}
	
	public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}