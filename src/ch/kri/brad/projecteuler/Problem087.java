package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.TreeSet;

import ch.kri.brad.utility.Primes;

public class Problem087 {

	public static void main(String[] args) {
		final int LIMIT = 50000000;
		TreeSet<Long> nums = new TreeSet<>();
		
		Primes.generatePrimesErastothenes(100 + (int) Math.sqrt(LIMIT));
		ArrayList<Long> primes = Primes.getPrimes();
		
		long sumX = 0;
		for (int x = 0; sumX <= LIMIT; x++) {
			long primeX = primes.get(x);
			sumX = primeX * primeX;
			long sumY = 0;
			for (int y = 0; sumY <= LIMIT; y++) {
				long primeY = primes.get(y);
				sumY = sumX + primeY * primeY * primeY;
				long sumZ = 0;
				for (int z = 0; sumZ <= LIMIT; z++) {
					long primeZ = primes.get(z);
					sumZ = sumY + primeZ * primeZ * primeZ * primeZ;
					if (sumZ < LIMIT) {
						nums.add(sumZ);
						//System.out.println(primeX + ", " + primeY + ", " + primeZ + " = " + sumZ);
					}
				}
			}			
		}
		
		System.out.println(nums.size());
	}
}
