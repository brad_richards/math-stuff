package kattis.f_arrays;

import java.util.Arrays;
import java.util.Scanner;

public class pokerhand {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		char[] ranks = new char[5];
		for (int i = 0; i < 5; i++) {
			String card = in.next();
			ranks[i] = card.charAt(0);
		}
		
		Arrays.sort(ranks);
		char current = '-';
		int currentCount = 0;
		int maxCount = 0;
		for (int i = 0; i < 5; i++) {
			if (ranks[i] == current) {
				currentCount++;
			} else {
				current = ranks[i];
				currentCount = 1;
			}
			if (currentCount > maxCount) maxCount = currentCount;
		}
		System.out.println(maxCount);
	}

}
