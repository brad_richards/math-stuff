package ch.kri.brad.projecteuler;

/**
 * For a single step (say "D"), every 3rd number meets the requirement of n%3=0.
 * For two steps, every 9th number, n%9=6 For three steps, every 27th number,
 * n%27 = 15 The sequence continues. The 10 modulo values are: 0, 6, 15, 69,
 * 231, 231, 231, 231, 231, 231
 * 
 * So the answer for the sample is first number (greater than the threshold)
 * where n % 3*10 = 231
 * 
 * Given 231, we look for the first multiple (mult) of 3^10 such that mult*3^10+231 > 1000000
 */
public class Problem277 {
	private static String operations = "UDDDUdddDDUDDddDdDddDDUDDdUUDd";
	private static long Threshold = 1;
	private static long Increment = 1;
	private static long MinValue = 1000000000000000l;

	public static void main(String[] args) {

		// Find the lowest value that requires the given sequence of operations
		char[] opChars = operations.toCharArray();
		for (int pos = 0; pos < opChars.length; pos++) { // for all operations
			char opChar = opChars[pos]; // Next operation
			long modValue = modFromChar(opChar); // Modulo value of next op

			// Carry out previous operations; check whether the modulo value
			// corresponds to the next character in the sequence
			while (previousOps(pos, Threshold) % 3 != modValue)
				Threshold += Increment;
			Increment *= 3;
			System.out.println("Lowest value for " + Increment + " is " + Threshold);
		}
		System.out.println("Lowest value for the given sequence = " + Threshold);

		// Now find the lowest value above the given minimum value
		long multiple = MinValue / Increment;
		long answer = multiple * Increment + Threshold;
		if (answer < MinValue)
			answer += Increment;

		System.out.println("Answer = " + answer);
	}

	/**
	 * Translate from characters to module values
	 */
	private static long modFromChar(char c) {
		if (c == 'D')
			return 0;
		if (c == 'U')
			return 1;
		else
			return 2; // == 'd'
	}

	/**
	 * Perform a given number of operations on a number. While these operations
	 * are given by the first "numSteps" characters, we can also perform them
	 * directly, relying on the modulo values.
	 */
	private static long previousOps(int numSteps, long in) {
		for (int i = 0; i < numSteps; i++) {
			in = step(in);
		}
		return in;
	}

	/**
	 * Single step in the modified Collatz sequence.
	 */
	public static long step(long n) {
		long modValue = n % 3;
		if (modValue == 0)
			return n / 3; // D
		if (modValue == 1)
			return (4 * n + 2) / 3; // U
		else
			return (2 * n - 1) / 3; // d
	}
}
