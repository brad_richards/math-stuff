package ch.kri.brad.projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;

public class Problem068 {
	private static final int MAX_VALUE = 10;
	private static final int ringSize = MAX_VALUE / 2;

	public static void main(String[] args) {
		ArrayList<Ring> rings = generateInnerRings();
		
		// Convert values into BigIntegers, so that we can look for the maximum
		BigInteger maxValue = BigInteger.ZERO;
		for (Ring r : rings) {
			String sValue = r.toString();
			System.out.println(sValue);
			BigInteger value = new BigInteger(sValue);
			if (value.compareTo(maxValue) > 0) maxValue = value;
		}
		System.out.println("Maximum is: " + maxValue);
	}

	private static ArrayList<Ring> generateInnerRings() {
		ArrayList<Ring> rings = new ArrayList<>();
		Ring ring = new Ring();

		// Reset values, then choose first value
		// We exclude MAX_VALUE, as it must be on the outer ring
		for (int firstValue = 1; firstValue < MAX_VALUE; firstValue++) {
			ring.values[0] = firstValue;

			// Recurse, to choose all remaining values
			chooseRemainingValues(rings, ring, firstValue, 1);
		}
		return rings;
	}

	/**
	 * To avoid redundant rings, we are not allowed to choose values <= firstValue.
	 */
	private static void chooseRemainingValues(ArrayList<Ring> rings, Ring innerRing, int firstValue,
			int pos) {
		if (pos == ringSize - 1) {
			chooseFinalValue(rings, innerRing, firstValue);
		} else {
			// Generate all possible values, recursing for each
			for (int i = firstValue + 1; i < MAX_VALUE; i++) {
				if (!contains(innerRing.values, i)) {
					Ring newRing = innerRing.clone();
					newRing.values[pos] = i;
					chooseRemainingValues(rings, newRing, firstValue, pos + 1);
				}
			}
		}
	}

	private static boolean contains(int[] a, int v) {
		boolean found = false;
		for (int e : a) {
			if (e == v) {
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * To avoid redundant rings, we are not allowed to choose values <= firstValue.
	 * 
	 * We generate all possible last values. For all cases where this results in a valid inner ring,
	 * we add a ring to the collection
	 */
	private static void chooseFinalValue(ArrayList<Ring> rings, Ring innerRing, int firstValue) {
		Ring outerRing = new Ring();

		// Generate all possible values, recursing for each
		for (int i = firstValue + 1; i < MAX_VALUE; i++) {
			if (!contains(innerRing.values, i)) {
				innerRing.values[ringSize - 1] = i;
				innerRing.updateSums();

				// Sum % RingSize must be zero
				if (innerRing.ringSum % ringSize != 0) continue; // Skip this ring

				// Calculate values required for the outer ring. Each value must be (a) available
				// and (b) in the range 1-MAX_VALUE
				boolean outerValuesOk = true;
				for (int j = 0; j < ringSize && outerValuesOk; j++) {
					int pairSum;
					if (j == (ringSize - 1)) { // Last value + first value
						pairSum = innerRing.values[j] + innerRing.values[0];
					} else {
						pairSum = innerRing.values[j] + innerRing.values[j + 1];
					}

					int outerValue = innerRing.lineSum - pairSum;

					if (outerValue >= 1 && outerValue <= MAX_VALUE
							&& !contains(innerRing.values, outerValue)) {
						outerRing.values[j] = outerValue;
					} else {
						outerValuesOk = false;
					}
				}

				if (outerValuesOk) {
					// outer values must be all different - if they are, we have a valid inner ring
					// We also note which value is the lowest (see next code section)
					boolean allDifferent = true;
					int lowestVal = 999999;
					int lowestPos = -1;
					for (int k = 0; k < ringSize - 1 && allDifferent; k++) {
						if (outerRing.values[k] < lowestVal) {
							lowestPos = k;
							lowestVal = outerRing.values[k]; 
						}
						for (int j = k+1; j < ringSize && allDifferent; j++) {
							if (outerRing.values[k] == outerRing.values[j]) allDifferent = false;
						}
					}

					if (allDifferent) {
						// We have a solution.
						Ring newRing = innerRing.clone();
						newRing.updateSums();

						// However, for the solution to the final problem, we
						// want to rotate the ring so that the lowest value in the outer ring comes
						// first. The value of "lowestPos" tells us how many steps to rotate-left.
						// Since the size of the ring is a prime number, we can rotate in one loop.
						int tmp = newRing.values[0];
						int rotateTo = 0;
						int rotateFrom = lowestPos;
						// Shift all Elements
						for (int k = 0; k < ringSize - 1; k++) {
							newRing.values[rotateTo] = newRing.values[rotateFrom];
							rotateTo = rotateFrom;
							rotateFrom = (rotateFrom + lowestPos) % ringSize;
						}
						newRing.values[rotateTo] = tmp;

						// Store the solution
						rings.add(newRing);
					}
				}
			}
		}
	}

	private static class Ring {
		int ringSum;
		int lineSum;
		int[] values;

		public Ring() {
			values = new int[ringSize];
			for (int i = 0; i < ringSize; i++)
				values[i] = 0;
		}

		/**
		 * Must be called explicitly, to update the ringSum and lineSum
		 */
		public void updateSums() {
			int sum = 0;
			for (int v : values)
				sum += v;
			ringSum = sum;

			lineSum = (MAX_VALUE * (MAX_VALUE + 1) / 2 + ringSum) / ringSize;
		}

		@Override
		public Ring clone() {
			Ring clone = new Ring();
			for (int i = 0; i < ringSize; i++)
				clone.values[i] = values[i];
			return clone;
		}

		@Override
		public String toString() {
			StringBuffer s = new StringBuffer();
			for (int i = 0; i < (ringSize - 1); i++) {
				int outerValue = lineSum - values[i] - values[i + 1];
				s.append(outerValue);
				s.append(values[i]);
				s.append(values[i + 1]);
			}
			int outerValue = lineSum - values[0] - values[ringSize - 1];
			s.append(outerValue);
			s.append(values[ringSize - 1]);
			s.append(values[0]);

			return s.toString();
		}
	}
}
