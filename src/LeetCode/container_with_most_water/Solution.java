package LeetCode.container_with_most_water;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    private static int[] test1 = {1,1};
    private static int[] test2 = {1,8,6,2,5,4,8,3,7};
    private static int[] test3 = {1,2,4,3};
    private static int[] test4 = {1,3,2,5,25,24,5};
    public static void main(String[] ars) throws FileNotFoundException {
        Solution s = new Solution();
        System.out.println(s.maxArea(test1));
        System.out.println(s.maxArea(test2));
        System.out.println(s.maxArea(test3));
        System.out.println(s.maxArea(test4));

        Scanner in = new Scanner(new File("container_with_most_water.txt"));
        ArrayList<Integer> list = new ArrayList<>();
        while (in.hasNextInt()) {
            list.add(in.nextInt());
        }
        in.close();
        int[] test3 = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {test3[i] = list.get(i);}

        long t1 = System.currentTimeMillis();
        System.out.println(s.maxArea(test3));
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);
    }

    public int maxArea(int[] height) {
        int p1 = 0;
        int p2 = height.length - 1;
        int maxArea = (p2 - p1) * Math.min(height[p1], height[p2]);
        while (p1 < p2) {
            int newP1 = p1+1;
            int newP2 = p2-1;
            int m1 = (p2 - newP1) * Math.min(height[newP1], height[p2]);
            int m2 = (newP2 - p1) * Math.min(height[p1], height[newP2]);
            if (m1 > m2 && m1 > maxArea) {
                p1 = newP1;
                maxArea = m1;
            } else if (m2 > m1 && m2 > maxArea) {
                p2 = newP2;
                maxArea = m2;
            } else if (height[p1+1] < height[p1]) {
                p1++;
            } else if (height[newP1] < height[newP2]) {
                p1++;
            } else {
                p2--;
            }
        }
        return maxArea;
    }


    public int maxArea2(int[] height) {
        int maxArea = 0;
        for (int i = 0; i < height.length - 1; i++) {
            for (int j = i+1; j < height.length; j++) {
                int tmpArea = (j - i) * Math.min(height[i], height[j]);
                if (tmpArea > maxArea) maxArea = tmpArea;
            }
        }
        return maxArea;
    }
}
