package hackerRank.projectEuler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Problem004 {
	static ArrayList<Long> palindromes = new ArrayList<Long>();

	public static void main(String[] args) {
		long[] testCases = readData();
		
		findAllPalindromes();
		Collections.sort(palindromes);
		
		for (long value : testCases) 
			System.out.println(doOneCase(value));
	}

	public static long[] readData() {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		long[] data = new long[size];
		for (int i = 0; i < size; i++) {
			data[i] = s.nextLong();
		}
		s.close();
		return data;
	}

	public static long doOneCase(long limit) {
		long answer = 0;
		for (long p : palindromes) {
			if (p >= limit) break;
			if (p > answer) answer = p;
		}
		return answer;
	}
	
	public static void findAllPalindromes() {
		for (long i = 100; i <= 999; i++) {
			for (long j = i; j <= 999; j++) {
				long product = i * j;
				if (isPalindrome(product)) {
					palindromes.add(product);
				}
			}
		}
	}

	private static boolean isPalindrome(long val) {
		return (val == reversedNum(val));
	}

	private static long reversedNum(long valIn) {
		long valReversed = 0;
		while (valIn > 0) {
			long lastDigit = valIn % 10;
			valIn = valIn / 10;
			valReversed = valReversed * 10 + lastDigit;
		}
		return valReversed;
	}
	
}
