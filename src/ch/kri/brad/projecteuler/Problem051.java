package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.Primes;



public class Problem051 {
	static boolean[] rawSieve;
	static ArrayList<ArrayList<boolean[]>> patterns;
	static final int maxPrime = 999999;
	static final int primesWanted = 8;

	public static void main(String[] args) {
		Primes.generatePrimesErastothenes(maxPrime, true);
		rawSieve = Primes.getRawSieve();
		
		// Generate all possible patterns of digits to replace,
		// for up to 6 digits. A pattern is a boolean array whose
		// entries correspond to digits (true = replace). Patterns
		// for a particular length are stored in an ArrayList;
		// we have an ArrayList of such ArrayLists containing one
		// for each possible number of digits (1 to n)
		patterns = generatePatterns(Digits.numDigits(maxPrime));
		
		// Search for the first prime to meet the criteria
		boolean done = false;
		long prime = 0;
		for (int pos = 0; !done & pos < maxPrime-2; pos++) {
			if (rawSieve[pos]) {
				prime = pos+2;
				done = testPrime(prime);
			}
		}
		
		// Print answer
		System.out.println("Answer = " + prime);
	}
	
	static boolean testPrime(long prime) {
		int numDigits = Digits.numDigits(prime);
		ArrayList<boolean[]> patternSet = patterns.get(numDigits-1);
		int[] digits = Digits.toDigits(prime);

		for (boolean[] pattern : patternSet) {
			// Only proceed if all marked digits are, in fact, the same
			int digitToReplace = digitsSame(pattern, digits);
			if (digitToReplace != -1) {
				ArrayList<Long> primesFound = new ArrayList<Long>();
				primesFound.add(prime);
				for (int digit = digitToReplace+1; digit <= 9; digit++) {
					int temp = (int) replaceDigits(prime, pattern, digit);
					if (rawSieve[temp-2]) {
						primesFound.add(new Long(temp));
					}
				}
				if (primesFound.size() >= primesWanted) {
					printPattern(pattern);
					for (Long primeFound : primesFound) System.out.println(primeFound);
					return true; // ugly, diving out in the middle...
				}
			}
		}
		return false;
	}
	
	/**
	 * Inspects the digits in a prime that are marked for replacement in
	 * the pattern. Returns the value of the digit, if all digits have the
	 * same value. Otherwise returns -1. Special case: if no digits are
	 * marked in the pattern, returns -1
	 * 
	 * @return
	 */
	static int digitsSame(boolean[] pattern, int[] digits) {
		int digit = -1;
		boolean allOk = false;
		for (int i = 0; i < pattern.length; i++) {
			if (pattern[i]) {
				if (digit == -1) {
					digit = digits[i];
					allOk = true;
				} else {
					if (digits[i] != digit) {
						allOk = false;
						break;
					}
				}
			}
		}
		if (allOk == false) digit = -1;
		return digit;
	}
	
	static long replaceDigits(long prime, boolean[] pattern, int newDigit) {
		int[] digits = Digits.toDigits(prime);
		for (int i = 0; i < pattern.length; i++) {
			if (pattern[i]) digits[i] = newDigit;
		}
		return Digits.fromDigits(digits);
	}

	static ArrayList<ArrayList<boolean[]>> generatePatterns(int maxDigits) {
		ArrayList<ArrayList<boolean[]>> patterns = new ArrayList<ArrayList<boolean[]>>();
		for (int i = 1; i <= maxDigits; i++) {
			patterns.add(generatePatternSet(i));
		}		
		return patterns;
	}
	
	static ArrayList<boolean[]> generatePatternSet(int numDigits) {
		ArrayList<boolean[]> patternSet = new ArrayList<boolean[]>();
		generatePatterns(patternSet, new boolean[numDigits], 0, numDigits);
		return patternSet;
	}
	
	/**
	 * Recursive method to create patterns. Note that thisDigit is 0-based
	 */
	static void generatePatterns(ArrayList<boolean[]> patternSet, boolean[] pattern, int thisDigit, int numDigits) {
		// Base case - we are at the last digit, which is never replaced
		if (thisDigit >= (numDigits-1)) {
			pattern[thisDigit] = false;
			patternSet.add(Arrays.copyOf(pattern, numDigits));
		} else {
			int nextDigit = thisDigit+1;
			pattern[thisDigit] = true;
			generatePatterns(patternSet, pattern, nextDigit, numDigits);
			pattern[thisDigit] = false;
			generatePatterns(patternSet, pattern, nextDigit, numDigits);
		}
	}
	
	static void printPattern(boolean[] pattern) {
		for (boolean b : pattern) {
			if (b) System.out.print(1); else System.out.print(0);
		}
		System.out.println();
	}
}
