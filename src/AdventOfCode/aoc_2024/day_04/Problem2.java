package AdventOfCode.aoc_2024.day_04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Look for the A's, then see if the four corners make sense
public class Problem2 {
    static List<String> lines;

    public static void main(String[] args) throws Exception {
        readInput();
        char[][] chars = new char[lines.size()][];
        for (int i = 0; i < lines.size(); i++) {
            chars[i] = lines.get(i).toCharArray();
        }

        int sum = 0;
        for (int row = 1; row < chars.length-1; row++) {
            for (int col = 1; col < chars[row].length-1; col++) {
                if (chars[row][col] == 'A')
                    if (isXmas(chars, row, col)) sum++;
            }
        }
        System.out.println(sum);
    }

    // There are 8 possibilities. We just create a little method for each of them.
    // We love WET code
    private static boolean isXmas(char[][] chars, int row, int col) {
        return ( ( (chars[row-1][col-1] == 'M' && chars[row+1][col+1] == 'S') || (chars[row-1][col-1] == 'S' && chars[row+1][col+1] == 'M') )
                 &&
                 ( (chars[row-1][col+1] == 'M' && chars[row+1][col-1] == 'S') || (chars[row-1][col+1] == 'S' && chars[row+1][col-1] == 'M') )
               );
    }

    private static void readInput() throws IOException {
        Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
        try (Stream<String> stream = Files.lines(path)) {
            lines = stream.collect(Collectors.toList());
        }
    }
}