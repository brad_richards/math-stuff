package AdventOfCode.aoc_2024.day_07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<Equation> equations;

	public static void main(String[] args) throws Exception {
		readInput();
		long sum = 0;
		for (Equation equation : equations) {
			if (equation.search()) {
				System.out.println(equation);
				sum+= equation.getAnswer();
			}
		}
		System.out.println("Part 1: " + sum);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			equations = stream.map(s -> toEquation(s)).collect(Collectors.toList());
		}
	}

	private static Equation toEquation(String line) {
		String[] parts = line.split(": ");
		Equation e = new Equation(Long.parseLong(parts[0]));
		parts = parts[1].split(" ");
		long[] values = new long[parts.length];
		for (int i = 0; i < parts.length; i++) {
			values[i] = Long.parseLong(parts[i]);
		}
		e.setOperands(values);
		return e;
	}
}