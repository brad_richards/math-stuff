package AdventOfCode.utility;

public record Point(int row, int col) {
}
