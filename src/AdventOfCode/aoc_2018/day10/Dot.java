package AdventOfCode.aoc_2018.day10;

public class Dot {
	int x, y; // Position
	int vx, vy; // Velocity
	
	public Dot(int x, int y, int vx, int vy) {
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
	}
}
