package AdventOfCode.aoc_2021.day03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem02 {
	private static List<String> numbers;

	public static void main(String[] args) throws IOException {
		readInput();

		String oxygen = filter(numbers, true);
		String co2 = filter(numbers, false);

		System.out.println(Integer.parseInt(oxygen, 2) * Integer.parseInt(co2, 2));
	}

	/**
	 * Prepare to recursively filter the input numbers, until only one remains.
	 * 
	 * @param numbers
	 *            The list of numbers
	 * @param mostCommon
	 *            True if we keep numbers with the most common bit value, false
	 *            if we keep numbers with the least common bit value
	 * @return The one number remaining after filtering
	 */
	private static String filter(List<String> numbers, boolean mostCommon) {
		List<String> result = filter(numbers, mostCommon, 0);
		return result.get(0);
	}
	
	private static List<String> filter(List<String> numbers, boolean mostCommon, int bit) {
		if (numbers.size() == 1)
			return numbers;
		else {
			int numOnes = countBits(numbers, bit);
			char matchBit;
			boolean matchOne = 2*numOnes >= numbers.size();
			if (matchOne && mostCommon || !matchOne && !mostCommon) {
				matchBit = '1';
			} else {
				matchBit = '0';
			}
			List<String> result = new ArrayList<>();
			for (String s : numbers) {
				if (s.charAt(bit) == matchBit) result.add(s);
			}
			//printResults(result);
			return filter(result, mostCommon, (bit+1));
		}
	}
	
	
	private static int countBits(List<String> numbers, int bit) {
		int count = 0;
		for (String s : numbers) {
			if (s.charAt(bit) == '1') count++;
		}
		return count;
	}

	private static void printResults(List<String> result) {
		for (String s : result)
			System.out.println(s);
		System.out.println();
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			numbers = stream.collect(Collectors.toList());
		}
	}
}
