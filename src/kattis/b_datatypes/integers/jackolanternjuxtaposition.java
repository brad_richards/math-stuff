package kattis.b_datatypes.integers;

import java.util.Scanner;

public class jackolanternjuxtaposition {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int eye = in.nextInt();
		int nose = in.nextInt();
		int mouth = in.nextInt();
		System.out.println(eye * nose * mouth);
	}

}
