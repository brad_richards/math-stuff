package AdventOfCode.aoc_2022.day06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	private static final int numChars = 14; // 4 for part 1, 14 for part 2
	static List<String> lines;
	
	public static void main(String[] args) throws Exception {
		readInput();
		
		for (String line : lines) {
			int answer = 0;
			
			char[] chars = line.toCharArray();
			boolean dupFound = true;
			for (int i = 0; dupFound && i < line.length()+1-numChars; i++) {
				dupFound = isDup(chars, i);
				if (!dupFound) answer = i+numChars;
			}
			System.out.println(answer);
		}
	}
	
	private static boolean isDup(char[] chars, int pos) {
		boolean dup = false;
		for (int i = pos; i <= pos+numChars-2; i++) {
			for (int j = i+1 ; j <= pos+numChars-1; j++) {
				dup |= chars[i]==chars[j];
			}
		}
		return dup;
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}
