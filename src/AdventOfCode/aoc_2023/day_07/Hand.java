package AdventOfCode.aoc_2023.day_07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Hand implements Comparable<Hand> {
	public enum Card { TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };
	public enum HandType { HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND, FULL_HOUSE, FOUR_OF_A_KIND, FIVE_OF_A_KIND }
	
	Card[] cards = new Card[5];
	HandType handType;
	long bid;
	
	public Hand(String line) {
		String[] parts = line.split(" ");
		bid = Long.parseLong(parts[1]);
		
		for (int i = 0; i < 5; i++) {
			char c = parts[0].charAt(i);
			switch (c) {
			case '2' -> cards[i] = Card.TWO;
			case '3' -> cards[i] = Card.THREE;
			case '4' -> cards[i] = Card.FOUR;
			case '5' -> cards[i] = Card.FIVE;
			case '6' -> cards[i] = Card.SIX;
			case '7' -> cards[i] = Card.SEVEN;
			case '8' -> cards[i] = Card.EIGHT;
			case '9' -> cards[i] = Card.NINE;
			case 'T' -> cards[i] = Card.TEN;
			case 'J' -> cards[i] = Card.JACK;
			case 'Q' -> cards[i] = Card.QUEEN;
			case 'K' -> cards[i] = Card.KING;
			case 'A' -> cards[i] = Card.ACE;
			}
		}
		
		handType = getHandType();
	}
	
	/**
	 * Ordering from weakest to strongest
	 */
	@Override
	public int compareTo(Hand h) {
		int result = this.handType.ordinal() - h.handType.ordinal();
		for (int i = 0; result == 0 && i < 5; i++) {
			result = this.cards[i].ordinal() - h.cards[i].ordinal();
		}
		return result;
	}
	
	private HandType getHandType() {
		List<Integer> cardCounts = getCardCounts();
		if (cardCounts.get(0) == 5) return HandType.FIVE_OF_A_KIND;
		if (cardCounts.get(0) == 4) return HandType.FOUR_OF_A_KIND;
		if (cardCounts.get(0) == 3 && cardCounts.get(1) == 2) return HandType.FULL_HOUSE;
		if (cardCounts.get(0) == 3) return HandType.THREE_OF_A_KIND;
		if (cardCounts.get(0) == 2 && cardCounts.get(1) == 2) return HandType.TWO_PAIR;
		if (cardCounts.get(0) == 2) return HandType.ONE_PAIR;
		return HandType.HIGH_CARD;
	}
	
	private List<Integer> getCardCounts() {
		List<Integer> counts = new ArrayList<>();
		List<Card> cardList = new ArrayList<>();
		for (Card c : cards) cardList.add(c);
		while (!cardList.isEmpty()) {
			Card card = cardList.remove(0);
			int count = 1;
			for (Iterator<Card> i = cardList.iterator(); i.hasNext();) {
				Card c = i.next();
				if (c.equals(card)) {
					i.remove();
					count++;
				}
			}
			counts.add(count);
		}
		Collections.sort(counts, Collections.reverseOrder());
		return counts;
	}
}
