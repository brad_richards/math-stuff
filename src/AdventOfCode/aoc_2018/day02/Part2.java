package AdventOfCode.aoc_2018.day02;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This second part seems to have no relationship to the checksum calculated in the first part.
 * There is no (stated) guarantee that the differing letter is, or is not, one of the pairs or
 * triplets.
 * 
 * We note that (a) all inputs have exactly the same length, and (by implication) only two boxes
 * will meet the stated criteria, so we stop as soon as we have found a valid pair.
 */
public class Part2 {
	private static ArrayList<String> inputValues = new ArrayList<>();

	public static void main(String[] args) {
		readInputs();

		boolean found = false;
		for (int i = 0; !found && i < inputValues.size() - 1; i++) {
			for (int j = i + 1; !found && j < inputValues.size(); j++) {
				if (differByOneLetter(inputValues.get(i), inputValues.get(j))) {
					found = true;
					String commonLetters = commonLetters(inputValues.get(i),
							inputValues.get(j));
					System.out.println(commonLetters);
				}
			}
		}
	}

	private static void readInputs() {
		InputStream fis = Part1.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				inputValues.add(in.nextLine());
			}
		}
	}

	private static boolean differByOneLetter(String s1, String s2) {
		boolean oneDifferenceFound = false;
		
		for (int i = 0; i < s1.length(); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				oneDifferenceFound = !oneDifferenceFound;
				if (!oneDifferenceFound) break; // second difference
			}
		}
		return oneDifferenceFound;
	}
	
	public static String commonLetters(String s1, String s2) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < s1.length(); i++) {
			if (s1.charAt(i) == s2.charAt(i)) {
				buf.append(s1.charAt(i));				
			}
		}
		return buf.toString();
	}
}
