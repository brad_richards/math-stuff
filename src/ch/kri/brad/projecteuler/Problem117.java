package ch.kri.brad.projecteuler;

/**
 * Similar to problems 114, 115 and 116. In order to solve this problem for _unrestricted_  block size:
 * 
 * Let m be the minimum block size, and n the length of the row. We can calculate the number of
 * arrangements for various numbers of blocks
 * 
 * - For 1 block: ((n-m) + 2)! / [(n-m)! * 2!]
 * 
 * - For 2 blocks: ((n-m) + 2)! / [((n-m) - 2)! * 4!]
 * 
 * - For 3 blocks: ((n-m) + 2)! / [((n-m) - 4)! * 6!]
 * 
 * and so forth, up to the maximum number of blocks, which is (n+1)/4. Finally, we add one to
 * account for a completely empty row.
 * 
 * Unfortunately, we are only allowed blocks up to size 5...
 */
public class Problem117 {
	public static final int m = 2;
	public static final int n = 5;

	public static void main(String[] args) {
		long sum = 0;
		for (long nBlocks = 1; nBlocks <= (n + 1) / (m + 1); nBlocks++) {
			long start = n - m - 2 * (nBlocks-1);
			long product = 1;
			for (long i = 1; i <= nBlocks * 2; i++) {
				System.out.print((start + i) + " ");
				product *= (start + i);
				product /= i;
			}
			System.out.println();
			sum += product;
		}
		sum += 1;
		System.out.println("for n = " + n + " the total is " + sum);
	}
}
