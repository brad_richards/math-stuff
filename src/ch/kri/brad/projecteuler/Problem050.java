package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Iterator;

import ch.kri.brad.utility.Primes;



public class Problem050 {
	static ArrayList<Long> primes;
	static boolean[] primeSieve;
	static final int maxValue = 1000000;
	
	public static void main(String[] args) {
		Primes.generatePrimesErastothenes(maxValue, false);
		primes = Primes.getPrimes();
		primeSieve = Primes.getRawSieve();
		int numPrimes = primes.size();
		
		int bestConsecutive = 1;
		long bestPrime = 2;

		for (int start = 0; start < numPrimes; start++) {
			long sum = primes.get(start);
			int numConsecutive = 1;
			for (int next = start+1; next < numPrimes && sum < maxValue; next++) {
				sum += primes.get(next);
				numConsecutive++;
				if (sum < maxValue && primeSieve[(int) sum - 2] && numConsecutive > bestConsecutive) {
					bestConsecutive = numConsecutive;
					bestPrime = sum;
				}
			}			
		}
		
		System.out.println("Prime " + bestPrime + " is the sum of " + bestConsecutive + " consecutive primes");
	}

}
