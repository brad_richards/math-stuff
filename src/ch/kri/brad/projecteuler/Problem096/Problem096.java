package ch.kri.brad.projecteuler.Problem096;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Lots of code stolen from the solver in FreeSudoku (which I mostly also wrote, so...)
 * 
 * Wow, how does this only have a difficulty of 25% ?? Some of the Sudokus are hard!
 */
public class Problem096 {
	private static String INPUT_FILE = "p096_sudoku.txt";

	public static void main(String[] args) {
		try {
			int[][][] puzzles = readPuzzles();
			solvePuzzles(puzzles);
			int answer = sumKeys(puzzles);
			System.out.println("\nThe answer is " + answer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Solve all puzzles
	 * 
	 * @throws Exception
	 */
	private static void solvePuzzles(int[][][] puzzles) throws Exception {
		for (int i = 0; i < puzzles.length; i++) {
			System.out.print(solvePuzzle(puzzles[i]) ? "+" : "-");
		}
	}

	/**
	 * To aid in solving the puzzle, we create a new representation of the puzzle, where each cell
	 * contains a list of possible values. This is an int array with positions 0-9: position 0
	 * contain the number of possible values, and each of the other positions is either 1 (true,
	 * possible), or 0 (false, not possible).
	 * 
	 * Solve a single puzzle, using ever more advanced strategies. For each iteration, we try
	 * strategies until one of them successfully sets a number. Then we repeat. This way, we will
	 * primarily use the simple strategies.
	 * 
	 * At the end, we update the incoming puzzle to reflect our work, whether or not it was
	 * completely solved.
	 * 
	 * returns true if puzzle is solved
	 */
	private static boolean solvePuzzle(int[][] puzzle) throws Exception {
		boolean progress = true;

//		printPuzzle(puzzle);
		int[][][] puzz = transform(puzzle);
		
		Strategies.init(puzz);

		while (!solved(puzz) && progress) {
			progress = Strategies.strategy0(puzz) || Strategies.strategy1(puzz)
					|| Strategies.strategy2(puzz) || Strategies.strategy3(puzz)
					|| Strategies.strategy4(puzz) || Strategies.strategy5(puzz)
					|| Strategies.strategy6(puzz) || Strategies.strategy7(puzz)
					|| Strategies.strategy8(puzz) || Strategies.strategy9(puzz);
		}

		saveSolution(puzz, puzzle);

//		printPuzzle(puzzle);
		return progress;
	}

	private static int[][][] transform(int[][] puzzle) {
		int[][][] puzz = new int[9][9][10];
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				if (puzzle[row][col] != 0) {
					int[] possVals = new int[] { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
					possVals[puzzle[row][col]] = 1;
					puzz[row][col] = possVals;
				} else {
					puzz[row][col] = new int[] { 9, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
				}
			}
		}
		return puzz;
	}

	private static void saveSolution(int[][][] puzz, int[][] puzzle) {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				if (puzz[row][col][0] != 1) {
					puzzle[row][col] = 0;
				} else {
					puzzle[row][col] = Utilities.getDefiniteVal(puzz[row][col]);
				}
			}
		}
	}

	/**
	 * A puzzle is solved if it contains no zeroes
	 */
	private static boolean solved(int[][][] puzzle) {
		boolean solved = true;
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				solved &= puzzle[row][col][0] == 1;
			}
		}
		return solved;
	}

	/**
	 * Add the numbers in the top-left corner of all given puzzles
	 */
	private static int sumKeys(int[][][] puzzles) {
		int sum = 0;
		for (int[][] puzzle : puzzles) {
			int key = puzzle[0][0] * 100 + puzzle[0][1] * 10 + puzzle[0][2];
			sum += key;
		}
		return sum;
	}

	/**
	 * Read 50 puzzles from the given file
	 * 
	 * @throws IOException
	 */
	private static int[][][] readPuzzles() throws IOException {
		int[][][] puzzles = new int[50][][];
		InputStream fis = Problem096.class.getResourceAsStream(INPUT_FILE);
		BufferedReader in = new BufferedReader(new InputStreamReader(fis));
		for (int p = 0; p < 50; p++)
			puzzles[p] = readPuzzle(in);
		return puzzles;
	}

	/**
	 * Read a puzzle in the expected format. Not much error tolerance...
	 */
	private static int[][] readPuzzle(BufferedReader in) throws IOException {
		int[][] puzzle = new int[9][9];
		String sIn = in.readLine();
		if (!sIn.subSequence(0, 4).equals("Grid")) throw new IOException("Incorrect header line");
		for (int row = 0; row < 9; row++) {
			sIn = in.readLine();
			if (sIn.length() != 9) throw new IOException("Incorrect line in puzzle");
			for (int col = 0; col < 9; col++)
				puzzle[row][col] = sIn.charAt(col) - '0';
		}
		return puzzle;
	}

	/**
	 * Print the given puzzle to the console
	 */
	private static void printPuzzle(int[][] puzzle) {
		System.out.println();
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				System.out.print(puzzle[row][col]);
			}
			System.out.println();
		}
		System.out.println();
	}
}
