package AdventOfCode.aoc_2020.day_12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	private List<Navigation> values;
	
	public InputReader(String fileName) throws IOException {
		Path path = Path.of(this.getClass().getResource(fileName).getPath());
		try (Stream<String> stream = Files.lines(path)) {
			values = stream.map(line -> new Navigation(line)).collect(Collectors.toList());
		}
	}
	
	public List<Navigation> getValues() {
		return values;
	}
}
