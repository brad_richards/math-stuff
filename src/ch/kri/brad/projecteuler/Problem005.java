package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import ch.kri.brad.utility.Primes;



public class Problem005 {

	/**
	 * Note: this is an odd problem - frankly, easier to solve by hand than by computer.
	 * Nonetheless, here is an attempt at automating it...
	 */
	public static void main(String[] args) {
		long topFactor = 20;
		HashMap<Long, Long> factorList = new HashMap<Long, Long>();
		
		// Collect all prime factors for the numbers from 2 through topFactor
		for (int i = 2; i <= topFactor; i++) {
			ArrayList<Long> primeFactors = Primes.primeFactors(i);
			
			// Convert the factor list to a HashMap
			HashMap<Long, Long> factorCounts = new HashMap<Long, Long>();
			for (long factor : primeFactors) {
				if (factorCounts.containsKey(factor)) {
					long oldCount = factorCounts.get(factor);
					factorCounts.put(factor, oldCount+1);					
				} else { // new factor found
					factorCounts.put(factor, new Long(1));
				}
			}
			
			// Update the master list of factors
			for (Iterator<Long> keys = factorCounts.keySet().iterator(); keys.hasNext(); ) {
				Long key = keys.next();
				if (factorList.containsKey(key)) {
					if (factorList.get(key) < factorCounts.get(key)) factorList.put(key, factorCounts.get(key));
				} else {
					factorList.put(key, factorCounts.get(key));
				}
			}
		}
		
		// Construct the answer by multiplying all the factors in the master list
		long answer = 1;
		for (Iterator<Long> keys = factorList.keySet().iterator(); keys.hasNext(); ) {
			long factor = keys.next();
			long number = factorList.get(factor);
			for (int i = 1; i <= number; i++) answer *= factor;
		}
		
		System.out.println("Answer = " + answer);
	}

}
