package AdventOfCode.aoc_2022.day01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	private static class Elf {
		ArrayList<Long> food = new ArrayList<>();
		
		@Override
		public String toString() {
			String s = "";
			for (long f : food) s += " " + f;
			return s;
		}
	}
	
	static ArrayList<Elf> elves = new ArrayList<>();
	
	static List<String> lines;
	
	
	public static void main(String[] args) throws Exception {
		readInput();
		Elf elf = new Elf();
		elves.add(elf);
		for (String line : lines) {
			if (line.isEmpty()) {
				elf = new Elf();
				elves.add(elf);
			} else {
				elf.food.add(Long.parseLong(line));
			}
		}
		
		long largest = 0;
		for (Elf e : elves) {
			System.out.println(e);
			long total = 0;
			for (long amount : e.food) total += amount;
			if (total > largest) largest = total;
		}
		System.out.println(largest);
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
