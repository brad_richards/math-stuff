package AdventOfCode.aoc_2018.day11;

import java.awt.Point;

/**
 * We have to be careful with our array indices, since the problem is specified with a lowest index
 * of 1, but we work with ordinary arrays starting with 0.
 * 
 * Fascinating: for part 2, the best grid power increases, may wander around a while, but ultimately
 * decreases to -1 and stays there. I haven't looked into why this behavior exists, but we use it to
 * end the search as soon as we see the first -1.
 */
public class Day11 {
	private static final int GRID_SN = 5791;
	private static final int SIZE = 300;
	private static final int SUBGRID_SIZE = 3;

	private static int[][] fuelLevels = new int[SIZE][SIZE];

	public static void main(String[] args) {
		calculateFuelLevels();

		boolean stop = false;
		Tuple bestTuple = new Tuple(0, 0, 0, 0);
		for (int subgrid_size = SUBGRID_SIZE; !stop && subgrid_size <= SIZE; subgrid_size++) {
			Tuple thisTuple = findBestSubgrid(subgrid_size);
//			System.out.println("This subgrid has top-left corner at " + (thisTuple.x + 1) + ", "
//					+ (thisTuple.y + 1) + " size " + thisTuple.subgrid_size + " and power "
//					+ thisTuple.power);
			if (thisTuple.power > bestTuple.power) bestTuple = thisTuple;
			else if (thisTuple.power == -1) {
				stop = true;
			}
		}
		System.out.println("Best subgrid has top-left corner at " + (bestTuple.x + 1) + ", "
				+ (bestTuple.y + 1) + " size " + bestTuple.subgrid_size + " and power "
				+ bestTuple.power);
	}

	private static void calculateFuelLevels() {
		for (int x = 0; x < SIZE; x++) {
			for (int y = 0; y < SIZE; y++) {
				fuelLevels[y][x] = calculateFuelLevel(x, y, GRID_SN);
			}
		}
	}

	/**
	 * We pass GRID_SN as a parameter, to allow for jUnit tests
	 */
	static int calculateFuelLevel(int x, int y, int grid_sn) {
		int rackID = x + 11; // adjust array index by 1
		int fuelLevel = rackID * (y + 1); // adjust array index by 1
		fuelLevel += grid_sn;
		fuelLevel *= rackID;
		int hundreds = fuelLevel % 1000 / 100;
		fuelLevel = hundreds - 5;
		return fuelLevel;
	}

	private static Tuple findBestSubgrid(int subgrid_size) {
		Tuple bestResult = new Tuple(0, 0, subgrid_size, -1);
		for (int x = 0; x < SIZE + 1 - subgrid_size; x++) {
			for (int y = 0; y < SIZE + 1 - subgrid_size; y++) {

				int totalPower = 0;
				for (int xsg = 0; xsg < subgrid_size; xsg++) {
					for (int ysg = 0; ysg < subgrid_size; ysg++) {
						totalPower += fuelLevels[y + ysg][x + xsg];
					}
				}
				if (totalPower > bestResult.power) {
					bestResult.x = x;
					bestResult.y = y;
					bestResult.power = totalPower;
				}
			}
		}
		return bestResult;
	}

	private static class Tuple {
		int x;
		int y;
		int subgrid_size;
		int power;

		public Tuple(int x, int y, int subgrid_size, int power) {
			this.x = x;
			this.y = y;
			this.subgrid_size = subgrid_size;
			this.power = power;
		}
	}
}
