package AdventOfCode.aoc_2019.day03;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * We really do not want to use an array: (1) we don't know if the wires will go
 * left, right, up, or down from the central port, and (2) we have no idea what
 * possible dimensions the array should have.
 * 
 * Hence, we will transform the input for each wire into a list of points that
 * it goes through, assuming the central port to be (0,0), which we omit from
 * the list.
 * 
 * We can then sort these points for each wire by their Manhattan Distance, and
 * search for the first match.
 */
public class Problem01 {
	private static ArrayList<Wire> wires = new ArrayList<>();
	
	public static void main(String[] args) {
		readInput();
		
		// Find the first common point
		Iterator<Point> i0 = wires.get(0).getPoints().iterator();
		Iterator<Point> i1 = wires.get(1).getPoints().iterator();
		Point point = null;
		Point p0 = i0.next();
		Point p1 = i1.next();
		while (point == null) {
			if (p0.compareTo(p1) < 0) p0 = i0.next();
			else if (p0.compareTo(p1) > 0) p1 = i1.next();
			else point = p0; // match found
		}

		System.out.print("Solution is the point: " + point);
	}
	
	private static void readInput() {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis) ) {
			while (in.hasNextLine()) {
				String wire = in.nextLine();
				String[] wireMoves = wire.split(",");
				wires.add(parseWire(wireMoves));
			}
		}
	}

	private static Wire parseWire(String[] wireMoves) {
		Wire wire = new Wire();
		for (String move : wireMoves) {
			char direction = move.charAt(0);
			int distance = Integer.parseInt(move.substring(1));
			for (int i = 0; i < distance; i++) wire.move(direction);
		}
		return wire;
	}	
}
