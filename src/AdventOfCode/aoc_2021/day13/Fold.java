package AdventOfCode.aoc_2021.day13;

public class Fold {
	public boolean foldY;
	public int value;
	
	public Fold(boolean foldY, int value) {
		this.foldY = foldY;
		this.value = value;
	}
}
