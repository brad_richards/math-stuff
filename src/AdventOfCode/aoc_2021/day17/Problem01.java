package AdventOfCode.aoc_2021.day17;

import java.util.Scanner;

/**
 * We assume that the probe will have an x-velocity of zero when it enters the
 * target area. Hence, the first task is to calculate an initial x velocity that
 * will reach the target area.
 * 
 * The y velocity is more difficult, since we cannot "pass through" the target.
 * It seems possible that extremely high y velocities could still (by
 * coincidence) land within the designated area. Have to think about this...
 * 
 * @author brad
 *
 */
public class Problem01 {
	private static int x1, x2, y1, y2;

	public static void main(String[] args) {
		readInput();
		
		// Range of possible x values
		int minX = (int) Math.ceil( (Math.sqrt(1 + 8 * x1) - 1) / 2 );
		int maxX = (int) Math.floor( (Math.sqrt(1 + 8 * x2) - 1) / 2 );
		System.out.println("X is " + minX + ".." + maxX);
		
		// Range of possible y values. We assume that y is initially positive
		int maxYvel = 0;
		int maxYmax = 0;
		for (int yi = 1; yi < 1000; yi++) {
			int ymax = yi * (yi+1)/2;
			int yd1 = ymax - y1;
			int maxY = (int) Math.floor( (Math.sqrt(1 + 8 * yd1) - 1) / 2 );
			int yd2 = ymax - y2;
			int minY = (int) Math.ceil( (Math.sqrt(1 + 8 * yd2) - 1) / 2 );
			if (minY <= maxY) {
				// System.out.println("Initial Yvel of " + yi + " works");
				maxYvel = yi;
				maxYmax = ymax;
			}
		}
		System.out.println("Part 1: Initial Yvel of " + maxYvel + " reaches a height of " + maxYmax);
		
		// For part 2, we are just going to do a dumb simulation.
		int minXvel = minX;
		int maxXvel = x2;
		int minYvel = y1;
		// maxYvel is from part 1

		int count = 0;
		for (int xVel = minXvel; xVel <= maxXvel; xVel++) {
			for (int yVel = minYvel; yVel <= maxYvel; yVel++) {
				if (hitsTarget(xVel, yVel)) {
					count++;
					// System.out.println(xVel + ", " + yVel);
				}
			}
		}
		System.out.println("Part 2: " + count + " solutions");
	}
	
	private static boolean hitsTarget(int xVel, int yVel) {
		int xPos = 0;
		int yPos = 0;
		boolean hit = false;
		while (!hit && xPos < x2 && yPos > y1) {
			xPos += xVel;
			yPos += yVel--;
			xVel = (xVel <= 0 ? 0 : xVel-1);
			hit = (xPos >= x1 && xPos <= x2 && yPos >= y1 && yPos <= y2);
		}
		return hit;
	}

	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		String line = in.nextLine();
		in.close();

		line = line.substring(15);
		String[] parts = line.split(", y=");
		String[] xs = parts[0].split("\\.\\.");
		String[] ys = parts[1].split("\\.\\.");
		x1 = Integer.parseInt(xs[0]);
		x2 = Integer.parseInt(xs[1]);
		y1 = Integer.parseInt(ys[0]);
		y2 = Integer.parseInt(ys[1]);
	}
}
