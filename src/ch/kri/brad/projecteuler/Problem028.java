package ch.kri.brad.projecteuler;

public class Problem028 {

	/**
	 * The fastest way to find the answer is to calculate the values on one of the diagonals.
	 * This can be done by noting the number of values in each concentric square, which are
	 * 1, 8, 16, ..., ((2n-1)² - (2n-3)²) - call the value at position n s[n]
	 * 
	 * The values on the lower-right diagonal are
	 * - 1, 3, 13, 31, 55, ...  - let the value at position n be v[n] = v[n-1] + s[n] + 2
	 * 
	 * The values on the lower-left diagonal are
	 * - 1, 5, 17, 37, 65, ...  - v[n] = v[n-1] + s[n] + 4
	 * 
	 * The upper-left diagonal uses the same formula, only with +6. The upper-right diagonal uses
	 * a value of +8.
	 *
	 * The following program adds up all of these numbers for a 1001x1001 array, which represents
	 * a value of n = 501
	 */
	public static void main(String[] args) {
		long lr = 1;
		long ll = 1;
		long ul = 1;
		long ur = 1;
		long sum = 1;
		long s_n = 0;
		for (int n = 2; n <= 501; n++) {
			lr = lr + s_n + 2;
			ll = ll + s_n + 4;
			ul = ul + s_n + 6;
			ur = ur + s_n + 8;
			sum += lr + ll + ul + ur;
			s_n = (2 * n - 1) * (2 * n -1) - (2 * n - 3) * (2 * n - 3);
		}
		
		System.out.println("Answer = " + sum);
	}

}
