package hackerRank.projectEuler;

import java.util.Scanner;

public class Problem001 {

	public static void main(String[] args) {
		int[] testCases = readData();

		for (int value : testCases) System.out.println(doOneCase(value));
	}
    
    public static int[] readData() {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] data = new int[size];
        for (int i = 0; i < size; i++) {
        	data[i] = s.nextInt();
        }
        s.close();
        return data;
    }
    
    public static long doOneCase(int limit) {
    	limit = limit - 1; // Values LESS THAN limit
    	
    	long num3 = limit / 3;
    	long sum3 = 3 * num3 * (num3+1) / 2;
    	
    	long num5 = limit / 5;
    	long sum5 = 5 * num5 * (num5+1) / 2;

    	long num15 = limit / 15;
    	long sum15 = 15 * num15 * (num15+1) / 2;
    	
		return sum3 + sum5 - sum15;
    }

}
