package hackerRank.projectEuler;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem009HR {
	private static ArrayList<Triplet> triplets = new ArrayList<>();
	private static int[] testCases;

	private static class Triplet {
		int a;
		int b;
		int c;

		public Triplet(int a, int b, int c) {
			if (a < b) {
				this.a = a;
				this.b = b;
			} else {
				this.b = a;
				this.a = b;
			}
			this.c = c;
		}

	}

	public static void main(String[] args) {
		readData();

		// Generate all required triplets
		int max = 0;
		for (int tc : testCases) if (tc > max) max = tc;
		generateTriplets(max);
		
		// Solve the individual test cases
		for (int tc : testCases)
			System.out.println(doOneCase(tc));
	}
	
	private static void readData() {
		Scanner s = new Scanner(System.in);
		int numTestCases = s.nextInt();
		testCases = new int[numTestCases];
		for (int i = 0; i < numTestCases; i++) {
			testCases[i] = s.nextInt();
		}
		s.close();
	}
	
	/**
	 * For the moment, we just search the triplets linearly. If this turns out 
	 * to be too slow, we'll add a more intelligent search algorithm
	 */
	private static long doOneCase(int limit) {
		long answer = -1;
		for (Triplet triplet : triplets) {
			int tripletSum = triplet.a + triplet.b + triplet.c; 
			if (limit % tripletSum == 0) {
				long mult = limit / tripletSum;
				long maybe = (mult * mult * mult) * triplet.a * triplet.b * triplet.c;
				if (maybe > answer) answer = maybe;
			}
		}
		return answer;
	}

	/**
	 * Generate all pythagorean triplets up to a maximum value of C
	 */
	private static void generateTriplets(int max) {
		Triplet lastTriplet = null;
		for (int p = 3; (lastTriplet == null || lastTriplet.c < max) && p <= Integer.MAX_VALUE; p += 2) {
			for (int q = 1; q < p; q += 2) {
				if (gcd(p, q) == 1) {
					int a = p * q;
					int b = (p * p - q * q) / 2;
					int c = (p * p + q * q) / 2;
					lastTriplet = new Triplet(a, b, c);
					triplets.add(lastTriplet);
				}
			}
		}
	}

	/**
	 * Adaptation of Euclid's GCD algorithm; num1 and num2 must both be non-zero
	 */
	public static int gcd(int num1, int num2) {
		int tmp;
		if (num2 > num1) {
			tmp = num2;
			num2 = num1;
			num1 = tmp;
		}
		while (num2 != 0) {
			tmp = num1 % num2;
			num1 = num2;
			num2 = tmp;
		}
		return num1;
	}
}
