package AdventOfCode.aoc_2024.day_09;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// This problem may be easier if we work from the original input data.
// Create an equivalent map containing file entry objects. A file
// entry object is either empty space (with size), or a file object
// (with size and id).
//
// After processing, convert this back to a disk map, so that we can
// use our old code from Problem1.

public class Problem2 {
    static List<String> lines;
    static List<FileEntry> fileEntries = new ArrayList<>();

    private static record FileEntry(int ID, int size) {};

    public static void main(String[] args) throws Exception {
        readInput();

        int[] entries = getEntries(lines.get(0));
        createFileEntries(entries);
        int diskSize = sumEntries(entries);
        int[] diskMap = createDiskMap(diskSize);
        printDiskmap(diskMap);
        defrag();
        diskMap = createDiskMap(diskSize);
        printDiskmap(diskMap);
        long checksum = checksum(diskMap);
        System.out.println(checksum);
    }

    private static void createFileEntries(int[] entries) {
        int fileID = 0;
        for (int i = 0; i < entries.length; i++) {
            if (i % 2 == 0) { // File
                fileEntries.add(new FileEntry(fileID++, entries[i]));
            } else {  // Empty space
                if (entries[i] > 0) fileEntries.add(new FileEntry(-1, entries[i]));
            }
        }
    }

    private static long checksum(int[] diskMap) {
        long checksum = 0;
        for (int i = 0; i < diskMap.length; i++) {
            checksum += diskMap[i] == -1 ? 0 : diskMap[i] * i;
        }
        return checksum;
    }

    private static void defrag() {
        int fileCursor = fileEntries.size() - 1;
        while (fileCursor >= 0) {
            if (fileEntries.get(fileCursor).ID == -1) { // Not a file
                fileCursor--;
            } else {
                int freeSpaceCursor = 0;
                boolean moved = false;
                while (!moved && freeSpaceCursor < fileCursor) {
                    if (fileEntries.get(freeSpaceCursor).ID != -1)
                        freeSpaceCursor++;
                    else if (fileEntries.get(freeSpaceCursor).size() < fileEntries.get(fileCursor).size())
                        freeSpaceCursor++;
                    else { // File move possible
                        if (fileEntries.get(freeSpaceCursor).size() == fileEntries.get(fileCursor).size()) { // Easy
                            FileEntry tmp = fileEntries.get(freeSpaceCursor);
                            fileEntries.set(freeSpaceCursor, fileEntries.get(fileCursor));
                            fileEntries.set(fileCursor, tmp);
                            fileCursor--;
                            moved = true;
                        } else { // Will have to split the free space
                            int fileSize = fileEntries.get(fileCursor).size();
                            int remainingFreeSpace = fileEntries.get(freeSpaceCursor).size() - fileEntries.get(fileCursor).size();
                            fileEntries.set(freeSpaceCursor, fileEntries.get(fileCursor));
                            fileEntries.set(fileCursor, new FileEntry(-1, fileSize));
                            fileEntries.add(freeSpaceCursor+1, new FileEntry(-1, remainingFreeSpace));
                            // fileCursor--; don't do this, because we have just added a new entry to the list
                            moved = true;
                        }
                    }
                }
                if (!moved) fileCursor--; // Skip the unmovable file
            }
        }
    }

    private static void printDiskmap(int[] diskMap) {
        for (int d : diskMap) System.out.print(d >= 0 ? d : ".");
        System.out.println();
    }

    // We use -1 for free space
    private static int[] createDiskMap(int diskSize) {
        int[] diskMap = new int[diskSize];
        int cursor = 0;
        for (FileEntry fileEntry : fileEntries) {
            for (int i = 0; i < fileEntry.size; i++) {
                diskMap[cursor+i] = fileEntry.ID();
            }
            cursor += fileEntry.size;
        }
        return diskMap;
    }

    private static int sumEntries(int[] entries) {
        int sum = 0;
        for (int value : entries) sum += value;
        return sum;
    }

    private static int[] getEntries(String line) {
        char[] chars = line.toCharArray();
        int[] ints = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            ints[i] = chars[i] - '0';
        }
        return ints;
    }

    private static void readInput() throws IOException {
        Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
        try (Stream<String> stream = Files.lines(path)) {
            lines = stream.collect(Collectors.toList());
        }
    }
}