package AdventOfCode.aoc_2015.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static String input = "3113322113";

	public static void main(String[] args) throws Exception {
		int iterations = 50;
		char[] oldChars = input.toCharArray();
		ArrayList<Character> old = new ArrayList<>();
		for (char c : oldChars)
			old.add(c);
		for (char c : old) System.out.print(c);
		System.out.println();

		for (int iteration = 0; iteration < iterations; iteration++) {
			ArrayList<Character> newChars = new ArrayList<>();
			int pos = 0;
			int charCounter = 1;
			while (pos < old.size() - 1) {
				if (old.get(pos).equals(old.get(pos + 1))) {
					charCounter++;
				} else {
					newChars.add((char) (charCounter + '0'));
					newChars.add(old.get(pos));
					charCounter = 1;
				}
				pos++;
			}
			// Process last character
			newChars.add((char) (charCounter + '0'));
			newChars.add(old.get(pos));
//			for (char c : newChars) System.out.print(c);
//			System.out.println();
			old = newChars;
		}
		System.out.println("Part 1 answer = " + old.size());

	}
}