package AdventOfCode.aoc_2015.day05;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();

		int count = 0;
		for (String line : lines) {
			if (isNice(line)) count++;
		}
		System.out.println("Part 1 answer is " + count);
	}
	
	private static boolean isNice(String in) {
		char[] chars = in.toCharArray();
		
		boolean hasPair = false;
		for (int i = 0; !hasPair && i < chars.length-3; i++) {
			for (int j = i+2; !hasPair && j < chars.length-1; j++) {
				hasPair = chars[i] == chars[j] && chars[i+1] == chars[j+1];
			}
		}

		boolean hasLetter = false;
		for (int i = 0; !hasLetter && i < chars.length-2; i++) {
			hasLetter = chars[i] == chars[i+2];
		}
		
		return hasPair && hasLetter;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}