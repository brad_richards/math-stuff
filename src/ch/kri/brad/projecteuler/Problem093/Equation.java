package ch.kri.brad.projecteuler.Problem093;

public class Equation {
	int[] digits = new int[4];
	Operation[] ops = new Operation[3];

	public Equation(int[] digits, Operation[] ops) {
		this.digits = digits;
		this.ops = ops;
	}

	public int evaluate1() {
		float result = ops[0].apply(digits[0], digits[1]);
		result = ops[1].apply(result, digits[2]);
		result = ops[2].apply(result, digits[3]);
		if (!Float.isNaN(result) && Math.abs(result - (int) result) < 0.001) { // integer
			return (int) (result + 0.001);
		} else {
			return -1;
		}
	}

	public int evaluate2() {
		float result = ops[1].apply(digits[1], digits[2]);
		result = ops[0].apply(digits[0], result);
		result = ops[2].apply(result, digits[3]);
		if (!Float.isNaN(result) && Math.abs(result - (int) result) < 0.001) { // integer
			return (int) (result + 0.001);
		} else {
			return -1;
		}
	}

	public int evaluate3() {
		float result = ops[0].apply(digits[0], digits[1]);
		float result2 = ops[2].apply(digits[2], digits[3]);
		result = ops[1].apply(result, result2);
		if (!Float.isNaN(result) && Math.abs(result - (int) result) < 0.001) { // integer
			return (int) (result + 0.001);
		} else {
			return -1;
		}
	}
}