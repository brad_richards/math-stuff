package ch.kri.brad.projecteuler;

import java.math.BigInteger;

import ch.kri.brad.utility.Digits;

public class Problem080 {
	static BigInteger TWENTY = BigInteger.valueOf(20);
	static BigInteger ONE_HUNDRED = BigInteger.valueOf(100);

	/**
	 * We assume that the natural numbers are 0-99. We skip all perfect squares: 0, 1, 4, 9, 16, 25,
	 * etc.
	 */
	public static void main(String[] args) {
		int digitSum = 0;
		for (int value = 2; value <= 99; value++) {
			if (value != 4 && value != 9 && value != 16 && value != 25 && value != 36 && value != 49
					&& value != 64 && value != 81) {
				BigInteger root = BI_sqrt(value, 100);
				digitSum += Digits.sumOfDigits(root);
			}
		}
		System.out.println("Answer: " + digitSum);
	}

	/**
	 * Calculate the square root digit-by-digit, for the specified number of digits. We assume that
	 * value is in the range 0-99 inclusive. Highly efficient versions of this algorithm are
	 * possible in binary, but we are working explicitly in decimal.
	 * 
	 * Important: This implementation has two issues: - it assumes that there is only one
	 * significant pair of digits in the value, i.e., it only works for integers from 0 to 99. - it
	 * does not detect perfect squares; sqrt(4) will be calculated as 1.999999...
	 */
	private static BigInteger BI_sqrt(int value, int digits) {
		BigInteger rootSoFar = BigInteger.ZERO;
		BigInteger remainder = BigInteger.ZERO;
		for (int i = 0; i < digits; i++) {
			int digitPair = 0;
			if (i == 0) digitPair = value;

			// Find digit, non-optimized search. We could presumably calculate the digit to use, or
			// at least estimate it, but we are lazy
			BigInteger topValue = ONE_HUNDRED.multiply(remainder)
					.add(BigInteger.valueOf(digitPair));
			BigInteger bottomValue = null;

			// Calculate a first guess for the digit; may be correct, or slightly too large. Doesn't
			// work for the first iteration
			BigInteger digit;
			if (i == 0) {
				digit = BigInteger.valueOf(9);
			} else {
				BigInteger tmp = TWENTY.multiply(rootSoFar);
				digit = topValue.divide(tmp);
			}
			boolean digitFound = false;
			while (!digitFound) {
				bottomValue = rootSoFar.multiply(TWENTY).add(digit).multiply(digit);
				if (bottomValue.compareTo(topValue) < 0) {
					digitFound = true;
				} else {
					digit = digit.subtract(BigInteger.ONE);
				}
			}
			rootSoFar = rootSoFar.multiply(BigInteger.TEN).add(digit);
			remainder = topValue.subtract(bottomValue);
		}
		return rootSoFar;
	}

}
