package ch.kri.brad.projecteuler;

/**
 * If n is the length of the row, then we can calculate the number of arrangements for various
 * numbers of blocks
 * 
 * - For 1 block: (n-1)! / [(n-3)! * 2!]
 * 
 * - For 2 blocks: (n-3)! / [(n-7)! * 4!]
 * 
 * - For 3 blocks: (n-5)! / [(n-11)! * 6!]
 * 
 * and so forth, up to the maximum number of blocks, which is (n+1)/4. Finally, we add one to
 * account for a completely empty row.
 */
public class Problem114 {
	public static final int n =29;

	public static void main(String[] args) {
		long sum = 0;
		for (long nBlocks = 1; nBlocks <= (n + 1) / 4; nBlocks++) {
			long start = n + 1 - 4 * nBlocks;
			long product = 1;
			for (long i = 1; i <= nBlocks * 2; i++) {
				System.out.print((start + i) + " ");
				product *= (start + i);
				product /= i;
			}
			System.out.println();
			sum += product;
		}
		sum += 1;
		System.out.println(sum);
	}
}
