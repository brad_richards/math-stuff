package ch.kri.brad.utility;

import static ch.kri.brad.utility.Factoring.*;
import static ch.kri.brad.utility.Primes.primeFactors;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class FactoringTest {

	@Test
	public void testGenerateAmicablePairs() {
		generateAmicablePairs(300);
		ArrayList<AmicablePair> pairs = getAmicablePairs();
		
		assertEquals(pairs.size(), 1);
		
		AmicablePair pair = pairs.get(0);
		
		assertEquals(pair.getValue1(), 220);
		assertEquals(pair.getValue2(), 284);
	}

	@Test
	public void testGenerateAbundantNumbers() {
		long[] someAbundantNumbers = {12, 20, 36, 60};
		long[] someNonabundantNumbers = {1, 6, 11, 28};
		long number_below_100 = 21; // Not including 100 
		
		generateAbundantNumbers(99);
		
		ArrayList<Long> abundantNumbers = getAbundantNumbers();
		
		assertEquals(abundantNumbers.size(), number_below_100);
		
		for (Long an : someAbundantNumbers) {
			assertTrue(abundantNumbers.contains(an));
		}
		
		for (Long an : someNonabundantNumbers) {
			assertFalse(abundantNumbers.contains(an));
		}
	}

	@Test
	public void testGetDivisors() {
		final long[] divisors_of_420 = {1, 2, 3, 4, 5, 6, 7, 10, 12, 14, 15, 20, 21, 28, 30, 35, 42, 60, 70, 84, 105, 140, 210, 420}; 
		ArrayList<Long> divisors = getDivisors(420);
		for (Long f : divisors_of_420) {
			divisors.remove(f);
		}
		assertTrue(divisors.size() == 0);
	}

	@Test
	public void testGetSumDivisors() {
		final long sum_of_divisors_of_420 = 924;
		long testResult = getSumDivisors(420);
		System.out.println(testResult);
		assertEquals(testResult, sum_of_divisors_of_420);
	}

	@Test
	public void testGetCommonPrimeFactors() {
		final long value1 = 546; // 2 * 3 * 7 * 13;
		final long value2 = 2145; // 3 * 5 * 11 * 13;
		
		ArrayList<Integer> commonFactors = getCommonPrimeFactors(value1, value2);
		assertEquals(commonFactors.size(), 2);
		assertTrue(commonFactors.contains(new Integer(3)));
		assertTrue(commonFactors.contains(new Integer(13)));
	}

}
