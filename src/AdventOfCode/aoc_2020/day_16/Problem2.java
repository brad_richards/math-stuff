package AdventOfCode.aoc_2020.day_16;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import AdventOfCode.aoc_2020.day_16.Field.Range;

public class Problem2 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		ArrayList<Field> fields = ir.getFields();
		Ticket yourTicket = ir.getYourTicket();
		ArrayList<Ticket> nearbyTickets = ir.getNearbyTickets();
		
		ArrayList<Range> allRanges = new ArrayList<>();
		for (Field field : fields) {
			allRanges.addAll(field.getRanges());
		}

		// Remove all invalid tickets
		for (Iterator<Ticket> i = nearbyTickets.iterator(); i.hasNext();) {
			Ticket t = i.next();
			int[] fieldValues = t.getFieldValues();
			for (int value : fieldValues) {
				boolean ok = false;
				for (int j = 0; !ok && j < allRanges.size(); j++) {
					ok = allRanges.get(j).inRange(value);
				}
				if (!ok) {
					i.remove();
					break; // ugly, but that's life
				}
			}
		}

		// Create a 2-dim array, where the rows are the Fields and the columns
		// are the positions of the values on the tickets. Initialize to true.
		//
		// For each ticket
		// -- For each position, move through the corresponding column
		//    and see if the value is valid for this position, AND the result
		//    with the current value
		
		boolean[][] results = new boolean[fields.size()][yourTicket.getFieldValues().length];
		for (int row = 0; row < results.length; row++)
			for (int col = 0; col < results[0].length; col++)
				results[row][col] = true;
		
		for (Ticket t : nearbyTickets) {
			int[] fieldValues = t.getFieldValues();
			for (int col = 0; col < fieldValues.length; col++) {
				int value = fieldValues[col];
				for (int row = 0; row < results.length; row++) {
					Field field = fields.get(row);
					results[row][col] &= field.inRange(value);
				}
			}
		}
		
		for (int row = 0; row < results.length; row++) {
			for (int col = 0; col < results[0].length; col++) {
				System.out.print(results[row][col] ? "T" : ".");
			}
			System.out.println();
		}
		
		// Repeat the following process "rows.length" times
		// -- Find the row containing a single true value
		//    -- Record the column in the corresponding field
		//    -- Set this column to false in all rows
		for (int repeat = 0; repeat < results.length; repeat++) {
			int rowFound = -1;
			int colFound = -1;
			for (int row = 0; rowFound == -1 && row < results.length; row++) {
				int countTrues = 0;
				for (int col = 0; col < results[row].length; col++) if (results[row][col]) {
					countTrues++;
					colFound = col;
				}
				if (countTrues == 1) rowFound = row;
			}
			
			fields.get(rowFound).setColumn(colFound);
			for (int row = 0; row < results.length; row++) results[row][colFound] = false;
		}

		// Now multiply the values of "yourTicket" for the first six fields
		long product = 1;
		for (int i = 0; i < 6; i++) {
			Field field = fields.get(i);
			int value = yourTicket.getFieldValues()[field.getColumn()];
			product *= value;
		}
		
		System.out.println("Answer = " + product);
	}

}
