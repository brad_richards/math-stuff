package ch.kri.brad.polyominos;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class represents a single polyomino of a given size. After creating a
 * polyonimo with the constructor, the fields must be set. Once all fields are
 * set, the polyomino should be justified (moved as far as possible towards the
 * upper-left) using the justify method.
 * 
 * Polyominos are compared using a "signature", which is a unique BigInteger
 * value encoded off of the individual field positions.
 */
public class Polyomino {
	private final int size;
	private byte[] signature; // used for fast comparison
	private boolean[][] fields;

	public Polyomino(int size) {
		this.size = size;
		fields = new boolean[size][size];
		signature = null;
	}

	public int getSize() {
		return size;
	}

	public void setField(int i, int j) {
		fields[i][j] = true;
	}

	/**
	 * Return the unique signature of this polyomino; if the signature has not
	 * yet been calculated, we do so...
	 */
	public byte[] getSignature() {
		if (signature == null) {
			calculateSignature();
		}
		return signature;
	}

	/**
	 * Shift the polyomino to the top-left within its field
	 */
	public void justify() {
		// shift up
		boolean empty = true;
		while (empty) {
			for (int col = 0; col < size; col++)
				empty = empty & !fields[0][col];
			if (empty) {
				for (int row = 1; row < size; row++) {
					for (int col = 0; col < size; col++) {
						fields[row - 1][col] = fields[row][col];
					}
				}
				for (int col = 0; col < size; col++)
					fields[size - 1][col] = false;
			}
		}

		// shift left
		empty = true;
		while (empty) {
			for (int row = 0; row < size; row++)
				empty = empty & !fields[row][0];
			if (empty) {
				for (int row = 0; row < size; row++) {
					for (int col = 1; col < size; col++) {
						fields[row][col - 1] = fields[row][col];
					}
				}
				for (int row = 0; row < size; row++)
					fields[row][size - 1] = false;
			}
		}
	}

	// Calculate the unique signature of this polyomino, by setting bits in a
	// byte-array to correspond to the values in the polyomino
	private void calculateSignature() {
		int numBytes = (size * size + 7) / 8;
		byte[] tmpValue = new byte[numBytes];

		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if (fields[row][col]) {
					setBit(tmpValue, row * size + col);
				}
			}
		}
		signature = tmpValue;
	}

	@Override
	public String toString() {
		return fieldsToString(this.fields);
	}

	private static String fieldsToString(boolean[][] fields) {
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < fields.length; i++) {
			for (int j = 0; j < fields.length; j++) {
				b.append(fields[i][j] ? '*' : ' ');
			}
			b.append('\n');
		}
		return "----\n" + b.toString() + "----\n";
	}

	@Override
	public boolean equals(Object o) {
		boolean identical = false;
		if (o.getClass() == Polyomino.class) {
			Polyomino in = (Polyomino) o;
			identical = Arrays.equals(this.getSignature(), in.getSignature());
		}
		return identical;
	}

	/**
	 * We extend an polyomino of size x to a list of polyonimoes of size x+1. To do
	 * this, we create a larger field and place the incoming polyomino first in the
	 * top-left then in the bottom-right. For both of these configurations, we
	 * then place one additional field in all possible fields, and keep the ones
	 * where the new field is adjacent to an existing field of the incoming
	 * omino. We do not remove duplicates, rotations or reflections.
	 * 
	 * @param in
	 *            The incoming polyomino
	 * @return A list of all derived ominos
	 */
	public static void extendPolyomino(ArrayList<Polyomino> results, Polyomino in) {
		// Create the two variants: top-left and bottom-right
		boolean[][] largerTopLeft = new boolean[in.size + 1][in.size + 1];
		boolean[][] largerBottomRight = new boolean[in.size + 1][in.size + 1];
		for (int i = 0; i < in.size; i++) {
			for (int j = 0; j < in.size; j++) {
				largerTopLeft[i][j] = in.fields[i][j];
				largerBottomRight[i + 1][j + 1] = in.fields[i][j];
			}
		}

		// Try placing a new field in all possible locations
		for (int i = 0; i <= in.size; i++) {
			for (int j = 0; j <= in.size; j++) {
				if (isValidField(largerTopLeft, i, j)) {
					Polyomino newPolyomino = copyPolyomino(largerTopLeft);
					newPolyomino.fields[i][j] = true;
					newPolyomino.justify();
					if (notPresent(results, newPolyomino))
						results.add(newPolyomino);
				}
				if (isValidField(largerBottomRight, i, j)) {
					Polyomino newPolyomino = copyPolyomino(largerBottomRight);
					newPolyomino.fields[i][j] = true;
					newPolyomino.justify();
					if (notPresent(results, newPolyomino))
						results.add(newPolyomino);
				}
			}
		}
	}

	private static boolean notPresent(ArrayList<Polyomino> list,
			Polyomino newPolyomino) {
		boolean present = false;
		for (Polyomino o : list) {
			if (duplicates(o, newPolyomino)) {
				present = true;
				break;
			}
		}
		return !present;
	}

	/**
	 * Determine whether two polyominos are actually identical, accounting rotation
	 * and reflection.
	 */
	public static boolean duplicates(Polyomino a, Polyomino b) {
		// direct comparison.
		if (a.equals(b)) return true;

		// rotations
		Polyomino o = b;
		for (int r = 1; r <= 3; r++) {
			o = Polyomino.rotate(o);
			if (a.equals(o)) return true;
		}

		// reflection, direct
		Polyomino reflect = null;
		reflect = Polyomino.reflect(b);
		if (a.equals(reflect)) return true;

		// rotations of the reflection
		o = reflect;
		for (int r = 1; r <= 3; r++) {
			o = Polyomino.rotate(o);
			if (a.equals(o)) return true;
		}
			
		return false;
	}

	/**
	 * Determine whether or not the field [i][j] is adjacent to an existing
	 * field
	 */
	private static boolean isValidField(boolean[][] fields, int i, int j) {
		boolean valid = false;
		if (!fields[i][j]) {
			valid = ((i - 1) >= 0 && fields[i - 1][j])
					|| ((j - 1) >= 0 && fields[i][j - 1])
					|| ((i + 1) < fields.length && fields[i + 1][j])
					|| ((j + 1) < fields.length && fields[i][j + 1]);
		}
		return valid;
	}

	private static Polyomino copyPolyomino(boolean[][] fieldsIn) {
		Polyomino newPolyomino = new Polyomino(fieldsIn.length);
		for (int i = 0; i < fieldsIn.length; i++) {
			for (int j = 0; j < fieldsIn.length; j++) {
				newPolyomino.fields[i][j] = fieldsIn[i][j];
			}
		}
		return newPolyomino;
	}

	public static Polyomino rotate(Polyomino in) {
		Polyomino newOmino = new Polyomino(in.size);
		for (int i = 0; i < in.size; i++) {
			for (int j = 0; j < in.size; j++) {
				newOmino.fields[i][j] = in.fields[in.size - j - 1][i];
			}
		}
		newOmino.justify();
		return newOmino;
	}

	public static Polyomino reflect(Polyomino in) {
		Polyomino newPolyomino = new Polyomino(in.size);
		for (int i = 0; i < in.size; i++) {
			for (int j = 0; j < in.size; j++) {
				newPolyomino.fields[i][j] = in.fields[j][i];
			}
		}
		return newPolyomino;
	}

	private static byte[] shiftLeft(byte[] in) {
		byte[] out = new byte[in.length];

		for (int i = 0; i < in.length; i++) {
			out[i] = (byte) (in[i] << 1); // shift the byte
			if ((i < (in.length - 1)) && (in[i + 1] < 0))
				out[i] = (byte) (out[i] + 1); // carry bit
		}
		return out;
	}

	private static void setBit(byte[] array, int bit) {
		int byteNum = bit / 8;
		int bitNum = bit % 8;

		int newBit = 0;
		switch (bitNum) {
		case 0:
			newBit = 1;
			break;
		case 1:
			newBit = 2;
			break;
		case 2:
			newBit = 4;
			break;
		case 3:
			newBit = 8;
			break;
		case 4:
			newBit = 16;
			break;
		case 5:
			newBit = 32;
			break;
		case 6:
			newBit = 64;
			break;
		case 7:
			newBit = 128;
			break;
		}
		array[byteNum] = (byte) (array[byteNum] | newBit);
	}
}
