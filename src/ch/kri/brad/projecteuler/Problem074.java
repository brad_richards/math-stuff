package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.HashMap;

import ch.kri.brad.utility.Digits;

/**
 * The number 145 is well known for the property that the sum of the factorial of its digits is
 * equal to 145:
 * 
 * 1! + 4! + 5! = 1 + 24 + 120 = 145
 * 
 * Perhaps less well known is 169, in that it produces the longest chain of numbers that link
 * back to 169; it turns out that there are only three such loops that exist:
 * 
 * 169 363601 1454 169 871 45361 871 872 45362 872
 * 
 * It is not difficult to prove that EVERY starting number will eventually get stuck in a loop.
 * For example,
 * 
 * 69 363600 1454 169 363601 ( 1454) 78 45360 871 45361 ( 871) 540 145 ( 145)
 * 
 * Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating
 * chain with a starting number below one million is sixty terms.
 * 
 * How many chains, with a starting number below one million, contain exactly sixty
 * non-repeating terms?
 */
public class Problem074 {
	public static final int[] FACTORIALS = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
	
	// Hashset of numbers that are contained in loops; built up dynamically as we go
	static HashMap<Integer, Integer> loopValues = new HashMap<>(500000);
	
	// ArrayList containing the sequence of the current number.
	static ArrayList<Integer> currentSequence;

	public static void main(String[] args) {
		int numAnswers = 0;
		for (int n = 1; n < 1000000; n++) {
			currentSequence = new ArrayList<>();
			int answer = sequence(n); 
			if (answer == 60) numAnswers++;
		}
		System.out.println(numAnswers);
	}

	/**
	 * The core method: construct a sequence for the given value. At each step, see if the number is
	 * already known to be in a loop. If it is, we can stop and return the number of terms generated
	 * before this element. Else, we check to see if the element has already appeared in this
	 * sequence; if so, we have a new loop, and need to enter the values in our record of loop
	 * values. If neither of these is the case, we continue by generating the next element in the
	 * sequence.
	 * 
	 * @return Returns the number of values that were not included in any loop.
	 */
	public static int sequence(int n) {
		if (loopValues.containsKey(n)) { // Base case
			return currentSequence.size() + loopValues.get(n);
		} else if (currentSequence.contains(n)) { // Base case
			// New loop discovered; add values to loopValues
			int loopStart = currentSequence.indexOf(n);
			int loopLength = currentSequence.size() - loopStart;
			for (int i = loopStart; i < currentSequence.size(); i++) {
				loopValues.put(currentSequence.get(i), loopLength);
			}			
			return currentSequence.size();
		} else { // Inductive case
			currentSequence.add(n);
			return sequence(sumDigitFactorials(n));
		}
	}
	
	private static int sumDigitFactorials(int n) {
		int digits[] = Digits.toDigits(n);
		int sum = 0;
		for (int d : digits) {
			sum += FACTORIALS[d];
		}
		return sum;
	}
}
