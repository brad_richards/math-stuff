package AdventOfCode.aoc_2018.day14;

public class Day14 {
	private static final int[] INITIAL_RECIPES = {3, 7};
	private static final int PRACTICE_RECIPES = 824501;
	private static int[] recipes;
	private static int numRecipes;
	private static final int NUM_ELVES = 2;
	private static int[] elves;

	public static void main(String[] args) {
		// We want the ten values after the practice recipes,
		// plus we need an extra space in case we generate one too many digits
		recipes = new int[PRACTICE_RECIPES+11];
		
		// Create array with the elves' positions
		elves = new int[NUM_ELVES];
		
		// Copy initial recipes into the array, and initialize the elves' positions
		for (int i = 0; i < INITIAL_RECIPES.length; i++) {
			recipes[i] = INITIAL_RECIPES[i];
			elves[i] = i;
		}
		numRecipes = 2;
		
		// Generate new recipes until we have enough
		while( numRecipes < PRACTICE_RECIPES + 10) {
			int sum = 0;
			for (int elf : elves) {
				sum += recipes[elf];
			}
			int[] digits = intToDigits(sum);
			for (int i = 0; i < digits.length; i++) recipes[numRecipes+i] = digits[i];
			numRecipes += digits.length;
			
			// Move the elves forward
			for (int i = 0; i < elves.length; i++) {
				elves[i] = (elves[i] + recipes[elves[i]] + 1) % numRecipes;
			}

//			System.out.print("Elves are currently at: ");
//			for (int elf : elves) System.out.print(elf + " ");
//			System.out.println("    numRecipes = " + numRecipes);			
//			
//			printRecipes();
		}
		
		// Print part 1 solution
		System.out.print("Part 1: digits are ");
		for (int i = PRACTICE_RECIPES; i < PRACTICE_RECIPES+10; i++) {
			System.out.print(recipes[i]);
		}
		System.out.println();
		
		// For part 2, find the first occurrence of the digits we just printed for part 1
		// For simplicity of coding, we are going to turn our nice digits into strings
		StringBuffer buf = new StringBuffer();
		for (int digit : recipes) buf.append(Integer.toString(digit));
		String fullString = buf.toString();
		buf = new StringBuffer();
		for (int i = PRACTICE_RECIPES; i < PRACTICE_RECIPES+10; i++) buf.append(Integer.toString(recipes[i]));
		String substring = buf.toString();
		System.out.println("Part 2: String first found at location " + fullString.indexOf(substring));;
	}

	private static int[] intToDigits(int value) {
		int firstDigit = 10;
		int[] scratch = new int[10];
		do {
			int digit = value % 10;
			value = value / 10;
			scratch[--firstDigit] = digit;
		} while (value > 0);
		int[] digits = new int[10-firstDigit];
		for (int i = 0; i < digits.length; i++) digits[i] = scratch[firstDigit+i];
		return digits;
	}
	
	private static void printRecipes() {
		for (int i = 0; i < numRecipes; i++) System.out.print(recipes[i] + " ");
		System.out.println();
	}
	
}
