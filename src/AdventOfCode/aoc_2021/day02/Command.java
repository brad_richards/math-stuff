package AdventOfCode.aoc_2021.day02;

public class Command {
	public enum Direction {forward, up, down};
	
	Direction direction;
	int distance;
}
