package ch.kri.brad.projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.Digits;

public class Problem056 {

	/**
	 * The maximum possible value of a sum of digits is 9 times the number of digits. As a first attempt, we will begin with the very largest numbers, and work down.
	 * When the largest sum found becomes greater than the largest sum remaining possible, we can stop.
	 */
	public static void main(String[] args) {
		int highestSum = 0;
		for (int a = 99; a > 0; a--) {
			BigInteger bigA = BigInteger.valueOf(a);
			for (int b = 99; b > 0; b--) {
				BigInteger bigAnswer = bigA.pow(b);
				ArrayList<Integer> digits = Digits.toDigits(bigAnswer);
				int maxPossible = digits.size() * 9;
				if (maxPossible < highestSum) {
					break; // No sense trying further
				} else {
					int sum = 0;
					for (int d : digits) sum += d;
					if (sum > highestSum) highestSum = sum;
				}
			}
		}
		System.out.println(highestSum);
	}

}
