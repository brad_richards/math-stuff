package AdventOfCode.aoc_2022.day07;

public class File extends FileSystemEntry {
	public File(String name, Directory parent, long size, int depth) {
		super(name, parent, size, depth);
	}
	
	@Override
	public String toString() {
		return super.toString() + " (" + this.getSize() + ")";
	}
}
