package AdventOfCode.aoc_2022.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;
	static List<TestCase> testCases = new ArrayList<>();;
	
	private static class TestCase {
		public XmasList left;
		public XmasList right;
	}

	public static void main(String[] args) throws Exception {
		readInput();
		for (int i = 0; i < lines.size(); i+=3) {			
			TestCase tc = new TestCase();
			tc.left = new XmasList(lines.get(i));
			tc.right = new XmasList(lines.get(i+1));
			testCases.add(tc);
		}
		
		int part1Answer = 0;
		int index = 1;
		for (TestCase tc : testCases) {
			int result = tc.left.compareTo(tc.right);
			System.out.print(result <= 0 ? "Correct: " : " Wrong: ");
			System.out.println(tc.left + " " + tc.right);
			
			if (result <= 0) part1Answer += index;
			index++;
		}
		System.out.println("Part 1 answer: " + part1Answer);
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}