package ch.kri.brad.projecteuler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;

public class Problem088_bruteforce {

	/**
	 * This brute force solution works, however, the search space apparently becomes exponentially
	 * larger as the problem size increases. Through size 60, the solution time is only 3-4 seconds.
	 * However, size 70 requires more than 10 times as long, and it only gets worse...
	 * 
	 * The fundamental approach is best-first search. A particular tuple [1,1,a,b,c] is always
	 * sorted, so that a <= b <= c, and is given the sort value of 1*1*a*b*c:1+1+a+b+c. When we
	 * extend the search, we take the lowest-valued entry, remove it from the collection, and extend
	 * it in all possible ways: This means for all numbers in the tuple, if that number is less then
	 * the following number, it can be incremented. The last number can be incremented to n, when n
	 * is the size of the tuple. For example, the tuple [1,1,2,2,3] will generate the new tuples
	 * [1,2,2,2,3], [1,1,2,3,3] and [1,1,2,2,4]. These will be valued and added to the collection.
	 * We are finished with tuples of length n, as soon as the first tuple in the collection meets
	 * the criteria "product = sum"
	 * 
	 * We always begin with the tuple [..., 1, 1, 2, 2]
	 * 
	 * Note that the solution for the Project Euler problem requires that duplicate values be
	 * eliminated. Hence, solutions are accumulated in a TreeSet, and read out again at the end.
	 */
	public static void main(String[] args) {
		TreeSet<Long> solutions = new TreeSet<Long>();
		
		for(int tupleLength = 2; tupleLength <= 70; tupleLength++) {
			if (tupleLength%100 == 0) System.out.print(".");
			if (tupleLength%1000 == 0) System.out.println();
			
			long[] numbers = new long[tupleLength];
			for (int i = 0; i < tupleLength-2; i++) numbers[i] = 1;
			numbers[tupleLength-2] = 2;
			numbers[tupleLength-1] = 2;
			
			Tuple firstTuple = new Tuple(numbers);
			ArrayList<Tuple> tupleList = new ArrayList<>();
			tupleList.add(firstTuple);
			
			while(!tupleList.get(0).isProductSum()) {
				Tuple oldTuple = tupleList.remove(0);
				ArrayList<Tuple> newTuples = oldTuple.extendTuple();
				tupleList = mergeTupleLists(tupleList, newTuples);
			}
			Tuple solution = tupleList.get(0);
			solutions.add(solution.getSum());
		}
		
		// Add up all the solution values
		long solutionSum = 0;
		for (long s : solutions) solutionSum += s;
		
		System.out.println("Value = " + solutionSum);
	}

	
	/**
	 * Linear merge of two sorted lists of tuples to produce a third sorted list.
	 */
	private static ArrayList<Tuple> mergeTupleLists(ArrayList<Tuple> list1, ArrayList<Tuple> list2) {
		ArrayList<Tuple> mergedTuples = new ArrayList<>();
		int idx1 = 0;
		int idx2 = 0;
		while (idx1 < list1.size() & idx2 < list2.size()) {
			Tuple tuple1 = list1.get(idx1);
			Tuple tuple2 = list2.get(idx2);
			if (tuple1.compareTo(tuple2) < 0) {
				mergedTuples.add(tuple1);
				idx1++;
			} else {
				mergedTuples.add(tuple2);
				idx2++;
			}
		}
		while (idx1 < list1.size()) {
			mergedTuples.add(list1.get(idx1++));
		}
		while (idx2 < list2.size()) {
			mergedTuples.add(list2.get(idx2++));
		}
		return mergedTuples;
	}
	
	
	private static class Tuple implements Comparable {
		private long[] numbers;
		private long product;
		private long sum;
		
		public Tuple(long[] numbers) {
			this.numbers = numbers;
			
			long sum = 0;
			long product = 1;
			for (long  i : numbers) {
				sum += i;
				product *= i;
			}
			this.sum = sum;
			this.product = product;
		}
		
		public boolean isProductSum() {
			return product==sum;
		}
		
		/**
		 * Generate all possible new tuples from this tuple, and return these in a sorted list
		 * @return
		 */
		public ArrayList<Tuple> extendTuple() {
			ArrayList<Tuple> newTuples = new ArrayList<>();
			for (int idx = 0; idx < numbers.length-1; idx++) {
				if (numbers[idx] < numbers[idx+1]) {
					long[] newNumbers = Arrays.copyOf(numbers, numbers.length);
					newNumbers[idx]++;
					newTuples.add(new Tuple(newNumbers));
				}
			}
			// Check last entry as well
			if (numbers[numbers.length-1] < numbers.length) {
				long[] newNumbers = Arrays.copyOf(numbers, numbers.length);
				newNumbers[numbers.length-1]++;
				newTuples.add(new Tuple(newNumbers));
			}
			Collections.sort(newTuples);
			return newTuples;
		}

		@Override
		public int compareTo(Object o) {
			int result = 0;
			Tuple t = (Tuple) o;
			
			if (this.product < t.product) {
				result = -1;
			} else if (this.product > t.product) {
				result = 1;
			} else if (this.sum < t.sum){ 
				result = -1;
			} else if (this.sum > t.sum){
				result = 1;
			}
			return result;
		}
		
		@Override
		public String toString() {
			StringBuffer buff = new StringBuffer();
			buff.append(numbers.length);
			buff.append(": [");
			for (long n : numbers) {
				buff.append(n);
				buff.append(" ");
			}
			buff.append("]");
			return buff.toString();
		}
		
		public long getSum() {
			return sum;
		}
	}
}
