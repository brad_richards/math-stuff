package AdventOfCode.aoc_2021.day13;

import java.util.ArrayList;
import java.util.Scanner;

import AdventOfCode.aoc_2021.day09.Point;

public class Problem01 {
	private static ArrayList<Point> points = new ArrayList<>();
	private static ArrayList<Fold> folds = new ArrayList<>();
	private static boolean[][] paper;

	public static void main(String[] args) {
		readInput();

		createPaper();
//		printPaper();
		System.out.println("Number of points: " + countPoints());

		for (Fold f : folds) {
			foldPaper(f);
//			printPaper();
			System.out.println("Number of points: " + countPoints());
		}
		
		printFinalPaper();
	}
	
	/**
	 * Fold the paper according to the given instruction. We erase all points
	 * from the region that no longer exists, as we fold them.
	 */
	private static void foldPaper(Fold fold) {
		int max = (fold.foldY ? paper.length : paper[0].length);
		for (int from = fold.value+1, to = fold.value-1;
				from < max; from++, to--) {
			int otherMax = (fold.foldY ? paper[0].length : paper.length);
			for (int i = 0; i < otherMax; i++) {
				if (fold.foldY) {
					if (paper[from][i]) {
						paper[to][i] = true;
						paper[from][i] = false;
					}
				} else {
					if (paper[i][from]) {
						paper[i][to] = true;
						paper[i][from] = false;
					}
				}
			}
		}
	}
	
	/**
	 * Print a visual representation of the paper
	 */
	private static void printPaper() {
		for (int y = 0; y < paper.length; y++) {
			for (int x = 0; x < paper[0].length; x++) {
				System.out.print(paper[y][x] ? "#" : ".");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * Print a visual representation of the paper, omitting ending columns and rows that contain no points
	 */
	private static void printFinalPaper() {
		int maxX = 0;
		int maxY = 0;
		for (int y = 0; y < paper.length; y++) {
			for (int x = 0; x < paper[0].length; x++) {
				if (paper[y][x]) {
					if (x > maxX) maxX = x;
					if (y > maxY) maxY = y;
				}
			}
		}
		
		for (int y = 0; y <= maxY; y++) {
			for (int x = 0; x <= maxX; x++) {
				System.out.print(paper[y][x] ? "#" : ".");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * Count the number of points on the paper
	 */
	private static int countPoints() {
		int count = 0;
		for (int y = 0; y < paper.length; y++) {
			for (int x = 0; x < paper[0].length; x++) {
				if (paper[y][x]) count++;
			}
		}
		return count;
	}
	
	/**
	 * Create paper, and add all points
	 */
	private static void createPaper() {
		int maxX = 0;
		int maxY = 0;
		for (Point p : points) if (p.x > maxX) maxX = p.x;
		for (Point p : points) if (p.y > maxY) maxY = p.y;
		
		paper = new boolean[maxY+1][maxX+1];
		
		for (Point p : points) paper[p.y][p.x] = true;
	}

	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.length() > 0) {
				if (line.charAt(0) == 'f') { // fold instruction
					boolean foldY = (line.charAt(11) == 'y');
					int value = Integer.parseInt(line.substring(13, line.length()));
					folds.add(new Fold(foldY, value));
				} else { // point
					String[] parts = line.split(",");
					int x = Integer.parseInt(parts[0]);
					int y = Integer.parseInt(parts[1]);
					points.add(new Point(x,y));
				}
			}
		}
		in.close();
	}
}
