package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Fraction;

public class Problem071 {

	public static void main(String[] args) {
		// We will save the largest fraction that we find in the following algorithm
		Fraction largestFraction = new Fraction(24, 120); // Start with a small value
		
		// For all denominators from 1000000 down to 8, find largest numerator such that
		// the value is smaller than 3/7. Probably we could identify a condition that
		// would allow us to stop earlier, but we won't bother...
		for (long denominator = 1000000; denominator >= 8; denominator--) {
			long numerator = 3 * denominator / 7;
			Fraction newFraction = new Fraction(numerator, denominator);
			
			// Find the highest numerator (possibly being the first one!), such that the
			// fraction is a reduced proper fraction.
			boolean reducedFraction = false;
			while (!reducedFraction) {
				if (newFraction.getDenominator() == denominator) {
					reducedFraction = true;
				} else {
					newFraction = new Fraction(--numerator, denominator);
				}
			}
		
			// Is this fraction larger than our previous best result? If so, save it!
			if (newFraction.compareTo(largestFraction) > 0) {
				largestFraction = newFraction;
			}			
		}
		
		System.out.println(largestFraction.toString());
	}

}
