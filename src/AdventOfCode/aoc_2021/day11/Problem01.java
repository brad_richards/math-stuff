package AdventOfCode.aoc_2021.day11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem01 {
	private static int[][] octopi = new int[10][10];

	public static void main(String[] args) throws IOException {
		readInput();

		// Problem 1: How many flashes during 100 iterations?
		int flashes = 0;
		for (int i = 0; i < 100; i++) {
			flashes += iterate();
		}
		System.out.println("Part 1: " + flashes);
	}

	private static int iterate() {
		// Raise all energies by 1
		for (int y = 0; y < 10; y++) {
			for (int x = 0; x < 10; x++) {
				octopi[y][x]++;
			}
		}

		// Flash the octopi until they stop
		// Flashed octopi are set to a negative value to prevent repeat flashing
		int totalFlashes = 0;
		int flashesThisRound = -1;
		while (flashesThisRound != 0) {
			flashesThisRound = 0;
			for (int y = 0; y < 10; y++) {
				for (int x = 0; x < 10; x++) {
					if (octopi[y][x] > 9) {
						octopi[y][x] = -999999;
						flashesThisRound++;

						// Update surrounding octopi
						for (int i = -1; i <= 1; i++) {
							for (int j = -1; j <= 1; j++) {
								int yy = y + i;
								int xx = x + j;
								if (!(i == 0 && j == 0) && yy >= 0 && yy <= 9 && xx >= 0 && xx <= 9) {
									octopi[yy][xx]++;
								}
							}
						}
					}
				}
			}
			totalFlashes += flashesThisRound;
		}
		
		// Optional: Print the flash pattern
//		for (int y = 0; y < 10; y++) {
//			for (int x = 0; x < 10; x++) {
//				System.out.print(octopi[y][x] < 0 ? '*' : '.');
//			}
//			System.out.println();
//		}
//		System.out.println();
		

		// Set all negative values to zero
		for (int y = 0; y < 10; y++) {
			for (int x = 0; x < 10; x++) {
				if (octopi[y][x] < 0) octopi[y][x] = 0;
			}
		}

		return totalFlashes;
	}

	private static void readInput() throws IOException {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		ArrayList<String> lines = new ArrayList<>();
		while (in.hasNextLine()) {
			lines.add(in.nextLine());
		}
		in.close();

		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				octopi[i][j] = line.charAt(j) - '0';
			}
		}
	}
}
