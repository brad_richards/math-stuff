package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Primes;



public class Problem003 {

	public static void main(String[] args) {
		long valToTest = 600851475143l;
		int maxPrime = (int) Math.sqrt(valToTest);
		
		// Pregenerate too many primes
		Primes.generatePrimesErastothenes(maxPrime/2);
		
		// Factor the number
		ArrayList<Long> primeFactors = Primes.primeFactors(valToTest);
		
		// The largest prime factor is the last one
		System.out.println("Answer = " + primeFactors.get(primeFactors.size()-1));
	}

}
