package ch.kri.brad.projecteuler;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Any triangle meeting the stated conditions is in fact based on a pythagorean triple. For example,
 * the 5,5,6 triangle is two 3,4,5 triangles back-to-back.
 * 
 * Hence, we need to generate all possible pythagorean triples that meet a narrow set of conditions,
 * namely, c = 2a +/- 1 _or_ c = 2b +/- 1
 * 
 * The formulae for generating pythagorian triples begin with two coprime numbers, m and n, one of
 * which is even, and m > n. We can derive that, given m, either
 * 
 * n = sqrt((m * m +/- 1)/3) or n = 2 * m - sqrt(3 * m * m +/- 1)
 * 
 * Where we obviously require integers throughout. Furthermore, the limit on the total circumference
 * of 10^9 means than M cannot be greater than 18257 (and likely smaller).
 */
public class Problem094 {

	public static void main(String[] args) {
		// For quick reference, we need a table of all squares up to 3 * m ^ 2
		HashMap<Integer, Integer> squares = new HashMap<>(40000);
		for (int i = 1; i < 20000; i++)
			squares.put(i * i, i);

		long circumference = 0;
		for (int m = 2; m <= 18257; m++) {
			// First option
			int n1 = 0;
			if ((m * m + 1) % 3 == 0) n1 = (m * m + 1) / 3;
			else if ((m * m - 1) % 3 == 0) n1 = (m * m - 1) / 3;
			if (n1 != 0 && squares.containsKey(n1)) {
				int n = squares.get(n1);
				int a = m * m - n * n;
				int c = m * m + n * n;
				int b = 2 * m * n;
				System.out.println("Using a: (" + m + ", " + n + "), " + a + ", " + b + ", " + c);
				int circ = 2 * c + 2 * a;
				if (circ < 1000000000) circumference += circ;
				else System.out.println("too large");
			}

			// Second option
			int n2 = 0;
			if (squares.containsKey(3 * m * m + 1)) n2 = 3 * m * m + 1;
			else if (squares.containsKey(3 * m * m - 1)) n2 = 3 * m * m - 1;
			if (n2 != 0) {
				int n = 2 * m - squares.get(n2);
				int b = 2 * m * n;
				int c = m * m + n * n;
				int a = m * m - n * n;
				System.out.println("Using b: (" + m + ", " + n + "), " + a + ", " + b + ", " + c);
				int circ = 2 * c + 2 * b;
				if (circ < 1000000000) circumference += circ;
				else System.out.println("too large");
			}
		}
		System.out.println(circumference);
	}

}
