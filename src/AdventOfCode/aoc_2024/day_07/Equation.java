package AdventOfCode.aoc_2024.day_07;

import java.util.ArrayList;
import java.util.List;

public class Equation {
    private final long answer;
    private long[] operands;
    private char[] operators;

    private char[] availableOperators = new char[]{ '+', '*', 'c'}; // c for concatenation


    public Equation(long answer) {
        this.answer = answer;
    }


    /** Brute-force attempt to find a combination of operators that works */
    public boolean search() {
        return search(0);
    }
    private boolean search(int index) {
        if (index > operators.length - 1) { // base case
            return solve();
        } else {
            for (int j = 0; j < availableOperators.length; j++) {
                operators[index] = availableOperators[j];
                if (search(index + 1)) return true;
            }
        }
        return false;
    }

    /** Evaluate the equation using the current set of operators
     *
     * For implementation of the concatenation operator it is important
     * to note that an operand of 0 never occurs in the data
     */
    private boolean solve() {
        long value = operands[0];
        for (int i = 1; i < operands.length; i++) {
            if (operators[i-1] == '+') {
                value += operands[i];
            } else if (operators[i-1] == '*') {
                value *= operands[i];
            } else if (operators[i-1] == 'c') {
                long o = operands[i];
                if (o == 0) throw new RuntimeException("Value 0 found!");
                long max = Long.MAX_VALUE / 10;
                while (o > 0) {
                    if (value > max) throw new RuntimeException("Concatenation exceeds maximum");
                    value *= 10;
                    o = o / 10;
                }
                value += operands[i];
            } else {
                System.out.println("Invalid operator");
            }
            if (value < 0) throw new RuntimeException("Negative value found!");
        }
        return value == answer;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < operands.length-1; i++) {
            sb.append(operands[i]);
            sb.append(" ");
            sb.append(operators[i]);
            sb.append(" ");
        }
        sb.append(operands[operands.length-1]);
        sb.append(" = ");
        sb.append(answer);
        return sb.toString();
    }

    // --- Getters and Setters ---

    public long getAnswer() {
        return answer;
    }

    public long[] getOperands() {
        return operands;
    }

    public void setOperands(long[] operands) {
        this.operands = operands;
        operators = new char[operands.length - 1];
    }

    public char[] getOperators() {
        return operators;
    }

    public char[] getAvailableOperators() {
        return availableOperators;
    }
}
