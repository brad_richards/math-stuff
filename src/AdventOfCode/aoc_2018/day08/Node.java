package AdventOfCode.aoc_2018.day08;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Node {
	private final ArrayList<Node> childNodes = new ArrayList<>();
	private final ArrayList<Integer> metadata = new ArrayList<>();

	public int getValue() {
		int value = 0;
		if (childNodes.size() == 0) {
			value= this.getMetadataSum();
		} else {
			for (Integer childID : metadata) {
				if (childID > 0 && childID <= childNodes.size())
					value += childNodes.get(childID-1).getValue();
			}
		}
		return value;
	}

	public void addChild(Node child) {
		this.childNodes.add(child);
	}

	public ArrayList<Node> getChildren() {
		return childNodes;
	}

	public void addMetadata(Integer metadatum) {
		this.metadata.add(metadatum);
	}

	public ArrayList<Integer> getMetadata() {
		return metadata;
	}

	public int getMetadataSum() {
		return metadata.stream().collect(Collectors.summingInt(Integer::intValue));
	}
}
