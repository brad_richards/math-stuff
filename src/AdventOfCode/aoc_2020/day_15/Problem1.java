package AdventOfCode.aoc_2020.day_15;

import java.io.IOException;

public class Problem1 {
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		NumberList1 numbers = new NumberList1(2020, ir.getInitialNumbers());
		numbers.fillNumbers();
		System.out.println("Answer = " + numbers.getByPos(2019)); // not 2020, because 0-based
	}
}
