package AdventOfCode.aoc_2021.day03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Seems simpler to just work with strings, until the very end...
 */
public class Problem01 {
	private static List<String> numbers;

	public static void main(String[] args) throws IOException {
		readInput();
		
		int len = numbers.get(0).length();
		int[] numOnes = new int[len];
		for (String number : numbers) {
			for (int i = 0; i < len; i++) {
				if (number.charAt(i) == '1') numOnes[i]++;
			}
		}
		
		int gammaRate = 0;
		int epsilonRate = 0;
		int threshold = numbers.size()/2;
		for (int i = 0; i < len; i++) {
			gammaRate *= 2;
			epsilonRate *= 2;
			if (numOnes[i] > threshold)
				gammaRate += 1;
			else
				epsilonRate += 1;
		}
		
		System.out.println(gammaRate * epsilonRate);
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem01.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			numbers = stream.collect(Collectors.toList());
		}
	}
}
