package AdventOfCode.aoc_2024.day_09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();

		int[] entries = getEntries(lines.get(0));
		int diskSize = sumEntries(entries);
		int[] diskMap = createDiskMap(diskSize, entries);
		printDiskmap(diskMap);
		defrag(diskMap);
		printDiskmap(diskMap);
		long checksum = checksum(diskMap);
		System.out.println(checksum);
	}

	private static long checksum(int[] diskMap) {
		long checksum = 0;
		for (int i = 0; i < diskMap.length; i++) {
			checksum += diskMap[i] == -1 ? 0 : diskMap[i] * i;
		}
		return checksum;
	}

	private static void defrag(int[] diskMap) {
		int cursorStart = 0;
		int cursorEnd = diskMap.length - 1;

		while (cursorStart < cursorEnd) {
			if (diskMap[cursorStart] != -1) {
				cursorStart++;
			} else if (diskMap[cursorEnd] == -1) {
				cursorEnd--;
			} else {
				diskMap[cursorStart] = diskMap[cursorEnd];
				diskMap[cursorEnd] = -1;
			}
		}
	}

	private static void printDiskmap(int[] diskMap) {
		for (int d : diskMap) System.out.print(d >= 0 ? d : ".");
		System.out.println();
	}

	// We use -1 for free space
	private static int[] createDiskMap(int diskSize, int[] entries) {
		int[] diskMap = new int[diskSize];
		int cursor = 0;
		int id = 0;
		boolean isFreeSpace = false;
		for (int entry: entries) {
			for (int i = 0; i < entry; i++) {
				if (isFreeSpace) {
					diskMap[cursor+i] = -1;
				} else {
					diskMap[cursor+i] = id;
				}
			}
			cursor += entry;
			if (isFreeSpace) id++;
			isFreeSpace = !isFreeSpace;
		}
		return diskMap;
	}

	private static int sumEntries(int[] entries) {
		int sum = 0;
		for (int value : entries) sum += value;
		return sum;
	}

	private static int[] getEntries(String line) {
		char[] chars = line.toCharArray();
		int[] ints = new int[chars.length];
		for (int i = 0; i < chars.length; i++) {
			ints[i] = chars[i] - '0';
		}
		return ints;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}