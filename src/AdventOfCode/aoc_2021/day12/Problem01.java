package AdventOfCode.aoc_2021.day12;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * A "path" is nothing but a list of rooms (Strings). An Edge is a pair of
 * rooms, and is bidirectional.
 */
public class Problem01 {
	private static ArrayList<Edge> edges = new ArrayList<>();
	private static ArrayList<ArrayList<String>> paths = new ArrayList<>();
	private static final String START = "start";
	private static final String END = "end";
	private static int pathCount = 0;

	private static class Edge {
		String r1;
		String r2;
		public Edge(String r1, String r2) { this.r1 = r1; this.r2 = r2; }
	}

	public static void main(String[] args) throws IOException {
		readInput();
		
		ArrayList<String> initialPath = new ArrayList<>();
		initialPath.add(START);
		findPaths(initialPath);
	}
	
	private static void findPaths(ArrayList<String> initialPath) {
		ArrayList<ArrayList<String>> paths = new ArrayList<>();
		paths.add(initialPath);
		while (!paths.isEmpty()) {
			ArrayList<String> nextPath = paths.remove(0);
			String lastRoom = nextPath.get(nextPath.size()-1);
			ArrayList<Edge> nextEdges = getEdges(lastRoom);
			ArrayList<ArrayList<String>> morePaths = extendPath(nextPath, nextEdges);
			for (ArrayList<String> morePath : morePaths) paths.add(morePath);
		}
		System.out.println("Total paths = " + pathCount);
	}

	private static ArrayList<Edge> getEdges(String room) {
		ArrayList<Edge> nextEdges = new ArrayList<>();
		for (Edge edge : edges) if (edge.r1.equals(room)) nextEdges.add(edge);
		return nextEdges;
	}
	
	private static ArrayList<ArrayList<String>> extendPath(ArrayList<String> path, ArrayList<Edge> edges) {
		ArrayList<ArrayList<String>> paths = new ArrayList<>();
		for (Edge edge : edges) {
			if (edge.r2.equals(END)) {
				path.add(END);
				printPath(path);
				pathCount++;
			} else if (!Character.isLowerCase(edge.r2.charAt(0)) || !path.contains(edge.r2)) {
				ArrayList<String> newPath = (ArrayList<String>) path.clone();
				newPath.add(edge.r2);
				paths.add(newPath);
			}
		}
		return paths;
	}
	
	private static void printPath(ArrayList<String> path) {
		for (String s : path) System.out.print(s + " - ");
		System.out.println();
	}
	
	private static void readInput() throws IOException {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			String[] rooms = in.nextLine().split("-");
			edges.add(new Edge(rooms[0], rooms[1])); // Add in both directions
			edges.add(new Edge(rooms[1], rooms[0]));
		}
		in.close();
	}
}
