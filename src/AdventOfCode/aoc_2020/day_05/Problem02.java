package AdventOfCode.aoc_2020.day_05;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem02 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> codes = ir.getLines();

		ArrayList<Integer> seatIDs = new ArrayList<>();
		for (String code : codes) {
			BoardingPass bp = new BoardingPass(code);
			seatIDs.add(bp.getSeatID());
		}
		Collections.sort(seatIDs);
		
		// Look for a suitable gap
		boolean found = false;
		int mySeat = -1;
		for (int i = 1; !found && i < seatIDs.size(); i++) {
			int nextSeat = seatIDs.get(i);
			int lastSeat = seatIDs.get(i-1);
			if (nextSeat - lastSeat == 2) {
				mySeat = lastSeat+1;
				found = true;
			}
		}
		
		System.out.println("My seat is " + mySeat);
	}

}
