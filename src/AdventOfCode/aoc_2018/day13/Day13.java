package AdventOfCode.aoc_2018.day13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

/**
 * The "Track" class defines a piece of track, with booleans specifying allowed transitions (up, down, left, right). From the input
 * we construct the Track[][]
 * 
 * The "Cart" class defines a cart with a unique ID and an X,Y position. Carts are in an array, sorted by their position.
 * 
 * We move all carts, then resort the array for the next round. However, after moving each cart, we check for collisions.
 * 
 * For part 1, we just need to report the position of the first collision
 */
public class Day13 {
	private static Track[][] playground;
	private static ArrayList<Cart> carts;
	
	public static void main(String[] args) {
		InputReader in = new InputReader();
		in.readInputs();
		playground = in.getPlayground();
		carts = in.getCarts();
		
//		printStatus(); System.out.println();

		while (carts.size() > 1) {
			Collections.sort(carts);
			
			// Move all carts, marking them as "crashed" if appropriate. Crashed carts are then ignored
			for (int i = 0; i < carts.size(); i++) {
				Cart c = carts.get(i);
				c.move(playground);
				if (c.isCollision(carts)) {
					System.out.println("Collision at x,y = " + c.getX() + ", " + c.getY());
				}
			}
			
			// Remove crashed carts. We did not do this earlier, so as to avoid complications with iteration
			for (Iterator<Cart> i = carts.iterator(); i.hasNext();) {
				Cart c = i.next();
				if (c.isCrashed()) i.remove();
			}
		}
		
		System.out.println("Last cart remaining at " + carts.get(0).getX() + ", " + carts.get(0).getY());
	}

	private static void printStatus() {
		ArrayList<StringBuffer> lines = new ArrayList<>();
		for (int row = 0; row < playground.length; row++) {
			StringBuffer buf = new StringBuffer();
			for (int col = 0 ; col < playground[0].length; col++) {
				buf.append(playground[row][col].toString());
			}
			lines.add(buf);
		}
		
		for (Cart c : carts) {
			lines.get(c.getY()).setCharAt(c.getX(),  c.toString().charAt(0));
		}
		
		for (StringBuffer buf : lines) System.out.println(buf.toString());
	}
	
}
