package LeetCode.reverse_integer;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
//        System.out.println(solution.reverse(23));
//        System.out.println(solution.reverse(-23));
//        System.out.println(solution.reverse(0));
//        System.out.println(solution.reverse(1));
//        System.out.println(solution.reverse(-1));
        System.out.println(solution.reverse(2000000009));
    }

    public int reverse(int in) {
        int answer = 0;
        int x = Math.abs(in);
        while (x > 0) {
            int digit = x % 10;
            x = x / 10;
            if ((Integer.MAX_VALUE - digit) / 10 < answer) return 0;
            answer = answer * 10 + digit;
        }
        return answer * (int) Math.signum(in);
    }
}