package AdventOfCode.aoc_2021.day04;

import java.util.ArrayList;
import java.util.List;

public class Board {
	private int[][] board = new int[5][5];
	private int lastNum = 0;

	/**
	 * Construct from five consecutive lines of 5 numbers each
	 */
	public Board(List<String> lines, int firstLine) {
		for (int i = 0; i < 5; i++) {
			String[] parts = lines.get(i + firstLine).trim().split("[ ]+");
			for (int j = 0; j < parts.length; j++) {
				board[i][j] = Integer.parseInt(parts[j]);
			}
		}
	}

	/**
	 * We mark numbers by making them negative
	 */
	public void mark(int num) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == num) board[i][j] = -num;
				if (board[i][j] == 0) board[i][j] = Integer.MIN_VALUE;
			}
		}
		this.lastNum = num;
	}

	/**
	 * A board has won if all numbers in a row or column are negative
	 */
	public boolean hasWon() {
		boolean hasWon = false;
		for (int i = 0; i < board.length && !hasWon; i++) {
			boolean colWon = true;
			boolean rowWon = true;
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] > 0) colWon = false;
				if (board[j][i] > 0) rowWon = false;
			}
			hasWon = colWon || rowWon;
		}
		return hasWon;
	}
	
	/**
	 * The score is the sume of all non-marked numbers multiplied by
	 * the last number marked.
	 */
	public int getScore() {
		int score = 0;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] > 0) score += board[i][j];
			}
		}
		score *= lastNum;
		return score;
	}
}
