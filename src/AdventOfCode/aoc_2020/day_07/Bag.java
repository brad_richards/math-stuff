package AdventOfCode.aoc_2020.day_07;

import java.util.ArrayList;

public class Bag {
	private final String color;
	public final ArrayList<Bag> contents;
	
	public Bag(String color) {
		this.color = color;
		this.contents = new ArrayList<>();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) return false;
		Bag b = (Bag) o;
		return this.color.equals(b.color);
	}
	
	public String getColor() {
		return color;
	}
	
	public void addContents(Bag b) {
		this.contents.add(b);
	}
	
	public ArrayList<Bag> getContents() {
		return contents;
	}
}
