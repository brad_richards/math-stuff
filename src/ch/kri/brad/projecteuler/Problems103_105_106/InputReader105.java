package ch.kri.brad.projecteuler.Problems103_105_106;

import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class InputReader105 {
	private static final String INPUT_FILE = "InputProblem105.txt";

	public ArrayList<int[]> readSets() {
		ArrayList<int[]> sets = new ArrayList<>();
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String s = in.nextLine();
				sets.add(parseSet(s));
			}
		}
		return sets;
	}

	private int[] parseSet(String line) {
		String[] strs = line.split(",");
		int[] nums = new int[strs.length];
		for (int i = 0; i < strs.length; i++)
			nums[i] = Integer.parseInt(strs[i]);
		Arrays.sort(nums);
		return nums;
	}
}
