package ch.kri.brad.projecteuler;

import java.util.ArrayList;

/**
 * This program is pretty fast, but eliminating the objects in favor of a purely procedureal program
 * would be even faster!
 */
public class Problem064 {
	private int n; // The number whose root we are finding
	private int intRoot; // The integer root of n

	public static void main(String[] args) {
		int count = 0;
		for (int i = 2; i <= 10000; i++) {
			int intRoot = intSqrt(i);
			if (intRoot * intRoot != i) {
				Problem064 p = new Problem064(i, intRoot);
				int period = p.solve();
				if (period % 2 == 1) count++;
			}
		}
		System.out.println(count);
	}

	public Problem064(int n, int intRoot) {
		this.n = n;
		this.intRoot = intRoot;
	}

	public int solve() {
		ArrayList<Digit_And_Fraction> steps = new ArrayList<>();
		Digit_And_Fraction df = new Digit_And_Fraction(intRoot, intRoot, 1);
		steps.add(df);
		int period = -1;
		while (period < 0) {
			Digit_And_Fraction next = df.nextStep();
			period = contains(steps, next);
			if (period < 0) steps.add(next);
			df = next;
		}
		return period;
	}

	/**
	 * Detects whether or not we have found a repeating state. If not, return -1. If so, return the
	 * length of the repeating cycle.
	 */
	private int contains(ArrayList<Digit_And_Fraction> steps, Digit_And_Fraction step) {
		int period = -1;
		for (int i = 0; i < steps.size() && period < 0; i++) {
			if (steps.get(i).equals(step)) period = steps.size() - i;
		}
		return period;
	}

	/**
	 * Class to represent the sum of an integer and the double-inverse of an S_Fraction
	 */

	/**
	 * We represent one step in the process as a digit, and the values a and b from the fraction:
	 * 
	 * (sqrt(n) - a) / b
	 * 
	 * For example, sqrt(23) begins with the value 4 + [ (sqrt23 - 4) / 1 ]
	 * 
	 * We represent this as digit = 4, a = 4, b = 1. The following step resolves the fraction to 1 /
	 * X,
	 * 
	 * where X is 1 + [ (sqrt23 - 3) / 7 ]
	 * 
	 * Which we represent as digit = 1, a = 3, b = 7
	 * 
	 * We can detect repetition at the point that we have two identical entries in sequence.
	 * 
	 */
	private class Digit_And_Fraction {
		private int digit;
		private int a, b;

		public Digit_And_Fraction(int digit, int a, int b) {
			this.digit = digit;
			this.a = a;
			this.b = b;
		}

		/**
		 * Method to transform this Digit_And_Fraction by one step, creating the next one in line
		 */
		public Digit_And_Fraction nextStep() {
			int new_b = (n - a * a) / b;
			int new_digit = (intRoot + a) / new_b;
			int new_a = new_digit * new_b - a;
			return new Digit_And_Fraction(new_digit, new_a, new_b);
		}

		@Override
		public boolean equals(Object obj) {
			Digit_And_Fraction df = (Digit_And_Fraction) obj;
			return (digit == df.digit && a == df.a && b == df.b);
		}

		@Override
		public String toString() {
			return digit + ", " + a + ", " + b;
		}
	}

	private static int intSqrt(int n) {
		int x = n / 2;
		do {
			x = x / 2;
		} while (x * x > n);

		while (x * x <= n)
			x++;

		return --x;
	}
}
