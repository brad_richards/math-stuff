package AdventOfCode.aoc_2022.day15;

public class Beacon {
	public final int x;
	public final int y;	
	public Beacon(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass() == this.getClass()) {
			Beacon b = (Beacon) o;
			return b.x == x && b.y == y;
		}
		return false;
	}
}
