package AdventOfCode.aoc_2020.day_10;

import static org.junit.Assert.*;

import org.junit.Test;

public class Problem02Test {

	@Test
	public void test() {
		assertTrue(Problem02.bitSet(3,0));
		assertTrue(Problem02.bitSet(3,1));
		assertTrue(!Problem02.bitSet(3,2));
		assertTrue(!Problem02.bitSet(4,0));
		assertTrue(!Problem02.bitSet(4,1));
		assertTrue(Problem02.bitSet(4,2));
		assertTrue(Problem02.bitSet(5,0));
		assertTrue(!Problem02.bitSet(5,1));
		assertTrue(Problem02.bitSet(5,2));
	}
}
