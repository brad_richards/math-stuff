package AdventOfCode.aoc_2024.day_02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<String> lines;

	public static void main(String[] args) throws Exception {
		readInput();

		int numSafe = 0;
		for (String line : lines) {
			if (isSafe(line)) numSafe++;
		}
		System.out.println(numSafe);
	}

	private static boolean isSafe(String line) {
		String[] parts = line.split(" ");
		int[] nums = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			nums[i] = Integer.parseInt(parts[i]);
		}

		boolean increasing = (nums[1] > nums[0]);
		for (int i = 0; i < nums.length-1; i++) {
			int diff = (increasing) ? nums[i+1] - nums[i] : nums[i] - nums[i+1];
			if (diff < 1 || diff > 3) return false;
		}
		return true;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}
}