package ch.kri.brad.projecteuler;

import java.util.TreeSet;

/**
 * We use a TreeSet, because some solutions can apparently be generated in
 * multiple ways.
 */
public class Problem125 {
	private static final long MAX_VALUE = 100000000;
	private static final long MAX_I = (long) Math.sqrt(MAX_VALUE / 2) + 1;
	private static final TreeSet<Long> solutions = new TreeSet<>();

	public static void main(String[] args) {

		for (long i = MAX_I; i >= 2; i--) {
			boolean tooBig = false;
			long sum = i * i;
			long current_i = i - 1;
			while (!tooBig && current_i > 0) {
				sum += current_i * current_i;
				if (sum > MAX_VALUE) {
					tooBig = true;
				} else {
					if (isPalindrome(sum)) solutions.add(sum);
					current_i--;
				}
			}
		}

		long total = 0;
		for (long s : solutions) {
			System.out.println(s);
			total += s;
		}
		System.out.println("Sum is " + total);
	}

	private static boolean isPalindrome(long in) {
		long[] digits = new long[10];
		int actualDigits = 0;
		while (in > 0) {
			digits[actualDigits++] = in % 10;
			in /= 10;
		}

		boolean isPalindrome = true;
		for (int i = 0; isPalindrome && i < actualDigits / 2; i++) {
			isPalindrome = (digits[i] == digits[actualDigits - 1 - i]);
		}
		return isPalindrome;
	}
}
