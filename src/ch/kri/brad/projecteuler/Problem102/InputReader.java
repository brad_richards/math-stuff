package ch.kri.brad.projecteuler.Problem102;

import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class InputReader {
	private static final String INPUT_FILE = "triangles.txt";

	public ArrayList<Triangle> readTriangles() {
		ArrayList<Triangle> triangles = new ArrayList<>();
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String s = in.nextLine();
				triangles.add(parseTriangle(s));
			}
		}
		return triangles;
	}

	private Triangle parseTriangle(String line) {
		String[] strs = line.split(",");
		int[] nums = new int[strs.length];
		for (int i = 0; i < strs.length; i++)
			nums[i] = Integer.parseInt(strs[i]);

		Triangle t = new Triangle(new Point(nums[0], nums[1]), new Point(nums[2], nums[3]),
				new Point(nums[4], nums[5]));

		// We need to ensure that we have read the points "counterclockwise"
		// (thanks, Tynan). We do this by summing the edges, using the formula
		// (x2 - x1)*(y2 + y1). If this result is negative, the points are counterclockwise.
		int v1 = (t.b.x - t.a.x)*(t.b.y + t.a.y);
		int v2 = (t.c.x - t.b.x)*(t.c.y + t.b.y);
		int v3 = (t.a.x - t.c.x)*(t.a.y + t.c.y);
		if ((v1 + v2 + v3) > 0) {
			Point p = t.b;
			t.b = t.c;
			t.c = p;
		}
		return t;
	}
}
