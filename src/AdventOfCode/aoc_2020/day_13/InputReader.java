package AdventOfCode.aoc_2020.day_13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputReader {
	private int timestamp;
	private String[] busIDstrings;
	private ArrayList<Integer> busIDs = new ArrayList<>();;
	
	public InputReader(String fileName) throws IOException {
		Scanner in = new Scanner(this.getClass().getResourceAsStream(fileName));
		this.timestamp = Integer.parseInt(in.nextLine());
		busIDstrings = in.nextLine().split(",");
		for (String s : busIDstrings) {
			if (!s.equals("x")) busIDs.add(Integer.parseInt(s));
		}
	}

	public int getTimestamp() {
		return timestamp;
	}

	public String[] getBusStrings() {
		return busIDstrings;
	}
	
	public ArrayList<Integer> getBusIDs() {
		return busIDs;
	}
}
