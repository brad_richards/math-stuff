package AdventOfCode.aoc_2020.day_06;

public class Group01 {
	private boolean[] answers = new boolean[26];
	
	public void addPerson(String line) {
		for (char c : line.toCharArray()) {
			answers[c - 'a'] = true;
		}
	}
	
	public int getCount() {
		int count = 0;
		for (boolean b : answers) if (b) count++;
		return count;
	}
}
