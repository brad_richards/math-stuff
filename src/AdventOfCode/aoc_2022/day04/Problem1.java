package AdventOfCode.aoc_2022.day04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	
	static List<String> lines;
	
	private static class Range {
		int start, end;
		public Range(String[] values) { 
			start = Integer.parseInt(values[0]);
			end = Integer.parseInt(values[1]);
		}
	}
	
	public static void main(String[] args) throws Exception {
		readInput();
		
		int answer = 0;
		for (String line : lines) {
			String[] parts = line.split(",");
			Range range1 = new Range(parts[0].split("-"));
			Range range2 = new Range(parts[1].split("-"));
			
			if (range1.start <= range2.start && range1.end >= range2.end
					||
					range2.start <= range1.start && range2.end >= range1.end) answer++;
		}

		System.out.println(answer);
	}
	
	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
	}

}
