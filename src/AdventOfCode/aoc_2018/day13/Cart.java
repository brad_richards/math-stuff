package AdventOfCode.aoc_2018.day13;

import java.util.ArrayList;

public class Cart implements Comparable<Cart> {
	public static enum Facing {
		UP, RIGHT, DOWN, LEFT
	}

	private static int nextID = 0;

	private int id, x, y;
	private Facing facing;
	private int intersectionsSeen = 0;
	private boolean crashed = false;

	public Cart(int x, int y, Facing facing) {
		id = nextID++;
		this.x = x;
		this.y = y;
		this.facing = facing;
	}

	/**
	 * A cart always moves one space in the direction it is facing. However, depending on the type
	 * of track it moves onto, it may change its facing. If it moves onto a corner piece, the new
	 * facing is clear (turn the corner). If a cart moves onto an intersection, it follows the
	 * pattern described in the problem: a cycle of left-straight-right.
	 */
	public void move(Track[][] playground) {
		// First move
		if (facing == Facing.UP) y--;
		else if (facing == Facing.DOWN) y++;
		else if (facing == Facing.LEFT) x--;
		else if (facing == Facing.RIGHT) x++;
		else System.out.println("Invalid facing!");

		// Now orientation
		if (playground[y][x].isIntersection()) {
			intersectionsSeen++;
			if (intersectionsSeen % 3 == 1) turnLeft();
			else if (intersectionsSeen % 3 == 0) turnRight();
		} else if ((facing == Facing.UP || facing == Facing.DOWN) && playground[y][x].goRight())
			facing = Facing.RIGHT;
		else if ((facing == Facing.UP || facing == Facing.DOWN) && playground[y][x].goLeft())
			facing = Facing.LEFT;
		else if ((facing == Facing.LEFT || facing == Facing.RIGHT) && playground[y][x].goUp())
			facing = Facing.UP;
		else if ((facing == Facing.LEFT || facing == Facing.RIGHT) && playground[y][x].goDown())
			facing = Facing.DOWN;
		else {
			// no change
		}
	}

	/**
	 * Determine whether this cart has caused a collision: iterate through all carts, and look for a
	 * cart at the same position. Here, we use the ID to eliminate this cart itself.
	 */
	public boolean isCollision(ArrayList<Cart> carts) {
		for (Cart c : carts) {
			if (c.id != this.id && c.x == this.x && c.y == this.y)  {
					this.crashed = true;
					c.crashed = true;
					return true;
			}
		}
		return false;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Facing getFacing() {
		return facing;
	}
	
	public boolean isCrashed() {
		return crashed;
	}

	public void moveUp() {
		y--;
	}

	public void moveDown() {
		y++;
	}

	public void moveLeft() {
		x--;
	}

	public void moveRight() {
		x++;
	}

	public void turnLeft() {
		int o = facing.ordinal() - 1;
		if (o < 0) o = Facing.LEFT.ordinal();
		facing = Facing.values()[o];
	}

	public void turnRight() {
		int o = facing.ordinal() + 1;
		if (o > Facing.LEFT.ordinal()) o = 0;
		facing = Facing.values()[o];
	}

	@Override
	public int compareTo(Cart c) {
		int value = Integer.compare(y, c.y);
		if (value == 0) value = Integer.compare(x, c.x);
		return value;
	}
	
	@Override
	public String toString() {
		if (facing == Facing.UP) return "^";
		else if (facing == Facing.DOWN) return "v";
		else if (facing == Facing.LEFT) return "<";
		else if (facing == Facing.RIGHT) return ">";
		else return "?";
	}

}