package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.Iterator;

public class Problem018 {
	// Ah, the joy of flexible Java arrays...
	private static int[][] pyramid = { { 75 }, { 95, 64 }, { 17, 47, 82 },
			{ 18, 35, 87, 10 }, { 20, 04, 82, 47, 65 }, { 19, 01, 23, 75, 03, 34 },
			{ 88, 02, 77, 73, 07, 63, 67 }, { 99, 65, 04, 28, 06, 16, 70, 92 },
			{ 41, 41, 26, 56, 83, 40, 80, 70, 33 },
			{ 41, 48, 72, 33, 47, 32, 37, 16, 94, 29 },
			{ 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14 },
			{ 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57 },
			{ 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48 },
			{ 63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31 },
			{ 04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23 } };

	private static class Position {
		int row;
		int col;

		Position(int row, int col) {
			this.row = row;
			this.col = col;
		}
	}

	private static class Path {
		ArrayList<Position> positions;
		int value;
		boolean complete;

		Path() {
			positions = new ArrayList<Position>();
			positions.add(new Position(0, 0));
			this.value = pyramid[0][0];
			complete = false;
		}
		
		Path(Path oldPath) {
			this.positions = new ArrayList<Position>();
			for (Position p : oldPath.positions) {
				this.positions.add(p);
			}
			this.value = oldPath.value;
			this.complete = oldPath.complete;
		}

		void addPosition(Position newPos) {
			if (!complete) {
				positions.add(newPos);
			}
			value += pyramid[newPos.row][newPos.col];
			complete = (positions.size() == pyramid.length);
		}

		Position getLastPosition() {
			return positions.get(positions.size() - 1);
		}
		
		@Override
		protected	Path clone() {
			Path newPath = new Path(this);
			return newPath;
		}
	}

	/**
	 * As a baseline, we establish an initial solution using simple hill-climbing.
	 * Afterwards, we perform a best-first search, maintaining a list of all
	 * uncompleted paths. The partial paths are expanded using breadth-first
	 * search, and at each level they are compared to our baseline solution. If a
	 * partial path is so poor that it could never exceed the value of our
	 * baseline path, it is discarded. Once all surviving paths have been
	 * completed, we search for the best of them.
	 */
	public static void main(String[] args) {
		// Get baseline path
		Path baseline = getHillclimbingPath();

		// Set up for our breadth-first search
		ArrayList<Path> paths = new ArrayList<Path>();
		paths.add(new Path());
		
		// Extend all paths repeatedly, until they are complete
		while (paths.size() > 0 && !paths.get(0).complete) {
			ArrayList<Path> newPaths = new ArrayList<Path>();
			for (Iterator<Path> i = paths.iterator(); i.hasNext(); ) {
				Path p = i.next();
				Path q = p.clone();
				Position[] nextPositions = nextPositions(p.getLastPosition());
				p.addPosition(nextPositions[0]);
				q.addPosition(nextPositions[1]);
				
				int maxDiff = (pyramid.length - p.positions.size()) * 99;
				if ((baseline.value - p.value) <= maxDiff) newPaths.add(p); 
				if ((baseline.value - q.value) <= maxDiff) newPaths.add(q); 
			}
			System.out.println("Number of partial paths: " + newPaths.size());
			paths = newPaths;
		}
			
		// Search through all paths, and find the one with the highest value;
		int bestValue = 0;
		Path bestPath = null;
		for (Path p : paths) {
			if (p.value > bestValue) {
				bestValue = p.value;
				bestPath = p;
			}			
		}
		
		System.out.println("Answer = " + bestValue);
	}

	private static Position[] nextPositions(Position fromPosition) {
		Position[] toPositions = new Position[2];
		toPositions[0] = new Position(fromPosition.row + 1, fromPosition.col);
		toPositions[1] = new Position(fromPosition.row + 1, fromPosition.col + 1);
		return toPositions;
	}

	private static int getPyramidValue(Position pos) {
		return pyramid[pos.row][pos.col];
	}

	private static Path getHillclimbingPath() {
		Path path = new Path();

		while (!path.complete) {
			Position[] nextPositions = nextPositions(path.getLastPosition());
			if (getPyramidValue(nextPositions[0]) >= getPyramidValue(nextPositions[1])) {
				path.addPosition(nextPositions[0]);
			} else {
				path.addPosition(nextPositions[1]);
			}
		}
		return path;
	}

}
