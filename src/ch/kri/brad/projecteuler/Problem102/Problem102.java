package ch.kri.brad.projecteuler.Problem102;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Basic idea: If we have triangle ABC and a point P inside the triangle, P is inside the triangle
 * iff the origin is "to the left" of all edges, as we move counterclockwise around the triangle.
 * For this purpose, for each side:
 * 
 * - Create a vector pointing "left"
 * 
 * - Create a vector from the starting-point of hat side to the origin (this is just the inverse of
 * the point coordinates)
 * 
 * - Calculate the dot product (effectively, the common component of these vectors)
 * 
 * - If the dot-product is positive, then the origin is on the "left"
 *
 * See "3rd method" in this article for the relevant equations:
 * http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html
 * 
 * Note that we abuse the Point class to represent vectors...
 */
public class Problem102 {
	

	public static void main(String[] args) {
		InputReader in = new InputReader();
		ArrayList<Triangle> triangles = in.readTriangles();
		int count = 0;
		int tmp = 0;
		for (Triangle t : triangles) {
			boolean result = triangleContainsOrigin(t);
			if (result) count++;
		}
		System.out.println(count);
	}

	public static boolean triangleContainsOrigin(Triangle t) {
		Point vector1 = new Point(t.a.y - t.b.y, t.b.x - t.a.x);
		Point vector2 = new Point(t.b.y - t.c.y, t.c.x - t.b.x);
		Point vector3 = new Point(t.c.y - t.a.y, t.a.x - t.c.x);

		Point vectorA = new Point(-t.a.x, -t.a.y);
		Point vectorB = new Point(-t.b.x, -t.b.y);
		Point vectorC = new Point(-t.c.x, -t.c.y);

		// Dot products
		double dot1A = vector1.x * vectorA.x + vector1.y * vectorA.y;
		double dot2B = vector2.x * vectorB.x + vector2.y * vectorB.y;
		double dot3C = vector3.x * vectorC.x + vector3.y * vectorC.y;

		return dot1A >= 0 && dot2B >= 0 && dot3C >= 0;
	}
}
