package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Digits;
import ch.kri.brad.utility.FractionBigInteger;

public class Problem065 {

	/**
	 * Calculate from the last term to the first. We manage all fractions as the ratio of two
	 * integers, using our existing class for handling BigInteger fractions class, and perform all
	 * operations using this class as well.
	 */
	public static void main(String[] args) {
		final int NUM_TERMS = 100; // NUM_TERMS % 3 must equal 1

		// Calculate the first terms, using the weird pattern given.
		int[] values = new int[NUM_TERMS];
		values[0] = 2;
		for (int i = 0; i < NUM_TERMS / 3; i++) {
			values[i * 3 + 1] = 1;
			values[i * 3 + 2] = 2 * (i + 1);
			values[i * 3 + 3] = 1;
		}

		// Initialize our fraction with zero
		FractionBigInteger f = new FractionBigInteger(values[NUM_TERMS - 1], 1);
		for (int i = NUM_TERMS - 2; i >= 0; i--) {
			f = f.inverse();
			f = f.add(values[i]);
		}
		System.out.println(f + " : sum of digits is " + Digits.sumOfDigits(f.getNumerator()));
	}
}
