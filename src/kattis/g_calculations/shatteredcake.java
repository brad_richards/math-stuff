package kattis.g_calculations;

import java.util.Scanner;

public class shatteredcake {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int width = in.nextInt();
		int pieces = in.nextInt();
		int totalArea = 0;
		
		for (int i = 0; i < pieces; i++) {
			int w = in.nextInt();
			int h = in.nextInt();
			totalArea += w * h;
		}

		int length = totalArea / width;
		System.out.println(length);
	}

}
