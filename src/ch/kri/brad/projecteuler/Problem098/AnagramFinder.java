package ch.kri.brad.projecteuler.Problem098;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class AnagramFinder {
	public ArrayList<AnagramPair> findAnagrams(ArrayList<String> words) {
		// Sort the words into groups that could be anagrams
		HashMap<Integer, ArrayList<String>> wordGroups = findWordGroups(words);
		System.out.println("Number of word groups: " + wordGroups.size());

		// Find anagrams within each group.
		ArrayList<AnagramPair> anagrams = findAnagrams(wordGroups);
		System.out.println("Number of anagram groups: " + anagrams.size());

//		System.out.println();
//		for (AnagramPair ag : anagrams) {
//			System.out.println(ag.getWord1() + " " + ag.getWord2());
//		}

		return anagrams;
	}
	
	/**
	 * Sort words into groups that could possible be anagrams. We do this by
	 * creating a key for each word: the length of the word multiplied by 10000
	 * plus the sum of the letter-values in the word (A = 1, etc.).
	 */
	private HashMap<Integer, ArrayList<String>> findWordGroups(ArrayList<String> words) {
		HashMap<Integer, ArrayList<String>> wordGroups = new HashMap<>();
		for (String word : words) {
			Integer key = wordKey(word);
			if (!wordGroups.containsKey(key)) {
				ArrayList<String> value = new ArrayList<>();
				wordGroups.put(key, value);
			}
			ArrayList<String> wordsInGroup = wordGroups.get(key);
			wordsInGroup.add(word);
		}
		return wordGroups;
	}

	private static Integer wordKey(String word) {
		int sum = 0;
		for (int i = 0; i < word.length(); i++) {
			sum += word.charAt(i) - 'A';
		}
		return word.length() * 10000 + sum;
	}

	/**
	 * For all words in each group, we then sort and compare their letters. Any
	 * words with identical letters are anagrams. The words that are anagrams
	 * are then placed into ArrayLists
	 * 
	 * Note that it is possible, if unlikely, for a word-group to contain more
	 * than one set of anagrams. Also, this is the first point at which we
	 * filter out groups containing only a single word.
	 */
	private ArrayList<AnagramPair> findAnagrams(HashMap<Integer, ArrayList<String>> wordGroups) {
		ArrayList<AnagramPair> anagrams = new ArrayList<>();
		for (Integer key : wordGroups.keySet()) {
			ArrayList<String> wordGroup = wordGroups.get(key);
			ArrayList<AnagramPair> anagramGroup = findAnagramGroups(wordGroup);
			for (AnagramPair anagram : anagramGroup)
				anagrams.add(anagram);
		}
		return anagrams;
	}

	/**
	 * Here, we take a group of candidate words, and look for words that really
	 * are anagrams. Single words are discarded.
	 * 
	 * If we find a group of more than two words (there is one such group in the
	 * input data), we generate all possible pairs from that group.
	 */
	private ArrayList<AnagramPair> findAnagramGroups(ArrayList<String> wordGroup) {
		ArrayList<AnagramPair> anagramGroups = new ArrayList<>();
		if (wordGroup.size() > 1) {
			// Create a parallel list of the words, but with the letters sorted
			ArrayList<String> parallel = new ArrayList<>();
			for (String word : wordGroup) {
				char[] chars = word.toCharArray();
				Arrays.sort(chars);
				String sortedLetters = new String(chars);
				parallel.add(sortedLetters);
			}

			// Pull out groups of words, until none are left
			while (!wordGroup.isEmpty()) {
				ArrayList<String> anagramGroup = new ArrayList<>();
				anagramGroup.add(wordGroup.remove(0));
				String matcher = parallel.remove(0);
				for (int i = wordGroup.size() - 1; i >= 0; i--) {
					if (parallel.get(i).equals(matcher)) {
						anagramGroup.add(wordGroup.remove(i));
						parallel.remove(i);
					}
				}
				if (anagramGroup.size() == 2) anagramGroups.add(new AnagramPair(anagramGroup.get(0), anagramGroup.get(1)));
				else if (anagramGroup.size() > 2) {
					for (int i = 0; i < anagramGroup.size()-1; i++) {
						for (int j = i+1; j < anagramGroup.size(); j++) {
							anagramGroups.add(new AnagramPair(anagramGroup.get(i), anagramGroup.get(j)));
						}
					}
				}
			}
		}
		return anagramGroups;
	}	
}
