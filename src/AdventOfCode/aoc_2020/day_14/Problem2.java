
package AdventOfCode.aoc_2020.day_14;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * We have no way in Java to allocate enough memory to cover a 36-bit address space. So we have to work
 * with - effectively - a sparse array. We do this via a HashMap.
 */
public class Problem2 {
	private static HashMap<Long, Long> memory = new HashMap<>();
	
	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Instruction> instructions = ir.getInstructions();
		
		// Run the program
		executeProgram(instructions);
		
		// Sum memory for the final result
		long answer = sumMemory();
		System.out.println("\nAnswer = " + answer);
	}

	private static void executeProgram(List<Instruction> instructions) {
		Mask currentMask = null;
		for (Instruction instruction : instructions) {
			if (instruction.getClass() == Mask.class) {
				currentMask = (Mask) instruction;
			} else { // Must be an assignment
				Assignment assignment = (Assignment) instruction;
				for (long address : currentMask.getAddresses(assignment.getAddress())) {
					memory.put(address, assignment.getValue());
				}
			}
		}
	}
	
	private static long sumMemory() {
		long sum = 0;
		for (long value : memory.values()) sum += value;
		return sum;
	}
}
