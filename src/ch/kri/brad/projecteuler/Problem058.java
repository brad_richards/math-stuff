package ch.kri.brad.projecteuler;

import ch.kri.brad.utility.Primes;

public class Problem058 {

	/**
	 * Gawd, really? Side length nearly 30k, meaning we need 900 million primes?
	 */
	public static void main(String[] args) {
		Primes.generatePrimesErastothenes(900000000, false, false);
		boolean[] primes = Primes.getRawSieve();
		
		int totalNumbers = 1; // Because of "1" in the center
		int totalPrimes = 0;
		int n = 1;
		for (int sideLength = 3; sideLength < 30000; sideLength += 2) {
		  int increment = sideLength - 1;
		  for (int side = 1; side <= 4; side++) {
		    n += increment;
		    if (primes[n-2]) totalPrimes++;
		  }
		  totalNumbers += 4;
		  System.out.println(sideLength + ", " + totalPrimes + ", " + totalNumbers);
		  if ((totalPrimes * 10) < totalNumbers) {
		    System.out.println("Answer: " + sideLength);
		    break;
		  }
		}
	}

}
