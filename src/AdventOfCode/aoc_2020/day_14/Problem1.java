package AdventOfCode.aoc_2020.day_14;

import java.io.IOException;
import java.util.List;

public class Problem1 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<Instruction> instructions = ir.getInstructions();
		
		// Find the highest memory address used, and dimension memory
		// to this size
		int highestAddress = getHighestAddress(instructions);
		long[] memory = new long[highestAddress+1];
		
		// Run the program
		executeProgram(memory, instructions);
		
		// Sum memory for the final result
		long answer = sumMemory(memory);
		System.out.println("Answer = " + answer);
	}
	
	private static int getHighestAddress(List<Instruction> instructions) {
		int highest = -1;
		for (Instruction instruction : instructions) {
			if (instruction.getClass() == Assignment.class) {
				Assignment assignment = (Assignment) instruction;
				if (assignment.getAddress() > highest) highest = assignment.getAddress();
			}
		}
		return highest;
	}

	private static void executeProgram(long[] memory, List<Instruction> instructions) {
		Mask currentMask = null;
		for (Instruction instruction : instructions) {
			if (instruction.getClass() == Mask.class) {
				currentMask = (Mask) instruction;
			} else { // Must be an assignment
				Assignment assignment = (Assignment) instruction;
				memory[assignment.getAddress()] = currentMask.applyMask(assignment.getValue());
			}
		}
	}
	
	private static long sumMemory(long[] memory) {
		long sum = 0;
		for (long value : memory) sum += value;
		return sum;
	}
}
