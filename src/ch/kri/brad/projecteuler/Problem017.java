package ch.kri.brad.projecteuler;

public class Problem017 {
  public static String[] digits = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
  public static String[] teens = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
  public static String[] tens = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
  public static String hundred = "hundred";
  public static String word_and = "and";
  public static String thousand = "onethousand";
	
	/**
	 * This is a problem that is easier by hand, than by program. The program
	 * only serves to document the calculation...
	 */
	public static void main(String[] args) {
		// Calculate the letters for all numbers 1-99
		int numLettersForDigits = numLettersInArray(digits);
		int numLettersForTeens = numLettersInArray(teens);
		int numLettersForTens = numLettersInArray(tens);
		int numLettersTo99 = 9 * numLettersForDigits + numLettersForTeens + 10 * numLettersForTens;
		
		// Extend this result for the hundreds
		int numLettersForLastTwoDigits = 10 * numLettersTo99;
		int numLettersForHundreds = 100 * numLettersForDigits + 900 * hundred.length();
		int numLettersForAnds = 9 * 99 * word_and.length();
		int numLettersForOneThousand = thousand.length();
		
		int answer = numLettersForLastTwoDigits + numLettersForHundreds + numLettersForAnds + numLettersForOneThousand;
		System.out.println("Answer = " + answer);
	}

	private static int numLettersInArray(String[] in) {
		int num = 0;
		for (String s : in) {
			num += s.length();
		}
		return num;
	}
}
