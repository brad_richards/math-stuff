package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.HashSet;


public class Problem026 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int bestValue = 0;
		int longestCycle = 0;
		for (int value = 1; value < 1000; value++) {
			int cycle = cycleLength(value);
			if (cycle > longestCycle) {
				longestCycle = cycle;
				bestValue = value;
				System.out.println("Divisor " + value + " has a cycle length of " + cycle);
			}
		}
		System.out.println("Answer = " + bestValue);
	}
	
	private static int cycleLength(int divisor) {
		ArrayList<Integer> remainders = new ArrayList<Integer>();
		int length = 0;
		int remainder = 10 % divisor;
		while (remainder > 0 && !remainders.contains(remainder)) {
			remainders.add(remainder);
			int dividend = remainder * 10;
			remainder = dividend % divisor;
		}
		if (remainder != 0) {
			// Count the number of digits in the cycle
			length = remainders.size() - remainders.indexOf(remainder);
		}
		return length;
	}

}
