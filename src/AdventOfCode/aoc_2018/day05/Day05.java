package AdventOfCode.aoc_2018.day05;

import java.io.InputStream;
import java.util.Scanner;

public class Day05 {

	public static void main(String[] args) {
		String polymer = readPolymer();
		polymer = react(polymer);
		System.out.println("Part 1: polymer length after all reactions is " + polymer.length());

		int shortest = partTwo(polymer);
		System.out.println("Part 2: shortest polymer length is " + shortest);
	}

	/**
	 * Read the polymer
	 */
	private static String readPolymer() {
		String polymer;
		InputStream fis = Day05.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			polymer = in.nextLine().trim();
		}
		System.out.println("Characters in polymer: " + polymer.length());
		return polymer;
	}

	/**
	 * For the reaction: it does not matter in which order they occur, as long as all are carried
	 * out. Hence, we just process the entire polymer from start to finish, and then repeat the
	 * process until the length no longer changes.
	 */
	private static String react(String polymer) {
		int lastLength = -1;
		StringBuffer reactor = new StringBuffer(polymer);
		int currentLength = reactor.length();
		while (lastLength != currentLength) {
			lastLength = currentLength;
			int pos = 0;
			while (pos < reactor.length() - 1) {
				if (isReaction(reactor.charAt(pos), reactor.charAt(pos + 1))) {
					// Remove these two character, leaving "pos" unchanged
					reactor.delete(pos, pos + 2);
				} else {
					pos++;
				}
			}
			currentLength = reactor.length();
		}
		return reactor.toString();
	}

	private static boolean isReaction(char a, char b) {
		return (Math.abs(a - b) == 32);
	}

	/**
	 * Part two basically tries the code from part 1 26 times - once for each letter of the
	 * alphabet. Note that we don't actually have to return _which_ letter produces the best result.
	 */
	private static int partTwo(String polymer) {
		int shortest = polymer.length();
		for (char c = 'A'; c <= 'Z'; c++) {
			String tmpPolymer = adaptPolymer(polymer, c);
			String tmpResult = react(tmpPolymer);
			if (tmpResult.length() < shortest) shortest = tmpResult.length();
		}
		return shortest;
	}

	/**
	 * Remove the given character (upper and lower case) from the string, and return the result. The
	 * incoming character is upper case.
	 */
	private static String adaptPolymer(String in, char c) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < in.length(); i++) {
			if (in.charAt(i) != c && in.charAt(i) != (c + 32)) buf.append(in.charAt(i));
		}
		return buf.toString();
	}
}
