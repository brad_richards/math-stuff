package AdventOfCode.aoc_2021.day15;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem02b {
	private static int[][] map;
	private static int[][] costs;
	private static boolean[][] visited;
	private static int mapSize;
	private static Point startPoint = new Point(0, 0);
	private static Point endPoint;

	public static void main(String[] args) {
		readInput();

		Point p = findUnvisited();
		while (p != null) {
			if (p.y > 0 && !visited[p.y - 1][p.x])
				costs[p.y - 1][p.x] = Math.min(costs[p.y - 1][p.x], costs[p.y][p.x] + map[p.y - 1][p.x]);
			if (p.x > 0 && !visited[p.y][p.x - 1])
				costs[p.y][p.x - 1] = Math.min(costs[p.y][p.x - 1], costs[p.y][p.x] + map[p.y][p.x - 1]);
			if (p.y < endPoint.y && !visited[p.y + 1][p.x])
				costs[p.y + 1][p.x] = Math.min(costs[p.y + 1][p.x], costs[p.y][p.x] + map[p.y + 1][p.x]);
			if (p.x < endPoint.x && !visited[p.y][p.x + 1])
				costs[p.y][p.x + 1] = Math.min(costs[p.y][p.x + 1], costs[p.y][p.x] + map[p.y][p.x + 1]);
			visited[p.y][p.x] = true;

			p = findUnvisited();
		}

		System.out.println("Cost to end-point (p2): " + costs[endPoint.y][endPoint.x]);
	}

	private static Point findUnvisited() {
		Point p = null;
		for (int y = 0; y < mapSize; y++) {
			for (int x = 0; x < mapSize; x++) {
				if (!visited[y][x])
					if (p == null || costs[y][x] < costs[p.y][p.x]) p = new Point(x, y);
			}
		}
		return p;
	}

	/**
	 * Read input and initialize our data structures
	 * 
	 * For simplicity, we assume that the map is square
	 */
	private static void readInput() {
		Scanner in = new Scanner(Problem02b.class.getResourceAsStream("input.txt"));
		ArrayList<String> lines = new ArrayList<>();
		while (in.hasNextLine()) {
			lines.add(in.nextLine());
		}
		int dataSize = lines.size();
		mapSize = dataSize * 5;

		map = new int[mapSize][mapSize];
		costs = new int[mapSize][mapSize];
		visited = new boolean[mapSize][mapSize];
		for (int y = 0; y < dataSize; y++) {
			String line = lines.get(y);
			for (int x = 0; x < dataSize; x++) {
				map[y][x] = line.charAt(x) - '0';
				// Now propagate into the other 24 positions
				for (int ym = 0; ym <= 4; ym++) {
					for (int xm = 0; xm <= 4; xm++) {
						map[dataSize * ym + y][dataSize * xm + x] = (map[y][x] - 1 + xm + ym) % 9 + 1;
						costs[dataSize * ym + y][dataSize * xm + x] = Integer.MAX_VALUE;
						visited[dataSize * ym + y][dataSize * xm + x] = false;
					}
				}
			}
		}

		costs[0][0] = 0; // Starting position
		endPoint = new Point(mapSize - 1, mapSize - 1);
	}

}
