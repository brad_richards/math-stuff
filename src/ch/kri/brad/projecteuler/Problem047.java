package ch.kri.brad.projecteuler;
import java.util.ArrayList;
import java.util.HashSet;

import ch.kri.brad.utility.Primes;



public class Problem047 {

	public static void main(String[] args) {
		boolean done = false;
		int numInARow = 0;
		long numToTest = 1;
		
		// Pregenerate primes
		Primes.generatePrimesErastothenes(1000000);
		
		while (!done) {
			numToTest++;
			ArrayList<Long> primeFactors = Primes.primeFactors(numToTest);
			int numPrimeFactors = distinctPrimeFactors(primeFactors);
			if (numPrimeFactors >= 4) {
				numInARow++;
				if (numInARow >= 4) done = true;
			} else {
				numInARow = 0; // start over
			}
			if (numToTest % 1000 == 0) System.out.print(".");
		}
		long firstNumber = numToTest + 1 - numInARow;
		System.out.println("Answer: " + firstNumber);
	}

	private static int distinctPrimeFactors(ArrayList<Long> primeFactors) {
		HashSet<Long> distinctFactors = new HashSet<Long>();
		for (Long factor : primeFactors) {
			distinctFactors.add(factor);
		}
		return distinctFactors.size();
	}
}
