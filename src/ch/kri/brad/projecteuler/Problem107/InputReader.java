package ch.kri.brad.projecteuler.Problem107;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import ch.kri.brad.projecteuler.Problem107.Graph_a.Edge;

/**
 * The input graph is provided as a matrix of edge weights. Nodes are implicit; we take them to be
 * contiguous integers beginning at 0. Note that all edges appear twice in the matrix; hence, we
 * only look at the upper-right triangle.
 * 
 * This class provides various methods for reading the input, depending on the desired result.
 */
public class InputReader {
	private static final String INPUT_FILE = "network.txt";

	public Graph_a readGraph() {
		Graph_a graph = new Graph_a();
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			ArrayList<Edge> edges = new ArrayList<>();
			int lineNumber = 0;
			while (in.hasNext()) {
				String s = in.nextLine();
				String[] values = s.split(",");
				for (int i = lineNumber + 1; i < values.length; i++) {
					if (!values[i].equals("-")) {
						edges.add(new Graph_a.Edge(lineNumber, i, Integer.parseInt(values[i])));
					}
				}
				lineNumber++;
			}
			graph.numNodes = lineNumber;
			graph.edges = edges.toArray(new Edge[edges.size()]);
			graph.removable = new boolean[graph.edges.length];
			for (int i = 0; i < graph.removable.length; i++) graph.removable[i] = true;
			for (Edge e : graph.edges) graph.weight += e.weight;
		}		
		return graph;
	}
	
	public int[][] readMatrix() {
		int[][] matrix = null;
		InputStream fis = this.getClass().getResourceAsStream(INPUT_FILE);
		try (Scanner in = new Scanner(fis)) {
			int row = 0;
			while (in.hasNext() ) {
				String s = in.nextLine();
				String[] values = s.split(",");
				if (matrix == null) matrix = new int[values.length][values.length];
				for (int col = 0; col < values.length; col++) {
						matrix[row][col] = values[col].equals("-") ? 0 :  Integer.parseInt(values[col]);
				}
				row++;				
			}
		}
		return matrix;
	}
}
