package AdventOfCode.aoc_2020.day_12;

import AdventOfCode.aoc_2020.day_12.Navigation.Instruction;

public class Ship2 {
	private int north = 0;
	private int east = 0;
	private int waypointNorth = 1;
	private int waypointEast = 10;
	private int orientation = 90; // 0 is north, 90 is east
	
	public void navigate(Navigation nav) {
		switch (nav.getInstruction()) {
		case R:
			rotate(nav.getAmount());
			break;
		case L:
			rotate(-nav.getAmount());
			break;
		case F:
			north += nav.getAmount() * waypointNorth;
			east += nav.getAmount() * waypointEast;
			break;
		case N:
			waypointNorth += nav.getAmount();
			break;
		case S:
			waypointNorth -= nav.getAmount();
			break;
		case E:
			waypointEast += nav.getAmount();
			break;
		case W:
			waypointEast -= nav.getAmount();
			break;
		}
	}
	
	/**
	 * Unexpectedly, the inputs are always 90 or 180. Hence, we can skip sines and cosines.
	 * Our L command negates the argument, so if it is negative, we at 360. We then have
	 * three possible values: 90, 180 and 270.
	 */
	private void rotate(int amount) {
		if (amount < 0) amount += 360;
		if (amount == 90) {
			int tmp = waypointEast;
			waypointEast = waypointNorth;
			waypointNorth = -tmp;
		} else if (amount == 180) {
			waypointNorth = -waypointNorth;
			waypointEast = - waypointEast;
		} else if (amount == 270) {
			int tmp = waypointEast;
			waypointEast = -waypointNorth;
			waypointNorth = tmp;
		} else {
			System.out.println("Unexpected amount");
		}
	}

	public int getNorth() {
		return north;
	}

	public int getEast() {
		return east;
	}

	public int getOrientation() {
		return orientation;
	}
}
