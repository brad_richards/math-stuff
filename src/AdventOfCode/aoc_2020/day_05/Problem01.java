package AdventOfCode.aoc_2020.day_05;

import java.io.IOException;
import java.util.List;

public class Problem01 {

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		List<String> codes = ir.getLines();

		int highest = 0;
		for (String code : codes) {
			BoardingPass bp = new BoardingPass(code);
			int bpID = bp.getSeatID();
			if (bpID > highest) highest = bpID;
		}
		System.out.println(highest);
	}

}
