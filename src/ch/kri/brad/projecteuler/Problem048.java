package ch.kri.brad.projecteuler;
import java.math.BigInteger;
import java.util.ArrayList;

import ch.kri.brad.utility.Digits;



public class Problem048 {

	public static void main(String[] args) {
		BigInteger sum = BigInteger.ZERO;
		for (int i = 1; i <= 1000; i++) {
			BigInteger addend = BigInteger.valueOf(i).pow(i);
			sum = sum.add(addend);
		}
		ArrayList<Integer> digits = Digits.toDigits(sum);
		for (int i = digits.size()-10; i < digits.size(); i++) System.out.print(digits.get(i));
	}
}
