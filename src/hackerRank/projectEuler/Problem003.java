package hackerRank.projectEuler;

import java.util.Scanner;

/**
 * Small number of test cases, large numbers --> do not generate primes, instead, factor each number.
 */
public class Problem003 {

	public static void main(String[] args) {
		long[] testCases = readData();
		
		for (long value : testCases)
			System.out.println(doOneCase(value));
	}

	public static long[] readData() {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		long[] data = new long[size];
		for (int i = 0; i < size; i++) {
			data[i] = s.nextLong();
		}
		s.close();
		return data;
	}

	
	public static long doOneCase(long valToTest) {
		long maxPrime = 2;
		while (valToTest % 2 == 0) valToTest /= 2;
		
		long limit = (long) Math.sqrt(valToTest);
		
		for (long divisor = 3; divisor <= limit; divisor += 2) {
			while (valToTest % divisor == 0) {
				valToTest /= divisor;
				maxPrime = divisor;
			}
		}
		
		if (valToTest > maxPrime) maxPrime = valToTest;
		
		return maxPrime;
	}

}
