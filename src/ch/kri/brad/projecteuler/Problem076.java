package ch.kri.brad.projecteuler;

public class Problem076 {
	private static final int TARGET = 100;
	private static int solutionCount = 0;

	public static void main(String[] args) {
		for (int numValues = 2; numValues <= TARGET; numValues++) {
			// Initiate the recursion, using values from 1 to TARGET/numValues
			for (int firstValue = 1; firstValue <= TARGET / numValues; firstValue++) {
				countSolutions(numValues-1, firstValue, firstValue);
			}
			System.out.println(numValues + " - " + solutionCount);
		}
	}
	
	/**
	 * Recursive method to generate all possible solutions. We follow the principle that each value must be >= the previous value; this avoids any duplicates, because each solution is then sorted from first to last
	 */
	private static void countSolutions(int numValuesLeft, int sumSoFar, int lastValue) {
		if (numValuesLeft == 1) { // base case
			if (TARGET - sumSoFar >= lastValue) { // Solution valid
				solutionCount++;
			}
		} else { // inductive case
			for (int value = lastValue; value <= (TARGET-sumSoFar) / numValuesLeft; value++) {
				countSolutions(numValuesLeft-1, sumSoFar+value, value);
			}
		}
	}

}
