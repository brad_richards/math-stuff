package AdventOfCode.aoc_2021.day12;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem02 {
	private static ArrayList<Edge> edges = new ArrayList<>();
	private static ArrayList<Path> paths = new ArrayList<>();
	private static final String START = "start";
	private static final String END = "end";
	private static int pathCount = 0;

	private static class Edge {
		String r1;
		String r2;

		public Edge(String r1, String r2) {
			this.r1 = r1;
			this.r2 = r2;
		}
	}

	private static class Path {
		ArrayList<String> path = new ArrayList<>();
		boolean smallCaveTwice = false; // Does the path currently contain one
										// small cave twice

		public Path() {
			path.add(START);
		}

		public Path(Path in) {
			this.path = (ArrayList<String>) in.path.clone();
			this.smallCaveTwice = in.smallCaveTwice;
		}
	}

	public static void main(String[] args) throws IOException {
		readInput();

		findPaths(new Path());
	}

	private static void findPaths(Path initialPath) {
		ArrayList<Path> paths = new ArrayList<>();
		paths.add(initialPath);
		while (!paths.isEmpty()) {
			Path nextPath = paths.remove(0);
			String lastRoom = nextPath.path.get(nextPath.path.size() - 1);
			ArrayList<Edge> nextEdges = getEdges(lastRoom);
			ArrayList<Path> morePaths = extendPath(nextPath, nextEdges);
			for (Path morePath : morePaths)
				paths.add(morePath);
		}
		System.out.println("Total paths = " + pathCount);
	}

	private static ArrayList<Edge> getEdges(String room) {
		ArrayList<Edge> nextEdges = new ArrayList<>();
		for (Edge edge : edges)
			if (edge.r1.equals(room)) nextEdges.add(edge);
		return nextEdges;
	}

	private static ArrayList<Path> extendPath(Path path, ArrayList<Edge> edges) {
		ArrayList<Path> paths = new ArrayList<>();
		for (Edge edge : edges) {
			if (edge.r2.equals(END)) {
				path.path.add(END);
				printPath(path);
				pathCount++;
			} else {
				boolean roomPresent = path.path.contains(edge.r2);
				boolean isLowerCase = Character.isLowerCase(edge.r2.charAt(0)); 
				if (!isLowerCase || !path.smallCaveTwice || !roomPresent) {
					Path newPath = new Path(path);
					newPath.path.add(edge.r2);
					paths.add(newPath);
					newPath.smallCaveTwice = path.smallCaveTwice || (roomPresent && isLowerCase);
				}
			}
		}
		return paths;
	}

	// One small cave can be visited twice, all others only once
	private static boolean isOK(Path path, String room) {
		boolean isOK = !Character.isLowerCase(room.charAt(0)) || !path.smallCaveTwice;
		if (!isOK) isOK = !path.path.contains(room);
		return isOK;
	}

	private static void printPath(Path path) {
		for (String s : path.path)
			System.out.print(s + " - ");
		System.out.println();
	}

	private static void readInput() throws IOException {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			String[] rooms = in.nextLine().split("-");
			 // Add in both directions (except START)
			if (!rooms[1].equals(START)) edges.add(new Edge(rooms[0], rooms[1]));
			if (!rooms[0].equals(START))edges.add(new Edge(rooms[1], rooms[0]));
		}
		in.close();
	}
}
