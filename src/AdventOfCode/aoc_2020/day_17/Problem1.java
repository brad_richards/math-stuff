
package AdventOfCode.aoc_2020.day_17;

import java.io.IOException;

/**
 * We then read the input as a 2-dimensional array, and discover how large it is.
 * 
 * Cubes can spread by one position per cycle. We define the number of cycles in a constant (in fear
 * of what may come in part 2), and shift the representation appropriately.
 * 
 * If the initial grid is 5x5 and the number of cycles is 7, then we need x and y dimensions of 7 +
 * 5 + 7 or 19. The Z dimension needs to be 7 + 1 + 7 = 15. However, we don't want to have to worry
 * about cubes on the edge of our representation (in the last cycle), so we add 2 two all
 * dimensions.
 * 
 * Our 3-dimensional grid is conceived as int[z][y][x]
 */
public class Problem1 {
	private static final int NUM_CYCLES = 6;
	private static boolean[][][] grid;

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		boolean[][] grid2d = ir.getGrid();
		int xySize = 2 * NUM_CYCLES + grid2d.length;
		int zSize = 2 * NUM_CYCLES + 1;
		grid = new boolean[zSize + 2][xySize + 2][xySize + 2];

		// Copy grid2d into place in grid
		copyIntoGrid(grid2d);
		
		// Print the grid
		printGrid(-1);

		// Execute the required number of cycles
		for (int cycle = 0; cycle < NUM_CYCLES; cycle++) {
			executeCycle();

			// Print the grid
			printGrid(cycle);
		}

		// Print the number of active cells
		System.out.println("Number of active cells = " + countActive());
	}

	private static void copyIntoGrid(boolean[][] grid2d) {
		int z = NUM_CYCLES+1;
		for (int y = 0; y < grid2d.length; y++) {
			for (int x = 0; x < grid2d[0].length; x++) {
				grid[z][y + NUM_CYCLES][x + NUM_CYCLES] = grid2d[y][x];
			}
		}
	}

	/**
	 * We create a new grid, calculate the new activations, and then replace the existing grid. Our
	 * grid is deliberately 1 position larger than needed on all sides, so we do not have to worry
	 * about cells on the edges.
	 */
	private static void executeCycle() {
		boolean[][][] newGrid = new boolean[grid.length][grid[0].length][grid[0][0].length];

		for (int z = 1; z < grid.length - 1; z++) {
			for (int y = 1; y < grid[0].length - 1; y++) {
				for (int x = 1; x < grid[0][0].length - 1; x++) {
					int count = countNeighbors(z, y, x);
					newGrid[z][y][x] = (grid[z][y][x] && (count == 2 || count == 3))
							|| (!grid[z][y][x] && count == 3);
				}
			}
		}
		grid = newGrid;
	}

	private static int countNeighbors(int z, int y, int x) {
		int count = grid[z][y][x] ? -1 : 0;
		for (int cz = z - 1; cz <= z + 1; cz++) {
			for (int cy = y - 1; cy <= y + 1; cy++) {
				for (int cx = x - 1; cx <= x + 1; cx++) {
					if (grid[cz][cy][cx]) count++;
				}
			}
		}
		return count;
	}

	private static void printGrid(int cycle) {
		System.out.println("\n===== Cycle " + (cycle + 1) + " =====\n");
		for (int z = 1; z < grid.length - 1; z++) {
			System.out.println("----- Layer " + z + " -----");
			for (int y = 1; y < grid[0].length - 1; y++) {
				for (int x = 1; x < grid[0][0].length - 1; x++) {
					System.out.print(grid[z][y][x] ? "#" : ".");
				}
				System.out.println();
			}
			System.out.println();
		}
	}

	private static int countActive() {
		int count = 0;
		for (int z = 1; z < grid.length - 1; z++) {
			for (int y = 1; y < grid[0].length - 1; y++) {
				for (int x = 1; x < grid[0][0].length - 1; x++) {
					if (grid[z][y][x]) count++;
				}
			}
		}
		return count;
	}
}
