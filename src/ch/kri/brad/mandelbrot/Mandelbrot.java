package ch.kri.brad.mandelbrot;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * The classic fractal: draw and explore the Mandelbrot figure
 * 
 * Copyright 2012-2016 Brad Richards (http://richards.kri.ch/)
 * 
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 */
public class Mandelbrot extends JFrame implements Runnable {
	static Mandelbrot mainProgram;
	private JPanel canvas = new Canvas();
	private JComponent mousePane = new MyGlassPane(this);
	private JTextField txtX = new JTextField();
	private JTextField txtY = new JTextField();
	private JTextField txtSize = new JTextField();
	JCheckBox chkColor = new JCheckBox("Colors");
	private JButton btnUseCoords = new JButton("Go to");

	// Initial canvas size
	int canvasWidth = 0;
	int canvasHeight = 0;

	// Stop calculated when we reach this number of iterations
	private final long max_iterations = Long.MAX_VALUE;

	// Stop calculating if the last round changed very few pixels
	private int fewChanges;

	private volatile long[][] mandelbrot;
	ColorMapper mapper = new ColorMapper();

	// The initial number of iterations to use in calculations. This becomes larger as we zoom.
	// Each time we zoom, we being with the value where we left off the last time.
	private long startingIterations;

	// Display coordinates within the Mandelbrot figure. The initial values display the
	// entire figure. These values will change as the user moves around
	double m_pixel; // Mathematical size of a pixel
	double m_left; // Mathematical coordinate of left edge
	double m_bottom; // Mathematical coordinate of bottom edge

	// Dynamic variables used during calculation
	volatile boolean calculating; // Is the calculation currently running?
	volatile long iterations; // the number of iterations performed so far

	/**
	 * The main method creates the GUI, and immediately launches the first calculation
	 */
	public static void main(String[] args) {
		mainProgram = new Mandelbrot();
		mainProgram.reset();
	}

	/**
	 * Reset coordinates and iterations to default initial values, based on the current window size
	 */
	void reset() {
		if (!calculating) {
			int oldWidth = canvasWidth;
			canvasWidth = canvas.getWidth();
			
			int oldHeight = canvasHeight;
			canvasHeight = canvas.getHeight();
			
			fewChanges = canvasWidth * canvasHeight / 100;
			startingIterations = 128;
			
			double oldMpixel = m_pixel;
			m_pixel = 3.5 / canvasWidth;
			m_left = -2.5;
			m_bottom = 0 - canvasHeight * m_pixel / 2;

			// Only recalculate if the size or the zoom actually changed
			if (oldWidth != canvasWidth || oldHeight != canvasHeight || oldMpixel != m_pixel) {
				mandelbrot = new long[canvasHeight][canvasWidth];
				calculate();
			}
		}
	}

	public Mandelbrot() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(true);
		this.setLayout(new BorderLayout());

		// The top toolbar
		Box toolbar = Box.createHorizontalBox();
		Dimension dimText = new Dimension(100, 15);
		toolbar.add(Box.createHorizontalGlue());
		toolbar.add(chkColor);
		toolbar.add(Box.createHorizontalStrut(30));
		toolbar.add(new JLabel("X position"));
		toolbar.add(txtX);
		txtX.setPreferredSize(dimText);
		toolbar.add(Box.createHorizontalStrut(30));
		toolbar.add(new JLabel("Y position"));
		toolbar.add(txtY);
		txtY.setPreferredSize(dimText);
		toolbar.add(Box.createHorizontalStrut(30));
		toolbar.add(new JLabel("Size"));
		toolbar.add(txtSize);
		txtSize.setPreferredSize(dimText);
		toolbar.add(Box.createHorizontalStrut(30));
		toolbar.add(btnUseCoords);
		toolbar.add(Box.createHorizontalGlue());
		this.add(toolbar, BorderLayout.SOUTH);

		// The drawing canvas
		canvas.setPreferredSize(new Dimension(800, 600)); // arbitrary initial size
		this.add(canvas, BorderLayout.CENTER);

		// The transparent panel for mouse stuff
		this.setGlassPane(mousePane);
		mousePane.setVisible(true);
		AWTEventListener al = (AWTEventListener) mousePane;
		Toolkit.getDefaultToolkit().addAWTEventListener(al,
				AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK);

		this.pack();
		this.setVisible(true);

		// Event handling for the JButton
		btnUseCoords.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					double newSize = Double.parseDouble(txtSize.getText());
					double newX = Double.parseDouble(txtX.getText());
					double newY = Double.parseDouble(txtY.getText());

					m_pixel = newSize / canvasHeight;
					m_left = newX - (canvasWidth / 2) * m_pixel;
					m_bottom = newY - (canvasHeight / 2) * m_pixel;
					calculate();
				} catch (Exception ignore) {
				}
			}
		});
		
		this.addComponentListener(new ComponentAdapter() {
			@Override
		    public void componentResized(ComponentEvent e) {
		        reset();   
		    }
		});
	}

	/**
	 * Begin a fresh calculation, using the coordinates currently set. This calculation is
	 * progressive, meaning that we iterate repeatedly across the entire figure. While this is less
	 * efficient, it lets the user watch the calculations progress.
	 * 
	 * Calculations stop either when we reach max_iterations or when very few values changed during
	 * the last round of calculations.
	 * 
	 * Simultaneously start a thread to regularly repaint the display as the calculation progresses.
	 */
	void calculate() {
		// This is only possible if we are not already calculating
		if (!calculating) {
			calculating = true;

			// Update the coordinates
			txtX.setText(Double.toString(m_left + (canvasWidth / 2) * m_pixel));
			txtY.setText(Double.toString(m_bottom + (canvasHeight / 2) * m_pixel));
			txtSize.setText(Double.toString(canvasHeight * m_pixel));

			// Start a thread for the calculations
			Thread tt = new Thread(this, "Calculations");
			tt.start();

			// Start a refresh thread for the GUI, 30fps
			Thread t = new Thread("GUI Repaint") {
				@Override
				public void run() {
					while (calculating) {
						repaint();
						try {
							Thread.sleep(33);
						} catch (InterruptedException e) {
						}
					}
				}
			};
			t.start();
		}
	}

	@Override
	public void run() {
		int numChanges = fewChanges;
		long nextIterations = startingIterations;
		while (nextIterations <= max_iterations && numChanges >= fewChanges) {
			iterations = nextIterations;
			numChanges = 0;
			for (int x = 0; x < canvasWidth; x++) {
				for (int y = 0; y < canvasHeight; y++) {
					// Get the coordinates within the Mandelbrot figure
					double m_x = m_left + m_pixel * x;
					double m_y = m_bottom + m_pixel * y;

					// Calculate the value at this location
					long newValue = calcMandelbrot(m_x, m_y, iterations);
					long oldValue = mandelbrot[y][x];
					if (oldValue != newValue) {
						mandelbrot[y][x] = newValue;
						numChanges++;
					}
				}
			}
			nextIterations *= 2;
		}
		startingIterations = iterations;
		calculating = false;
	}

	/**
	 * Calculate whether this point is within the Mandelbrot set, limiting to the given number of
	 * iterations
	 * 
	 * @param x
	 * @param y
	 * @param limit
	 * @return number of iterations executed
	 */
	private long calcMandelbrot(double m_x, double m_y, long limit) {
		boolean stillValid = true;
		double x = 0;
		double y = 0;
		long iteration = 1;
		while (iteration <= limit && stillValid) {
			double x_temp = x * x - y * y + m_x;
			y = 2 * x * y + m_y;
			x = x_temp;

			stillValid = ((x * x + y * y) < 4);
			iteration++;
		}
		if (iteration >= limit) iteration = -1;
		return iteration;
	}

	/**
	 * A version of calcMandelbrot where the intermediate points are saved in an array, so that they
	 * can be displayed. This version begins with mouse coordinates, which must first be translated
	 * into Mandelbrot coordinates
	 * 
	 * @param x
	 * @param y
	 * @return array of intermediate points
	 */
	ArrayList<Point> calcMandelbrotArray(Point mousePosition) {
		double m_x = m_left + m_pixel * mousePosition.getX();
		double m_y = m_bottom + m_pixel * mousePosition.getY();
		
		final int limit = 64; // Arbitrary...
		ArrayList<Point> points = new ArrayList<>();
		boolean stillValid = true;
		double x = 0;
		double y = 0;
		long iteration = 1;
		while (iteration <= limit && stillValid) {
			double x_temp = x * x - y * y + m_x;
			y = 2 * x * y + m_y;
			x = x_temp;
			points.add(translateMandelbrotToScreen(x, y));

			stillValid = ((x * x + y * y) < 4);
			iteration++;
		}
		if (iteration >= limit) iteration = -1;
		return points;
	}

	/**
	 * Translate coordinates in the Mandelbrot figure to screen coordinates
	 * 
	 * @param m_x
	 *            x coordinate in the Mandelbrot figure
	 * @param m_y
	 *            y coordinate in the Mandelbrot figure
	 * @return Point containing screen coordinates
	 */
	private Point translateMandelbrotToScreen(double m_x, double m_y) {
		Point p = new Point();
		p.x = (int) ( (m_x - m_left) / m_pixel );
		p.y = (int) ( (m_y - m_bottom) / m_pixel );
		return p;
	}

	/**
	 * A panel to draw on...
	 */
	private class Canvas extends JPanel {
		private Canvas() {
			this.setDoubleBuffered(true);
		}

		@Override
		public void paint(Graphics g) {
			boolean psychedelic = chkColor.isSelected();
			for (int y = 0; y < canvasHeight; y++) {
				for (int x = 0; x < canvasWidth; x++) {
					Color c = mapper.map(psychedelic, mandelbrot[y][x], iterations);
					g.setColor(c);
					g.drawLine(x, y, x, y);
				}
			}
		}
	}
}
