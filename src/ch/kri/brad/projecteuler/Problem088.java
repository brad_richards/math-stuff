package ch.kri.brad.projecteuler;

import java.util.TreeSet;

/**
 * Note that for any k >= 2, one sum-product solution will be of the
 * form 1, 1, ..., 1, 1, 2, k. Hence, the real question is whether or
 * not a product-sum exists that is smaller than 2*k
 * 
 * If such a solution exists, it will have at least three multiplicands,
 * and the largest of these will be less than k/2.
 */
public class Problem088 {

	
	public static void main(String[] args) {
		TreeSet<Integer> answerValues = new TreeSet<>(); // We neet a Set to eliminate duplicates
		
		final int MAX_K = 12000;
		int[] solutions = new int[MAX_K-1]; // Array to hold solutions
		for (int k = 2; k <= MAX_K; k++) {
			solutions[k-2] = 2 * k;         // Every k has the solution 2*k
			findBestSolution(solutions, k);
			answerValues.add(solutions[k-2]);
			// System.out.println("Best solution for " + k + " is " + solutions[k-2]);
		}
		System.out.println("Final answer = " + answerValues.stream().mapToInt(Integer::intValue).sum());
	}
	
	/**
	 * Method to prepare the recursion. We then recurse through all integers
	 * in the list, calculating the sum and product as we go. Note that the
	 * integers are not actually stored anywhere, except in the recursion
	 * itself, and the initial 1's do not exist at all.
	 */
	private static void findBestSolution(int[] solutions, int k) {
		// Maximum positions that can ever be > 1 (without exceeding 2*n)
		// is log2(2*k). All earlier digits are 1's
		int maxPositions = (int) ( Math.log10(2 * k) / Math.log10(2) );
		
		// Calculate the sum and product to this point (all 1's)
		int sum = k - maxPositions;
		int prod = 1;
		
		// Generate the next digit, at position "pos" (1-based)
		nextDigit(solutions, k, sum + 1, sum, prod, 1);
	}
	
	/**
	 * A bit slow in Java, since recursion is not optimized
	 */
	private static void nextDigit(int[] solutions, int k, int pos, int sum, int prod, int lastVal) {
		// Calculate the maximum possible value for this position
		int limit = (int) (Math.pow((2 * k / prod), (1 / (double) (1 + k - pos))) + 0.01);
		
		for (int val = lastVal; val <= limit; val++) {
			int newSum = sum + val;
			int newProd = prod * val;
			if (newProd > newSum || newProd > solutions[k-2]) break; // No sense in going farther
			if (pos == k) { // This is the last digit; check for a solution
				if (newProd == newSum && newProd < solutions[k-2]) {
					solutions[k-2] = newProd; // Better solution found
					break;
				}
			} else { // recurse for next integer
				nextDigit(solutions, k, pos+1, newSum, newProd, val);
			}
		}
	}
}
