package ch.kri.brad.projecteuler;
import java.util.HashSet;


public class Problem045 {
	static HashSet<Long> hexagonalNumbers = new HashSet<Long>();
	static HashSet<Long> PentHexNumbers = new HashSet<Long>();
	
	public static void main(String[] args) {
		final long LIMIT = 1000000;
		
		// Generate the hexagonal numbers
		for (long i = 1; i <= LIMIT; i++) {
			hexagonalNumbers.add(i * (2*i-1));			
		}
		
		// Generate the pentagonal numbers, looking for matches with the triangle numbers
		for (long i = 1; i <= LIMIT; i++) {
			long pentagonalNumber = i * (3*i-1) / 2;
			if (hexagonalNumbers.contains(pentagonalNumber)) {
				PentHexNumbers.add(pentagonalNumber);
			}
		}
		
		// Generate the triangular numbers, looking for matches
		for (long i = 1; i <= LIMIT; i++) {
			long traingularNumber = i * (i-1) / 2;
			if (PentHexNumbers.contains(traingularNumber)) {
				System.out.println("Found a match: " + traingularNumber);
			}
		}
	}
}
