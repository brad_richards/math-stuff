package hackerRank.java.strings;

import java.util.Scanner;

public class JavaStringReverse {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String str = s.next();
		s.close();

		boolean isPalindrome = true;
		for (int pos = 0; pos < str.length()/2 && isPalindrome; pos++) {
			isPalindrome = (str.charAt(pos) == str.charAt(str.length() - pos - 1));
		}
		
		System.out.println(isPalindrome ? "Yes" : "No");
	}

}
