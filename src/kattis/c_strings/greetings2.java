package kattis.c_strings;

import java.util.Scanner;

public class greetings2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String greeting = in.nextLine();
		
		String out = "" + greeting.charAt(0);
		
		int numE = greeting.length()-2;
		for (int i = 0; i < numE; i++) out += "ee";
		
		out += greeting.charAt(greeting.length()-1);
		
		System.out.println(out);
	}
}
