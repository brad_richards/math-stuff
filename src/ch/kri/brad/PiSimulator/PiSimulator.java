package ch.kri.brad.PiSimulator;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Calculating Pi with bouncing balls
 * 
 *  See the video "Pi and Bouncing Balls" by Numberphile
 * http://www.youtube.com/watch?v=abv4Fz7oNr0
 * 
 * This simulation is only accurate for N in the range 0 to 2
 * 
 */
public class PiSimulator extends JFrame implements Runnable {
	// GUI components
	JTextField txtN = new JTextField("0");
	JButton btnGo = new JButton("Start");
	Canvas canvas = new Canvas();
	JLabel lblBigVelocity = new JLabel();
	JLabel lblCollisions = new JLabel();

	// Simulation components
	Ball bigBall;
	Ball littleBall;
	Thread simThread;
	
	// Simulation constants
	final int ticsPerSecond = 1000;
	final int fieldWidth = 1000;

	public static void main(String[] args) {
		new PiSimulator();
	}

	private PiSimulator() {
		super("Pi Simulator");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		Box topBox = Box.createHorizontalBox();
		topBox.add(new JLabel("Enter N"));
		topBox.add(Box.createHorizontalStrut(5));
		topBox.add(txtN);
		topBox.add(Box.createHorizontalStrut(50));
		topBox.add(lblCollisions);
		topBox.add(Box.createHorizontalStrut(15));
		topBox.add(lblBigVelocity);
		topBox.add(Box.createHorizontalStrut(50));
		topBox.add(btnGo);
		this.add(topBox, BorderLayout.NORTH);

		// Set up canvas
		canvas.setPreferredSize(new Dimension(fieldWidth, 100));
		canvas.setBackground(Color.WHITE);
		this.add(canvas, BorderLayout.CENTER);

		// Event handling
		btnGo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				long n = Long.parseLong(txtN.getText());
				bigBall = new Ball(16 * (long) Math.pow(100, n), 10, 50, 40);
				littleBall = new Ball(1, 100, 10, 0);

				simThread = new Thread(PiSimulator.this);
				simThread.start();
			}
		});

		pack();
		setVisible(true);
	}

	@Override
	public void run() {
		long tics = 0;
		long collisions = -1;
		while (bigBall.getVelocity() > 0) {
			bigBall.move(ticsPerSecond);
			littleBall.move(ticsPerSecond);

			// Handle collision
			if (bigBall.getRight() > littleBall.getLeft()
					& bigBall.getVelocity() > littleBall.getVelocity()) {
				double newBigVel = bigBall.collisionVelocity(littleBall);
				double newLittleVel = littleBall.collisionVelocity(bigBall);
				bigBall.setVelocity(newBigVel);
				littleBall.setVelocity(newLittleVel);
				collisions++;
				lblCollisions.setText("Collisions: " + collisions);
			}

			// Handle wall
			if (bigBall.getRight() >= fieldWidth) {
				bigBall.setVelocity(0 - bigBall.getVelocity());
			}
			if (littleBall.getRight() >= fieldWidth) {
				littleBall.setVelocity(0 - littleBall.getVelocity());
			}

			// Update display every 30 tics
			if ((tics % 30) == 0) {
				Graphics g = canvas.getGraphics();
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, fieldWidth, 100);
				bigBall.draw(g);
				littleBall.draw(g);
				lblBigVelocity.setText("Velocity: " + Double.toString(bigBall.getVelocity()));
			}

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}

			tics++;
		}
		// final update of velocity
		lblBigVelocity.setText("Velocity: " + Double.toString(bigBall.getVelocity()));
	}

}
