package ch.kri.brad.projecteuler;
import java.util.ArrayList;

import ch.kri.brad.utility.Factoring;



public class Problem012 {

		/**
		 * This method works, but is slow because of all the method calls and ArrayLists
		 */
//	public static void main(String[] args) {
//		long counter = 0;
//		long triangleNumber = 0;
//		int numFactors = 0;
//		while (numFactors <= 500) {
//			counter++;
//			triangleNumber += counter;
//			ArrayList<Long> factors = Factoring.getDivisors(triangleNumber);
//			int newNumFactors = factors.size();
//			if (newNumFactors > numFactors) {
//				numFactors = newNumFactors;
//				System.out.println("Triangle number " + triangleNumber + " has " + numFactors + " divisors");
//			}
//		}
//		System.out.println("Answer = " + triangleNumber);
//	}

	/**
	 * This is a fast-but-ugly version - we don't actually care what the divisors are,
	 * we just need to count them...
	 */
	public static void main(String[] args) {
		int maxSoFar = 0;
		long counter = 0;
		long triangleNumber = 0;
		boolean done = false;
		while (!done) {
			counter++;
			triangleNumber += counter;

			long divisor = 1;
			int numDivisors = 0;
			long limit = (long) Math.sqrt(triangleNumber);
			while (divisor <= limit && numDivisors <= 500) {
				if (triangleNumber % divisor == 0) {
				  // each one found actually represents two, except for the sqrt
					// We do not correct the off-by-one for the sqrt, since it makes
					// no difference for this problem.
					numDivisors += 2; 
				}
				divisor++;
			}
			
			if (numDivisors > maxSoFar) {
				maxSoFar = numDivisors;
				System.out.println("Triangle number " + triangleNumber + " has " + numDivisors + " divisors");
			}
			
			if (numDivisors > 500) done = true; // Note: breaks off before finishing, for this number
		}
		System.out.println("Answer = " + triangleNumber);
	}
	
}
