package AdventOfCode.aoc_2021.day16;

public class ValuePacket extends Packet {
	protected ValuePacket(int version, int type) {
		super(version, type);
	}
	
	public long evaluate() {
		return value;
	}
	
	public void setValue() {
		long block = Binary.getBits(in, 5, nextBit);
		nextBit += 5;
		long value = block & 0xF;
		while  ((block & 0x10) != 0) {
			block = Binary.getBits(in, 5, nextBit);
			nextBit += 5;
			value = value * 16 + (block & 0xF);
		}
		this.value = value;
	}
	
	@Override
	public String toString() {
		return super.toString() + ": value " + value;
	}
}
