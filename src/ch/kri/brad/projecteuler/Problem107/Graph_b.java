package ch.kri.brad.projecteuler.Problem107;

public class Graph_b implements Comparable<Graph_b> {
	public static class Edge {
		int node1, node2, weight;

		public Edge(int node1, int node2, int weight) {
			this.node1 = node1;
			this.node2 = node2;
			this.weight = weight;
		}
		
		@Override
		public String toString() {
			return node1 + "-" + node2;
		}
	}

	Edge[] edges; // undirected edges in this graph
	int nextEdge;
	int numNodes;
	boolean[] reachable; // Indicates which nodes are reachable
	int weight; // The total weight of all edges in this graph, maintained here for efficiency
	
	/**
	 * Initially, node 0 is reachable, no edges
	 */
	public Graph_b(int numNodes) {
		edges = new Edge[numNodes-1];
		nextEdge = 0;
		weight = 0;
		reachable = new boolean[numNodes];
		reachable[0] = true;
	}
	
	public void addEdge(Edge edge) {
		edges[nextEdge++] = edge;
		reachable[edge.node2] = true;
		weight += edge.weight;
	}

	@Override
	public int compareTo(Graph_b other) {
		return Integer.compare(weight, other.weight);
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (Edge edge : edges) buf.append(edge.toString() + " ");
		return buf.toString();
	}
}
