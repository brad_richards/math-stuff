package AdventOfCode.aoc_2020.day_12;

public class Navigation {
	public enum Instruction {F, N, E, W, S, R, L}
	
	private final Instruction instruction;
	private final int amount;
	
	public Navigation(String line) {
		this.instruction = Instruction.valueOf(line.substring(0,1));
		this.amount = Integer.parseInt(line.substring(1));
	}

	public Instruction getInstruction() {
		return instruction;
	}

	public int getAmount() {
		return amount;
	}
	
	
}
