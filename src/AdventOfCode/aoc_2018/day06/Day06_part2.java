package AdventOfCode.aoc_2018.day06;

import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Part two is completely separate, because the logic is so different. Here, we want to propagate
 * each point individually, noting how far each cell is from the point's origin. After doing this
 * for all points, we will then sum the cells across all matrices to get the final matrix. In this
 * final matrix, we then count the number of locations with a value less than 10000.
 * 
 * Note: We could just calculate directly into the final matrix, but handling each point separately
 * seems logically simpler to me...
 */
public class Day06_part2 {
	private static final ArrayList<Point> points = new ArrayList<>();
	private static Point topLeft = new Point();
	private static Point bottomRight = new Point();

	public static void main(String[] args) {
		readPoints();
		findBoundingBox();

		// We now create an array of bounding boxes, one for each point. The first dimension is the
		// point ID. The next two array indices will be offset by the values
		// of "topLeft"
		int[][][] boxes = new int[points.size()][bottomRight.y + 1 - topLeft.y][bottomRight.x + 1
				- topLeft.x];

		// Propagate within each box, from the point outwards
		propagate(boxes);

		// Create a new array to contain the sums, and sum across all boxes
		int[][] sums = new int[bottomRight.y + 1 - topLeft.y][bottomRight.x + 1 - topLeft.x];
		for (int id = 0; id < boxes.length; id++) {
			for (int row = 0; row < boxes[0].length; row++) {
				for (int col = 0; col < boxes[0][0].length; col++)
					sums[row][col] += boxes[id][row][col];
			}
		}

		// Count the number of squares in the sums that are less than 10000
		int count = 0;
		for (int row = 0; row < boxes[0].length; row++) {
			for (int col = 0; col < boxes[0][0].length; col++)
				if (sums[row][col] < 10000) count++;
		}

		// Print the final answer
		System.out.println("Answer is " + count);
	}

	/**
	 * Parse the points
	 */
	private static void readPoints() {
		InputStream fis = Day06_part1.class.getResourceAsStream("input.txt");
		try (Scanner in = new Scanner(fis)) {
			while (in.hasNext()) {
				String line = in.nextLine();
				String[] nums = line.split(",");
				assert (nums.length == 2);
				Point p = new Point();
				p.x = Integer.parseInt(nums[0].trim());
				p.y = Integer.parseInt(nums[1].trim());
				points.add(p);
			}
		}
		System.out.println(points.size() + " points read");
	}

	private static void findBoundingBox() {
		topLeft.x = Integer.MAX_VALUE;
		topLeft.y = Integer.MAX_VALUE;
		bottomRight.x = 0;
		bottomRight.y = 0;
		for (Point p : points) {
			if (p.x < topLeft.x) topLeft.x = p.x;
			if (p.y < topLeft.y) topLeft.y = p.y;
			if (p.x > bottomRight.x) bottomRight.x = p.x;
			if (p.y > bottomRight.y) bottomRight.y = p.y;
		}
	}

	/**
	 * For each point, use it's particular layer in boxes. We can directly calculate the Manhattan
	 * value for each cell in the box.
	 */
	public static void propagate(int[][][] boxes) {
		for (int id = 0; id < points.size(); id++) {
			Point p = points.get(id);
			int pRow = p.y - topLeft.y;
			int pCol = p.x - topLeft.x;

			for (int row = 0; row < boxes[0].length; row++) {
				for (int col = 0; col < boxes[0][0].length; col++) {
					boxes[id][row][col] = Math.abs(row - pRow) + Math.abs(col - pCol);
				}
			}
		}
	}
}
