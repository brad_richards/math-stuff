package AdventOfCode.aoc_2021.day16;

public abstract class Packet {
	protected static String in;
	protected static int nextBit; // Next bit to process
	protected static int versionSum = 0; // For part 1
	
	public int version;
	public int type;
	public long value;
	
	protected Packet(int version, int type) {
		this.version = version;
		this.type = type;
	}
	
	public abstract long evaluate();
	
	/**
	 * Called to start the hierarchical parsing
	 */
	public static Packet parsePacket(String in) {
		Packet.in = in;
		Packet.nextBit = 0;
		Packet.versionSum = 0;
		return parsePacket();
	}
	
	/**
	 * Called to parse packets recursively
	 */
	public static Packet parsePacket() {
		// Parse header
		int header = (int) Binary.getBits(in, 7, nextBit);
		int version = (header & 0x70) / 16;
		versionSum += version; // for part 1
		int type = (header & 0x0E) / 2;
		int lT = (header & 0x01); // length-type, if applicable 
		
		Packet packet;
		if (type == 4) { // parse value packet
			ValuePacket vp = new ValuePacket(version, type);
			nextBit = nextBit + 6;
			vp.setValue();
			packet = vp;
		} else { // parse operator packet
			OperatorPacket op = new OperatorPacket(version, type);
			op.lengthTypeID = (lT == 1);
			nextBit = nextBit + 7;
			op.parseSubpackets();
			packet = op;
		}
		return packet;
	}
	
	@Override
	public String toString() {
		return "v" + version + "/t" + type;
	}
}
