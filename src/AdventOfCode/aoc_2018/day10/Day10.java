package AdventOfCode.aoc_2018.day10;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Initial hypothesis: When the dots occupy the smallest bounding rectangle, we have foudn the
 * message.
 */
public class Day10 {
	private static ArrayList<Dot> dots;

	public static void main(String[] args) {
		InputReader in = new InputReader();
		dots = in.readDots();
		
		long lastSize = Long.MAX_VALUE;
		long size = Long.MAX_VALUE - 1;
		int count = 0;
		while (size < lastSize) {
			lastSize = size;
			moveDots(dots);
			Point boundingRect = boundingRectSize(dots);
			size = (long) boundingRect.x * (long) boundingRect.y;
			count++;
		}
		
		// Once last-size has started to increase, we have gone too far, so back up one step
		unmoveDots(dots);
		count--;
		
		// Construct an array containing the bounding rectangle
		boolean[][] messageArray = constructDisplay(dots);
		
		// Display array in console
		System.out.println("After " + count + " seconds, the following message appears\n");
		showMessage(messageArray);
	}
	
	private static void moveDots(ArrayList<Dot> dots) {
		for (Dot dot : dots) {
			dot.x += dot.vx;
			dot.y += dot.vy;
		}
	}

	private static void unmoveDots(ArrayList<Dot> dots) {
		for (Dot dot : dots) {
			dot.x -= dot.vx;
			dot.y -= dot.vy;
		}
	}
	
	private static Bounds getBoundingRect(ArrayList<Dot> dots) {
		Bounds bounds = new Bounds();
		
		for (Dot dot : dots) {
			if (dot.x > bounds.maxX) bounds.maxX = dot.x;
			if (dot.x < bounds.minX) bounds.minX = dot.x;
			if (dot.y > bounds.maxY) bounds.maxY = dot.y;
			if (dot.y < bounds.minY) bounds.minY = dot.y;
		}
		return bounds;
	}
	
	private static Point boundingRectSize(ArrayList<Dot> dots) {
		Bounds bounds = getBoundingRect(dots);
		Point p = new Point();
		p.x = bounds.maxX - bounds.minX;
		p.y = bounds.maxY - bounds.minY;
		return p;
	}
	
	private static boolean[][] constructDisplay(ArrayList<Dot> dots) {
		Bounds bounds = getBoundingRect(dots);
		System.out.println("Bounding rectangle: " + bounds.minX + "-" + bounds.maxX + " , " + bounds.minY + "-" + bounds.maxY);
		int height = bounds.maxY + 1 - bounds.minY;
		int width = bounds.maxX + 1 - bounds.minX;
		
		boolean[][] display = new boolean[height][width];
		for (Dot dot : dots) {
			display[dot.y - bounds.minY][dot.x - bounds.minX] = true; 
		}
		return display;
	}

	private static void showMessage(boolean[][] display) {
		for (int row = 0; row < display.length; row++) {
			for (int col = 0; col < display[0].length; col++) {
				System.out.print(display[row][col] ? '#' : ' ');
			}
			System.out.println();
		}
	}
}
