package ch.kri.brad.projecteuler;

import java.awt.Point;

/**
 * A manual analysis shows that there are 3 * n² where the right angle is at the
 * origin, on the X-axis, or on the Y-axis.
 * 
 * For solutions where the right angle is somewhere in the midst of the field,
 * we require an integer solution to the equation Xp * Xq + Yp * Yq = Xq² + Yq²
 * 
 * This solution just brute-forces the integer solutions. Note that there is a
 * symmetric set of solutions reflected along the diagonal axis, so we need to
 * double this number.
 */
public class Problem091 {
	private static final int n = 50;

	public static void main(String[] args) {
		int count = 3 * n * n;
		for (int xp = 0; xp < n; xp++) {
			for (int xq = xp+1; xq <= n; xq++) {
				for (int yq = 0; yq < n; yq++) {
					for (int yp = yq+1; yp <= n; yp++) {
						Point p = new Point(xp, yp);
						Point q = new Point(xq, yq);
						if (satisfiesEquation(p, q)) count += 2;
					}
				}
			}
		}
		
		System.out.println("Total triangles is " + count);
	}

	private static boolean satisfiesEquation(Point p, Point q) {
		int leftSide = p.x * q.x + p.y * q.y;
		int rightSide = q.x * q.x + q.y * q.y;
		
//		if (leftSide == rightSide) {
//			System.out.println("P = " + p.x + "," + p.y + "  and  Q = " + q.x + "," + q.y);
//		}
		
		return leftSide == rightSide;
	}

}
