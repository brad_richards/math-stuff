package AdventOfCode.aoc_2022.day11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem2 {
	static List<String> lines;
	static List<Monkey2> monkeys = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		readInput();

		for (int round = 0; round < 10000; round++) {
			for (Monkey2 monkey : monkeys) monkey.throwItems(monkeys);
			
			if ((round+1) % 1000 == 0) {
				for (Monkey2 monkey : monkeys) System.out.println(monkey);
				System.out.println();
			}
		}
		
		ArrayList<Integer> business = new ArrayList<>();
		for (Monkey2 monkey : monkeys) business.add(monkey.getCount());
		Collections.sort(business);
		System.out.println("Part 1 answer = " + ((long) business.get(business.size()-1) * (long) business.get(business.size()-2)));
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem2.class.getResource("input.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			lines = stream.collect(Collectors.toList());
		}
		
		for (int i = 0; i < lines.size(); i+=7) {
			if (lines.get(i).startsWith("Monkey")) {
				monkeys.add(parseMonkey(i+1));
			}
		}
	}
	
	private static Monkey2 parseMonkey(int pos) {
		String[] tmp = lines.get(pos).split(":");
		tmp = tmp[1].split(",");
		long[] items = new long[tmp.length];
		for (int i = 0; i < tmp.length; i++) items[i] = Long.parseLong(tmp[i].trim());
		
		tmp = lines.get(pos+1).split("=");
		String operation = tmp[1].trim();
		tmp = operation.split(" ");
		char operator = tmp[1].charAt(0);
		String operand = tmp[2];
		
		tmp = lines.get(pos+2).split("by");
		long divBy = Integer.parseInt(tmp[1].trim());
		Monkey2.allDivs *= divBy;
		
		tmp = lines.get(pos+3).split("monkey");
		int trueTarget = Integer.parseInt(tmp[1].trim());
		
		tmp = lines.get(pos+4).split("monkey");
		int falseTarget = Integer.parseInt(tmp[1].trim());
		
		return new Monkey2(monkeys.size(), operator, operand, divBy, trueTarget, falseTarget, items);
	}
}