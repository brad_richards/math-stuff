package AdventOfCode.aoc_2021.day19;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class test {
	private static ArrayList<Scanners> scanners = new ArrayList<>();

	public static void main(String[] args) {
		readInput();

		createOffsets();
		
		for (int i = 0; i < scanners.size(); i++)
			System.out.println(i + " contains " + scanners.get(i).beacons.size());
		
		for (int i = 0; i < scanners.size()-1; i++) {
			for (int j = i+1; j < scanners.size(); j++) {
				int count = countCommonDistances(scanners.get(i), scanners.get(j));
				System.out.println("Common distances " + i + "/" + j + ": " + count);
			}
		}
		
		
		
//		printOffsets();
	}
	
	private static int countCommonDistances(Scanners s1, Scanners s2) {
		int pos1 = 0;
		int pos2 = 0;
		int count = 0;
		while (pos1 < s1.distances.size() && pos2 < s2.distances.size()) {
			Distance d1 = s1.distances.get(pos1);
			Distance d2 = s2.distances.get(pos2);
			if (d1.totalDistance - d2.totalDistance == 0) {
				count++;
				pos1++; pos2++;
			} else if (d1.compareTo(d2) < 0)
				pos1++;
			else
				pos2++;
		}
		return count;
	}

	private static void printOffsets() {
		for (Scanners s : scanners) {
			double lastDist = 0.0;
			for (Distance d : s.distances) {
				System.out.print(d);
				if (d.totalDistance - lastDist == 0) System.out.print(" *****");
				lastDist = d.totalDistance;
				System.out.println();
			}
			System.out.println();
		}
	}
	
	private static void createOffsets() {
		for (Scanners s : scanners) {
			for (int i = 0; i < s.beacons.size() - 1; i++) {
				Beacon b1 = s.beacons.get(i);
				for (int j = i + 1; j < s.beacons.size(); j++) {
					Beacon b2 = s.beacons.get(j);
					Distance d = new Distance(Math.abs(b1.x - b2.x), Math.abs(b1.y - b2.y), Math.abs(b1.z - b2.z));
					s.distances.add(d);
				}
			}
			Collections.sort(s.distances);
		}
	}

	private static void readInput() {
		Scanner in = new Scanner(test.class.getResourceAsStream("sample.txt"));

		Scanners currentScanner = null;
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.isBlank()) {
				// Nothing
			} else if (line.subSequence(0, 3).equals("---")) {
				currentScanner = new Scanners();
				scanners.add(currentScanner);
			} else {
				String[] nums = line.split(",");
				int[] coords = new int[nums.length];
				for (int i = 0; i < nums.length; i++)
					coords[i] = Integer.parseInt(nums[i]);
				Beacon beacon = new Beacon(coords);
				currentScanner.beacons.add(beacon);
			}
		}
	}
}
