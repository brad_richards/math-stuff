/**
 * 
 */
package ch.kri.brad.utility;

import static ch.kri.brad.utility.Primes.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author brad
 *
 */
public class PrimesTest {
	private static final long[] somePrimes = {2, 3, 5, 73, 79, 83, 179, 181, 191, 3433, 3449, 3457};
	private static final long[] someNonPrimes = {1, 4, 9, 15, 189, 190, 3435, 3436, 3451, 3453};

	/**
	 * Test method for {@link ch.kri.brad.utility.Primes#primeFactors(long)}.
	 */
	@Test
	public void testPrimeFactors() {
		final long[] prime_factors_of_420 = {2, 2, 3, 5, 7}; 
		ArrayList<Long> primeFactors = primeFactors(420);
		for (Long pf : prime_factors_of_420) {
			primeFactors.remove(pf);
		}
		assertTrue(primeFactors.size() == 0);
	}

	/**
	 * Test method for {@link ch.kri.brad.utility.Primes#isPrime(long)}.
	 */
	@Test
	public void testIsPrime() {
		boolean isPrime;
		for (long prime : somePrimes) {
			assertTrue(isPrime(prime));
		}
		for (long notPrime : someNonPrimes) {
			assertFalse(isPrime(notPrime));
		}
	}

	/**
	 * Test method for {@link ch.kri.brad.utility.Primes#generatePrimesErastothenes(int, boolean[])}.
	 */
	@Test
	public void testGeneratePrimesErastothenes() {
		final long number_of_primes_below_1000 = 168;
		generatePrimesErastothenes(1000);
		boolean[] rawSieve = getRawSieve();
		int count = 0;
		for (boolean b : rawSieve) {
			if (b) count++;
		}
		assertEquals(count, number_of_primes_below_1000);
	}

	/**
	 * Test method for {@link ch.kri.brad.utility.Primes#generatePrimeFactorsErastothenes(int)}.
	 */
	@Test
	public void testGeneratePrimeFactorsErastothenes() {
		final int NUM_PRIMES = 1000;
		generatePrimesErastothenes(NUM_PRIMES);
		generatePrimeFactorsErastothenes(NUM_PRIMES);
		boolean[] rawSieve = getRawSieve();
		ArrayList<ArrayList<Integer>> primeFactorSieve = getPrimeFactorSieve();
		for (int i = 0 ; i < NUM_PRIMES-1; i++) {
			if ((rawSieve[i] && primeFactorSieve.get(i) != null) ||
			 (!rawSieve[i] && primeFactorSieve.get(i) == null)) {
				System.out.println(i + ": " + rawSieve[i] + "    " + primeFactorSieve.get(i));
				fail();
			}
		}
		
	}
}
