package hackerRank.projectEuler;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem002 {

	public static void main(String[] args) {
		
		long[] testCases = readData();

		for (long value : testCases)
			System.out.println(doOneCase(value));
	}

	public static long[] readData() {
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		long[] data = new long[size];
		for (int i = 0; i < size; i++) {
			data[i] = s.nextLong();
		}
		s.close();
		return data;
	}

	public static long doOneCase(long limit) {
		generateFibonacci(limit);
		long sum = 0;
		for (Long val : fibonacci) {
			if (val > limit) break;
			if (val % 2 == 0) sum += val;
		}
		return sum;
	}
	
	
	
	private static ArrayList<Long> fibonacci = new ArrayList<Long>();
	static {
		fibonacci.add((long) 1);
		fibonacci.add((long) 1);
	}

	/**
	 * Generate numbers in the Fibonacci sequence. This method can be used to sequentially extend the list of already generated numbers.
	 * 
	 * Note: The test-cases push the limits of longs - we have to ensure we do not try to generate past MAX_VALUE
	 * 
	 * @param maxValue Generate all numbers up to and including this value.
	 */
	public static void generateFibonacci(long maxValue) {
		int numNums = fibonacci.size();
		long beforeLastNum = fibonacci.get(numNums-2);
		long lastNum = fibonacci.get(numNums-1);
		long sum = beforeLastNum + lastNum;
		while (sum <= maxValue) {
			if (sum < lastNum) break; // overflow
			fibonacci.add(sum);
			beforeLastNum = lastNum;
			lastNum = sum;
			sum = beforeLastNum + lastNum;
		}
	}
	

}
