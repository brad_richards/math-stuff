package AdventOfCode.aoc_2022.day07;

import java.util.ArrayList;

public class Directory extends FileSystemEntry {
	private ArrayList<FileSystemEntry> contents = new ArrayList<>();
	private long totalSize = 0;

	public Directory(String name, Directory parent) {
		super(name, parent, 0, ((parent == null) ? 0 : parent.getDepth()+1));
	}

	public void addEntry(FileSystemEntry fe) {
		// Only add if it does not already exist
		if (!contents.contains(fe)) contents.add(fe);
	}

	/**
	 * Find and return a subdirectory. If the subdirectory does not exist,
	 * create it, because we are exploring a file system and building a picture
	 * of it.
	 */
	public Directory getDirectory(String name) {
		for (FileSystemEntry fe : contents) {
			if (fe.getName().equals(name)) return (Directory) fe;
		}
		// If we are here, the directory did not exist - so create it
		Directory dir = new Directory(name, this);
		contents.add(dir);
		return dir;
	}

	public long getTotalSize() {
		return totalSize;
	}

	/**
	 * Called only after all file-system contents have been enumerated
	 */
	public void calculateTotalSize() {
		totalSize = 0;
		for (FileSystemEntry fe : contents) {
			if (fe instanceof Directory) {
				Directory de = (Directory) fe;
				de.calculateTotalSize();
				totalSize += de.getTotalSize();
			} else {
				totalSize += fe.getSize();
			}
		}
	}
	
	/**
	 * Place all directory sizes into an ArrayList
	 */
	public void getDirSizes(ArrayList<Long> sizes) {
		sizes.add(this.totalSize);
		for (FileSystemEntry fe : contents) {
			if (fe instanceof Directory) {
				Directory de = (Directory) fe;
				de.getDirSizes(sizes);
			}
		}
	}
	
	/**
	 * Recursive method for part 1
	 */
	public long part1() {
		long answer = (this.totalSize <= 100000) ? this.totalSize : 0;
		for (FileSystemEntry fe : contents) {
			if (fe instanceof Directory) {
				Directory de = (Directory) fe;
				answer += de.part1();
			}
		}
		return answer;
	}
	
	@Override
	public String toString() {
		String out = super.toString();
		for (FileSystemEntry fe : contents) {
			out += "\n" + fe.toString();
		}
		return out;
	}
}
