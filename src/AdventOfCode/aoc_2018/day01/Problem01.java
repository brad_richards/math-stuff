package AdventOfCode.aoc_2018.day01;

import java.io.InputStream;
import java.util.Scanner;

/**
 * This first part just verifies that we are reading the input values correctly.
 */
public class Problem01 {
	public static void main(String[] args) {
		InputStream fis = Problem01.class.getResourceAsStream("input.txt");
		int value = 0;
		try (Scanner in = new Scanner(fis) ) {
			while (in.hasNext()) {
				value += in.nextInt();
			}
			System.out.println(value);
		}
	}
}
