package AdventOfCode.aoc_2021.day18;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Rather than anything fancy, we just turn a number into a sequence of integers, that we will process left-to-right. Left-parens are -1, right-parens are -2
 */
public class Problem01 {
	private static ArrayList<ArrayList<Integer>> numbers = new ArrayList<>();
	
	public static void main(String[] args) {
		readInput();
		
		// Part 2
		System.out.println("Max pair magnitude " + getMaxMagnitude());		
		
		while (numbers.size() > 1) {
			ArrayList<Integer> sum = add(numbers.remove(0), numbers.remove(0));
			
			boolean actionFound = true;
			while (actionFound) actionFound = reduce(sum);
			
			numbers.add(0, sum);
		}

		// Part 1
		print(numbers.get(0));
		System.out.println("Magnitude " + magnitude(numbers.get(0)));
	}
	
	private static ArrayList<Integer> add(ArrayList<Integer> left, ArrayList<Integer> right) {
		ArrayList<Integer> sum = new ArrayList<>();
		sum.add(-1);
		sum.addAll(left);
		sum.addAll(right);
		sum.add(-2);
		return sum;
	}
	
	private static int getMaxMagnitude() {
		int max = 0;
		for (int i = 0; i < numbers.size()-1; i++) {
			for (int j = i+1; j < numbers.size(); j++) {
				ArrayList<Integer> sum = add(numbers.get(i), numbers.get(j));
				boolean actionFound = true;
				while (actionFound) actionFound = reduce(sum);
				max = Math.max(max, magnitude(sum));
				
				sum = add(numbers.get(j), numbers.get(i));
				actionFound = true;
				while (actionFound) actionFound = reduce(sum);
				max = Math.max(max, magnitude(sum));
			}
		}
		return max;
	}
	
	private static void print(ArrayList<Integer> parts) {
		for (int i = 0; i < parts.size(); i++) {
			Integer part = parts.get(i);
			if (part == -1) {
				System.out.print("[");
			} else if (part == -2) {
				System.out.print("]");
				if (parts.size() > (i+1) && parts.get(i+1) != -2) System.out.print(",");
			} else {
				System.out.print(part);
				if (parts.size() > (i+1) && parts.get(i+1) != -2) System.out.print(",");
			}
		}
		System.out.println();
	}
	
	private static int magPos = 0; // Kludge, because we really want to pass this by reference
	private static int magnitude(ArrayList<Integer> parts) {
		magPos = 0;
		return magnitude2(parts);
	}
	private static int magnitude2(ArrayList<Integer> parts) {
		magPos++;
		int left = (parts.get(magPos) == -1) ? magnitude2(parts) : parts.get(magPos++);
		int right = (parts.get(magPos) == -1) ? magnitude2(parts) : parts.get(magPos++);
		magPos++;
		return 3 * left + 2 * right;
	}

	
	
	private static boolean reduce(ArrayList<Integer> parts) {
		boolean actionFound = false;
		int depth = 0;
		for (int i = 0; i < parts.size() && !actionFound; i++) {
			Integer part = parts.get(i);
			if (part == -1) {
				depth++;
			} else if (part == -2) {
				depth--;
			} else {
				if (depth > 4) {
					actionFound = true;
					int left = part;
					int right = parts.get(i+1);
					i = i - 1;
					for (int j = 1; j <= 3; j++) parts.remove(i+1); // remove 4 parts
					
					parts.set(i, 0); // Replace exploded pair with 0
					
					boolean placed = false;
					for (int j = i-1; j >= 0 && !placed; j--) {
						if (parts.get(j) >= 0) {
							parts.set(j, parts.get(j)+ left);
							placed = true;
						}
					}
					placed = false;
					for (int j = i+1; j < parts.size() && !placed; j++) {
						if (parts.get(j) >= 0) {
							parts.set(j, parts.get(j)+ right);
							placed = true;
						}
					}
				}
			}
		}
		
		if (!actionFound) {
			for (int i = 0; i < parts.size() && !actionFound; i++) {
				int value = parts.get(i);
				if (value > 9) {
					actionFound = true;
					parts.set(i, -1);
					parts.add(i+1, value/2);
					parts.add(i+2, (value+1)/2);
					parts.add(i+3, -2);
				}
			}			
		}
		return actionFound;
	}
	
	private static void readInput() {
		Scanner in = new Scanner(Problem01.class.getResourceAsStream("input.txt"));
		while (in.hasNextLine()) {
			ArrayList<Integer> parts = parse(in.nextLine());
			numbers.add(parts);
		}
		in.close();
	}
	
	private static ArrayList<Integer> parse(String in) {
		ArrayList<Integer> parts = new ArrayList<>();
		for (char c : in.toCharArray()) {
			if (c == '[') parts.add(-1);
			else if (c == ']') parts.add(-2);
			else if (c >= '0' && c <= '9') {
				parts.add(c - '0');
			}
		}
		return parts;
	}
}
