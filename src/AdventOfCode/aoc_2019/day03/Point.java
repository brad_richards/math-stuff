package AdventOfCode.aoc_2019.day03;

public class Point implements Comparable<Point> {
	private final int x;
	private final int y;
	private final int manhattanDistance;
	private int steps;
	
	public Point(int x, int y, int steps) {
		this.x = x;
		this.y = y;
		this.steps = steps;
		this.manhattanDistance = Math.abs(x) + Math.abs(y);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != Point.class) return false;
		Point p = (Point) o;
		return this.x == p.x && this.y == p.y;
	}
	
	@Override
	public int compareTo(Point p) {
		int result = Integer.compare(this.manhattanDistance, p.manhattanDistance);
		if (result == 0) result = Integer.compare(this.x, p.x);
		if (result == 0) result = Integer.compare(this.y, p.y);
		return result;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ") with distance " + manhattanDistance + " and steps " + steps;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getSteps() {
		return steps;
	}
	
	public void addSteps(int moreSteps) {
		steps += moreSteps;
	}

	public int getManhattanDistance() {
		return manhattanDistance;
	}
}
