package kattis.e_loops;

import java.util.Scanner;

public class tarifa {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int X = in.nextInt();
		int N = in.nextInt();
		
		int used = 0;
		for (int i = 0; i < N; i++)
			used += in.nextInt();
		
		int paid = X * (N+1);
		int available = paid - used;
		System.out.println(available);
	}

}
