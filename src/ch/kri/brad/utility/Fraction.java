package ch.kri.brad.utility;

public class Fraction extends Number implements Comparable<Fraction> {
	private long numerator;
	private long denominator;
	
	public Fraction(long numerator, long denominator) {
		long gcd = GenericMath.gcd(numerator, denominator);
		this.numerator = numerator / gcd;
		this.denominator = denominator / gcd;
	}

	public long getNumerator() {
		return numerator;
	}

	public long getDenominator() {
		return denominator;
	}

	@Override
	public int intValue() {
		return (int) longValue();
	}

	@Override
	public long longValue() {
		return numerator / denominator;
	}

	@Override
	public float floatValue() {
		return (float) doubleValue();
	}

	@Override
	public double doubleValue() {
		return ((double) numerator) / ((double) denominator);
	}

	@Override
	public int compareTo(Fraction o) {
		Double thisDouble = this.doubleValue();
		Double otherDouble = o.doubleValue();
		return thisDouble.compareTo(otherDouble);
	}
	
	@Override
	public String toString() {
		return Long.toString(numerator) + "/" + Long.toString(denominator);
	}
}
