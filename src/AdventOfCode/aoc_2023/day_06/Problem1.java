package AdventOfCode.aoc_2023.day_06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem1 {
	static List<Long> times = new ArrayList<Long>();
	static List<Long> distances = new ArrayList<Long>();

	public static void main(String[] args) throws Exception {
		readInput();

		long answer = 1;
		for (int i = 0; i < times.size(); i++) {
			long x = waysToWin(times.get(i), distances.get(i));
			answer *= x;
		}
		System.out.println(answer);
	}

	// Assume we are going to see some stupidly big numbers.
	// Let's try to optimize this...
	// Using the quadratic equation:
	// t = (time +/= sqrt(time^2 - 4 distance) / 2
	// yields two solutions. For all time values between
	// these two solutions, we win.
	private static long waysToWin(long time, long distance) {
		double sValue = Math.sqrt((double)(time * time - 4 * distance));
		sValue -= 0.000000001; // Eliminate ties - we must win!
		long lowerValue = (long) Math.ceil((time - sValue) / 2);
		long higherValue = (long) Math.floor((time + sValue) / 2);
		return higherValue + 1 - lowerValue;
	}

	private static void readInput() throws IOException {
		Path path = Path.of(Problem1.class.getResource("input2.txt").getPath());
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lines = stream.collect(Collectors.toList());
			
			String[] parts0 = lines.get(0).split(" +");
			String[] parts1 = lines.get(1).split(" +");
			if (parts0.length != parts1.length) System.out.println("Input problem!");
			for (int i = 1; i < parts0.length; i++) {
				times.add(Long.parseLong(parts0[i]));
				distances.add(Long.parseLong(parts1[i]));
			}
		}
	}
}