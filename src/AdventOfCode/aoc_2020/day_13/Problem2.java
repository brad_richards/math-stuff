package AdventOfCode.aoc_2020.day_13;

import java.io.IOException;

public class Problem2 {
	private static long[] values;
	private static long[] offsets;
	

	public static void main(String[] args) throws IOException {
		InputReader ir = new InputReader("input.txt");
		String[] busStrings = ir.getBusStrings();

		optimized(busStrings);
	}
	
	/**
	 * Naive approach - too slow for the full problem
	 */
	private static void naive(String[] busStrings) {
		boolean found = false;
		for (long time = 0; !found && time <= Long.MAX_VALUE; time++) {
			found = true;
			for (int idx = 0; found && idx < busStrings.length; idx++) {
				String bus = busStrings[idx];
				if (!bus.equals("x")) {
					long busID = Long.parseLong(bus);
					found = (time + idx) % busID == 0;
				}
			}
			if (found) System.out.println(time);
		}		
	}
	
	/**
	 * Optimized approach:
	 * - Transform the input array in advance into numeric values and their offsets (original indexes)
	 * - The first entry is a real bus, so we use this as the initial counting increment
	 * - Each time we match up with the next entry, the increment is multiplied by that value
	 */
	private static void optimized(String[] busStrings) {
		// Count number of non-x values, so we can dimension the arrays
		int count = 0;
		for (int idx = 0; idx < busStrings.length; idx++) {
			if (!busStrings[idx].equals("x")) count++;
		}
		values = new long[count];
		offsets = new long[count];
		
		// Now fill the arrays
		count = 0;
		for (int idx = 0; idx < busStrings.length; idx++) {
			String bus = busStrings[idx];
			if (!bus.equals("x")) {
				values[count] = Long.parseLong(bus);
				offsets[count++] = idx;
			}
		}
		
		// We increment by the product of all numbers whose conditions we have satisfied.
		boolean done = false;
		long increment = values[0];
		int nextIndexToTest = 1;
		for (long time = 0; !done && time <= Long.MAX_VALUE; time += increment) {
			if ((time + offsets[nextIndexToTest]) % values[nextIndexToTest] == 0) { // found value for next index
				increment *= values[nextIndexToTest++];
				done = (nextIndexToTest == count); // Check to see if we are done
			}
			if (done) System.out.println(time);
		}
	}	
}
